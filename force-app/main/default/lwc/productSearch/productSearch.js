import { LightningElement, wire, track } from 'lwc';
            import findProducts from '@salesforce/apex/ProductSearch.findProducts';
            const columns = [
               // { label: 'Product Name', fieldName: 'Name',type:'url' },
               { label: 'Name', fieldName: 'ProductName', type: 'url',
               typeAttributes: { label: { fieldName: 'Name' }, target: '_blank'} },
              //  { label: 'Product Code', fieldName: 'ProductCode'},
                { label: 'Work Type', fieldName: 'workType'}
            ];
            export default class ProductSearch extends LightningElement {
                searchKey = '';
                totalProducts =[];
                error;
                columns = columns;
                visibleProducts;
             @wire(findProducts, { searchKey: '' })
                handeleProducdMethod({data,error}){
                    if(data){
                        console.log('dataTEST',data);
                        this.totalProducts =JSON.parse(JSON.stringify(this.handleSetProducts(data)));                       
                        this.error = undefined;
                        console.log('dataTEST11',this.totalProducts);
                    }
                    else{
                        this.error = error;
                        this.totalProducts = undefined;   
                    }
                }
                handleKeyChange(event) {
                    this.searchKey = event.target.value;
                }

                handleSearch() {
                    findProducts({ searchKey: this.searchKey })
                        .then((result) => {
                            this.totalProducts =JSON.parse(JSON.stringify(this.handleSetProducts(result)));
                            this.error = undefined;
                            console.log('dataTEST11',this.totalProducts);
                        


                        })
                        .catch((error) => {
                            this.error = error;
                            this.totalProducts = undefined;
                        });
                }
                handleSetProducts(data){
                    let tempProducts = [];
                    data.forEach( ( record ) => {
                        let tempProduct = Object.assign( {}, record );  
                        tempProduct.ProductName = '/' + tempProduct.Id;
                        tempProduct.workType=tempProduct.Product2.Work_Type__c!=null ? tempProduct.Product2.Work_Type__r.Name :'';
                        tempProducts.push(tempProduct);                        
                    });
                    return tempProducts;
                }
                get productCountValue(){
                    if(this.totalProducts!=null){
                        return this.totalProducts.length > 0 ? this.totalProducts.length : 0 ;
                    }
                return 0;
                }
            get isProductsAvaliable(){
                if(this.totalProducts!=null){
                    return this.totalProducts.length > 0 ? true : false ;
                }
            return false;
            }
            updateProductHandler(event){
                console.log('updateProductHandler')
                console.log(event.detail.records)
                this.visibleProducts=[...event.detail.records]
                console.log(event.detail.records)
            }

            }