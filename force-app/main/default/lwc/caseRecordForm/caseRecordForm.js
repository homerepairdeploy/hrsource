import { LightningElement,api,track } from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class CaseRecordForm extends LightningElement {
    @api recordId;
     fieldsArray=['Maintenance_Required__c','Maintenance_Status__c','Maintenance_Details__c','Cash_Settlement__c','Cash_Settlement_Reason__c','Cash_Settlement_Comments__c','Insurable__c','Claim_Proceeding__c','Non_Fit_Reason__c','Cancelled_Reason__c','Cause_and_Decline_Details__c'];

    handleSuccess(event){
        const toastEvent = new ShowToastEvent({
            title : 'Claim Update',
            message : 'Claim details and Task has been updated',
            variant : 'success',
        });
        this.dispatchEvent(toastEvent);
    }
   

}