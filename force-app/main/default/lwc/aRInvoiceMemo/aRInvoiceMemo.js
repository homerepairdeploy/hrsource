/* eslint-disable no-undef */
/* eslint-disable no-console */
import { LightningElement,api,wire,track } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import createCreditMemoAR from '@salesforce/apex/APInvoiceCreditMemo_cc.createCreditMemoAR';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import NAME_FIELD from '@salesforce/schema/AR_Invoice__c.Name';
import CLAIM_NUMBER from '@salesforce/schema/AR_Invoice__c.Claim_NumberAuto__c';
import STATUS from '@salesforce/schema/AR_Invoice__c.Status__c';
import AUTHORITYTYPE from '@salesforce/schema/AR_Invoice__c.Authority_Type__c';
import INVOICETYPE from '@salesforce/schema/AR_Invoice__c.Invoice_Type__c';
import SOURCECHANNEL from '@salesforce/schema/AR_Invoice__c.SourceChannel__c';
import ISEVENT from '@salesforce/schema/AR_Invoice__c.IsEvent__c'
import CustLabourExGST from '@salesforce/schema/AR_Invoice__c.Labour_ex_GST__c';
import CustMaterialExGST from '@salesforce/schema/AR_Invoice__c.Material_ex_GST__c';
const fields=[NAME_FIELD,CLAIM_NUMBER,STATUS,AUTHORITYTYPE,INVOICETYPE,SOURCECHANNEL,ISEVENT,CustLabourExGST,CustMaterialExGST]; 


export default class ARInvoiceMemo extends NavigationMixin(LightningElement) {

    _title = 'AR Invoice Created';
    message = 'AR Invoice Created';
    variant = 'success';
    variantOptions = [
        { label: 'error', value: 'error' },
        { label: 'warning', value: 'warning' },
        { label: 'success', value: 'success' },
        { label: 'info', value: 'info' },
    ];

                 
    // Flexipage provides recordId and objectApiName
    @api recordId;
    @api objectApiName='AR_Invoice__c';
    @wire(getRecord,{recordId:'$recordId',fields}) AR_Invoice;

    @track message;
    @track error;
    @track creditmemodetails=''; 
    @track labourAmtExGST=0;
    @track materialAmtExGST=0;
    @track totalAmount;
    @track gst;
    @track margin;
    @track showLoadingSpinner = false;  
   // @track status=false;
     



    @track areDetailsVisible = false;

    

    handleChange(event) {
        this.areDetailsVisible=getFieldValue(this.AR_Invoice.data,INVOICETYPE) ==='Customer'?event.target.checked:false;

    }
    
    get custLabourExGST(){
        return getFieldValue(this.AR_Invoice.data,CustLabourExGST);
    } 

    get custMaterialExGST(){
        return getFieldValue(this.AR_Invoice.data,CustMaterialExGST);
    }
    get nameField(){
        return getFieldValue(this.AR_Invoice.data,NAME_FIELD);
    }

    get claimNumber(){
        return getFieldValue(this.AR_Invoice.data,CLAIM_NUMBER);
    }
      
    get status(){
        
        return getFieldValue(this.AR_Invoice.data,STATUS) ==='Approved'? false:true;
        
    }
    
    handleCreditMemoChange(event){
        this.creditmemodetails=event.target.value;
         
    }

    handleLabourAmountChange(event){
        var totalExGSTIncMargin;
        var totalAmountNbr;
        var totalMargin;
        var total;
        this.labourAmtExGST=event.target.value;
        total=Number(this.labourAmtExGST)+Number(this.materialAmtExGST);
        const auth=getFieldValue(this.AR_Invoice.data,AUTHORITYTYPE);
        const sourceChannel=getFieldValue(this.AR_Invoice.data,SOURCECHANNEL);
        const isEvent=getFieldValue(this.AR_Invoice.data,ISEVENT);

        totalMargin=(auth==='Contents'?(sourceChannel==='SCRO'?total*0.166:total*0.145) :
                    auth==='doAndCharge'?(sourceChannel==='SCRO'?total*0.166:total*0.145):
                    auth==='assessment'?(sourceChannel==='SCRO'?total*0.166:total*0.145):
                    auth==='report'?(sourceChannel==='SCRO'?total*0.166:total*0.145):
                    auth==='Quote'?(sourceChannel==='SCRO'?total*0.166:total*0.145):
                    auth==='homeAssist'?total*0.25:
                    auth==='makeSafe'?total*0.25:0);
        totalExGSTIncMargin=Number(total)+Number(totalMargin);
        totalAmountNbr=totalExGSTIncMargin+(totalExGSTIncMargin*0.10);
        this.totalAmount='$'+parseFloat(totalAmountNbr).toFixed(2);
        this.gst='$'+parseFloat(totalExGSTIncMargin*0.10).toFixed(2);
        this.margin='$'+parseFloat(totalMargin).toFixed(2);
        
        
    }

    handleMaterialAmountChange(event){
        var totalExGSTIncMargin;
        var totalAmountNbr;
        var totalMargin;
        var total;
        this.materialAmtExGST=event.target.value;
        total=Number(this.labourAmtExGST)+Number(this.materialAmtExGST);
        const auth=getFieldValue(this.AR_Invoice.data,AUTHORITYTYPE);
        const sourceChannel=getFieldValue(this.AR_Invoice.data,SOURCECHANNEL);
        const isEvent=getFieldValue(this.AR_Invoice.data,ISEVENT);
        totalMargin=(auth==='Contents'?(sourceChannel==='SCRO'?total*0.166:total*0.145) :
                    auth==='doAndCharge'?(sourceChannel==='SCRO'?total*0.166:total*0.145):
                    auth==='assessment'?(sourceChannel==='SCRO'?total*0.166:total*0.145):
                    auth==='report'?(sourceChannel==='SCRO'?total*0.166:total*0.145):
                    auth==='Quote'?(sourceChannel==='SCRO'?total*0.166:total*0.145):
                    auth==='homeAssist'?total*0.25:
                    auth==='makeSafe'?total*0.25:0);
                            
        totalExGSTIncMargin=Number(total)+Number(totalMargin);
        totalAmountNbr=totalExGSTIncMargin+(totalExGSTIncMargin*0.10);
        this.totalAmount='$'+parseFloat(totalAmountNbr).toFixed(2);
        this.gst='$'+parseFloat(totalExGSTIncMargin*0.10).toFixed(2);
        this.margin='$'+parseFloat(totalMargin).toFixed(2);
    }
    
    createCreditMemo(){
        var inptextareacmp = this.template.querySelector(".input8");
        const inptextarea=inptextareacmp.value;
        inptextareacmp.reportValidity();
        // eslint-disable-next-line vars-on-top
        var apexcall=false;
        
        apexcall =getFieldValue(this.AR_Invoice.data,INVOICETYPE)==='CM Fees' && inptextarea ?true:
                  getFieldValue(this.AR_Invoice.data,INVOICETYPE)==='Assessment Fee' && inptextarea ?true:
                  getFieldValue(this.AR_Invoice.data,INVOICETYPE)==='Customer' && this.areDetailsVisible===false && inptextarea?true:
                  getFieldValue(this.AR_Invoice.data,INVOICETYPE)==='Customer' && this.areDetailsVisible===true && inptextarea &&
                  this.labourAmtExGST <=this.custLabourExGST && this.materialAmtExGST <=this.custMaterialExGST?true:false;
                 
        let parameterObject = {
            recordId: this.recordId,
            creditMemoDetails:this.creditmemodetails,
            labourAmtExGST:this.labourAmtExGST,
            materialAmtExGST:this.materialAmtExGST,
            partialAmount:this.areDetailsVisible
        };
        
         
        if  (apexcall){
            this.showLoadingSpinner = true;
           
            createCreditMemoAR({ arinvoicewrapper:parameterObject })
            
                
                .then(result => {
                    console.log('result '+ result);
                    this.showLoadingSpinner = false;
                    const evt = new ShowToastEvent({
                        title: this._title,
                        message: this.message,
                        variant: this.variant,
                        mode: 'dismissable'
                    });
                   
                    this.dispatchEvent(evt);
                    this.dispatchEvent(new CustomEvent('closedialog'));
                    //this.status=true;
                    this[NavigationMixin.Navigate]({
                        type: 'standard__recordPage',
                        attributes: {
                            recordId: result,
                            objectApiName: 'AR_Invoice__c',
                            actionName: 'view'
                        },
                    });
               
                })
                .catch(error => {
                    this.message = undefined;
                    this.error = error;
            });
           
        }  
        
    }

     
}