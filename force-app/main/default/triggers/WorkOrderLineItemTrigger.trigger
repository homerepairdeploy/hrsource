trigger WorkOrderLineItemTrigger on WorkOrderLineItem (after insert, after update,before delete) {
   
    Home_Repairs_Trigger_Switch__c triggerSwitch = Home_Repairs_Trigger_Switch__c.getInstance(UserInfo.getUserId());
    
    if(triggerSwitch.Trigger_On_WorkOrderLineItem__c){
        if(RecursiveTriggerHandler.isFirstTime){
            RecursiveTriggerHandler.isFirstTime = false;
            TriggerFactory.createAndExecuteHandler(WorkOrderLineItem.sObjectType);
        }
    }
}