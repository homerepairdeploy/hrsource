trigger TriggeronFileHelper on File_Helper__e (after insert) {
    List<File_Helper__e> fileHelpers = trigger.new;
    for(File_Helper__e record: fileHelpers){
        String request = record.Raw_Request__c;
        if(record.Action__c == 'create') {
            List<ContentDocumentLink> contentLinks = (List<ContentDocumentLink>) JSON.deserialize(request, 
                List<ContentDocumentLink>.class);
            insert contentLinks;
        } else if(record.Action__c == 'delete') {
            List<ContentDocument> documentFiles = (List<ContentDocument>) JSON.deserialize(request, List<ContentDocument>.class);
            delete documentFiles;
        }
    }
    
}