trigger ServiceAppointmentTrigger on ServiceAppointment(before insert, after insert, before update, after update) {
    Home_Repairs_Trigger_Switch__c triggerSwitch = Home_Repairs_Trigger_Switch__c.getInstance(UserInfo.getUserId());
     
    if(triggerSwitch.Trigger_On_ServiceAppointment__c){
      TriggerFactory.createAndExecuteHandler(ServiceAppointment.sObjectType);
    }
}