trigger APInvoiceTrigger on AP_Invoice__c (before insert, before update, after insert, after update) {
     Home_Repairs_Trigger_Switch__c triggerSwitch = Home_Repairs_Trigger_Switch__c.getInstance(UserInfo.getUserId());
     if(triggerSwitch.Trigger_On_AP_Invoice__c){
         TriggerFactory.createAndExecuteHandler(AP_Invoice__c.sObjectType);
     }
}