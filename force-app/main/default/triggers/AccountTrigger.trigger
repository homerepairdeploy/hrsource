trigger AccountTrigger on Account (After insert, After update,Before insert,Before update) {
    Home_Repairs_Trigger_Switch__c triggerSwitch = Home_Repairs_Trigger_Switch__c.getInstance(UserInfo.getUserId());
     
    if(triggerSwitch.Trigger_On_Account__c){
      TriggerFactory.createAndExecuteHandler(Account.sObjectType);
    }
}