trigger  WorkOrderTrigger on WorkOrder(before insert, after insert, before update, after update) {
    Home_Repairs_Trigger_Switch__c triggerSwitch = Home_Repairs_Trigger_Switch__c.getInstance(UserInfo.getUserId());
    if(triggerSwitch.Trigger_On_WorkOrder__c){
      TriggerFactory.createAndExecuteHandler(WorkOrder.sObjectType);
    }
}