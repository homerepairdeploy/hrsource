trigger ARInvoiceTrigger on AR_Invoice__c (before insert,before update,after update) {
    Home_Repairs_Trigger_Switch__c triggerSwitch = Home_Repairs_Trigger_Switch__c.getInstance(UserInfo.getUserId());
     
    if(triggerSwitch.Trigger_On_AR_Invoice__c){
      TriggerFactory.createAndExecuteHandler(AR_Invoice__c.sObjectType);
    }
}