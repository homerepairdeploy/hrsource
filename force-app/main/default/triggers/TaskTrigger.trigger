trigger TaskTrigger on Task (Before insert, before update, after insert, after update) {    
    Home_Repairs_Trigger_Switch__c triggerSwitch = Home_Repairs_Trigger_Switch__c.getInstance(UserInfo.getUserId());
     
    if(triggerSwitch.Trigger_On_Task__c){
      TriggerFactory.createAndExecuteHandler(Task.sObjectType);
    }
}