trigger VariationTrigger on Variation__c(before insert, after insert, before update, after update) {
    Home_Repairs_Trigger_Switch__c triggerSwitch = Home_Repairs_Trigger_Switch__c.getInstance(UserInfo.getUserId());
    if(triggerSwitch.Trigger_On_Variation__c){
      TriggerFactory.createAndExecuteHandler(Variation__c.sObjectType);
    }
}