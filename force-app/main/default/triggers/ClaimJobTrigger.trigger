trigger ClaimJobTrigger on Claim_Job__c (before insert,before update,after update) {
    Home_Repairs_Trigger_Switch__c triggerSwitch = Home_Repairs_Trigger_Switch__c.getInstance(UserInfo.getUserId());
     
    if(triggerSwitch.Trigger_On_Claim_Job__c){
      TriggerFactory.createAndExecuteHandler(Claim_Job__c.sObjectType);
    }
}