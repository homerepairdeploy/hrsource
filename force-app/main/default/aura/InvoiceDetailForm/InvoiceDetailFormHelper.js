({
    getWoUpToAmount : function(component) {
        console.log('init Helper...');
        component.set("v.isProcessing", true);
        var recordId = component.get("v.recordId");
        component.set("v.recordId", recordId);
        console.log('Record Id on init ',recordId);
        var woRecord = component.get("v.woRecord");
        var action = component.get("c.getWorkOrdereRecord");
        action.setParams({
            "woId" : recordId
        });
        action.setCallback(this, function(response){
            component.set("v.isProcessing", false);
        	var state = response.getState();
            if(state == "SUCCESS"){
                var woObj = response.getReturnValue();
                var varAmount = 0;
                
                if(woObj.Variations__r!=undefined && woObj.Variations__r!=null)
                {	
                    var variations = woObj.Variations__r;
                    for(var i = 0 ; i < variations.length ; i++) 
                    {
                      //varAmount+=variations[i].Additional_Labour_Amount__c + variations[i].Additional_Materials_Amount__c;
                        varAmount+=variations[i].Addl_Labour_Add_Material_GST__c ;
                    }
                }
                //set work order up to amount as Total amount
                //component.set("v.woUpToAmount", woObj.Up_To_Amount__c + varAmount);
                var totV = 0 ;
                if(woObj.Total_Variations_Amounts_Ex_Rejected__c != null && woObj.Total_Variations_Amounts_Ex_Rejected__c != 0)
                {
                      totV = woObj.Total_Variations_Amounts_Ex_Rejected__c ;
                }else 
                {
                      totV = woObj.Up_To_Amount__c ;
                }

                console.log("Setting v.isRCTI to " + woObj.Trade_RCTI__c);
                component.set("v.isRCTI", woObj.Trade_RCTI__c);

                component.set("v.woUpToAmount", totV);
                component.set("v.woStatus", woObj.Status);
                //Validation to check if worktype is Assessor                
                component.set("v.woWorkType",woObj.Work_Type_Name__c);               
                var isAssessorWorkType = this.checkAssessorWorkType(component);

                var isStatusComplete = this.checkWOStatus(component);
                if(isStatusComplete === false){
                   window.setTimeout(
                        $A.getCallback(function() {
                            $A.get("e.force:closeQuickAction").fire();
                        }), 100
                    );
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error",
                        "type" : "error",
                        "message":"Invoice can be added only on completed work order."
                    });
                    toastEvent.fire();
                }
                //Validation to check if worktype is Assessor
                else if(isAssessorWorkType === false){
                    console.log('Is Assessor WorkType');
                    window.setTimeout(
                        $A.getCallback(function() {
                            $A.get("e.force:closeQuickAction").fire();
                        }), 100
                    );
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error",
                        "type" : "error",
                        "message":"Invoice cannot be added for Assessor Work Types."
                    });
                    toastEvent.fire();
                }
            }else{
                console.log('init fail');
            }
        });
        $A.enqueueAction(action);
    },
	updateLabourInGST : function(component) {
		var strLabExGST = component.get("v.LabourExGST");
        var labExGST = Number(strLabExGST);
        var valGST = component.get("v.GST");
		var labInGST = labExGST * (valGST+1);
        console.log(this.roundTwoPlaces(labInGST));
        labInGST = this.roundTwoPlaces(labInGST);
        component.set("v.LabourInGST", labInGST.toFixed(2));
        var matInGST = component.get("v.MaterialsInGST");
	},
    updateTotalExGST : function(component) {
        var valGST = component.get("v.GST");
        var labExGST = component.get("v.LabourExGST");
        var matExGST = component.get("v.MaterialsExGST");
        var gstFreeLab = component.get("v.GSTFreeLabour");
        var gstFreeMat = component.get("v.GSTFreeMaterial");
		
        if($A.util.isEmpty(matExGST)){
            matExGST = 0;
        }
        if($A.util.isEmpty(labExGST)){
            labExGST = 0;
        }
        if($A.util.isEmpty(gstFreeLab)){
            gstFreeLab = 0;
        }
        if($A.util.isEmpty(gstFreeMat)){
            gstFreeMat = 0;
        }
        var totalExGst = matExGST*1 + gstFreeLab*1 + gstFreeMat*1 + labExGST*1;
        component.set("v.TotalExGST", totalExGst.toFixed(2));
        var labGSTAmount = valGST*labExGST;
        var matGSTAmount = valGST*matExGST;
        // update total GST Amount
        var totalGST = labGSTAmount + matGSTAmount;
        labGSTAmount = this.roundTwoPlaces(labGSTAmount);
        matGSTAmount = this.roundTwoPlaces(matGSTAmount);
        totalGST = labGSTAmount + matGSTAmount;

        console.log(labGSTAmount);
        console.log(matGSTAmount);
        console.log(totalGST);
        console.log(totalExGst);

        component.set("v.labGSTAmount", labGSTAmount.toFixed(2));
        component.set("v.matGSTAmount", matGSTAmount.toFixed(2));
        component.set("v.TotalGST", totalGST.toFixed(2));
    },
    roundTwoPlaces : function(num) {
        return Math.round(num * 100) / 100
    },
    updateLabourExGST : function(component) {
        var srtLabInGST = component.get("v.LabourInGST");
        var labInGST = Number(srtLabInGST);
        var valGST = component.get("v.GST");
		var labExGST = labInGST / (valGST+1);
        labExGST = this.roundTwoPlaces(labExGST);
        component.set("v.LabourExGST", labExGST.toFixed(2));
    },
    updateTotalInGST : function(component) {
        var matInGST = component.get("v.MaterialsInGST");
        var labInGST = component.get("v.LabourInGST");
        var gstFreeLab = component.get("v.GSTFreeLabour");
        var gstFreeMat = component.get("v.GSTFreeMaterial");
        if($A.util.isEmpty(matInGST)){
            matInGST = 0;
        }
        if($A.util.isEmpty(labInGST)){
            labInGST = 0;
        }
        if($A.util.isEmpty(gstFreeLab)){
            gstFreeLab = 0;
        }
        if($A.util.isEmpty(gstFreeMat)){
            gstFreeMat = 0;
        }
        var totalInGST = matInGST*1 + labInGST*1 + gstFreeLab*1 + gstFreeMat*1;
        component.set("v.TotalInGST", totalInGST.toFixed(2));
    },
    updateMaterialsInGST : function(component) {
        var strMatExGST = component.get("v.MaterialsExGST");
        var matExGST = Number(strMatExGST);
        var valGST = component.get("v.GST");
		var matInGST = matExGST * (valGST+1);
        matInGST = this.roundTwoPlaces(matInGST);
        component.set("v.MaterialsInGST", matInGST.toFixed(2));
    },
    updateMaterialsExGST : function(component) {
        var strMatInGST = component.get("v.MaterialsInGST");
        var matInGST = Number(strMatInGST);
        var valGST = component.get("v.GST");
		var matExGST = matInGST / (valGST+1);
        matExGST = this.roundTwoPlaces(matExGST);
        component.set("v.MaterialsExGST", matExGST.toFixed(2));
    },
    initFileTable : function(component) {
        var actions = [ { 'label': "Delete", 'iconName': 'utility:delete', 'name': 'delete_details' }];      
        component.set("v.uploadedColumns", [ {label: "File Name", fieldName: 'name', type: 'text'},
                                             { type: 'action', typeAttributes: { rowActions: actions } }
                                           ]);   
    },
    createInvoice : function(component) {
        component.set("v.isProcessing", true);
        component.set("v.showError", false);
        var recordId = component.get("v.recordId");
        var valGST = component.get("v.GST");
        var labExGST = component.get("v.LabourExGST");
        var labInGST = component.get("v.LabourInGST");
        var matExGST = component.get("v.MaterialsExGST");
        var matInGST = component.get("v.MaterialsInGST");
        var gstFreeLab = component.get("v.GSTFreeLabour");
        var gstFreeMat = component.get("v.GSTFreeMaterial");
        var totalInGST = component.get("v.TotalInGST");
        var totalExGST = component.get("v.TotalExGST");
        var totalGST = component.get("v.TotalGST");
        var labGSTAmount = component.get("v.labGSTAmount");
        var matGSTAmount = component.get("v.matGSTAmount");        
        var invoiceObj = component.get("v.invoiceRecord");
        var isInvoiceGSTFree = component.get('v.isGSTFree');
        var isRCTI = component.get("v.isRCTI");
        
        invoiceObj.Supplier_Invoice_Ref__c = component.get("v.InvoiceNo");
        invoiceObj.Invoice_Date_1__c = component.get("v.InvoiceDate");
        invoiceObj.Labour_ex_GST__c = Number(labExGST) == 'NaN' ? 0 : Number(labExGST);
        invoiceObj.GST_Labour_New__c = Number(labGSTAmount) == 'NaN' ? 0 : Number(labGSTAmount);
        invoiceObj.Labour_Amount_inc_GST_New__c = Number(labInGST) == 'NaN' ? 0 : Number(labInGST);
        invoiceObj.Material_ex_GST__c = Number(matExGST) == 'NaN' ? 0 : Number(matExGST);
        invoiceObj.GST_Material_New__c = Number(matGSTAmount) == 'NaN' ? 0 : Number(matGSTAmount);
        invoiceObj.Material_Amount_inc_GST_New__c = Number(matInGST ) == 'NaN' ? 0 : Number(matInGST);
        invoiceObj.Labour_GST_Free__c = Number(gstFreeLab) == 'NaN' ? 0 : Number(gstFreeLab);
        invoiceObj.Material_GST_Free__c = Number(gstFreeMat) == 'NaN' ? 0 : Number(gstFreeMat);
        invoiceObj.Total_ex_GST__c = Number(totalExGST) == 'NaN' ? 0 : Number(totalExGST);
        invoiceObj.Total_GST__c = Number(totalGST) == 'NaN' ? 0 : Number(totalGST);
        invoiceObj.Total_inc_GST__c = Number(totalInGST) == 'NaN' ? 0 : Number(totalInGST);
        invoiceObj.Invoice_Type__c = 'Supplier';
        if (isRCTI) {
            invoiceObj.Status__c = 'Approved';
        }
        else {
            invoiceObj.Status__c = 'Submitted';
        }
        invoiceObj.Manual_Invoice__c = component.get("v.manualInvoice");
        
        if(isInvoiceGSTFree) {
            invoiceObj.Labour_ex_GST__c = invoiceObj.Labour_ex_GST__c + invoiceObj.Labour_GST_Free__c;
            invoiceObj.Material_ex_GST__c = invoiceObj.Material_ex_GST__c + invoiceObj.Material_GST_Free__c;
        }
        var thisOrderAtt = component.get('v.thisOrder');
        if( thisOrderAtt.RecordType.Name == 'Other' && !$A.util.isEmpty(component.get('v.selectedLookUpRecord'))) 
            invoiceObj.New_Trade_Account__c = component.get('v.selectedLookUpRecord').Id;
            
        var invoice = JSON.stringify(invoiceObj)
        var action = component.get("c.createInvoiceRecord");
        action.setParams({
            "invoice" : invoice,
            "woId" : recordId
        });
        action.setCallback(this, function(response){
            component.set("v.isOpen", false);
            component.set("v.isProcessing", false);
            var state = response.getState();
            if(state == "SUCCESS"){
                var result = response.getReturnValue();
                console.log('result >>'+result);
                if(result == 'Success'){                    
                    location.reload();                    
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                    
                }else{
                    console.log('result '+result);
                    component.set("v.showError", true);
                    component.set("v.errorTitle", "Error");
                    component.set("v.errorMesg", result);
                }
            }else{
                console.log('createInvoice fail');
            }
        });
        $A.enqueueAction(action);
    },
    displayError : function(component, title, messg) {
        component.set("v.showError", true);
        component.set("v.errorTitle", title);
        component.set("v.errorMesg", messg);
    },
    clearError : function(component) {
        component.set("v.showError", false);
        component.set("v.errorTitle", "");
        component.set("v.errorMesg", "");

    },
    displaySuccess : function(component, title, messg) {
        component.set("v.showSuccess", true);
        component.set("v.successTitle", title);
        component.set("v.successMesg", messg);
    },
    checkRequiredFields : function(component) {
        var InvoiceNo = component.get("v.InvoiceNo");
        var InvoiceDate = component.get("v.InvoiceDate");
        var labExGST = component.get("v.LabourExGST");
        var labInGST = component.get("v.LabourInGST");
        var matExGST = component.get("v.MaterialsExGST");
        var matInGST = component.get("v.MaterialsInGST");
        var isInoiceDateValid = component.get("v.validInvoiceDate");
        var isFileUpload = component.get("v.isFileUpload");
        var isRCTI = component.get("v.isRCTI");
        
        var isValid = true;

        isValid = isInoiceDateValid;

        // RCTI File Upload
        if (isRCTI == true && isFileUpload == true) {
            isValid = false;
        }

        // Non-RCTI File Upload
        if (isRCTI == false && isFileUpload == false) {
            isValid = false;
        }

        // RCTI Invoice Number
        if (isRCTI == true && !$A.util.isEmpty(InvoiceNo)) {
            isValid = false;
        }

        // Non-RCTI Invoice Number
        if (isRCTI == false && $A.util.isEmpty(InvoiceNo)) {
            isValid = false;
        }

        console.log('Invoice Valid Date >>> '+isInoiceDateValid);
        console.log('isFileUpload successful >>> '+isFileUpload);

        if($A.util.isEmpty(InvoiceDate)){
            isValid = false;
        }
        if($A.util.isEmpty(labExGST) || labExGST == 'NaN'){
            isValid = false;
        }
        if($A.util.isEmpty(labInGST) || labInGST == 'NaN'){
            isValid = false;
        }
        if($A.util.isEmpty(matExGST) || matExGST == 'NaN'){
            isValid = false;
        }
        if($A.util.isEmpty(matInGST) || matInGST == 'NaN'){
            isValid = false;
        }
        //component.set("v.isDisableSave", !isValid);
        return isValid;
    },
    checkWOStatus : function(component) {
        var woStatus = component.get("v.woStatus");
        console.log('woStatus > '+woStatus);
        var isStatusComplete = true;
        if(woStatus != 'Job Complete'){
            isStatusComplete = false;
        }
        return isStatusComplete;
    },
    //Validation to check if worktype is Assessor
    checkAssessorWorkType : function(component) {
        var woWorkType = component.get("v.woWorkType");
        console.log('woWorkType > '+woWorkType);
        var isAssessorWorkType = true;
        var assWorkTypes = ["Internal/Assesssor","Homerepair/Assessor","Assessment", "Reassessment", "Coordinator Assessment", "Quality Assurance"];
        if(assWorkTypes.includes(woWorkType)){
            isAssessorWorkType = false;
        }

        return isAssessorWorkType;
    },
    updateContentVersion: function(component,fileName,parentId,docType,documentId) {
        console.log('All Values :'+'--'+parentId+'--'+docType+'--'+documentId);
        var action = component.get("c.updateFileDescription");
        action.setParams({
            parentId:parentId,
            docType:docType,
            documentId : documentId
        });
        action.setCallback(this, function(a) {
            var state = a.getState();            
            console.log('state'+state);
            if (state === "SUCCESS") {
                component.set("v.isFileUpload", true);
                component.set("v.documentId", documentId);
                component.set("v.fileName",fileName);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type" : "success",
                    "message":"files has been uploaded successfully!"
                });
                //toastEvent.fire();
            }else{
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Unknown error"+errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                
            }
        });
        $A.enqueueAction(action);
    },
    deleteFile: function(component, docId) {
        var action = component.get("c.deleteFile");
        let that = this;
        component.set("v.isProcessing", true);
        action.setParams({
            contentDocumentId : docId
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            component.set("v.isProcessing", false);
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                that.removeFile(component, docId);
                toastEvent.setParams({
                    "title": "Success!",
                    "type" : "success",
                    "message":"File Deleted Successfully!"
                });
                toastEvent.fire();
                
            } else{
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Unknown error"+errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                } 
            }
        });
        $A.enqueueAction(action);
    },
    removeFile : function(component, docId) {
        let newFiles = [];
        let upFiles = component.get("v.uploadedFiles");
        for(let fl = 0 ; fl < upFiles.length ; fl++) {
            if(upFiles[fl].documentId != docId) {
                newFiles.push(upFiles[fl]);
            }
        }
        component.set("v.uploadedFiles", newFiles);
        component.set("v.uploadedFilesSize", newFiles.length);
        component.set("v.isFileUpload", false);
    },
    
    checkDuplicateOrSplCharsHelper : function(component) {
        if(component.get("v.InvoiceNo")!= component.get("v.InvoiceNoPreviousValue"))
        {
            
            var invoiceNo = component.get("v.InvoiceNo");
            component.set("v.InvoiceNoPreviousValue", invoiceNo);
            var action = component.get("c.checkDuplicateController");
            action.setParams({
                "invoiceNo" : invoiceNo
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state == "SUCCESS"){
                    var result = response.getReturnValue();
                    console.log('result >>'+result);
                    if(result == 'duplicate'){                    
                        component.set("v.showError", true);
                        component.set("v.errorTitle", "Error");
                        component.set("v.errorMesg", "Invoice Number already exists in the system. Kindly re-enter");
                        
                    }
                }else{
                    console.log('checkDuplicateController call failed');
                }
            });
            $A.enqueueAction(action);
        }
    }
})