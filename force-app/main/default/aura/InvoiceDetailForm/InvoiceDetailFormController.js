({
    init : function(component, event, helper) {
        console.log('Invoice...');

        var availableActions = component.get('v.availableActions');
        for (var i = 0; i < availableActions.length; i++) {
            if (availableActions[i] == "BACK") {
                component.set("v.canBack", true);
            }
        }

        var timezone = $A.get("$Locale.timezone");
        console.log('Time Zone Preference in Salesforce ORG :'+timezone);
        var mydate = new Date().toLocaleString("en-US", {timeZone: timezone})
        console.log('Date Instance with Salesforce Locale timezone : '+mydate);
        
        var localDate = new Date();
        console.log('Local Date in Your Laptop : '+localDate);
        var timezone = localDate.getTimezoneOffset();
        console.log(timezone); 
        
        var today = $A.localizationService.formatDate(mydate, "YYYY-MM-DD");
        console.log('today >>> '+today);
        
        component.set('v.InvoiceDate', today);
        helper.getWoUpToAmount(component);   
        helper.initFileTable(component); 
        console.log('Invoice Loaded...');
    },
    onButtonPressed: function(component, event, helper) {
        // Figure out which action was called
        var actionClicked = event.getSource().getLocalId();
        // Fire that action
        var navigate = component.get('v.navigateFlow');
        navigate(actionClicked);
    },
	changeGSTFree : function(component, event, helper) {
		component.set("v.GSTFreeLabour", '');
        component.set("v.GSTFreeMaterial", '');
        helper.updateTotalExGST(component);
	},
    onblurLabourExGST : function(component, event, helper) {
        helper.updateLabourInGST(component);
        var a = component.get("c.onchangeLabourInGST");
        $A.enqueueAction(a);
	},
    onchangeLabourExGST : function(component, event, helper) {
        helper.updateTotalExGST(component);
    },
    onblurLabourInGST : function(component, event, helper) {
        helper.updateLabourExGST(component);   
        var a = component.get("c.onchangeLabourExGST");
        $A.enqueueAction(a);
	},
    onchangeLabourInGST : function(component, event, helper) {
        helper.updateTotalInGST(component);
    },
    onblurMaterialsExGST : function(component, event, helper) {
		helper.updateMaterialsInGST(component);
        var a = component.get("c.onchangeMaterialsInGST");
        $A.enqueueAction(a);
    },
    onchangeMaterialsExGST : function(component, event, helper) {
        helper.updateTotalExGST(component);
    },
    onblurMaterialsInGST : function(component, event, helper) {
        helper.updateMaterialsExGST(component);
        var a = component.get("c.onchangeMaterialsExGST");
        $A.enqueueAction(a);
	},
     handleRowAction : function(component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'delete_details':
                  helper.deleteFile(component, row.documentId);
                  break; 
        }
    },
    onchangeMaterialsInGST : function(component, event, helper) {
        helper.updateTotalInGST(component);
	},
    onchangeGSTFreeLabour : function(component, event, helper) {
        helper.updateTotalExGST(component);
        helper.updateTotalInGST(component);
    },
    onchangeGSTFreeMaterial : function(component, event, helper) {
        helper.updateTotalExGST(component);
        helper.updateTotalInGST(component);
    },
    saveDetails : function(component, event, helper) {
        console.log('Save Details');
        var isValid = helper.checkRequiredFields(component);

        if(isValid===true){
            var isStatusComplete = helper.checkWOStatus(component);
            console.log('checkWOStatus isStatusComplete >> '+isStatusComplete);
            if(isStatusComplete === true){
                helper.clearError(component);
                var woUpToAmount = Number(component.get("v.woUpToAmount"));
                var TotalExGST = Number(component.get("v.TotalExGST"));
                console.log('kem -TotalExGST ' , TotalExGST);
                console.log('kem -woUpToAmount ' , woUpToAmount);
                if(TotalExGST > woUpToAmount){
                    helper.displayError(component,"Error", "The amount is greater than the Work Order amount.");
                  }else{
                     helper.createInvoice(component);
                }
            }else{
                helper.displayError(component,"Error", "Invoice can be raised only on completed work order.");
            }

        }else{
            helper.displayError(component,"Error", "Fill the required fields and upload an attachment (non RCTI only).");
        }
    },
    closeModel : function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
    },
    saveInvoice : function(component, event, helper) {
        helper.createInvoice(component);
    },
    onchangeInvoiceDate : function(component, event, helper) {
        var invoiceDate = component.get("v.InvoiceDate");
        var result = new Date();
		result.setDate(result.getDate() - 30);
        var lastMonthDate = $A.localizationService.formatDate(result, "YYYY-MM-DD");
        if (invoiceDate > lastMonthDate) {  
            helper.clearError(component);
            component.set("v.validInvoiceDate", true);
        }else {  
            helper.displayError(component,"Error", "Invoice Date can only be in future or past 30 days.");
            component.set("v.validInvoiceDate", false);
        } 
    },
    handleUploadFinished: function(component, event, helper) {
        var uploadedFiles = event.getParam("files");
		var fileName='';
        
        console.log(JSON.stringify(uploadedFiles));
        let oldFiles = component.get("v.uploadedFiles");

        for(let cnt = 0 ; cnt < uploadedFiles.length ; cnt++) {
            let fle = { };
            fle.name = uploadedFiles[cnt].name;
            fileName=uploadedFiles[cnt].name;
            fle.documentId = uploadedFiles[cnt].documentId;
            oldFiles.push(fle);
        }
        component.set("v.uploadedFiles", oldFiles);
        component.set("v.uploadedFilesSize", oldFiles.length);
        console.log(JSON.stringify(component.get("v.uploadedFiles")));

        var documentId = uploadedFiles[0].documentId;
        var parentId = component.get("v.recordId");
        var docType = 'Invoice';
        helper.updateContentVersion(component,fileName,parentId,docType,documentId);
        
    },
    handleFilesChange: function(component, event, helper) {
        var fileName = 'No File Selected..';
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName", fileName);
    },

    checkDuplicateOrSplChars: function(component, event, helper) {
        helper.checkDuplicateOrSplCharsHelper(component);
    },
    
})