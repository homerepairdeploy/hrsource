({
	doInit : function(component, event, helper) {     
        helper.getClaimdetails(component,event,helper);
        helper.getBAdetails(component,event,helper);            
        helper.getCalculateCashSettle(component,event,helper);
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.today', today);        
       //helper.getContentFiles(component, event, helper); 
         
    }
})