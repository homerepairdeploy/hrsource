({
    getClaimdetails : function(component,event,helper) {
        var action = component.get("c.getClaimRecord");
        action.setParams({            
            'ClaimID' : component.get("v.recordId")
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null){
                component.set("v.caseWrapper",response.getReturnValue());
                //alert('<<<'+JSON.stringify(component.get("v.caseWrapper")));
            }
        });
        
        $A.enqueueAction(action);
    },
    
    getBAdetails : function(component,event,helper) {
        var action = component.get("c.getBARecord");
        action.setParams({            
            'BARecordId' : component.get("v.BARecordId")
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null){
                component.set("v.stagingBA",response.getReturnValue());
                this.setBAfields(component,event,helper);
                this.getContentFiles(component, event, helper); 
            }
        });
        
        $A.enqueueAction(action);
    },
    
    setBAfields : function(component,event,helper)
    {
        
        //Populate values
        var sBA = component.get("v.stagingBA");
        if(sBA.Building_Height__c == 'Other')    
        {
            component.set('v.buildingHeight',sBA.Other_Building_Height__c);
        }
        else
        {
            component.set('v.buildingHeight',sBA.Building_Height__c);
        }               
        
        
        if(sBA.Roof_Type__c == 'Other')    
        {
            component.set('v.roofType',sBA.Other_Roof_Type__c);
        }
        else
        {
            component.set('v.roofType',sBA.Roof_Type__c);
        }                    
        
        // get Asbestos description
        this.getAsbestosDescription(component,event,helper);        
        
        // get TradeRisk  description
        this.getpTradeItemDescription(component,event,helper);
        
        // get safety repair working heights  description
        this.getWSItemDescription(component,event,helper);        
        
    },
    
    getAsbestosDescription : function(component,event,helper)
    {       
        var Asbestos = component.get("v.stagingBA.Asbestos__c");
        
        if (Asbestos !=null && Asbestos !='')
        {        
            var action = component.get("c.getWorkItemDescription");  
            action.setParams({ "Item" : Asbestos});              
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {                
                    var itemDescription = response.getReturnValue(); 
                    component.set("v.Asbestos_Desc",itemDescription);
                }             
            });
            $A.enqueueAction(action);  
        }
    },
    
    getpTradeItemDescription : function(component,event,helper)
    {       
        var tradeList = [];
        var tradedesc ='';
        var linebreak = " ";
        var trade = component.get("v.stagingBA.Potential_Risks_to_Trades__c"); 
        if(trade!=null)
        {            
            tradeList=trade.split(';');
            for(var i=0;i<tradeList.length;i++)
            {             
                var action = component.get("c.getWorkItemDescription");  
                action.setParams({ "Item" : tradeList[i]});              
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {                
                        tradedesc = tradedesc + response.getReturnValue() + linebreak;  
                        component.set("v.TradeRisk_Desc",tradedesc);
                    }             
                });
                $A.enqueueAction(action);
            }              
        }
    },
    
    getWSItemDescription : function(component,event,helper)
    {       
        var srwh = component.get("v.stagingBA.Safety_Repair_Working_Heights__c");               
        
        if(srwh !=null && srwh !='')
        {        
            var action = component.get("c.getWorkItemDescription");  
            action.setParams({ "Item" : srwh});              
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {                
                    var itemDescription = response.getReturnValue();  
                    component.set("v.Wfheights_Desc",itemDescription);
                }             
            });
            $A.enqueueAction(action);  
        }
    },
    
    getContentFiles : function(component, event, helper)
    {
        
        var sObjectId = component.get("v.stagingBA.Id");              
        var action = component.get("c.getUploadedPhotos");  
        action.setParams({ "sObjectId" : sObjectId});              
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.lstContentDoc', response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);  
    },
    
    getCalculateCashSettle : function(component,event,helper)
    {
        var rList = component.get("v.roomNameList");         
        let cashSettleWithGST = component.get("v.cashSettlementInclGST");
        let cashtotalwithoutgst = component.get("v.cashSettlementExclGST");
        for(var i=0;i<rList.length;i++)
        {                   
            cashSettleWithGST = cashSettleWithGST + Number(rList[i].cashtotalwithgst);
        }
        cashSettleWithGST = cashSettleWithGST.toFixed(2);
        component.set("v.cashSettlementInclGST",cashSettleWithGST);
        
        cashtotalwithoutgst = cashSettleWithGST /1.1;    
        cashtotalwithoutgst = cashtotalwithoutgst.toFixed(2);        
        component.set("v.cashSettlementExclGST",cashtotalwithoutgst);        
        
    },
})