({
    initHelper : function(component, event, helper) {
        var action = component.get("c.getWorkOrderDetails");        
        action.setParams({            
            'workOrderId' : component.get("v.recordId")
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var workOrder = response.getReturnValue();
                component.set('v.workorder', workOrder);
                if(workOrder.Case.Status == 'Assessment Scheduled' && workOrder.Case.Assessment_Count__c !=null && 
                   workOrder.Case.Assessment_Count__c > 1) {
                    component.set('v.isValidWorkOrder',false);
                    this.showToast('error', 'Error: Work order line cannot be added as assessment is in progress');
                }else{
                    component.set('v.isValidWorkOrder',true);
                    if(workOrder.Status == 'Cancelled' || workOrder.Status == 'Closed' || workOrder.Case.Status == 'Completed' || workOrder.Case.Status == 'cancelled') {
                        component.set('v.isValidWorkOrder',false);
                        this.showToast('error', 'Error: Work order line cannot be added to a work order of this status');
                    }else{
                        component.set('v.isValidWorkOrder',true);
                    }
                }
                
                
            }            
        });        
        $A.enqueueAction(action);
    },
    
    showToast : function(type, message) {        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "message": message,
            "type": type,
            "duration": 800,
            "mode": "dismissible",
        });
        toastEvent.fire();        
    }
})