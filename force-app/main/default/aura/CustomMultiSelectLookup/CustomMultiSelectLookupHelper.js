({
    searchHelper : function(component,event,getInputkeyWord) {
        // call the apex class method 
        var action;
        console.log(component.get("v.objectParentId") == '');
        console.log(component.get("v.objectAPIName"))
        console.log(component.get("v.objectParentId"));
        if(component.get("v.objectParentId") == ''){
			action = component.get("c.fetchLookUpValuesObject");            
            // set param to method  
            action.setParams({
                'searchKeyWord': getInputkeyWord,
                'ObjectName' : component.get("v.objectAPIName"),
                'ExcludeitemsList' : component.get("v.lstSelectedRecords")
            });
        }else{
            action = component.get("c.fetchLookUpValues");
            // set param to method  
            action.setParams({
                'searchKeyWord': getInputkeyWord,
                'objectName' : component.get("v.objectAPIName"),
                'parentId' : component.get("v.objectParentId")
            });
            
        }
        
        // set a callBack    
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            console.log('state',state);
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // if storeResponse size is equal 0 ,display No Records Found... message on screen.                }
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Records Found...');
                } else {
                    component.set("v.Message", '');
                    // set searchResult list with return value from server.
                }
                component.set("v.listOfSearchRecords", storeResponse); 
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    },
})