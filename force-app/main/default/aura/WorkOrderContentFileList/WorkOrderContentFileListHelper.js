({
	showLoader : function (cmp) {
        cmp.set("v.displaySpinner", true);
    },
    hideLoader :function (cmp) { 
        cmp.set("v.displaySpinner", false); 
    },
    showExampleModal : function(component) {      
        var modal = component.find("FileModal");
        $A.util.removeClass(modal, 'slds-hide'); 
        $A.util.addClass(modal, "slds-show");
    },
    hideExampleModal : function(component) {
        var modal = component.find("FileModal");
        $A.util.addClass(modal, 'slds-hide');
    },
})