({
	getuploadedFiles:function(component,event,helper){
        var sObjectId = component.get("v.recordId");
        var action = component.get("c.getFiles");  
        action.setParams({ "sObjectId" : sObjectId});      
        action.setCallback(this,function(response){  
            var state = response.getState(); 
            if(state=='SUCCESS'){  
                var result = response.getReturnValue(); 
                console.log('files---'+result);
                component.set("v.files",result);  
            }  
        });  
        $A.enqueueAction(action);  
    },
    
    delUploadedfiles : function(component,documentId) {  
        var action = component.get("c.deleteFiles");           
        action.setParams({
            "sdocumentId":documentId            
        });  
        action.setCallback(this,function(response){  
            var state = response.getState();  
            if(state=='SUCCESS'){  
                this.getuploadedFiles(component);
                component.set("v.Spinner", false); 
            }  
        });  
        $A.enqueueAction(action);  
    }
})