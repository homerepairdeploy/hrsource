({
	 doInit:function(component,event,helper){ 
        helper.getuploadedFiles(component, event, helper);
     },
    
    doAction:function(component,event,helper){ 
		helper.getuploadedFiles(component, event, helper);
    },      
    
    getFilesList: function(component, event, helper){
      // alert(component.get("v.recordId"));
        helper.getuploadedFiles(component, event, helper);
    },
        
    uploadFinished : function(component, event, helper) {  
        var uploadedFiles = event.getParam("files");  
        helper.getuploadedFiles(component, event, helper);         
        component.find('notifLib').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "File Uploaded successfully!!",
            closeCallback: function() {}
        });
    }, 
    
    delFiles:function(component,event,helper){
        component.set("v.Spinner", true); 
        var documentId = event.currentTarget.id;        
        helper.delUploadedfiles(component,documentId);  
    }
})