({
     /*
     * Initially this Method will be called and will fetch the records from the Salesforce Org 
     * Then we will hold all the records into the attribute of Lightning Component
     */
	getAllThumbnail : function(component, event, helper) {
		var currRecId = component.get("v.recordId");
		var action = component.get("c.getAllThumbnails");
        action.setParams({
            "WOId" : currRecId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var pageSize = component.get("v.pageSize");
				// hold all the records into an temp list named "conDocWrapper"
                var conDocWrapper = response.getReturnValue();
				//setting start page and end page
                component.set("v.startPage", 0);
                component.set("v.endPage",pageSize-1);
                // hold all the records into an attribute named "claimFileDataList" and "roomFileDataList"
                component.set("v.claimFileDataList", conDocWrapper.lstFileOnClaimRecord);
                component.set("v.roomFileDataList", conDocWrapper.lstFileOnWOLIRecords);
                // variables to store length of file Data list's
                var claimListDataSize = component.get("v.claimFileDataList").length;
                var roomListDataSize = component.get("v.roomFileDataList").length;
                if(claimListDataSize > 0){
                    component.set("v.claimFileTotalRecords", claimListDataSize);
                }
                if(roomListDataSize > 0){
                    component.set("v.roomFileTotalRecords", roomListDataSize);
                }
                if(claimListDataSize >= roomListDataSize){
                    component.set("v.totalRecords", claimListDataSize);
                }else {
                    component.set("v.totalRecords", roomListDataSize);
                }
                var claimFilePaginationLst = [];
                var roomFilePaginationLst = [];
                for(var i=0; i< pageSize; i++){
                    if(claimListDataSize> i){
                        claimFilePaginationLst.push(conDocWrapper.lstFileOnClaimRecord[i]);  
                    }
                    if(roomListDataSize>i){
                        roomFilePaginationLst.push(conDocWrapper.lstFileOnWOLIRecords[i]);
                    }
                }
				component.set("v.claimFilePaginationLst", claimFilePaginationLst);
                component.set("v.roomFilePaginationLst", roomFilePaginationLst);
            }else{
                console.log('getAllThumbnails fail');
            }
        });
        $A.enqueueAction(action);
	},
    /*
     * Method will be called when use clicks on next button and performs the 
     * calculation to show the next set of records
     */
    next : function(component, event){
        var claimFileDataList = component.get("v.claimFileDataList");
        var roomFileDataList = component.get("v.roomFileDataList");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var claimFilePaginationLst = [];
        var roomFilePaginationLst = [];
        var counter = 0;
        for(var i=end+1; i<end+pageSize+1; i++){
            if(claimFileDataList.length> i){
                claimFilePaginationLst.push(claimFileDataList[i]);  
            }
            if(roomFileDataList.length>i){
                roomFilePaginationLst.push(roomFileDataList[i]);
            }
            counter ++ ;
        }
        start = start + counter;
        end = end + counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set("v.claimFilePaginationLst", claimFilePaginationLst);
        component.set("v.roomFilePaginationLst", roomFilePaginationLst);
        var claimListDataSize = component.get("v.claimFilePaginationLst").length;
        var roomListDataSize = component.get("v.roomFilePaginationLst").length;
        component.set("v.claimFileTotalRecords", claimListDataSize);
        component.set("v.roomFileTotalRecords", roomListDataSize);
     
    },
    /*
     * Method will be called when use clicks on previous button and performs the 
     * calculation to show the previous set of records
     */
    previous : function(component, event){
        var claimFileDataList = component.get("v.claimFileDataList");
        var roomFileDataList = component.get("v.roomFileDataList");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var claimFilePaginationLst = [];
        var roomFilePaginationLst = [];
        var counter = 0;
        for(var i= start-pageSize; i < start ; i++){
            if(i > -1){
                if(claimFileDataList.length> i){
                    claimFilePaginationLst.push(claimFileDataList[i]);  
                }
                if(roomFileDataList.length>i){
                    roomFilePaginationLst.push(roomFileDataList[i]);
                }
                counter ++;
            }else{
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set("v.claimFilePaginationLst", claimFilePaginationLst);
        component.set("v.roomFilePaginationLst", roomFilePaginationLst);
        var claimListDataSize = component.get("v.claimFilePaginationLst").length;
        var roomListDataSize = component.get("v.roomFilePaginationLst").length;
        component.set("v.claimFileTotalRecords", claimListDataSize);
        component.set("v.roomFileTotalRecords", roomListDataSize);
    },
})