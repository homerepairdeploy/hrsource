({
	doInit : function(component, event, helper) {
        helper.getAllThumbnail(component, event, helper);
	},
    next: function (component, event, helper) {
        helper.next(component, event);
    },
     previous: function (component, event, helper) {
        helper.previous(component, event);
    },
})