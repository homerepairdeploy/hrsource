({
    doInit: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.get("v.PageSize");        
        helper.getStagingBAList(component, pageNumber, pageSize);
    },
     
    handleNext: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.get("v.PageSize");       
        pageNumber++;
        helper.getStagingBAList(component, pageNumber, pageSize);
    },
     
    handlePrev: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.get("v.PageSize");      
        pageNumber--;
        helper.getStagingBAList(component, pageNumber, pageSize);
    },     
       
    searchKeyRecord: function (component, event, helper) {
        
        component.set("v.PageNumber",1);
        component.set("v.TotalPages",1);
        
        var params = event.getParam('arguments');
          
        var searchKey = params.searchKey;
        if((searchKey == '') || (searchKey == null ))
        {
       	   component.set("v.srchResults",false);
            var pageNumber = 1;          //if search value is empty reload records 
            var pageSize = component.get("v.PageSize");              
            helper.getStagingBAList(component, pageNumber, pageSize);            
        }
        else
        {
           component.set("v.srchResults",true);
            var action = component.get("c.getStagingBADataSrch");       
            action.setParams({
                "searchKey": params.searchKey
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS")
                {
                    component.set("v.stagingBAList",response.getReturnValue());                                          
                }                
            });        
            $A.enqueueAction(action);  
        }         
    },
    
      navigate : function(component, event, helper) {          
          var selectedRec = event.getSource().get("v.name");
          var sList = component.get("v.stagingBAList");
          var selRec = component.get("v.selectedRecord");
          for(var i = 0; i < sList.length; i++){
              if(sList[i].Case__r.Id == selectedRec){              
                  selRec=sList[i];              
              }
          }    
          component.set("v.selectedRecord",selRec);
          helper.NavigateToRec(component, event, helper);
      },    
    
})