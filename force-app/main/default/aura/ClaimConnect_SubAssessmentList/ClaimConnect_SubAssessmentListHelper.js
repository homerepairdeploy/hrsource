({
    getSubAssessList: function(component, pageNumber, pageSize) {
        var action = component.get("c.getSubAssessments");
        action.setParams({
            "pageNumber": pageNumber,
            "pageSize": pageSize
        });
        action.setCallback(this, function(result) { 
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                var resultData = result.getReturnValue();
                
                if (resultData.servApptWrapperList.length ==0)
                {
                    component.set("v.RecordStart", 0);
                    component.set("v.RecordEnd", 0);
                    component.set("v.PageNumber", 1);
                    component.set("v.TotalRecords",0 );
                    component.set("v.TotalPages", 1);
                }
				else
                {
                component.set("v.subAssessmentList", resultData.servApptWrapperList);
                component.set("v.PageNumber", resultData.pageNumber);
                component.set("v.TotalRecords", resultData.totalRecords);
                component.set("v.RecordStart", resultData.recordStart);
                component.set("v.RecordEnd", resultData.recordEnd);
                component.set("v.TotalPages", Math.ceil(resultData.totalRecords / pageSize));
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    NavigateToRec : function(component, event, helper){                
        component.getEvent("navToAssessmentEvt").setParams({"ClmRecId" : component.get("v.selectedRecord.CaseId") }).fire();             
    },

})