({
    doInit: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.get("v.PageSize");  
        // var pageSize = component.find("pageSize").get("v.value"); 
        helper.getSubAssessList(component, pageNumber, pageSize);
    },
    
    handleNext: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.get("v.PageSize");  
        //   var pageSize = component.find("pageSize").get("v.value");
        pageNumber++;
        helper.getSubAssessList(component, pageNumber, pageSize);
    },
    
    handlePrev: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.get("v.PageSize");
        //  var pageSize = component.find("pageSize").get("v.value");
        pageNumber--;
        helper.getSubAssessList(component, pageNumber, pageSize);
    },
    
    /* onSelectChange: function(component, event, helper) {
        var page = 1
        var pageSize = component.find("pageSize").get("v.value");
        helper.getStagingBAList(component, page, pageSize);
    },*/
    
    searchKeyRecord: function (component, event, helper) {
        component.set("v.PageNumber",1);
        component.set("v.TotalPages",1);
        
        var params = event.getParam('arguments');        
        var searchKey = params.searchKey;
        if((searchKey == '') || (searchKey == null ))
        {
            component.set("v.srchResults",false);
            var pageNumber = 1;          //if search value is empty reload records 
            var pageSize = component.get("v.PageSize"); 
            helper.getSubAssessList(component, pageNumber, pageSize);
        }
        else
        {
            component.set("v.srchResults",true);  
            var action = component.get("c.getSubAssessmentsSrch");       
            action.setParams({
                "searchKey": params.searchKey
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS")
                {
                    component.set("v.subAssessmentList",response.getReturnValue());
                }
                
            });        
            $A.enqueueAction(action);        
        }               
    },
    
    navigate : function(component, event, helper) {
        var selectedRec = event.getSource().get("v.name");
        var sList = component.get("v.subAssessmentList");
        var selRec = component.get("v.selectedRecord");
        for(var i = 0; i < sList.length; i++){
            if(sList[i].CaseId == selectedRec){              
                selRec=sList[i];              
            }
        }    
        component.set("v.selectedRecord",selRec);         
        helper.NavigateToRec(component, event, helper);
    },    
})