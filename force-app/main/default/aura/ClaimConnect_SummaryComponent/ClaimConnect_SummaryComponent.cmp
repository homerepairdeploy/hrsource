<!--
*************************************************************************************************** 
Name        : ClaimConnect_SummaryComponent
Description : This component is used on the summary tab when any claim is clicked 
				on 'My draft assessments' in ClaimConnect Application 

Purpose     : This component shows all the Trade Items from the SoW screen categorized by room,
			  when 'By Room' is selected. And when 'By Trade' is selected, this screen shows all 
			  the trade items categorized by Trade type field. 
			  On the same screen, assessment can be saved using 'Save Draft' button,
			  previewed using 'Preview Assessment' button and can be submitted using 
			  'Submit Assessment' button 
                                                            
VERSION        AUTHOR                     		DATE           DETAIL      DESCRIPTION   
1.0            Vidhya, Prasanth,Anketha        20-05-2020     Created     ClaimConnect APP
***************************************************************************************************
-->
<aura:component
    implements="force:appHostable,flexipage:availableForAllPageTypes,flexipage:availableForRecordHome,force:hasRecordId"
    controller="ClaimConnect_SummaryController" access="global">

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    </head>
    <!--  this tag sets modal width -->
    <aura:html tag="style">
        .slds-modal__container {
        min-width: 75vw;
        }
    </aura:html>

    <aura:handler name="init" value="{!this}" action="{!c.doInit}" />
    <!-- <aura:attribute name="summaryWrapper" type="ClaimConnect_SummaryController.SummaryWrapper"/> -->
    <aura:attribute name="roomWrapperList" type="ClaimConnect_SummaryController.RoomWrapper" />
    <aura:attribute name="roomItemList" type="List" />
    <aura:attribute name="roomNameList" type="List" />

    <aura:attribute name="caseId" type="string" default="" />
    <aura:attribute name="recordId" type="String" default="" />
    <aura:attribute name="SAid" type="String" default="" />

    <aura:attribute name="margin" type="Decimal" default="0" />
    <aura:attribute name="gst" type="Decimal" default="0" />
    <aura:attribute name="subTotal" type="Decimal" default="0" />
    <aura:attribute name="gstTotal" type="Decimal" default="0" />
    <aura:attribute name="totalAmount" type="Decimal" default="0" />
    <aura:attribute name="totalAmountCond" type="Boolean" default="false" />
    <aura:attribute name="saveData" type="Boolean" default="false" />

    <aura:attribute name="tradeMap" type="Map" />
    <aura:attribute name="dateMap" type="Map" />

    <aura:attribute name="buildingHeight" type="String" />
    <aura:attribute name="totalCashAmount" type="Decimal" default="0" />
    <aura:attribute name="NewStagingBA" type="Staging_Assessor_ba__c"
        default=" { 'sobjectType' : 'Staging_Assessor_ba__c' } " />
    <!--<aura:attribute name="recordId" type="String"/> -->

    <aura:attribute name="toggleValue" type="Boolean" default="false" />
    <aura:attribute name="summaryList" type="Staging_Assessor_sow_rooms__c[]" />


    <!-- Start by Vidhya - BA Cash Settlement Details -->
    <aura:attribute name="CashSettlementDefPickList" type="Object" />
    <aura:attribute name="TempAccomReasonPickList" type="Object" />
    <aura:attribute name="csDisable" type="boolean" default="false" />
    <aura:attribute name="cashSettleDetailDisable" type="boolean" default="true" />
    <aura:attribute name="tempAccomReasonDisable" type="boolean" default="true" />
    <aura:attribute name="errorMessage" type="string" default="" />
    <aura:attribute name="showSpinner" type="Boolean" default="false" />
    <aura:attribute name="isValid" type="Boolean" default="false" />


    <aura:attribute name="rptoptions" type="List" default="[
    {'label': 'Building Assessment Report', 'value': 'BARept'},
    {'label': 'Pricing Data Report', 'value': 'PricedRpt'}
    ]" />
    <aura:attribute name="rptvalue" type="String" default="BARept" />

    <aura:attribute name="showRadio" type="Boolean" default="false" />
    <aura:attribute name="sendData" type="object" />
    <aura:attribute name="isModalOpen" type="boolean" default="false" />
    <aura:attribute name="showbaRept" type="boolean" default="true" />
    <aura:attribute name="showpricedRept" type="boolean" default="false" />

    <aura:method name="invokedoInit" action="{!c.doInit}" description="Initialize data in summary component"
        access="public">
    </aura:method>

    <c:ClaimConnect_PickListController sObjectName="Case" fieldName="Cash_Settlement_Reason__c"
        picklistValues="{!v.CashSettlementDefPickList}" />
    <c:ClaimConnect_PickListController sObjectName="Case" fieldName="Temporary_Accommodation_Reason__c"
        picklistValues="{!v.TempAccomReasonPickList}" />
    <aura:handler name="change" value="{!v.toggleValue}" action="{!c.selectChange}" />
    <aura:if isTrue="{!v.showSpinner}">
        <lightning:spinner alternativeText="Loading" size="large" />
    </aura:if>

    <aura:attribute name="progress" type="Integer" default="0" />
    <aura:attribute name="progressText" type="String" default="Started" />
    <aura:attribute name="showProgress" type="Boolean" default="false" />


    <!-- End by Vidhya - BA Cash Settlement Details -->


    <div class="slds-scrollable" style="width:100%;height=100%;overflow-y:auto;">


        <div style="font-size: 75px; font-weight=">



            <lightning:input name="Summary Site Order" label="Summary Site Order" type="toggle"
                messageToggleActive="By Room" messageToggleInactive="By Trade" checked="{!v.toggleValue}"
                class="font" />
        </div>
        <br />

        <lightning:notificationsLibrary aura:id="notifLib" />
        
        <aura:if isTrue="{!v.toggleValue}">
            
             <aura:if isTrue="{!not(empty(v.errorMessage))}">
                <lightning:formattedText label="errmsg" class="err-text" value="{!v.errorMessage}" /> <br />
            </aura:if>           
            
            <aura:iteration items="{!v.roomNameList}" var="ri" indexVar="key">

                <p class="slds-p-horizontal_small">
                <div class="slds-grid">
                    <div class="slds-col slds-size_1-of-3 field-title">
                        Room Name:
                        <lightning:formattedText label="Height" labelclass="text-label" value="{!ri.roomName}" /> <br />
                        <!--   <lightning:formattedText label="Height" labelclass="text-label" value ="Room" /> :  Garage <br/> -->
                    </div>
                </div>
                <br />
                </p>


                <table class="slds-table slds-table_bordered slds-table_resizable-cols">
                    <thead>
                        <tr>
                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Order">Visit Order</div>
                            </th>
                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Trade">Trade Type</div>
                            </th>
                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Item Desc">Item Desc</div>
                            </th>
                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Qty">Qty</div>
                            </th>
                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Min">Labour Mins</div>
                            </th>
                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Labour">Total labour</div>
                            </th>
                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Total">Total Material</div>
                            </th>
                            <th scope="col">
                                <div class="slds-truncate" title="CashSettle">Cash Settlement</div>
                            </th>
                            <th></th>
                        </tr>
                    </thead>

                    <!-- Table Row -->

                    <aura:iteration items="{!ri.roomwrappers}" var="roomItemWrapper" indexVar="index">
                        <tbody>
                            <!-- Table Row -->
                            <tr>

                                <td>
                                    <lightning:formattedText value="{!roomItemWrapper.siteVisitNumber}"
                                        style="width:60px;height:0.05px" />
                                </td>

                                <td>
                                    <lightning:formattedText value="{!roomItemWrapper.workType}" />
                                </td>
                                <td>
                                    <lightning:formattedText value="{!roomItemWrapper.itemDesc}" />
                                </td>
                                <td>
                                    <lightning:formattedNumber value="{!roomItemWrapper.quantity}" />
                                </td>
                                <td>
                                    <lightning:formattedNumber value="{!roomItemWrapper.labourTime}" />
                                </td>
                                <td>
                                    <lightning:formattedNumber value="{!roomItemWrapper.labourAmount}" />
                                </td>
                                <td>
                                    <lightning:formattedNumber value="{!roomItemWrapper.materialAmount}" />
                                </td>
                                <td>
                                    <lightning:input type="checkbox" checked="{!roomItemWrapper.cashSettlement}"
                                        aura:id="cashSettlementRoom" name="cashSettleCheckBox" disabled="true" />
                                </td>
                            </tr>
                        </tbody>
                    </aura:iteration>
                </table>
                <br /><br />
                <div class="slds-clearfix slds-p-right_none field-title" style="align:right;margin-right:15%">
                    <div class="slds-float_right">
                        <table>
                            <tr>
                                <td align="left"> Total Cash Settlement incl GST: </td>
                                <td align="right">${!ri.cashtotalwithgst}</td>
                            </tr>
                            <tr>
                                <td align="left"> Total Estimate Cost incl GST: </td>
                                <td align="right">${!ri.totalwithgst}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br />
            </aura:iteration>
            <br />
        </aura:if>

        <aura:if isTrue="{!!v.toggleValue}">

            <!-- <div class="slds-text-heading_small slds-m-bottom__medium">
            <strong>Building Assessment Details</strong>
        </div> -->

            <!-- Start by Vidhya - BA Cash Settlement Details -->
            <lightning:card>
                <div class="SF1padding slds-size_1-of-1 slds-medium-size--5-of8 slds-large-size--5-of-8">
                    <div class="slds-grid slds-wrap" id="BAcsGrid ">
                        <div class="SF1padding slds-size_1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                            <lightning:input type="checkbox" label="Cash Settlement" aura:id="cashSettlementBA"
                                name="cashSettleCheckBox" checked="{!v.NewStagingBA.Cash_Settlement__c}"
                                onchange="{!c.handleCashSettlementChange}" disabled="{!v.csDisable}"/><br></br>

                            <lightning:Select label="Cash Settlement Definition" aura:id="cashSettleDef"
                                value="{!v.NewStagingBA.Cash_Settlement_Reason__c}"
                                disabled="{!v.cashSettleDetailDisable}" class="font">
                                <option value="">--None--</option>
                                <aura:iteration items="{!v.CashSettlementDefPickList}" var="val">
                                    <aura:if isTrue="{!v.NewStagingBA.Cash_Settlement_Reason__c == val }">
                                        <option value="{!val}" selected="selected">{!val}</option>
                                        <aura:set attribute="else">
                                            <option text="{!val}" value="{!val}"></option>
                                        </aura:set>
                                    </aura:if>
                                </aura:iteration>
                            </lightning:Select>
                            <lightning:Textarea aura:id="CashSettleReason" label="Cash Settlement Reason"
                                value="{!v.NewStagingBA.Cash_Settlement_Comments__c}"
                                disabled="{!v.cashSettleDetailDisable}" class="font" />
                        </div>


                        <div class="SF1padding slds-size_1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                            <lightning:input type="checkbox" aura:id="tempAccommodation"
                                name="tempAccommodationCheckBox" label=" Temporary Accommodation Required"
                                checked="{!v.NewStagingBA.Temporary_Accommodation_Required__c }"
                                onchange="{!c.handleTempAccomChange}" disabled="{!v.csDisable}" /><br></br>

                            <lightning:Select label="Temporary Accommodation Reason" aura:id="tempAccReason"
                                value="{!v.NewStagingBA.Temporary_Accommodation_Reason__c}"
                                disabled="{!v.tempAccomReasonDisable}" class="font">
                                <option value="">--None--</option>
                                <aura:iteration items="{!v.TempAccomReasonPickList}" var="val">
                                    <aura:if isTrue="{!v.NewStagingBA.Temporary_Accommodation_Reason__c == val }">
                                        <option value="{!val}" selected="selected">{!val}</option>
                                        <aura:set attribute="else">
                                            <option text="{!val}" value="{!val}"></option>
                                        </aura:set>
                                    </aura:if>
                                </aura:iteration>
                            </lightning:Select>
                        </div>
                    </div>
                </div>
            </lightning:card>

            <aura:if isTrue="{!not(empty(v.errorMessage))}">
                <lightning:formattedText label="errmsg" class="err-text" value="{!v.errorMessage}" /> <br />
            </aura:if>            


            <!-- End by Vidhya - BA Cash Settlement Details -->
            <aura:iteration items="{!v.roomItemList}" var="ri" indexVar="key">


                <p class="slds-p-horizontal_small">
                <div class="slds-grid">
                    <div class="slds-col slds-size_1-of-3 field-title ">
                        Trade Type:
                        <lightning:formattedText label="Height" labelclass="text-label" value="{!ri.tradeType}" />
                        <br />
                    </div>
                </div>
                <br />
                </p>
                <table class="slds-table slds-table_bordered slds-table_resizable-cols">
                    <thead>
                        <tr>
                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Order">Visit Order</div>
                            </th>

                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Name">Room Name</div>
                            </th>
                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Item Desc">Item Desc</div>
                            </th>
                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Qty">Qty</div>
                            </th>
                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Min">Labour Mins</div>
                            </th>
                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Labour">Total labour</div>
                            </th>
                            <th scope="col" style="padding:4px">
                                <div class="slds-truncate" title="Total">Total Material</div>
                            </th>
                            <th scope="col">
                                <div class="slds-truncate" title="CashSettle">Cash Settlement</div>
                            </th>
                            <th></th>
                        </tr>
                    </thead>
                    <aura:iteration items="{!ri.roomwrappers}" var="roomItemWrapper" indexVar="index">
                        <tbody>
                            <!-- Table Row -->
                            <tr>
                                <td style="height: 40px;">
                                    <lightning:input value="{!roomItemWrapper.siteVisitNumber}" class="siteVisitClass" />
                                </td>

                                <td>
                                    <lightning:formattedText value="{!roomItemWrapper.roomName}" />
                                </td>
                                <td>
                                    <lightning:formattedText value="{!roomItemWrapper.itemDesc}" />
                                </td>
                                <td>
                                    <lightning:formattedNumber value="{!roomItemWrapper.quantity}" />
                                </td>
                                <td>
                                    <lightning:formattedNumber value="{!roomItemWrapper.labourTime}" />
                                </td>
                                <td>
                                    <lightning:formattedNumber value="{!roomItemWrapper.labourAmount}" />
                                </td>
                                <td>
                                    <lightning:formattedNumber value="{!roomItemWrapper.materialAmount}" />
                                </td>
                                <td>
                                    <lightning:input type="checkbox" checked="{!roomItemWrapper.cashSettlement}"
                                        aura:id="cashSettlementTrade" name="cashSettleCheckBox" />
                                </td>
                            </tr>
                        </tbody>
                    </aura:iteration>
                </table>
                <br /><br />
                <p class="slds-p-horizontal_small field-title">
                <div class="slds-grid">
                    <div class="slds-col slds-size_1-of-3">
                        <lightning:input label="Preferred Date Time" type="datetime" name="input1"
                            value="{!ri.PreferedDateTime}" />
                    </div>
                    <div class="slds-col slds-size_1-of-3.width:55px" style="margin-left: 11%;margin-top: 3%;">
                        <table border="0px" width="40%">
                            <tr>
                                <td align="left"> Total Cash Settlement incl GST: </td>
                                <td align="right">${!ri.cashtotalwithgst}</td>
                            </tr>
                            <tr>
                                <td align="left"> Total Estimate Cost incl GST: </td>
                                <td align="right">${!ri.totalwithgst}</td>
                            </tr>
                            
                            <aura:if isTrue="{!ri.showMinimuworkMinutes}">
                                <tr class="bckgrnd">
                                    <td align="left"> Minimum Labour Mins: </td>
                                    <td align="right">{!ri.typeminimumworkhour}</td>
                                </tr>
                            </aura:if>
                        </table>
                        <!--   Total Estimate Cost incl GST           :{!ri.totalwithgst} <br/>
                    Total Cash Settlement incl GST         : {!ri.cashtotalwithgst} -->
                    </div>
                </div>
                </p>

                <br />
            </aura:iteration>
            <br />
        </aura:if>

        <aura:if isTrue="{!v.showRadio}">
            <lightning:radioGroup name="radioButtonGroup" label="Preview Report Type" options="{!v.options }"
                value="{!v.value}" type="button" onchange="{!c.downloadDocument}" />
        </aura:if>


        <aura:if isTrue="{!v.totalAmountCond}">
            <div class="slds-clearfix slds-p-right_none field-title" style="align:right;margin-right:15%">
                <div class="slds-float_right">
                    <table border="0px" width="40%">
                        <tr>
                            <td colspan="2" align="left">
                                <h1>ESTIMATE SUMMARY</h1>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">----------------------------------------------------------------- </td>
                        </tr>
                        <tr>
                            <td align="left"> Total Estimated Cost : </td>
                            <td align="right">{!v.totalAmount}</td>
                        </tr>
                        <tr>
                            <td align="left"> Margin(14.5%) : </td>
                            <td align="right"> {!v.margin}</td>
                        </tr>
                        <tr>
                            <td colspan="2">----------------------------------------------------------------- </td>
                        </tr>
                        <tr>
                            <td align="left"> Subtotal : </td>
                            <td align="right">{!v.subTotal}</td>
                        </tr>
                        <tr>
                            <td align="left"> GST(10%) : </td>
                            <td align="right">{!v.gst}</td>
                        </tr>
                        <tr>
                            <td colspan="2">----------------------------------------------------------------- </td>
                        </tr>
                        <tr>
                            <td align="left"> Total with GST :</td>
                            <td align="right">${!v.gstTotal}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </aura:if>
        <br />

        <aura:if isTrue="{!v.saveData}">
        <lightning:button variant="brand" label="Preview Assessment" title="Preview Assessment"
            onclick="{!c.previewAssessment }" />
        <lightning:button variant="brand" label="Submit Assessment" title="Submit Assessment"
            onclick="{!c.submitAssessment}"/>
            <aura:set attribute="else">
              <lightning:button variant="brand" label="Preview Assessment" title="Preview Assessment"
            onclick="{!c.previewAssessment }" disabled="true"/>
              <lightning:button variant="brand" label="Submit Assessment" title="Submit Assessment"
            onclick="{!c.submitAssessment}" disabled="true"/>
            </aura:set>
        </aura:if>
        <!-- <lightning:button variant="brand" label="Upload Photos" title="Upload Photos" onclick="{!c.uploadPhotos}" />-->
    </div>



    <div class="slds-size--1-of-1">
        <footer class="slds-modal__footer">
            <lightning:button label="Save Draft" variant="brand" onclick="{!c.saveDraft}" />
        </footer>
    </div>



    <!-- Preview  - start -->
    <aura:if isTrue="{!v.isModalOpen}">
        <!-- Modal/Popup Box starts here-->
        <div class="demo-only" style="height: 100px;">
            <section role="dialog" tabindex="-1" aria-labelledby="modal-heading-01" aria-modal="true"
                aria-describedby="modal-content-id-1" class="slds-modal slds-fade-in-open">
                <div class="slds-modal__container">
                    <!-- Modal/Popup Box Header Starts here-->
                    <header class="slds-modal__header">
                        <lightning:buttonIcon iconName="utility:close" onclick="{! c.closeModel }"
                            alternativeText="close" variant="bare-inverse" class="slds-modal__close" />
                        <h2 id="modal-heading-01" class="slds-text-heading_medium slds-hyphenate">Preview Report</h2>
                    </header>

                    <!--Modal/Popup Box Body Starts here-->
                    <div class="slds-modal__content slds-p-around_medium" id="modal-content-id-1">
                        <lightning:radioGroup name="radioButtonGroup" aura:id="reportType" label="Preview Report Type"
                            options="{!v.rptoptions }" value="{!v.rptvalue}" type="button"
                            onchange="{!c.showPreview}" />
                        <aura:if isTrue="{!v.showbaRept}">
                            <div>
                                <c:ClaimConnect_PreviewAssessment recordId="{!v.caseId}" BARecordId="{!v.recordId}"
                                    roomNameList="{!v.roomNameList}" />
                            </div>
                        </aura:if>
                        <aura:if isTrue="{!v.showpricedRept}">
                            <c:ClaimConnect_PreviewPricedRpt recordId="{!v.caseId}" BARecordId="{!v.recordId}"
                                                             roomNameList="{!v.roomNameList}"
                                                             margin="{!v.margin}"
                                                             gst="{!v.gst}"
                                                             subTotal="{!v.subTotal}"
                                                             gstTotal="{!v.gstTotal}"
                                                             totalAmount="{!v.totalAmount}" />
                        </aura:if>
                    </div>
                </div>
            </section>
            <div class="slds-backdrop slds-backdrop_open"></div>
        </div>
    </aura:if>
    <!-- preview - end -->

    <aura:if isTrue="{!v.showProgress}">

        <div class="demo-only" style="height: 50px;">
            <section role="dialog" tabindex="-1" aria-labelledby="modal-heading-01" aria-modal="true"
                aria-describedby="modal-content-id-1" class="slds-modal slds-fade-in-open">
                <div class="slds-modal__container">
                    <div class="slds-modal__content slds-p-around_medium" id="modal-content-id-1">
                        <div class="slds-grid slds-grid_align-spread slds-p-bottom_x-small"
                            id="progress-bar-label-id-6">
                            <span>{!v.progressText}</span>
                            <span aria-hidden="true">
                                <strong>{!v.progress}% Complete</strong>
                            </span>
                        </div>
                        <div class="slds-progress-bar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="25"
                            aria-labelledby="progress-bar-label-id-6" role="progressbar">
                            <span class="slds-progress-bar__value" style="{!'width:'+v.progress+'%'}">
                                <span class="slds-assistive-text">{!'Progress: ' +v.progress+'%'}</span>
                            </span>
                        </div>
                    </div>
                </div>
            </section>
            <div class="slds-backdrop slds-backdrop_open"></div>
        </div>
    </aura:if>

</aura:component>