({
    // Start by Prasanth for Summary Details
    fetchRoomDetails: function (component, event, helper) {
        console.log('inside fetchRoomDetails', component.get("v.recordId"));
        component.set('v.showSpinner', true);
        // alert('te'+component.get("v.toggleValue"));
        var action = component.get("c.fetchSummaryRecords");
        action.setParams({
            'BaId': component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            //  alert('res'+JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                var ri = response.getReturnValue();;
                console.log('roomeresult', ri);
                var roomItemList = [];
                var dateMap = component.get('v.dateMap');
                console.log('dateMap1', dateMap);
                for (let i = 0; i < ri.length; i++) {
                    let gst = ri[i].totalAmount * (.1);
                    let cashtotalwithgst = ri[i].cashtotalAmount * (.1) + ri[i].cashtotalAmount;
                    let totalwithgst = ri[i].totalAmount + gst;
                    totalwithgst = totalwithgst.toFixed(2);
                    cashtotalwithgst = cashtotalwithgst.toFixed(2);
                    ri[i].totalwithgst = totalwithgst;
                    ri[i].cashtotalwithgst = cashtotalwithgst;
                    ri[i].showMinimuworkMinutes = (ri[i].typeminimumworkhour > ri[i].Nocashsettledlabourmin) && ri[i].Nocashsettledlabourmin > 0 ? true : false;
                    console.log('ri[i]', ri[i]);
                    
                    if (dateMap && dateMap.has(ri[i].roomwrappers[0].siteVisitNumber+'_'+ri[i].roomwrappers[0].workType)){
                    ri[i].PreferedDateTime=dateMap.get(ri[i].roomwrappers[0].siteVisitNumber+'_'+ri[i].roomwrappers[0].workType);
                      console.log('dateMap2', dateMap.get(ri[i].roomwrappers[0].siteVisitNumber+'_'+ri[i].roomwrappers[0].workType));  
                    }
                    
                }
                //console.log('amount',totalAmount);
                component.set("v.roomItemList", ri);
                // disable cash settlement if no room items
                if(ri == null || ri == 'undefined' || ri=='') 
                {
                    component.set("v.csDisable",true);
                }
                else
                {
                    component.set("v.csDisable",false);
                }
                
                component.set('v.showSpinner', false);
                console.log('roomeresult final=', ri);
            }
        })
        $A.enqueueAction(action);
    },


    fetchAmount: function (component, event, helper) {
        var action = component.get("c.fetchAmountDetails");
        action.setParams({
            'BaId': component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            // alert('res'+JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                var res = response.getReturnValue();
                component.set("v.totalAmount", res);
                if (res > 0) {
                    component.set("v.totalAmountCond", true);
                }
                let margin = res * (14.5 / 100);
                let subtotal = res + margin;
                let gst = subtotal * (.1);
                let totalwithgst = subtotal + gst;
                margin = margin.toFixed(2);
                subtotal = subtotal.toFixed(2);
                gst = gst.toFixed(2);
                totalwithgst = totalwithgst.toFixed(2);
                component.set("v.margin", margin);
                component.set("v.gst", gst);
                component.set("v.subTotal", subtotal);
                component.set("v.gstTotal", totalwithgst);
            }
        })
        $A.enqueueAction(action);
    },

    fetchRoomNameDetails: function (component, event, helper) {
        var action = component.get("c.fetchSummaryNameRecords");
        action.setParams({
            'BaId': component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            // alert('res'+JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                var roomNameList = [];
                var ri = response.getReturnValue();
                for (let i = 0; i < ri.length; i++) {
                    let gst = ri[i].totalAmount * (.1);
                    let cashtotalwithgst = ri[i].cashtotalAmount * (.1) + ri[i].cashtotalAmount;
                    let totalwithgst = ri[i].totalAmount + gst;
                    totalwithgst = totalwithgst.toFixed(2);
                    cashtotalwithgst = cashtotalwithgst.toFixed(2);
                    ri[i].totalwithgst = totalwithgst;
                    ri[i].cashtotalwithgst = cashtotalwithgst;
                    console.log('ri[i]', ri[i]);
                }
                component.set("v.roomNameList", ri);
                console.log('roomnameresult final=', ri);
            }
        })
        $A.enqueueAction(action);
    },
    // End by prasanth for Summary Details

    //Start by Vidhya for Cash Settlement 
    fetchBADetails: function (component, event, helper) {
        console.log('BARecordId', component.get("v.recordId"));
        var action = component.get("c.getBARecord");
        action.setParams({
            'BARecordId': component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                component.set("v.NewStagingBA", response.getReturnValue());

            }
        })
        $A.enqueueAction(action);

        var cashSettle = component.get("v.NewStagingBA.Cash_Settlement__c");
        this.cashSettlementDis(component, event, cashSettle);

        var tempAccom = component.get("v.NewStagingBA.Temporary_Accommodation_Required__c");
        this.tempAccommodationDis(component, event, tempAccom);
    },

    cashSettlementDis: function (component, event, cashSettle) {
        if (cashSettle == true) {
            component.set("v.cashSettleDetailDisable", false);
        }
        else {
            component.set("v.cashSettleDetailDisable", true);
            component.set("v.NewStagingBA.Cash_Settlement_Reason__c", "");
            component.set("v.NewStagingBA.Cash_Settlement_Comments__c", "");
        }

    },

    tempAccommodationDis: function (component, event, tempAccom) {
        if (tempAccom == true) {
            component.set("v.tempAccomReasonDisable", false);
        }
        else {
            component.set("v.tempAccomReasonDisable", true);
            component.set("v.NewStagingBA.Temporary_Accommodation_Reason__c", "");
        }
    },


    validateCashSettle: function (component, event, helper) {
        //var sel = component.find("cashSettlementBA");
        //var cashSettleBA = sel.get("v.checked");
        var cashSettleBA = component.get("v.NewStagingBA.Cash_Settlement__c");
        var records = [];
        var noofRoomsItems = 0;
        var noofCashSettle = 0;
        component.set("v.errorMessage", "");
        component.set("v.isValid", true);

        //var cashSettleDef = component.find("cashSettleDef").get("v.value");
        var cashSettleDef = component.get("v.NewStagingBA.Cash_Settlement_Reason__c");
        var ItemList = component.get("v.roomItemList");

        for (var j = 0; j < ItemList.length; j++) {
            records = ItemList[j].roomwrappers;
            for (var i = 0; i < records.length; i++) {
                noofRoomsItems = noofRoomsItems + 1;
                if (records[i].cashSettlement) {
                    noofCashSettle = noofCashSettle + 1;
                }
            }
        }
       
        if(noofRoomsItems > 0)
        {
        if (noofCashSettle == noofRoomsItems) {
            if (!cashSettleBA) {
                component.set("v.errorMessage", "Cash Settlement details not provided for full Cash settlement");
                component.set("v.isValid", false);
            }
            else {
                if (cashSettleDef !== "Full cash details") {
                    component.set("v.errorMessage", "Cash Settlement definition is not appropriate for full Cash settlement");
                    component.set("v.isValid", false);
                }
            }
        }

        if (noofCashSettle > 0 && noofCashSettle < noofRoomsItems) {
            if (!cashSettleBA) {
                component.set("v.errorMessage", "Cash Settlement details not provided for partial cash settlement");
                component.set("v.isValid", false);
            }
            else {
                if (cashSettleDef !== "Partial") {
                    component.set("v.errorMessage", "Cash Settlement definition is not appropriate for partial cash settlement");
                    component.set("v.isValid", false);
                }
            }
        }
        if (noofCashSettle <= 0) {
            if (cashSettleBA) {
                component.set("v.errorMessage", "Cash Settlement details  provided for no cash settlement");
                component.set("v.isValid", false);
            }
        }
        }
    },
    updateCashSettle: function (component, event, helper) {

        var action = component.get("c.updateCashSettleBADetails");
        action.setParams({
            'StagingBA': component.get("v.NewStagingBA")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Updated cash Settlement in BA record");
            }
        })
        $A.enqueueAction(action);
    },
    // End by Vidhya for Cash settlement   

    updateroomItems: function (cmp, event, helper) {
        cmp.set('v.showSpinner', true);
        console.log('updating roomitem');
        var siteVisitDateArr = [];
         var dateMap = new Map();
        var roomlist = cmp.get("v.roomItemList");
        for (let i = 0; i < roomlist.length; i++) {
            let myobj = {
                type_visit: roomlist[i].roomwrappers[0].siteVisitNumber + '_' + roomlist[i].tradeType,
                preferdate: roomlist[i].PreferedDateTime
            };
            siteVisitDateArr.push(myobj);
            dateMap.set(roomlist[i].roomwrappers[0].siteVisitNumber + '_' + roomlist[i].tradeType,roomlist[i].PreferedDateTime);
        }
        //console.log('siteVisitDateArr',siteVisitDateArr);
        var typeversionDateWrapper = { dateMap: siteVisitDateArr }; 
         console.log('typeversionDateWrapper',typeversionDateWrapper);
          console.log('dateMap',dateMap);
        cmp.set('v.dateMap',dateMap);
        
        var keyvaluemap = cmp.get("v.roomItemList");
        var records = [];
        for (let i = 0; i < keyvaluemap.length; i++) {
            let roomitem = keyvaluemap[i].roomwrappers;
            Array.prototype.push.apply(records, roomitem);
        }
        console.log('updating records', records);

        var upateaction = cmp.get('c.updateroomItem');
        var RoomWrapper = { roomItemList: records };
        var jsonstring = JSON.stringify(RoomWrapper);

        console.log('jsonstring', jsonstring)
        upateaction.setParams({ 'roomItemsjson': jsonstring });
        upateaction.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('updated successfully');
                cmp.set('v.showSpinner', false);
                this.fetchRoomDetails(cmp, event, helper);
                this.fetchRoomNameDetails(cmp, event, helper);
                cmp.set('v.saveData', true);
                
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "message": "Draft Assessment Record Saved"
                });
                resultsToast.fire();
            }
            else {
                console.log('respons', response.getReturnValue());
                console.log('error came', response.geterror());

            }
        })
        $A.enqueueAction(upateaction);

    },

    fetchAppointmentDetails: function (component, event, helper) {
        var action = component.get('c.getAssessmentServiceAppointmentDetails');
        action.setParams({
            'ClaimId': component.get("v.caseId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //alert(component.get("v.SAid"));
                component.set("v.SAid", response.getReturnValue());
            }
        })
        $A.enqueueAction(action);
    },
    
    submitAssessment: function (component, event, helper) {
        var siteVisitDateArr = [];
        var roomlist = component.get("v.roomItemList");
        for (let i = 0; i < roomlist.length; i++) {
            let myobj = {
                type_visit: roomlist[i].roomwrappers[0].siteVisitNumber + '_' + roomlist[i].tradeType,
                preferdate: roomlist[i].PreferedDateTime
            };
            siteVisitDateArr.push(myobj);
        }
        var typeversionDateWrapper = { typeversionDateMapList: siteVisitDateArr };
        var datejson = JSON.stringify(typeversionDateWrapper);
        console.log('typeversionDateWrapper', datejson);
        //component.set('v.showSpinner', true);
        component.set('v.showProgress', true);
        component.set("v.progress", "0");
        component.set("v.progressText", "Creating workorder and workItems");
        var action = component.get('c.submitCompletedAssessment');
        action.setParams({
            'ClaimId': component.get("v.caseId"),
            'ServApptId': component.get("v.SAid"),
            'BARecordId': component.get("v.recordId"),
            'datejson': datejson
        });
        console.log('HomerepairappointmentId' + component.get("v.SAid"));
        action.setCallback(this, function (response) {
            var toastEvent = $A.get("e.force:showToast");
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.progress", "100");
                component.set("v.progressText", "Completing Creating workorder and workItems");
                var res = response.getReturnValue();
                var callGenReportAndUploadPhotos = false;
                if (callGenReportAndUploadPhotos) {
                    helper.generateReport(component, event, helper);
                    console.log('Report will be generate offline line mode');
                }
                toastEvent.setParams({
                    title: 'Submitted',
                    message: 'Claim Assessment Submitted'
                });
                toastEvent.fire();
                
                var navigateEvent = $A.get("e.force:navigateToComponent");
                navigateEvent.setParams({
                    componentDef: "c:ClaimConnect_HomePage"
                });
                navigateEvent.fire();
                
                //component.set('v.showSpinner', false);               
            }
            else {
                console.log('error', response.getError());
                component.set("v.progress", "0");
                component.set("v.progressText", "Error Submitting assessment");
                component.set('v.showProgress', false);
                toastEvent.setParams({
                    title: 'Failure',
                    type: 'error',
                    message: 'Claim Assessment Submission Failed :'
                });
                toastEvent.fire();
                component.set('v.showProgress', false);
            }

        })
        $A.enqueueAction(action);
    },

    generateReport: function (component, event, helper) {

        /*component.set("v.progressText", "Generating report");
        var action = component.get('c.generateReports');
        action.setParams({
            'CurrentClaimId': component.get("v.caseId"),
            'ServApptId': component.get("v.SAid")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var toastEvent = $A.get("e.force:showToast");
            var message = '';
            if (state === "SUCCESS") {
                component.set("v.progress", "75");
                component.set("v.progressText", "Completed Generating Assessment reports");*/
                
                var cashSettleBA = component.get("v.NewStagingBA.Cash_Settlement__c");
        		var cashSettleDef = component.get("v.NewStagingBA.Cash_Settlement_Reason__c");
                if(cashSettleBA){
                    if(cashSettleDef == 'Partial' || cashSettleDef == 'Full cash details'){
                        helper.generateCashSettlementReports(component, event, cashSettleDef);
                    }
                }
                helper.uploadPhotos(component, event, helper);
           /* }
            else if (state === "ERROR") {
                component.set("v.progress", "0");
                component.set("v.progressText", "Error Submitting assessment");
                component.set('v.showProgress', false);

                toastEvent.setParams({
                    title: 'Failure',
                    type: 'error',
                    message: 'Claim Assessment Submission Failed'
                });
                toastEvent.fire();
                component.set('v.showProgress', false);
            }
        })
        $A.enqueueAction(action);*/
    },
    
    generateCashSettlementReports: function (component, event, cashSettlementDefinition) {

        component.set("v.progressText", "Generating Cash settlement report");
        var action = component.get('c.generateCashSettlementReports');
        action.setParams({
            'CurrentClaimId': component.get("v.caseId"),
            'ServApptId': component.get("v.SAid"),
            'cashSettlementDefn' : cashSettlementDefinition 
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var toastEvent = $A.get("e.force:showToast");
            var message = '';
            if (state === "SUCCESS") {
                component.set("v.progress", "80");
                component.set("v.progressText", "Completed Generating Cash Settlement reports");
            }
            else if (state === "ERROR") {
                component.set("v.progress", "0");
                component.set("v.progressText", "Error Submitting assessment");
                component.set('v.showProgress', false);

                toastEvent.setParams({
                    title: 'Failure',
                    type: 'error',
                    message: 'Claim Assessment Submission Failed'
                });
                toastEvent.fire();
                component.set('v.showProgress', false);
            }
        })
        $A.enqueueAction(action);
    },

    uploadPhotos: function (component, event, helper) {

        component.set("v.progressText", "Uploading photos to AWS");
        var toastEvent = $A.get("e.force:showToast");

        var action = component.get('c.UploadDocument');
        action.setParams({
            'ClaimId': component.get("v.caseId"),
            'BARecordId': component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {                
                var res = response.getReturnValue();
                var isSuccess = res.submitFlag;
                if(isSuccess)
                {
                    
                    component.set("v.progress", "100");
                    component.set("v.progressText", "Photos Uploaded");
                    toastEvent.setParams({
                        title: 'Submitted',
                        message: 'Claim Assessment Submitted'
                    });
                    toastEvent.fire();
                    
                    var navigateEvent = $A.get("e.force:navigateToComponent");
                    navigateEvent.setParams({
                        componentDef: "c:ClaimConnect_HomePage"
                    });
                    navigateEvent.fire();
                }
                else
                {
                    component.set("v.progress", "0");
                    component.set("v.progressText", "Error Submitting assessment");
                    component.set('v.showProgress', false);
                    var tmsg;                  
                    if (res.submitMsg =='' || res.submitMsg ==null)
                    { tmsg = 'Claim Assessment Submission Failed';}
                    else
                    { tmsg=res.submitMsg; }
                    
                    toastEvent.setParams({
                        title: 'Failure',
                        type: 'error',
                        message: tmsg
                    });
                    toastEvent.fire();                            
                }
            } else {
                component.set("v.progress", "0");
                component.set("v.progressText", "Error Submitting assessment");

                toastEvent.setParams({
                    title: 'Failure',
                    type: 'error',
                    message: 'Claim Assessment Submission Failed'
                });
                toastEvent.fire();
            }
            component.set('v.showProgress', false);
        })
        $A.enqueueAction(action);
    }
})