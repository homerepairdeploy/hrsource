({
	doInit : function(component, event, helper) {        
        helper.fetchRoomDetails(component, event, helper); 
        helper.fetchRoomNameDetails(component, event, helper);          
        helper.fetchBADetails(component,event,helper); //added by Vidhya for BA Cash Settlement	
        helper.fetchAmount(component,event,helper); 	
        helper.fetchAppointmentDetails(component,event,helper); //added by Anketha for submit assessment and generating report
		//helper.fetchCashSettlementAndCongaDetails(component,event,helper);
    },
    // Start by Vidhya - for Cash Settlement
    selectChange : function(component, event, helper) {
        var sel = component.find("cashSettlementBA");
	    var cashSettle =	sel.get("v.checked");      
        helper.cashSettlementDis (component, event, cashSettle);     
    },
     // End by Vidhya - for Cash Settlement
    
    keyCheck : function(component, event, helper) {
		helper.fetchRoomDetails(component, event, helper); 
	},
    
   //Start by Vidhya for cash Settlement
    handleCashSettlementChange: function (component, event, helper) {
          var sel = component.find("cashSettlementBA");
	      var cashSettle =	sel.get("v.checked");        
          helper.cashSettlementDis (component, event, cashSettle);  
        
        //set Cash settlement line items
           var toggleVal = component.get("v.toggleValue");
            if (toggleVal==true)
            {                
                var checkCmp =component.find("cashSettlementRoom");          
                const chk1 = (checkCmp.length == null) ? [checkCmp] : checkCmp;            
                for (var i = 0; i < chk1.length; i++) {    
                    chk1[i].set("v.checked",cashSettle);              
                }
            }
            else
            {
                var checkCmp =component.find("cashSettlementTrade");          
                const chk2 = (checkCmp.length == null) ? [checkCmp] : checkCmp;            
                for (var i = 0; i < chk2.length; i++) {    
                    chk2[i].set("v.checked",cashSettle);              
                }
            }
    },
    
    handleTempAccomChange: function (component, event, helper) {
          var sel = component.find("tempAccommodation");
          var tempAccom =	sel.get("v.checked");
          helper.tempAccommodationDis (component, event, tempAccom);         
    },
    //End by Vidhya for cash Settlement
    
    // Added By Prasanth for saving Items
    saveDraft :function (component, event, helper) {
        console.log('saveDraft',component.get("v.roomItemList"));
       helper.validateCashSettle (component,event,helper);
       
       var valid = component.get("v.isValid");
       var errMsg = component.get("v.errorMessage")
       if (valid)
       {
       helper.updateCashSettle(component, event, helper);
       helper.updateroomItems(component, event, helper);
       }
       else
       {
           component.find('notifLib').showNotice({
            "variant": "error",
            "header": "Error on Page!",
            "message": errMsg,
            closeCallback: function() {
                //alert('You closed the alert!');
            }
        });
    }
    },
    // End by Prasanth for saving Items
    submitAssessment: function(component, event, helper){        
        helper.submitAssessment(component, event, helper);
    },
    
    previewAssessment: function(component, event, helper){
        component.set("v.isModalOpen", true);
        component.set("v.showbaRept", true);
        component.find("reportType").set("v.value","BARept")
        //helper.submitAssessment(component, event, helper);
    },    
    
     closeModel: function(component, event, helper) {
         // Set isModalOpen attribute to false       
      component.set("v.isModalOpen", false);
      component.set("v.showbaRept", false);
      component.set("v.showpricedRept", false);
      
   },   
    
      uploadPhotos: function(component, event, helper){
        helper.uploadPhotos(component, event, helper);
    },
    
     showPreview: function(component, event, helper){
         var rtype = component.find("reportType").get("v.value") 
         if(rtype=='BARept')
         {
             component.set("v.showbaRept", true);
             component.set("v.showpricedRept", false);
         }
         else if (rtype=='PricedRpt')
         {
             component.set("v.showbaRept", false);
             component.set("v.showpricedRept", true);
         }
     },
})