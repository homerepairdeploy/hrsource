({
    getTasks : function(component,recordId) {
        var action = component.get("c.getActivities");
        action.setParams({ recordId : recordId });
       // component.set('v.actions',null);
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            console.log(response.getReturnValue().length);
                       
            if (state === "SUCCESS") {
                
                 //alert("From server: " + JSON.stringify(response.getReturnValue()));
                if(response.getReturnValue().length > 0)
                {
                    component.set('v.actions',response.getReturnValue()[0].awList);
                    // alert(JSON.stringify(component.get('v.actions')));
                    
                    component.set('v.taskRecords',response.getReturnValue());
                     $A.get('e.force:refreshView').fire();
                }
                else{
                    component.set('v.taskRecords',null);
                    component.set('v.actions',null);
                    //$A.get('e.force:refreshView').fire();
                    //alert(component.get('v.taskRecords'));
                    // alert("From server: " + JSON.stringify(response.getReturnValue()));
                    
                }
                component.set('v.spinner',false);
            }
            else if (state === "INCOMPLETE") {
                component.set('v.spinner',false);
                //$A.get('e.force:refreshView').fire();
                // do something
            }
                else if (state === "ERROR") {
                    component.set('v.spinner',false);
                    
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    doview:function(component, event,currentRecord ){
        var recId = currentRecord.Id;
        component.set('v.spinner',false);
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recId,
            "slideDevName": "detail"
        });
        navEvt.fire();
        //window.location.href = "/"+recId;   
    },
    doClose:function(component, event,currentRecord ){
        component.set('v.spinner',false);
        component.set("v.isModalOpen", true);
        var flow = component.find("flowData");
        var inputVariables = [
            { name : "recordId", type : "String", value: currentRecord.Id }, 
        ];
            // In that component, start your flow. Reference the flow's API Name.
            flow.startFlow($A.get("$Label.c.Task_Complete_Task_Flow_Name"),inputVariables);
            },
            doChangeStatus:function(component, event,currentRecord ){
            if(component.get('v.options') != null)
            {
            var action = component.get("c.getTaskPickLListValues");
            action.setCallback(this, function(response) {
            //alert(JSON.stringify(response.getReturnValue()));
            
            var state = response.getState();
            if (state === "SUCCESS") {
            component.set('v.options',response.getReturnValue());
            component.set('v.spinner',false);
            component.set('v.task',currentRecord);
            component.set('v.changeStatusModal',true);
            }
            else if (state === "INCOMPLETE") {
            component.set('v.spinner',false);
            
            alert(JSON.stringify(response.getReturnValue()));
            }
            else if (state === "ERROR") {
            component.set('v.spinner',false);
            
            var errors = response.getError();
            if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + 
                            errors[0].message);
            }
    } else {
    console.log("Unknown error");
}
 }
 });
$A.enqueueAction(action);
}
},
    submitStatusHelper:function(component, event,selectedStatus,task ){
        var action = component.get("c.updateTask");
        action.setParams({ tsk : task,
                          Status : selectedStatus});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.spinner',false);
                
                component.set('v.changeStatusModal',false);
                this.showSuccess(component, event, response.getReturnValue());
                //location.reload();
                $A.get('e.force:refreshView').fire();
                
            }
            else if (state === "INCOMPLETE") {
                // component.set('v.changeStatusModal',false);
                this.showError(component, event, response.getReturnValue());
                component.set('v.spinner',false);
                
                // alert(JSON.stringify(response.getReturnValue()));
            }
                else if (state === "ERROR") {
                    component.set('v.spinner',false);
                    
                    //  component.set('v.changeStatusModal',false);
                    this.showError(component, event, response.getReturnValue());                    //  alert(JSON.stringify(response.getReturnValue()));
                    
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
        showSuccess : function(component, event, message) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Success',
                message: message,
                duration:' 3000',
                key: 'info_alt',
                type: 'success',
                mode: 'pester'
            });
            toastEvent.fire();
        },
            showError : function(component, event, message) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message:message,
                    duration:' 3000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            },
                
})