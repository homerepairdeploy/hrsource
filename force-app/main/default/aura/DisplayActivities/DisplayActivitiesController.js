({
    doInit : function(component, event, helper) {
        //var recordId = component.get('v.caseId');
        var recordId = component.get('v.recordId');
        helper.getTasks(component,recordId);
        
    },
	
	handleRefresh : function(component, event, helper) {
        // $A.get('e.force:refreshView').fire();
      
       var recordId = component.get('v.recordId');
       helper.getTasks(component,recordId);
        
          
	          
    },
    handleSectionToggle: function (cmp, event) {
        
        var openSections = event.getParam('openSections');
        if (openSections.length === 0) {
            cmp.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            cmp.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    },
    handleSelect:function(component,event,helper){
        
        component.set('v.spinner',true);
        var currentRecord = event.detail.menuItem.get("v.value");
        var menuValue = event.detail.menuItem.get("v.accesskey");
        /*component.set('v.spinner',false);
        var res = menuValue.split(":");
        var actionName = 'Task.'+res[1];
        var actionType = res[0];
        var actionAPI = component.find("quickActionAPI");
        var recordId = component.get('v.recordId');
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": currentRecord.Id,
            "slideDevName": "detail"
        });
        navEvt.fire();
        var actionAPI = component.find("quickActionAPI");
        actionAPI.getAvailableActions().then(function(result){
            console.log('in')
            console.log('available actions');
            console.log(JSON.stringify(result));
        }).catch(function(e){
            console.log('error');
            console.log(e);
        });
        
        var args = {actionName :actionName, entityName: "Task"};
        actionAPI.getAvailableActionFields(args).then(function(result){
            console.log('Available Fields are ', JSON.stringify(result));
        }).catch(function(e){
            if(e.errors){
                console.log('Action Field Log Errors are ',e.errors);
                console.error('Full error is ', JSON.stringify(e));
            }
        });*/
        
        switch(menuValue) {
            case "1": helper.doview(component, event,currentRecord ); 
                break;
            case "2": helper.doClose(component, event, currentRecord); 
                break; 
            case "3": helper.doChangeStatus(component, event, currentRecord); 
                break; 
        }  
        
    },  
    closeModel: function(component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModalOpen", false);
        component.set('v.changeStatusModal',false);
        
    },
    submitStatus :  function(component, event, helper) {
        component.set('v.spinner',true);
        
        var selectedStatus = component.get('v.selectedStatus');
        var task = component.get('v.task');
        if(selectedStatus != null && selectedStatus != '')
        {
            helper.submitStatusHelper(component,event,selectedStatus,task);
        }else{
            var message= 'Please Fill All the required details'
            helper.showError(component, event, message);
            component.set('v.spinner',false);
            
        }
    },handleStatusChange:  function(component, event, helper) {
        
        if(event.getParam("status") === "FINISHED") {
            component.set("v.isModalOpen", false);
            $A.get('e.force:refreshView').fire();
            
        }
    }
    
})