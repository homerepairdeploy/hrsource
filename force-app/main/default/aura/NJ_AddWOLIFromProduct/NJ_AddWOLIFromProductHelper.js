({
  initWoli : function(component) {
    let that = this;
    that.showLoader(component);
    let params = {  "woId" : component.get("v.recordId") };
    that.callApexFunction(component, 'c.initAddWOLIFromProduct', params)
      .then($A.getCallback(function(result){
        that.hideLoader(component);
        let workOrder = result.woInfo;
        component.set("v.wInfo", result);
        component.set("v.currWo", result.woInfo);
        component.set("v.selwtName", result.woInfo.WorkType.Name);
        component.set("v.lvl1", result.lvl1);
        component.set("v.lvl2", result.lvl2);
        component.set("v.lvl3", result.lvl3);
        component.set("v.lvl4", result.lvl4);
        component.set("v.lvl5", result.lvl5); 
        component.set("v.selectedPricebookEntryLookUpRecord", result.pbe);
        that.setProductName(component, result.pbe);
        if(workOrder.Case.Status == 'Assessment Scheduled' && workOrder.Case.Assessment_Count__c !=null && 
          workOrder.Case.Assessment_Count__c > 1) {
          component.set('v.isValidWorkOrder',false);
          that.showToast("error", "Error", 'Error: Work order line cannot be added as assessment is in progress', 20000 );
        } else {
            component.set('v.isValidWorkOrder',true);
            if(workOrder.Status == 'Cancelled' || workOrder.Status == 'Closed' || workOrder.Authority__r.Status == 'Completed' || workOrder.Authority__r.Status == 'cancelled') {
              component.set('v.isValidWorkOrder',false);
              that.showToast("error", "Error", 'Error: Work order line cannot be added to a work order of this status', 20000 );
            } else{
              component.set('v.isValidWorkOrder',true);
            }
        }
        let validWO = component.get("v.isValidWorkOrder");
        if(validWO === true) {
          that.adjustTimevalues(component);
        }
      }))
      .catch($A.getCallback(function(error){
        that.hideLoader(component);
        that.showToast("error", "Error",error.message, 20000 );
      })); 
  },
  adjustTimevalues : function(component) {
      console.log('adjustTimevalues >>> ');
      let pbe = component.get("v.selectedPricebookEntryLookUpRecord"); 
      if(pbe != undefined){
        console.log('Labour Time >>> '+pbe.Labour_TIME_mins__c);
        var labourTime=Math.floor(pbe.Labour_TIME_mins__c);
        var quantity=1;
        component.find("quantity").set("v.value",quantity);
        var HrMin = (Number(labourTime)/60).toFixed(2);
        var hr= parseInt(HrMin);
        var min=labourTime-(hr*60);
        component.find("labourHr").set("v.value",hr);
        component.find("labourMin").set("v.value",min);
        
        var labourAmount = pbe.Labour_Price__c;
        var labourMin=labourTime;
          
        if(isNaN(((labourAmount*labourMin)/60)*quantity)){
            component.find("labourAmount").set("v.value",0.0);
            component.find("totalAmount").set("v.value",0.0);
        }else{
            component.find("labourAmount").set("v.value",((labourAmount*labourMin)/60)*quantity);
            var totalAmount=((labourAmount*labourMin)/60)*quantity + (pbe.Material_Price__c*quantity);
            component.find("totalAmount").set("v.value",totalAmount);
        }
        var materialPrice = pbe.Material_Price__c;
        if(isNaN(materialPrice)){
            component.find("materialAmount").set("v.value",0.0);
        }
          
      }
    },
  populateDropdown : function(component, attrName, selField, whrFields, whrFieldVals) {
    let that = this;
    that.showLoader(component);
    let params = {  "selField" : selField, "whrFields" : whrFields, "whrFieldValues" : whrFieldVals};
    that.callApexFunction(component, 'c.getProductDropdown', params)
      .then($A.getCallback(function(result){
        that.hideLoader(component);
        component.set(attrName, result);
      }))
      .catch($A.getCallback(function(error){
        that.hideLoader(component);
        that.showToast("error", "Error",error.message, 20000 );
      })); 
  },
  getPriceBookEntry : function(component) {
    let that = this;
    that.showLoader(component); 
    let currWo = component.get("v.currWo");
    let params = { "woTypeName" : component.get("v.selwtName"), 
                   "state" : currWo.Case.State1__c,
                   "lvl1" : component.get("v.sellvl1"), 
                   "lvl2" : component.get("v.sellvl2"),
                   "lvl3" : component.get("v.sellvl3"),
                   "lvl4" : component.get("v.sellvl4"),
                   "lvl5" : component.get("v.sellvl5")};
    that.callApexFunction(component, 'c.getPriceBookEntry', params)
      .then($A.getCallback(function(result){
        that.hideLoader(component);
        console.log(result);
        component.set("v.selectedPricebookEntryLookUpRecord", result);
        
        that.setProductName(component, result);
          
        that.adjustTimevalues(component);
      }))
      .catch($A.getCallback(function(error){
        that.hideLoader(component);
        that.showToast("error", "Error",error.message, 20000 );
      })); 
  },
  setProductName : function(component, pbe) {
    if(pbe && pbe.Product2.Name != null) {
      component.set("v.selProd", pbe.Product2.Name);
    } else {
      component.set("v.selProd", ""); 
    }
  },
  saveWoliRecord : function(component, event, helper) {
    var that = this;
    var woliObj = component.get("v.objWorkOrderLineItem");
    let pbe = component.get("v.selectedPricebookEntryLookUpRecord");
    let room = component.get("v.selectedRoomLookUpRecord");
    var type;
    if(pbe != undefined){ 
      if(component.get("v.selectedRoomLookUpRecord") != undefined){
        woliObj.Room__c = room.Id;                
        var labourAmount = component.find("labourAmount").get("v.value");
        var materialAmount = component.find("materialAmount").get("v.value");
        var labourHr;
        var quantity = component.find("quantity").get("v.value");
        woliObj.PricebookEntryId = component.get("v.selectedPricebookEntryLookUpRecord").Id;
      //  woliObj.Service_Appointment__c = component.get("v.selectedServiceApptLookUpRecord").Id;
        var hr = component.find("labourHr").get("v.value");
        var min = component.find("labourMin").get("v.value");            
        labourHr=parseInt((hr*60))+parseInt(min);
        woliObj.WorkOrderId = component.get("v.recordId");
        woliObj.Labour_Time__c = labourHr;
        woliObj.Quantity = quantity;
        woliObj.Labour_Rate__c=component.get("v.selectedPricebookEntryLookUpRecord").Labour_Price__c;
        woliObj.Material_Rate__c=component.get("v.selectedPricebookEntryLookUpRecord").Material_Price__c;
      } else{
        type = "error";  
        that.showToast("error", "Error", 'Room cannot be empty. Please select Room', 20000 );
      }
    }else{
      var message;
      if(component.get("v.selectedPricebookEntryLookUpRecord") == undefined){
          message = "Product cannot be empty. Please select Product"; 
      }
      type = "error";   
      that.showToast("error", "Error", message, 20000 );          
    }
    if (type != 'error'){
      let params = { "woli" : woliObj };
      that.showLoader(component);
      that.callApexFunction(component, 'c.createWorkOrderLineItem', params)
          .then($A.getCallback(function(result){
            that.hideLoader(component);
            var woLineItemNumber = result;
            console.log('woNumber ' + woLineItemNumber);
            var message = "WorkOrderLineItem" + ' ' + woLineItemNumber + ' ' + "created successfuly";
            var type = "success";             
            that.showToast("success", "Success",message, 20000 );
            $A.get('e.force:refreshView').fire();          
          })).catch($A.getCallback(function(error){
            that.hideLoader(component);
            that.showToast("error", "Error",error.message, 20000 );
          }));   
    }
  }
})