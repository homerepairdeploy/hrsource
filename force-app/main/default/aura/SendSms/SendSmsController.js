({
    doInit : function(component, event, helper) {
       var spinner = component.find("mySpinner");
		$A.util.toggleClass(spinner, "slds-hide");
        
    	var action = component.get("c.getClaimDetails");        
        action.setParams({            
            'caseId' : component.get('v.recordId')            
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.claim', response.getReturnValue()); 
                var thisClaim = response.getReturnValue();                
                if(!$A.util.isEmpty(thisClaim.ContactMobile)) {
                    component.set('v.phoneNo', thisClaim.ContactMobile);
                }
                else 
                    component.set('v.phoneNo', thisClaim.Primary_Contact_Number__c);
            }            
            $A.util.toggleClass(spinner, "slds-hide");
        });        
        $A.enqueueAction(action);
    },
	sendSMS : function(component, event, helper) {  
        var spinner = component.find("mySpinner");
		$A.util.toggleClass(spinner, "slds-hide");
        var ContactId = component.get('v.claim').ContactId;
    	var action = component.get("c.sendSMSToClaim");        
        action.setParams({            
            'phoneNumber' : component.get('v.phoneNo'),
            'smsBody' : component.get('v.messageBody'),
            'ContactId' : ContactId,
            'caseId' : component.get('v.recordId')
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var callResponse = JSON.parse(response.getReturnValue());
                
                if(callResponse.isSuccess) {                    
					helper.showToast('success', callResponse.message);    
                    $A.get('e.force:refreshView').fire();
                }
                else {
                    helper.showToast('error', callResponse.message);                    
                }
            }            
            $A.util.toggleClass(spinner, "slds-hide");
        });        
        $A.enqueueAction(action);
	},
    validateComponent : function(component, event, helper) {
        var inputComp = component.find("inputElement");
        component.set('v.isValidPhone', inputComp.get("v.validity").valid);
    }
})