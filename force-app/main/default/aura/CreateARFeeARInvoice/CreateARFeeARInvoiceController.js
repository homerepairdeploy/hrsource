({
	doInit : function(component, event, helper) {
        console.log('initiating');
        var buttonValue=component.get('v.buttonValue');
		var action = component.get("c.createInvoiceRecord");
        var spinner = component.find("mySpinner");
		$A.util.toggleClass(spinner, "slds-hide");
        action.setParams({            
            'recordId' : component.get('v.recordId'),
            'buttonValue' : component.get('v.buttonValue')
        });

         action.setCallback(this, function(response) { 
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
			dismissActionPanel.fire();
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {                
                if(response.getReturnValue().includes('Success')) {                 	
                    helper.showToast('Success', 'Update is successful');
                    $A.get('e.force:refreshView').fire();

				 }
                else {
                    helper.showToast('error', response.getReturnValue());
                }
            } 
            $A.util.toggleClass(spinner, "slds-hide");

        });        
        $A.enqueueAction(action);
	}
  
})