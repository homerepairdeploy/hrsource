({
    hideWindow : function(component, event, helper) {        
        helper.hideModal(component, "modal");
    },
    initiateDocuments: function(component, event, helper){
        helper.getDocuments(component);
    },
    clickAdd : function(component, event, helper) {
        component.set("v.isProcess",true);
        var valueMap = {};
        component.find("compliance").set("v.valueMap",valueMap);
        component.set("v.buttonTradeLabel","Add");
        var options = [];
        console.log(component.get('v.dataWorkType').length);
        if(component.get('v.dataWorkType').length == 0){
            var action = component.get("c.getselectOptions") ;
            helper.execute(action).then(function(response) {                 
                var allValues = response.getReturnValue();
                component.set('v.dataWorkType',allValues)
                console.log('Select rows ',allValues[0]);
                if (allValues != undefined && allValues.length > 0) {
                    for (var i = 0; i < allValues.length; i++) { 
                        options.push({
                            class: "optionClass",
                            label: allValues[i],
                            value: allValues[i]
                        });
                    }
                    var card = component.find("compliance").find("card");
                    var sections = card.find("sections");
                    sections.find("fieldInput").forEach(function(fieldInput){
                        if(fieldInput.get("v.apiName") === "Trade_Type__c.Trade_Type__c"){
                            fieldInput.set("v.options", options);
                        }
                    });                    
                    component.find("compliance").find("card").refresh();
                    helper.showModal(component, "modal");
                    component.set("v.isProcess",false);
                }
            });
        }else{        
            component.set("v.isProcess",false);
            var allValues = component.get('v.dataWorkType');
            console.log('Select rows ',allValues[0]);
            if (allValues != undefined && allValues.length > 0) {
                for (var i = 0; i < allValues.length; i++) { 
                    options.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                var card = component.find("compliance").find("card");
                var sections = card.find("sections");
                sections.find("fieldInput").forEach(function(fieldInput){
                    if(fieldInput.get("v.apiName") === "Trade_Type__c.Trade_Type__c"){
                        fieldInput.set("v.options", options);
                        fieldInput.set("v.editable",false);
                    }
                });
                component.find("compliance").find("card").refresh();
                helper.showModal(component, "modal");
            }
        }
    },
    handleRowAction: function (component, event, helper) {
        component.set("v.isProcess",true);
        var rows = component.get('v.data');
        var actionName = event.getSource().get("v.name");
        var rowIndex = event.getSource().get("v.tabindex");
        var row = rows[rowIndex];
        var compliance = component.find("compliance");
        
        switch (actionName) {
            case 'show_details':
                console.log('Edit :' ,JSON.stringify(rows[rowIndex]));
                
                console.log('Trade Id :' + rows[rowIndex].Id);
                if (rows[rowIndex].Id == 'undefined'||rows[rowIndex].Id == ''){
                    component.set("v.buttonTradeLabel","Add");
                }else{
                    component.set("v.buttonTradeLabel","Edit");
                }
                compliance.set("v.valueMap", helper.convertToValueMap(row));
                compliance.set("v.rowIndex", rowIndex);
                
                var options = [];
                console.log(component.get('v.dataWorkType').length);
                if(component.get('v.dataWorkType').length == 0){
                    var action = component.get("c.getselectOptions") ;
                    helper.execute(action).then(function(response) {                 
                        var allValues = response.getReturnValue();
                        component.set('v.dataWorkType',allValues)
                        console.log('Select rows ',allValues[0]);
                        if (allValues != undefined && allValues.length > 0) {
                            for (var i = 0; i < allValues.length; i++) { 
                                options.push({
                                    class: "optionClass",
                                    label: allValues[i],
                                    value: allValues[i]
                                });
                            }
                            var card = component.find("compliance").find("card");
                            var sections = card.find("sections");
                            sections.find("fieldInput").forEach(function(fieldInput){
                                if(fieldInput.get("v.apiName") === "Trade_Type__c.Trade_Type__c"){
                                    fieldInput.set("v.options", options);
                                    console.log('Select option');
                                    fieldInput.set("v.editable",true);
                                    console.log(fieldInput.get("v.editable"));
                                }
                            });                    
                            component.find("compliance").find("card").refresh();
                            helper.showModal(component, "modal");
                            component.set("v.isProcess",false);
                        }
                    });
                }else{        
                    component.set("v.isProcess",false);
                    var allValues = component.get('v.dataWorkType');
                    console.log('Select rows ',allValues[0]);
                    if (allValues != undefined && allValues.length > 0) {
                        for (var i = 0; i < allValues.length; i++) { 
                            options.push({
                                class: "optionClass",
                                label: allValues[i],
                                value: allValues[i]
                            });
                        }
                        var card = component.find("compliance").find("card");
                        var sections = card.find("sections");
                        sections.find("fieldInput").forEach(function(fieldInput){
                            if(fieldInput.get("v.apiName") === "Trade_Type__c.Trade_Type__c"){
                                fieldInput.set("v.options", options);
                                fieldInput.set("v.editable",true);
                            }
                        });
                        component.find("compliance").find("card").refresh();
                        helper.showModal(component, "modal");
                    }
                }
                break;
            case 'delete':
                console.log('rowIndex :',rowIndex);
                component.set('v.deleteRow',rowIndex);
                $A.createComponent("c:OverlayLibraryModal", {},
                                   function(content, status) {
                                       if (status === "SUCCESS") {
                                           var modalBody = content;
                                           component.find('overlayLib').showCustomModal({
                                               header: "Are you sure you want to delete this item?",
                                               body: modalBody, 
                                               showCloseButton: false,
                                               closeCallback: function(ovl) {
                                                   console.log('Overlay is closing');
                                               }
                                           }).then(function(overlay){
                                               console.log("Overlay is made");
                                           });
                                       }
                                   });
                
                //rows.splice(rowIndex, 1);
                //component.set('v.data', rows);
                break;
            case 'show_file_composer':
                break;
        }
    },
    handleApplicationEvent : function(component, event) {
        component.set("v.isProcess",true);
        var rowIndex = component.get('v.deleteRow');
        var message = event.getParam("message");
        if(message == 'Ok'){
        	// if the user clicked the OK button do your further Action here
        	var rows = component.get('v.data');
            console.log('rows',rows[rowIndex].Id);
            console.log('rows',(rows[rowIndex].Id != undefined));
             var rowIndex = component.get('v.deleteRow');
            console.log('rowIndex :',rowIndex);
        	var row = rows[rowIndex];
            if(rows[rowIndex].Id != undefined){
                var tradeId = rows[rowIndex].Id;
                console.log('formFeed :',tradeId);
                var action = component.get("c.deleteTradeCompliance");
        		action.setParams({"tradeId": tradeId});
        		action.setCallback(this, function(response) {
                    console.log(JSON.stringify(response));
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        rows.splice(rowIndex, 1);
                		component.set('v.data', rows);
                        component.set("v.isProcess",false);
                    }else if (state === "ERROR") {
                        component.set("v.isProcess",false);
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                         errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
                });
                $A.enqueueAction(action);
            }else{
                rows.splice(rowIndex, 1);
                component.set('v.data', rows);
                component.set("v.isProcess",false);
            }
            //rows.splice(rowIndex, 1);
            //component.set('v.data', rows);
            component.set('v.deleteRow',null);
        }
       else if(message == 'Cancel'){
        // if the user clicked the Cancel button do your further Action here for canceling
      }
    },
    saveData : function(component, event, helper) {
        helper.saveAll(component);
    },
    addCompliance: function(component, event, helper) {
        component.set("v.isProcess",true);
        var compliance = component.find("compliance");
        var valueMap = compliance.get("v.valueMap");
        var index = compliance.get("v.rowIndex");
        var parentId = component.get("v.parentId");
        var formFeed = component.find("compliance").find("card").convertToRecord(valueMap);
        console.log(' formFeed ',JSON.stringify(formFeed));
        
        var cacheData= compliance.get("v.cacheData");
        console.log(' cacheData ',JSON.stringify(cacheData));
        
        var data = component.get("v.data") || [];
        var isValid=true;

        if((formFeed.Trade_Type__c=='' || formFeed.Trade_Type__c == undefined) && 
           (formFeed.Work_Type_Other__c==''||formFeed.Work_Type_Other__c== undefined)){
            isValid=false;
            alert('Please add trade type');
        }else{
            for(var i = 0; i < data.length; i++) {
                console.log(data[i].Trade_Type__c+'-----'+formFeed.Id);
                console.log(data[i].Trade_Type__c+'-----'+formFeed.Trade_Type__c);
                if(data[i].Trade_Type__c == formFeed.Trade_Type__c && formFeed.Id == undefined){   
                    alert('This trade type is already selected');
                    isValid=false;
                    break;
                }
            }
        }
        
       if(isValid && formFeed.License_Expiry__c != undefined){
            var today = new Date();
            var licenseExpiryDate = new Date(formFeed.License_Expiry__c);
            if(licenseExpiryDate < today){
                alert('Expiry date must be in future');
                isValid=false;
            }
        }
        
        console.log(isValid);
        if(isValid){
            if(parentId){
                console.log('Update Data');
                helper.saveData(component, formFeed).then(function(response) {
                    var returnValue = response.getReturnValue();
                    console.log(' returnValue ',returnValue);
                    var row = JSON.parse(returnValue)[0];
                    helper.updateDataRows(component, row);                    
                });
            } else {
                helper.updateDataRows(component, formFeed);
            }
            var tradeTypeTable = component.find("tradeTypeTable");
            tradeTypeTable.populateNames();
            helper.hideModal(component, "modal");
            component.set("v.isProcess",false);
        }else{
            component.set("v.isProcess",false);
        }
        
        
    }
})