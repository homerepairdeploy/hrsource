({
	populateTradeTypeNames : function(component) {
		let action = component.get("c.getWorkTypeNames");
		let tradeTypes = component.get("v.tradeTypes");
		let wtIds = [];
		for(let cnt = 0; cnt < tradeTypes.length ; cnt++) {
			wtIds.push(tradeTypes[cnt].Work_Type__c);
		}
        // set param to method  
        action.setParams({
            'wtIds': wtIds
        });            
    
    // set a callBack    
    action.setCallback(this, function(response) {
        var state = response.getState();
        console.log('state',state);
        if (state === "SUCCESS") {
        	let wtNames = response.getReturnValue();
            console.log('wtNames ',wtNames);
        	let modifiedTradeTypes = [];
            for(let cnt = 0; cnt < tradeTypes.length ; cnt++) {
                for(let cnt1 = 0; cnt1 < wtNames.length ; cnt1++) {
                    if(wtNames[cnt1].Id === tradeTypes[cnt].Work_Type__c) {
            		  let tt = tradeTypes[cnt];
            		  tt.Work_Type_Name__c = wtNames[cnt1].Name;
                      console.log('modifiedTradeTypes tt',tt);
                      //tt.Work_Type__r.Name = wtNames[cnt1].Name;
            		  modifiedTradeTypes.push(tt);
                    }
            	}
            }
            console.log('modifiedTradeTypes ',JSON.stringify(modifiedTradeTypes));
        	component.set("v.tradeTypes", modifiedTradeTypes);
        }
    });
    $A.enqueueAction(action);
	}
})