({
	isValidName : function(roomNme) {
    let isValid = true;
    let regEx = new RegExp("[A-Za-z0-9]+$");
    for (let i = 0; i < roomNme.length; i++) {
      let strChar = roomNme.charAt(i).toLowerCase();
      let result = regEx.test(strChar); 
      if(result === false) {
        if(!(strChar === "!" || strChar === "-" || strChar === "_" || strChar === "*" || strChar === "("
           || strChar === ")" || strChar === "'")) {
          isValid = false;
          break;
        }
      }
    }
    return isValid;
	}
})