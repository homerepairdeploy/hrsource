({
    /*call apex controller method "fetchContentDocument" to get salesforce file records*/
    doInit : function(component, event, helper) {   
        helper.getContentFiles(component, event, helper);
        helper.getAWSFiles(component, event, helper);               
    },
    getSelected : function(component,event,helper){
        // display modal and set seletedDocumentId attribute with selected record Id           
        component.set("v.hasModalOpen" , true);
        component.set("v.selectedDocumentId" , event.currentTarget.getAttribute("data-Id"));         
    },
    
    closeModel: function(component, event, helper) {
        // for Close Model, set the "hasModalOpen" attribute to "FALSE"  
        component.set("v.hasModalOpen", false);        
        component.set("v.selectedDocumentId" , null);  
    },    
    
})