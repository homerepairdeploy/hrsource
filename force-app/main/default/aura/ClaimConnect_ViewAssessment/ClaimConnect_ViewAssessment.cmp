<!--
/********************************************************************************************** 
Name        : ClaimConnect_ViewAssessment
Description : This component is used in ClaimConnect Application to display details of submitted
              Assessment. 

Purpose     : This component is invoked when claim is selected from 'My Submitted Assessments' list
              in ClaimConnect application home page. 
              Also this component is used in 'Previous Assessments' tab when creating/updating 
              draft Assessments for 'Reassessment' work type
                                                            
VERSION        AUTHOR                     DATE           DETAIL      DESCRIPTION   
1.0            Vidhya Sivaperuman         20-05-2020     Created     ClaimConnect APP
/********************************************************************************************** 
-->

<aura:component implements="force:appHostable,flexipage:availableForAllPageTypes,flexipage:availableForRecordHome,force:hasRecordId,forceCommunity:availableForAllPageTypes,force:lightningQuickAction"
                access="global"
                controller="ClaimConnect_LightningFileUploadHandler">
    
     <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    
    <!--aura attributes-->  
    <aura:attribute name="recordId" type="Id"/>
    <aura:attribute name="selectedDocumentId" type="string"/>    
    <aura:attribute name="lstContentDoc" type="List"/>    
    <aura:attribute name="lstAWSDoc" type="List"/>    
    <aura:attribute name="hasModalOpen" type="boolean" default="false"/>    
    <aura:attribute name="domain" type="string"/>
    <aura:attribute name="IdList" type="List"/>   
    <aura:attribute name="showSpinner" type="Boolean" default="false" />
    <aura:attribute name="ErrLoadingAWSFiles" type="Boolean" default="false" />
    
    <!--aura doInit handler-->    
    <aura:handler name="init" value="{!this}" action="{!c.doInit}"/>           
    
    <!-- Custom DataTable to Display List Of Available ContentDocuments Start-->  
    <lightning:card> 
        <aura:set attribute = "title" >
            <b> Assessment Reports </b>
        </aura:set>
        
         <aura:if isTrue="{!v.showSpinner}">
        <lightning:spinner alternativeText="Loading" size="large" />
        </aura:if>
        
    <table class="slds-table slds-table_cell-buffer slds-table_fixed-layout slds-table_bordered">
        <thead>
            <tr class="slds-line-height_reset">
                <th class="slds-text-title_caps" width="60%" scope="col">
                    <div class="slds-truncate" title="Title">Title</div>
                </th>
                <th class="slds-text-title_caps" width="20%" scope="col">
                    <div class="slds-truncate" title="File Type">File Type</div>
                </th>
                <th class="slds-text-title_caps" width="20%" scope="col">
                    <div class="slds-truncate" title="Created By">Created By</div>
                </th>
                <th class="slds-text-title_caps" width="20%" scope="col">
                    <div class="slds-truncate" title="size">size(bytes)</div>
                </th>
            </tr>
        </thead>
        <tbody>
            <aura:iteration items="{!v.lstContentDoc}" var="CD">
                <tr>
                    <th scope="row">
                        <div class="slds-truncate" title="{!CD.Title}">
                            <!--store contentDocument Id in data-Id attribute-->
                            <a onclick="{!c.getSelected}" data-Id="{!CD.Id}">{!CD.Title}</a>                            
                        </div>
                    </th>
                    <th scope="row">
                        <div class="slds-truncate" title="{!CD.FileType}">{!CD.FileType}</div>
                    </th>
                    <th scope="row">
                        <div class="slds-truncate" title="{!CD.CreatedBy.Name}">{!CD.CreatedBy.Name}</div>
                    </th>
                    <th scope="row">
                        <div class="slds-truncate" title="{!CD.ContentSize}">{!CD.ContentSize}</div>
                    </th>
                </tr>  
            </aura:iteration>
        </tbody>
    </table>
    </lightning:card>
    <br/>
    <br/>
    
    <lightning:card>        
        <aura:set attribute = "title" >
           <b> AWS files</b>
        </aura:set>
        <aura:if isTrue="{!v.ErrLoadingAWSFiles}">
            <div class="errcls">
                <b>Error Loading AWS Files</b>
            </div>
        </aura:if>
        	
        <aura:iteration items="{!v.lstAWSDoc}" var="AWSdoc">
            <lightning:fileCard fileId="{!AWSdoc}"/>
        </aura:iteration>
    </lightning:card>            
    
    <!--###### FILE PREVIEW MODAL BOX START ######--> 
    <aura:if isTrue="{!v.hasModalOpen}">
        <section onclick="{!c.closeModel}"
                 role="dialog"
                 aria-modal="true"
                 class="slds-modal slds-fade-in-open">
            <div class="slds-modal__container">
                <div class="slds-modal__content slds-p-around_medium slds-text-align_center"
                     style="background: transparent;">
                    <div style="width: 50%; margin: 0 auto; text-align: left">    
                        <lightning:fileCard fileId="{!v.selectedDocumentId}"/>
                    </div>
                </div>
            </div>
        </section>
        <div class="slds-backdrop slds-backdrop_open"></div>
    </aura:if>
    <!--###### FILE PREVIEW MODAL BOX END ######-->     
    
</aura:component>