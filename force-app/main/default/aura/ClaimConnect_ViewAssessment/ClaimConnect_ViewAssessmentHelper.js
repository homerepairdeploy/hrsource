({
	 getContentFiles : function(component, event, helper)
    {
       component.set('v.showSpinner', true);        
        var sObjectId = component.get("v.recordId");      
        var action = component.get("c.getFiles");  
        action.setParams({ "sObjectId" : sObjectId});              
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.lstContentDoc', response.getReturnValue());                
            }
            else 
            {
                console.log('Error loading Content document files');
             }
        });
        $A.enqueueAction(action);  
	},
    
    getAWSFiles : function(component, event, helper)
    {
		var sObjectId = component.get("v.recordId");      
        var action = component.get("c.getAWSS3Files");  
        action.setParams({ "sObjectId" : sObjectId});              
        action.setCallback(this, function(response) {
            var state = response.getState();
            var res = response.getReturnValue();
            if (state === "SUCCESS") {     
                if (res.status == false)
                {
                    console.log('Error loading AWS files');
                    component.set("v.ErrLoadingAWSFiles",true);
                }
                else
                { component.set('v.lstAWSDoc', res.SFfileIds);                 
                 component.set("v.ErrLoadingAWSFiles",false);
                }
            }
            else {
                console.log('Error loading AWS files');                
                component.set("v.ErrLoadingAWSFiles",true);
            }    
           component.set('v.showSpinner', false);            
        });
        $A.enqueueAction(action);  
	},     
    
})