({
    doInit: function (component, event, helper) {        
        
        // Get draft assessment in Staging BA
        helper.getSBARecord(component, event, helper);
        
        //Get Previous Assessment. If found, Previous Assessments tab will be displayed
        helper.getPreviousAssessment(component, event, helper);
        
    },

    saveAsDraft: function (component, event, helper) {
        helper.saveDraft(component, event, helper);
    },

    handleTabChange: function (component, event, helper) {
        var currentTab = event.getSource().get('v.id');
        var previousTab = component.get("v.prevTab");

        switch (previousTab) {
            case 'buildingAssessment':
                helper.saveBA(component, event, helper);
                break;
            case 'SOW':
                // helper.saveSOW(component, event, helper);                            
                break;
        }

        component.set("v.prevTab", currentTab);

        if (currentTab == 'buildingAssessment')
        { component.set("v.isSuppressDraft", false); }
        else { component.set("v.isSuppressDraft", true); }

        if (currentTab == 'Summary' && previousTab == 'SOW') {
            var childComponent = component.find("SummaryComp");
            childComponent.invokedoInit();
        }

    },
})