({    
    
    getSBARecord:function(component, event, helper) {        
        // Get Staging BA record
        var BARecordId = component.get("v.BARecordId");        
        var action = component.get("c.getBARecord");         
        action.setParams({            
            'BARecordId' : BARecordId
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS" || state === "DRAFT") {                
                component.set("v.StagingBA",response.getReturnValue());                                         
            }
            else {              
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Failure",
                    "message": "Assessment Record Load failed "  
                });
                resultsToast.fire();                 
            }
        })
        $A.enqueueAction(action); 
    },
     
    getPreviousAssessment: function(component, event, helper) {        
        var action = component.get("c.getPrevAssmnt");  
        action.setParams({            
            'ClaimId' : component.get("v.recordId")
        });        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {     
             component.set("v.prevAssessemntFound",response.getReturnValue());                                             
            }
            })
        $A.enqueueAction(action);  
    },
    
    
    saveBA: function(component, event, helper) {        
        var BADetails = component.get("v.StagingBA");            
        var action = component.get("c.updateBARecord");     
        action.setParams({            
            'StagingBA' : BADetails
        }); 
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS" || state === "DRAFT") {               
                console.log('BA Record Save' + BADetails.Name);                 
            }
            else {
                // record is not saved                              
                var resultsToast = $A.get("e.force:showToast");
                var msg = JSON.stringify(response.error);
                resultsToast.setParams({
                    "message": msg
                });
                resultsToast.fire();                
            }
        })
        $A.enqueueAction(action);
    },   
    
      saveDraft: function(component, event, helper) {
        var BADetails = component.get("v.StagingBA");            
        var action = component.get("c.updateBARecord");     
        action.setParams({            
            'StagingBA' : BADetails
        }); 
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS" || state === "DRAFT") {               
               var resultsToast = $A.get("e.force:showToast");                
               resultsToast.setParams({
              "message": "Draft Assessment Record Saved :" + component.get("v.StagingBA.Name")
          });
          resultsToast.fire();     
            }
           
        })
        $A.enqueueAction(action);                       
      },
})