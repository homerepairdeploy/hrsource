({
    doInit : function(component) {
        
        var SF1_mdt = component.get("v.SF1Picklist");
        
        if (SF1_mdt)
        {
            var action = component.get("c.getPickListmdt");
            action.setParams({
                fieldname: component.get("v.fieldName")
            });
            action.setCallback(this, function(response) {   
                var state = response.getState();
                if (state === "SUCCESS" && response.getReturnValue()!= null)
                {
                    var list = response.getReturnValue();
                    component.set("v.picklistValues", list);                    
                }
            })
            $A.enqueueAction(action);
        }              
        
        else
        {
            var action = component.get("c.getPicklistValues");
            action.setParams({
                objectName: component.get("v.sObjectName"),
                field_apiname: component.get("v.fieldName")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && response.getReturnValue()!= null)
                {
                    var list = response.getReturnValue();
                     var multiselect = component.get("v.multiSelect");                    
                    if(multiselect)
                    {
                        var plValues = [];
                        for (var i = 0; i < list.length; i++) {
                            plValues.push({
                                label: list[i],
                                value: list[i]
                            });
                        }
                        component.set("v.dualPickListValues", plValues);                       
                    }
                    else
                    {
                        component.set("v.picklistValues", list);
                    }
                }
            })
            $A.enqueueAction(action);
        }
    },   
})