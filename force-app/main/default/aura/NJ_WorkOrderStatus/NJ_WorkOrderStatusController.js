({
  handleInitWO : function(component, event, helper) {
    helper.initWO(component);
  },
  handleSave : function(component, event, helper) {
    let oldStatus = component.get("v.priorStatus");
    let newStatus = component.get("v.newStatus");
    let isValidTransition = helper.isValidTransition(component, oldStatus, newStatus);
    if(isValidTransition === false) {
      helper.showToast("error", "Error","This is not a valid work order status.", 20000 );
    } else {
      let wo = component.get("v.wo");
      let saveWo = component.get("v.woSave");
      saveWo.Id = component.get("v.recordId");
      saveWo.Status = newStatus;
      if(newStatus === "Rejected WO") {
        saveWo.Rejection_Reason__c = wo.Rejection_Reason__c;
      } else if(newStatus === "Customer Contacted") {
        saveWo.Customer_Contacted_Date__c = wo.Customer_Contacted_Date__c;  
      }
      helper.saveWorkOrder(component, saveWo);
    }
  },
  handleStatusChange : function(component, event, helper) {
    let oldStatus = component.get("v.priorStatus");
    let newStatus = component.get("v.newStatus");
    let isValidTransition = helper.isValidTransition(component, oldStatus, newStatus);
    if(isValidTransition === false) {
      helper.showToast("error", "Error","This is not a valid work order status.", 20000 );
      component.set("v.isSaveDisabled", true);
    } else {
      component.set("v.isSaveDisabled", false);
    }
  }
})