({
  initWO : function(component) {
    let that = this;
    that.showLoader(component);
    let rId = component.get("v.recordId");
    let params = {  "woId" : rId };
    that.callApexFunction(component, 'c.initWorkOrderInfo', params)
      .then($A.getCallback(function(result){
        that.hideLoader(component);
        component.set("v.status", result.status);
        component.set("v.wo", result.wo);
        component.set("v.isValidStatus", result.isValidStatus);
        component.set("v.priorStatus", result.wo.Status);
        component.set("v.newStatus", result.wo.Status);
      }))
      .catch($A.getCallback(function(error){
        that.hideLoader(component);
        that.showToast("error", "Error",error.message, 20000 );
      })); 
  },
  saveWorkOrder : function(component, woOrd) {
    let that = this;
    that.showLoader(component);
    let params = {  "wo" : woOrd };
    that.callApexFunction(component, 'c.saveWorkOrder', params)
      .then($A.getCallback(function(result){
        that.hideLoader(component);
        that.showToast("success", "Success","Updated Successfully!!!", 20000 );
      }))
      .catch($A.getCallback(function(error){
        that.hideLoader(component);
        that.showToast("error", "Error",error.message, 20000 );
      })); 
  },
  isValidTransition : function(component, oldStatus, newStatus) {
    let isValid = false;
    if(oldStatus === "Work Order Assigned" && newStatus === "Accepted WO") {
      isValid = true;
    } else if(oldStatus === "Accepted WO" && newStatus === "Customer Contacted") {
      isValid = true;
    } else if(oldStatus === "Accepted WO" && newStatus === "Work Commenced") {
      isValid = true;
    } else if(oldStatus === "Customer Contacted" && newStatus === "Work Commenced") {
      isValid = true;
    } else if(oldStatus === "Work Commenced" && newStatus === "Customer Contacted") {
      isValid = true;
    } else if(oldStatus === "Work Order Assigned" && newStatus === "Rejected WO") {
      isValid = true;
    } 
    return isValid;
  }
})