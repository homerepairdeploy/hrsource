({
	initiateAttachments : function(component, response) {
        var attachments = response.getReturnValue();
        component.set("v.attachments", attachments);
        var attachmentComps = component.find("attachment");
        attachmentComps.forEach(function(attachmentComp){
            attachmentComp.initiate();
        });
        attachments.forEach(function(attachment) {
            if(attachment.Description === "Bank Deposit Slip"){
                component.set("v.bankDepositDocAdded", true);
            }
            if(attachment.Description === "Work Cover Insurance Policy"){
                component.set("v.workCoverDocAdded", true);
            }
            if(attachment.Description === "Public Liability Insurance Policy"){
                component.set("v.publicLiabilityDocAdded", true);
            }
            if(attachment.Description === "Other Insurance Policy"){
                component.set("v.otherInsuranceDocAdded", true);
            }
            if(attachment.Description === "Police Check Document"){
                component.set("v.policeCheckDocAdded", true);
            }
            if(attachment.Description === "Safety Documents"){
                component.set("v.SafetyCheckDocAdded", true);
            }
        });
       component.find("compliance").initiateDocs(); 
	},
    validatePhone : function(phoneVal) {
        if(!phoneVal) {
            return true;
        }
        if (phoneVal.length != 10){
            return false;
        }
        var phoneno = /(^1300\d{6}$)|(^1800|1900|1902\d{6}$)|(^0[2|3|7|8]{1}[0-9]{8}$)|(^13\d{4}$)|(^04\d{2,3}\d{6}$)/;
        return phoneVal.match(phoneno);
	},
    addErrorMessage: function(message) {
        var messages = [];
        messages.push({"message": message});
        return messages;
    }
})