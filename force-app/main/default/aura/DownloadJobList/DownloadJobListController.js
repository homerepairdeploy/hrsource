({
    //this init function pulls the Work Orders and WorkOrderLineItems
    // created as an HTML Table by the apex class
    
    doInit : function(component, event, helper) {
        //set blank values into attributes
        component.set("v.data_map", "");
        component.set("v.info_message", "");
        
        //get the account Id from the page
        var recId = component.get("v.recordId");
        
        //check if there's a valid value, and then proceed
        if (recId) 
        {
            var action = component.get("c.getRelatedData"); // call the apex class's method 
            action.setParams({ "accId" : recId}); //pass parameters
            action.setCallback(this, function(response) {
                var state = response.getState(); //get the state of the call 
                if(state === "SUCCESS") {
                    component.set("v.data_map", response.getReturnValue());
                    component.set("v.render_button",true);
                    var data_table = component.get("v.data_map");
                    if(data_table != "" )
                    {
                        var dt = new Date();
                        var day = dt.getDate();
                        var month = dt.getMonth() + 1;
                        var year = dt.getFullYear();
                        var hour = dt.getHours();
                        var mins = dt.getMinutes();
                        var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
                        //creating a temporary HTML link element (they support setting file names)
                        var a = document.createElement('a');
                        //getting data from our div that contains the HTML table
                      // var data_type = 'data:application/vnd.ms-excel';
                        var data_type = 'data:application/vnd.ms-excel';
                        
                        var table_html = encodeURIComponent(data_table);
            
                        a.href = data_type + ', ' + table_html;
                        //setting the file name
                        a.download = 'Job List Report_' + postfix + '.xls';
                        //triggering the function
                        document.body.appendChild(a);
                        a.click();
                        component.set("v.info_message", "The file: " + "Job List Report_" + postfix + ".xls has been downloaded.");
                        //$A.get("e.force:closeQuickAction").fire();
                    }
                    else
                    {
                        component.set("v.info_message", "Oops! Nothing to download as of now.");
                    }
                } else 
                {
                    component.set("v.info_message", "Sorry, there was some problem fetching the data. Contact Support if problem persists.");
                    console.log('Problem fetching data; response state: ' + state);
                }
            });
            $A.enqueueAction(action); //trigger the call
        }
	}
})