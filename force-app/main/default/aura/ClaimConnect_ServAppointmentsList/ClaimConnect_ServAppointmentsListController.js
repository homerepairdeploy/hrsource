({
    doInit: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.get("v.PageSize");  
       // var pageSize = component.find("pageSize").get("v.value"); 
        helper.getServAppointmentList(component, pageNumber, pageSize);        
    },
     
    handleNext: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.get("v.PageSize");  
     //   var pageSize = component.find("pageSize").get("v.value");
        pageNumber++;
        helper.getServAppointmentList(component, pageNumber, pageSize);
    },
     
    handlePrev: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.get("v.PageSize");
      //  var pageSize = component.find("pageSize").get("v.value");
        pageNumber--;
        helper.getServAppointmentList(component, pageNumber, pageSize);
    },
     
    
    searchKeyRecord: function (component, event, helper) {
        
        var params = event.getParam('arguments');        
        var searchKey = params.searchKey;
        if((searchKey == '') || (searchKey == null ))
        {
       	   component.set("v.srchResults",false);
            var pageNumber = 1;          //if search value is empty reload records 
            var pageSize = component.get("v.PageSize");              
            helper.getServAppointmentList(component, pageNumber, pageSize);        
        }
        else
        {
           component.set("v.srchResults",true);  
            helper.getSearchRecord (component,event,searchKey);
        }        
    },
    
        navigate : function(component, event, helper) {
          component.set("v.isModalOpen", false);
          helper.NavigateToRec(component, event, helper);
      },
    
     
   openModel: function(component, event, helper) {
      // Set isModalOpen attribute to true
       var selectedRec = event.getSource().get("v.name");
       var sList = component.get("v.servAppointmentList");
       var selRec = component.get("v.selectedRecord");
       for(var i = 0; i < sList.length; i++){
           if(sList[i].CaseId == selectedRec){              
               selRec=sList[i];              
           }
       }    
       component.set("v.selectedRecord",selRec);
       component.set("v.isModalOpen", true);
   },
  
   closeModel: function(component, event, helper) {
      // Set isModalOpen attribute to false 
      component.set("v.selectedRecord",null); 
      component.set("v.isModalOpen", false);
   },   
    
})