({
	doInit : function(component, event, helper) {		
        component.set('v.showSpinner',true);
     //Set Default values for  record         
        var recId = component.get("v.recordId");           
        
     // Enable/disable fields based on input value
        helper.displayfields(component, event, helper);
      
     // Get Customer name
        var cName = component.get("v.NewStagingBA.Customer_Onsite__c");
        if (cName=="" || cName==null)
        {
            var action = component.get("c.getCustomerName");  
            action.setParams({            
                'ClaimID' : component.get("v.recordId")
            });
            action.setCallback(this,function(data){            
                component.set("v.NewStagingBA.Customer_Onsite__c",data.getReturnValue()); 
            });
            $A.enqueueAction(action);       	               
        }
        component.set('v.showSpinner',false);
	},    
   
       
	toggleBuildingHeight: function (component, event, helper) {
         var sel = component.find("buildingHeight");
         var buildingHeight =	sel.get("v.value");
         helper.buildingHeightDis(component, event, buildingHeight);
    },
    
    toggleRoofType: function (component, event, helper) {
         var sel = component.find("roofType");
         var roofType =	sel.get("v.value");
         helper.roofTypeDis (component, event, roofType);          
    },
    
    handleCarpetOtherChange: function (component, event, helper) {
          var sel1 = component.find("Carpet");
          var carpet =	sel1.get("v.checked");
          
          var sel2 = component.find("Other");
          var other =	sel2.get("v.checked");        
        
           if (carpet == true)
          {
              component.set("v.conDamDetailDisable", false);
              component.set("v.otherDisable", true);
              component.set("v.NewStagingBA.Contents_Damaged__c","Carpet");
          }
          else if (other ==true)
          {
            component.set("v.conDamDetailDisable", false);   
            component.set("v.carpetDisable", true);
            component.set("v.NewStagingBA.Contents_Damaged__c","Other");
          }
         else
         {component.set("v.conDamDetailDisable", true);
          component.set("v.otherDisable", false);
          component.set("v.carpetDisable", false);
          component.set("v.NewStagingBA.Contents_Damaged_Appoint_Restorer__c",false);
          component.set("v.NewStagingBA.Contents_damage_description__c","");
         }
          
    },    
    
    handleBuildingRestoreChange: function (component, event, helper) {
          var sel = component.find("buildingRestoration");
          var buildRestore =	sel.get("v.checked");
          helper.buildRestoreDis (component, event, buildRestore);
    },
    
    handleMouldDryingChange: function (component, event, helper) {
           var sel = component.find("buildingRestoration");
          var buildRestore =	sel.get("v.checked");
        
          var sel1 = component.find("Mould");
          var mould =	sel1.get("v.checked");
          
          var sel2 = component.find("Drying");
          var drying =	sel2.get("v.checked");
          helper.mouldDryingDis (component, event, mould,drying,buildRestore);        
    },

    handleMaintenaceReqChange: function (component, event, helper) {
          var sel = component.find("maintenanceReq");
          var mainReq =	sel.get("v.checked");
          helper.maintenanceDis (component, event, mainReq);       
    }, 
  
    
    handleRepairOHSChange: function (component, event, helper) {
          var sel = component.find("repairOHSReq");
          var repairOHSReq = sel.get("v.checked");
          helper.repairOHSDis (component, event, repairOHSReq);             
    },
    
    handlePotentialRiskChange:function (component, event, helper) {
         var sel = component.find("potentialRisk");
         var ptRisk =	sel.get("v.value");
         component.set("v.NewStagingBA.Potential_Risks_to_Trades__c",ptRisk);      
       
         helper.potentialRiskDis (component, event, ptRisk);          
    },
    
    
    handleInsurableChange: function (component, event, helper) {
          var sel = component.find("InsurableGrp");
          var Insurable =	sel.get("v.checked");
        
          // Claim Proceeding is common for Claim Decision and Claim Proceeding field.
          // when Insurance is changed, reset this value
          component.set("v.NewStagingBA.Claim_Proceeding__c","");
        
          helper.insurableDis (component, event, Insurable);         
    },     
         
    handleCauseDecisionChange:function (component, event, helper) {
         var sel = component.find("causeDecision");
         var causeDescision =	sel.get("v.value");
         helper.causeDecisionDis (component, event, causeDescision);        
    },
    
    handleReadyForRepairs:function (component, event, helper) {
        var sel = component.find("readyForRepairs");
        var readyforRepair =	sel.get("v.checked");
        
        if (readyforRepair == true) { 
             component.set("v.NewStagingBA.Job_Readiness__c", 'Yes');  
             component.set("v.ClaimProceedingDisable", true);
             component.set("v.AwtReportReasonDisable", true);
             component.set("v.ReportTypeDisable", true);
             component.set("v.NewStagingBA.Claim_Proceeding__c","Yes");
             //component.find("claimProceeding").set("v.value","Yes");
             component.set("v.NewStagingBA.Report_Type__c ","");
             component.set("v.NewStagingBA.Awaiting_Report_Comments__c","");                
        }
        else{
             component.set("v.NewStagingBA.Job_Readiness__c", 'No');   
             component.set("v.ClaimProceedingDisable", false);
            component.find("claimProceeding").set("v.value","");
            //component.set("v.AwtReportReasonDisable", false);
            component.set("v.ReportTypeDisable", false);      
             
        }      
    },
    
    handleClaimProceedingChange :function (component, event, helper) {
         var sel = component.find("claimProceeding");
         var claimProceeding =	sel.get("v.value");
         helper.claimProceedingDis (component, event, claimProceeding);
    },
    
    toggleClaimDecision: function (component, event, helper) {
         var sel = component.find("claimDecision");
         var claimDecision =	sel.get("v.value");
         helper.claimDecisionDis (component, event, claimDecision);
    },

     handleDeclineTypeChange: function (component, event, helper) {
         var sel = component.find("declineType");
         var declineType =	sel.get("v.value");          
         helper.declineTypeDis (component, event, declineType);     
         
     },    
})