({  
  
  displayfields : function(component, event, helper) 
  {	
     // display fields based on the field value in Staging BA
      var buildingHeight = component.get("v.NewStagingBA.Building_Height__c");
      this.buildingHeightDis (component, event, buildingHeight);
      
      var roofType = component.get("v.NewStagingBA.Roof_Type__c");
      this.roofTypeDis (component, event, roofType);
      
      var contentDamaged = component.get("v.NewStagingBA.Contents_Damaged__c");
      this.contentDamagedDis (component, event, contentDamaged);
      
      var buildRestore =	component.get("v.NewStagingBA.Building_Restoration__c");
      this.buildRestoreDis (component, event, buildRestore);
      
      var mould =	component.get("v.NewStagingBA.Building_Restoration_Type_Mould__c");
      var drying =	component.get("v.NewStagingBA.Building_Restoration_Type_Drying__c");
      this.mouldDryingDis (component, event, mould,drying,buildRestore);
      
      var mainReq =	component.get("v.NewStagingBA.Maintenance_Required__c");
      this.maintenanceDis (component, event, mainReq);     
      
      var repairOHSReq = component.get("v.NewStagingBA.Repair_OH_S__c");
      this.repairOHSDis (component, event, repairOHSReq);
      
      var ptRiskList =[];
      var ptRisk =	component.get("v.NewStagingBA.Potential_Risks_to_Trades__c"); 
      if (ptRisk !='' && ptRisk != null)
      {
      ptRiskList=ptRisk.split(';');
      }
      component.set("v.selectedPotentialRiskList",ptRiskList);  
      this.potentialRiskDis (component, event, ptRiskList);
      
      var Insurable = component.get("v.NewStagingBA.Insurable__c");
      this.insurableDis (component, event, Insurable);
      
      var causeDescision = component.get("v.NewStagingBA.Cause__c");
      this.causeDecisionDis (component, event, causeDescision);
      
      var readyforRepair = component.get("v.NewStagingBA.Job_Readiness__c");
      this.readyForRepairs (component, event, readyforRepair);
      
      var claimProceeding = component.get("v.NewStagingBA.Claim_Proceeding__c");
      this.claimProceedingDis (component, event, claimProceeding);
      
      var claimDecision =	component.get("v.NewStagingBA.Claim_Proceeding__c");
      this.claimDecisionDis (component, event, claimDecision);
      
      var declineType =	component.get("v.NewStagingBA.Decline_Type_c__c");
      this.declineTypeDis (component, event, declineType);      
  },
  
  buildingHeightDis : function(component, event, buildingHeight) 
  {	
     if (buildingHeight == "Other")
       {     
        component.set("v.otherBuildingHeight", false);
       }
     else 
       {
       component.set("v.otherBuildingHeight", true);   
       component.set("v.NewStagingBA.Other_Building_Height__c","");
       }
  },

  roofTypeDis : function(component, event, roofType) 
  {
      if (roofType == "Other") {     
          component.set("v.otherRoofType", false);}
      else {
          component.set("v.otherRoofType", true);   
          component.set("v.NewStagingBA.Other_Roof_Type__c",'')
      }
  },

  contentDamagedDis : function(component, event, contentDamaged) 
  {
      if (contentDamaged == "Carpet")
      {
          component.set("v.conDamDetailDisable", false);
          component.set("v.otherDisable", true);
          component.set("v.Carpet",true);
          component.set("v.Other",false);
      }
      else  if (contentDamaged == "Other")
      {
          component.set("v.conDamDetailDisable", false); 
          component.set("v.carpetDisable", true);
          component.set("v.Carpet",false);
          component.set("v.Other",true);
      }
          else
          {
              component.set("v.conDamDetailDisable", true);
              component.set("v.NewStagingBA.Contents_Damaged__c","");
          }      
  },    
      
  buildRestoreDis : function(component, event, buildRestore) 
  {
      if (buildRestore ==true){
          component.set("v.bldRstrDetailDisable", false);
          component.set("v.mouldDisable", false); 
          component.set("v.dryingDisable", false); 
      }      
      else 
      {
          component.set("v.bldRstrDetailDisable", true);  
          component.set("v.mouldDisable", true); 
          component.set("v.dryingDisable", true); 
          component.set("v.RestoreDescDisable", true);
                   
          component.set("v.NewStagingBA.Building_Restoration_Details__c","");
          component.set("v.NewStagingBA.Building_Restoration_Type_Mould__c",false);
          component.set("v.NewStagingBA.Building_Restoration_Type_Drying__c",false);
      }
  },

  mouldDryingDis : function(component, event, mould,drying,buildRestore) 
  {
      if (mould == true)
          {
              component.set("v.RestoreDescDisable", false);
              component.set("v.dryingDisable", true);
          }
          else if (drying ==true)
          {
             component.set("v.RestoreDescDisable", false);  
             component.set("v.mouldDisable", true);
          }
         else
         {
              if(buildRestore==false)
              {
                 component.set("v.dryingDisable", true);  
                 component.set("v.mouldDisable", true);
              }
              else
              {
                  component.set("v.dryingDisable", false);  
                  component.set("v.mouldDisable", false);
              }
          component.set("v.RestoreDescDisable", true);
          component.set("v.NewStagingBA.Building_Restoration_Details__c","");
         }
    },

  maintenanceDis : function(component, event, mainReq) 
  {
      if (mainReq == true){
          component.set("v.mainDescDetailDisable", false);  
      }      
      else 
      {
          component.set("v.mainDescDetailDisable", true);  
          component.set("v.NewStagingBA.Maintenance_Details__c","");
      }       
   },       
         
  repairOHSDis : function(component, event, repairOHSReq) 
  {
     if (repairOHSReq ==true){
         component.set("v.repairOHSDetailDisable", false);  
     }      
      else 
      {
          component.set("v.repairOHSDetailDisable", true);  
          component.set("v.NewStagingBA.Asbestos__c","");
          component.set("v.NewStagingBA.Safety_Repair_Working_Heights__c","");
          component.set("v.NewStagingBA.Potential_Risks_to_Trades__c","");
          component.set("v.selectedPotentialRiskList","");  
          component.set("v.PotentialRiskDescDisable", true);
          component.set("v.NewStagingBA.Describe_potential_risks__c", ""); 
      }    
       
  },
      
  potentialRiskDis : function(component, event, ptRisk) 
    {
      
      if (ptRisk == null) {     
          component.set("v.PotentialRiskDescDisable", true);
          component.set("v.NewStagingBA.Describe_potential_risks__c", ""); 
      }
      else {
          component.set("v.PotentialRiskDescDisable", false);          
      }
  },
      
  insurableDis : function(component, event, Insurable) 
  {
          var claimDecisionPlist;
          var causePlist;
          var causeReasonPlist;
          var jobReadinessPlist;
          var declineReasonPlist;   
          var claimProceedingPlist;
          var reportTypePlist;
          var declineTypePlist;
        
          if (Insurable ==true){
               component.set("v.ClaimDecisionDisable", true);  
               component.set("v.DeclineTypeDisable",true);
               component.set("v.DeclineReasonDisable",true);
               component.set("v.CancelledReasonDisable",true);
              
               component.set("v.CauseDecisionDisable", false);
               component.set("v.CauseReasonDisable",true);
               component.set("v.ReadyforRepDisable",false);
               component.set("v.ClaimProceedingDisable",false);
               component.set("v.AwtReportReasonDisable",true);
               //component.set("v.ReportTypeDisable",false); 
              
             //  component.set("v.NewStagingBA.Claim_Proceeding__c","");
               component.set("v.NewStagingBA.Decline_Type_c__c","");
               component.set("v.NewStagingBA.Decline_Reason_Details__c","");
               component.set("v.NewStagingBA.Cancelled_Reason__c","");               
              }      
             else 
               {
               component.set("v.CauseDecisionDisable", true);
               component.set("v.CauseReasonDisable",true);
               component.set("v.ReadyforRepDisable",true);
               component.set("v.ClaimProceedingDisable",true);
               component.set("v.AwtReportReasonDisable",true);
               //component.set("v.ReportTypeDisable",true);
                   
               component.set("v.ClaimDecisionDisable", false);  
               component.set("v.DeclineTypeDisable",false);
               component.set("v.DeclineReasonDisable",true);
               component.set("v.CancelledReasonDisable",false);
               
               component.set("v.NewStagingBA.Cause__c","");
               component.set("v.NewStagingBA.Cause_Detail__c",""); 
               component.set("v.NewStagingBA.Job_Readiness__c",'No');                   
               component.set("v.readyforRepair",false);                   
               //component.set("v.NewStagingBA.Claim_Proceeding__c",""); 
               component.set("v.NewStagingBA.Awaiting_Report_Comments__c","");
               component.set("v.NewStagingBA.Report_Type__c","");                 
              }      
      },
      
  causeDecisionDis : function(component, event, causeDescision) 
  {
      if (causeDescision == "Working Loss") { 
          component.set("v.CauseReasonDisable", false);  
      }
      else{
          component.set("v.CauseReasonDisable", true);   
          component.set("v.NewStagingBA.Cause_Detail__c","");
          //component.set("v.NewStagingBA.Awaiting_Report_Comments__c","");       
      }
     },
      
  readyForRepairs : function(component, event, readyforRepair) 
  {
      if (readyforRepair == 'Yes') { 
          component.set("v.readyforRepair",true);
          component.set("v.ClaimProceedingDisable", true);
          component.set("v.AwtReportReasonDisable", true);
       //   component.set("v.ReportTypeDisable", true);
      //    component.set("v.NewStagingBA.Claim_Proceeding__c","");
          component.set("v.NewStagingBA.Report_Type__c ","");
          component.set("v.NewStagingBA.Awaiting_Report_Comments__c","");         
      }
      else{
          component.set("v.readyforRepair",false);
          component.set("v.ClaimProceedingDisable", false);
          //component.set("v.AwtReportReasonDisable", false);
        //  component.set("v.ReportTypeDisable", false);   
          
      }        
      },
    
  claimProceedingDis : function(component, event, claimProceeding)
  {
      
      if (claimProceeding == 'Awaiting Report') { 
          component.set("v.AwtReportReasonDisable", false);
          component.set("v.NonFitReasonDisable", true);          
      }      
      else{
          component.set("v.AwtReportReasonDisable", true);
          component.set("v.NewStagingBA.Report_Type__c ","");
          component.set("v.NewStagingBA.Awaiting_Report_Comments__c","");         
          component.set("v.NonFitReasonDisable", true);
      }
      
      if(claimProceeding == 'Non fit')
      {
         component.set("v.NonFitReasonDisable", false);
      }
            
  },
      
  claimDecisionDis: function (component, event, claimDecision) 
  {
      if (claimDecision == "Decline") {     
              component.set("v.DeclineTypeDisable", false);
              component.set("v.CancelledReasonDisable", true); 
              component.set("v.DeclineReasonDisable", true); 
              component.set("v.NewStagingBA.Cancelled_Reason__c","");          
           }
            else if (claimDecision == "Cancelled"){
                component.set("v.DeclineTypeDisable", true);
                component.set("v.DeclineReasonDisable", true);
                component.set("v.CancelledReasonDisable", false);   
                component.set("v.NewStagingBA.Decline_Type_c__c","");
                component.set("v.NewStagingBA.Decline_Reason_Details__c","");  
            }
         else {
               component.set("v.DeclineTypeDisable", true);
               component.set("v.CancelledReasonDisable", true); 
               component.set("v.DeclineReasonDisable", true);
               component.set("v.NewStagingBA.Decline_Type_c__c","");
               component.set("v.NewStagingBA.Decline_Reason_Details__c","");      
               component.set("v.NewStagingBA.Cancelled_Reason__c","");      
              }           
     },
      
  declineTypeDis: function (component, event, declineType) 
  {
       if (declineType == "General" || declineType == "Specific") {     
             component.set("v.DeclineReasonDisable", false);
           }    
  },
  
 })