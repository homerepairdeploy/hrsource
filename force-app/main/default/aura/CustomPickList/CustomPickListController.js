({
    doInit : function(component) {
        var action = component.get("c.getPickListValuesIntoList");
        console.log('objectName: '+component.get("v.sObjectName"));
        console.log('selectedField'+component.get("v.fieldName"));
        action.setParams({
            objectType: component.get("v.sObjectName"),
            selectedField: component.get("v.fieldName")
        });
        action.setCallback(this, function(response) {
            var list = response.getReturnValue();
            component.set("v.picklistValues", list);
        })
        $A.enqueueAction(action);
    },
    onChange: function (cmp, evt, helper) {
        console.log(cmp.find('select').get('v.value'));
        var selectValueEvent = cmp.getEvent("selectValue"); 
        selectValueEvent.setParams({"selectOption":cmp.find('select').get('v.value')});                               
        selectValueEvent.fire();
    }
})