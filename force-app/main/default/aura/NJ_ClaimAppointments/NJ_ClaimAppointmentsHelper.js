({
	populateClaimServiceData : function(component) {
        // display the spinner
        component.set("v.isProcessing",true);
		// set up and execute call to Apex Controller
        var claimId = component.get("v.claimId");
        var getClaimServiceData = component.get("c.getClaimServiceData");
		getClaimServiceData.setParams(
			{
				"claimId": claimId
			}
		);
		getClaimServiceData.setCallback(this, function(response){
			var state = response.getState();
            console.log(state);
			if(component.isValid() && state === "SUCCESS"){
				 
                // transform the Claim Service Data returned into format suitable for
                // lightning:tree component
                console.log('Response'+response.getReturnValue());
                var claimServiceTreeData = this.transformClaimServiceData(component, JSON.parse(response.getReturnValue()));
                console.log(claimServiceTreeData);
                component.set("v.claimServiceData", claimServiceTreeData);
			}else{
			}
            // hide the spinner
        	component.set("v.isProcessing",false);
		});
		$A.enqueueAction(getClaimServiceData);
    },
    // helper method to transform claim service data returned
    // by Apex Controller into a suitable format for lightning:tree
    transformClaimServiceData : function(component, claimServiceData) {
        // populate first level in tree
        var claimServiceDataTree = [];
        for (var x = 0; x < claimServiceData.length; x++) {
            var index = x+1;
            var currWorkOrder = claimServiceData[x];
            var newTreeItem = {};
            if(currWorkOrder.claimNumber != null) {
                component.set("v.claimInfo", currWorkOrder.claimNumber);
            }
            newTreeItem.label = index+". "+ currWorkOrder.workTypeName + " - " + currWorkOrder.appointmentNumber + " - "+ currWorkOrder.appointmentDuration + "-" + currWorkOrder.workOrderNumber;
            console.log('currWorkOrder',currWorkOrder);
            if(currWorkOrder.team){
                newTreeItem.label +=' - T'+ currWorkOrder.teamSize;
            }
            newTreeItem.name = currWorkOrder.appointmentId;
            newTreeItem.metatext = currWorkOrder.appointmentStatus;
            newTreeItem.expanded = false;
            // populate 2nd level in tree
            newTreeItem.items = [];
            if(currWorkOrder.workScopeList){
                for (var y = 0; y < currWorkOrder.workScopeList.length; y++) {
                    var workScope = currWorkOrder.workScopeList[y];
                    var newSlotTreeItem = {};
                    newSlotTreeItem.label = (y+1)+'. '+ workScope.productName;
                    newSlotTreeItem.name = currWorkOrder.appointmentNumber;
                    newSlotTreeItem.metatext = workScope.prodDescription;

                    newSlotTreeItem.expanded = false;
                    newTreeItem.items.push(newSlotTreeItem);
                }
            }
            claimServiceDataTree.push(newTreeItem);
            
        }
        return claimServiceDataTree;
    },
})