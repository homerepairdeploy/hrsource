({
    // helper method to retrieve Service Appointment Details
    // to be used by Component
    populateServiceAppointment : function(component) {
        
        // display the spinner
        component.set("v.isProcessing",true);
        var that = this;
        // set up and execute call to Apex Controller
        var apptId = component.get("v.srvApptId");
        console.log("***[ServiceAppointmentManagerHelper.populateServiceAppointment] Retrieving Details for Appt Id: " + apptId);
        var getAppointmentDetails = component.get("c.getSADetails");
        getAppointmentDetails.setParams(
            {
                "srvAppointmentId": apptId
            }
        );
        getAppointmentDetails.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                var servAppWrap = response.getReturnValue()
                
                component.set("v.currSrvAppointment", servAppWrap);
                //console.log('DisableFindCandidate populateServiceAppointment'+ servAppWrap.disableFindCandidate);
                //console.log('DisableTier2Candidate populateServiceAppointment'+ servAppWrap.disableTier2Candidate);
                component.set("v.disableFindCandidate", servAppWrap.disableFindCandidate);
                component.set("v.disableTier2Candidate", servAppWrap.disableTier2Candidate);

                if(servAppWrap.status == 'New') {
                    that.showInfo(component, "The resources can be found within 30 days of Earliest Start Date", 5000);
                    component.find("appInfo").startTimer();
                }
                component.set("v.isTimeBound", servAppWrap.isTimeBound);
                if(servAppWrap.isTimeBound === true) {
                    component.set("v.valueInTimeBound", servAppWrap.isTimeBound);
                } else {
                    component.set("v.valueInTimeBound", false);
                }
            }else{
            }
            // hide the spinner
            component.set("v.isProcessing",false);
        });
        $A.enqueueAction(getAppointmentDetails);
    },
    saveInstruction : function(component) {
        // display the spinner
        component.set("v.isProcessing",true);
        var that = this;
        // set up and execute call to Apex Controller
        var saveInstructions = component.get("c.updateWorkOrder");
        var timeBoundSet = component.get("v.isTimeBound");
        var servApp = component.get("v.currSrvAppointment");
        saveInstructions.setParams(
            {
                "woId": component.get("v.currSrvAppointment.workOrderId"),
                "claimId" : component.get("v.currSrvAppointment.claimId"),
                "specIns" : component.get("v.currSrvAppointment.wo.Special_Instructions_New__c"),
                 "descr" : component.get("v.currSrvAppointment.wo.Description"),
                "applyAll" : component.get("v.appAllWorkOrder"),
                "saId" : component.get("v.srvApptId"),
                "isTimeBound" : component.get("v.isTimeBound"),
                "commStartTime" : component.get("v.currSrvAppointment.committedStartTime"),
                "commEndTime" : component.get("v.currSrvAppointment.committedFinishTime"),
                "scheStartTime" : servApp.scheduledStartTime,
                "scheEndTime" : servApp.scheduledFinishTime,
                "dura" : component.get("v.currSrvAppointment.duration")
            }
        );
        saveInstructions.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
              var isError = response.getReturnValue();
              if(isError) {
                that.displayError(component,"Error", isError);
              } else {
                  that.populateServiceAppointment(component);
                  if(timeBoundSet === true) {
                    component.set("v.valueInTimeBound", timeBoundSet);
                  }
              }
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        that.displayError(component,"Error", errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            // hide the spinner
            component.set("v.isProcessing",false);
        });
        $A.enqueueAction(saveInstructions);
    },
    // helper method to retrieve Service Appointment Details
    // to be used by Component
    getAppointmentCandidates : function(component) {
        // display the spinner
        component.set("v.isProcessing",true);
        
        // set up and execute call to Apex Controller
        var apptId = component.get("v.srvApptId");
        var workOrderId = component.get("v.currSrvAppointment.workOrderId");
        var workTypeName = component.get("v.currSrvAppointment.workTypeName");

        var getAppointmentCandidates = component.get("c.getSACandidates");
        getAppointmentCandidates.setParams(
            {
                "srvAppointmentId": apptId,
                "workOrderId" : component.get("v.currSrvAppointment.workOrderId"),
                "workType" : component.get("v.currSrvAppointment.workTypeName")            }
        );
        getAppointmentCandidates.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                var candidateTreeItems = this.transformCandidates(response.getReturnValue());
                component.set("v.currSrvAppointmentCandidates", candidateTreeItems);
            }else{
                console.log("***[ServiceAppointmentManagerHelper.getAppointmentCandidates] Failed with state: "+ state);
            }
            // hide the spinner
            component.set("v.isProcessing",false);
        });
        $A.enqueueAction(getAppointmentCandidates);
    },
    validateAppointment : function(component) {
        var that = this;
        let earDt = component.get("v.currSrvAppointment.earliestStart");
        let dueDt = component.get("v.currSrvAppointment.dueDate");
		var earStartComp = component.find("earStart");
        
        let isValid = true;
        if(earDt > dueDt) {
            isValid = false;
            that.displayError(component,"Error", "Due date cannot be less than earliest start date");
        } else {
            isValid = true;
            that.clearError(component);
        }
        return isValid;
    },
    searchAccounts : function(component) {
        component.set("v.isProcessing",true);
        
        // set up and execute call to Apex Controller
        var searchAcc = component.get("c.findTempCandidates");
        searchAcc.setParams(
            {
                "workOrderId" : component.get("v.currSrvAppointment.workOrderId"),
                "tradeType": component.get("v.tradeType"),
                "servArea" : component.get("v.servTerrit"),
                "nme" : component.get("v.selName")
            }
        );
        searchAcc.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                var searchResponse = response.getReturnValue();
                var candidatesTree = [];
                for (var x = 0; x < searchResponse.length; x++) {
                    var currCandidate = searchResponse[x];
                    var newTreeItem = {};
                   // newTreeItem.label = currCandidate.Name + '-' + currCandidate.Service_Areas__c;
                    newTreeItem.label = currCandidate.Name;
                    newTreeItem.name = currCandidate.Id;
                    newTreeItem.expanded = false;
                    candidatesTree.push(newTreeItem);
                }
                component.set("v.tempAccounts", candidatesTree);
            }else{
                console.log("***[ServiceAppointmentManagerHelper.getTempMetadata] Failed with state: "+ state);
            }
            // hide the spinner
            component.set("v.isProcessing",false);
        });
        $A.enqueueAction(searchAcc);
    },
    getTempMetadata : function(component) {
        // display the spinner
        component.set("v.isProcessing",true);
        
        // set up and execute call to Apex Controller
        var apptId = component.get("v.srvApptId");
        var getTempCandidatesMetadata = component.get("c.initTempCandidates");
        getTempCandidatesMetadata.setParams(
            {
                "srvAppointmentId": apptId
            }
        );
        getTempCandidatesMetadata.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                var tempMetaRes = response.getReturnValue();
                component.set("v.tempMeta", tempMetaRes);
                component.set("v.tradeType", tempMetaRes.workType);
                component.set("v.servTerrit", tempMetaRes.servTerritory);
                component.set("v.servArea", "");
                component.set("v.selName", "");
                component.set("v.selTempCandidate", "");
                component.set("v.isTempSelected", false);
                component.set("v.tempAccounts", []);

            }else{
                console.log("***[ServiceAppointmentManagerHelper.getTempMetadata] Failed with state: "+ state);
            }
            // hide the spinner
            component.set("v.isProcessing",false);
        });
        $A.enqueueAction(getTempCandidatesMetadata);
    },
    scheduleTempCandidate : function(component) {
        component.set("v.isProcessing",true);
        var that = this;
        // set up and execute call to Apex Controller
        var currAppt = component.get("v.currSrvAppointment");
        var scheTempCandidate = component.get("c.scheduleTempCandidate");
        console.log('currAppt',currAppt);
        scheTempCandidate.setParams(
            {
                "servAppId": currAppt.Id,
                "accId": component.get("v.selTempCandidate"),
                "scheStartTime": currAppt.scheduledStartTime,
                "scheEndTime": currAppt.scheduledFinishTime,
                "duration" : currAppt.duration,
                "workOrderId" : currAppt.workOrderId
            }
        );
        scheTempCandidate.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                component.set("v.schedulingReponseMessage", response.getReturnValue());
                var appEvent = $A.get("e.c:SvcApptMgrScheduledEvent");
                appEvent.setParams({
                    "messageType" : "APPOINTMENT_SCHEDULED"
                });
                appEvent.fire();
                that.showScheduleSuccess(component);

            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        that.displayError(component,"Error", errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }

            // hide the spinner
            component.set("v.isProcessing",false);
        });
        $A.enqueueAction(scheTempCandidate);
    },
    // helper method to transform candidates returned
    // by FSL into a suitable format for lightning:tree
    transformCandidates : function(candidates) {
        // populate first level in tree
        var candidatesTree = [];
        for (var x = 0; x < candidates.length; x++) {
            var currCandidate = candidates[x];
            console.log('currCandidate :' + currCandidate);
            var newTreeItem = {};
            newTreeItem.label = currCandidate.isCapacityBased ? currCandidate.serviceResourceName + " - (Capacity) - " + currCandidate.percentCapacityUsed + "% Used" : currCandidate.serviceResourceName + " - (Calendar)";
            newTreeItem.name = currCandidate.serviceResourceId;
            newTreeItem.metatext = currCandidate.appointmentSlots.length + " Options, Starting ";
            if(currCandidate.appointmentSlots.length > 0 && currCandidate.appointmentSlots[0].slotStart) {
                newTreeItem.metatext += moment(this.dateGMTHelper(currCandidate.appointmentSlots[0].slotStart)).format('llll');
            }
            newTreeItem.expanded = false;
            // populate 2nd level in tree
            newTreeItem.items = [];
            for (var y = 0; y < currCandidate.appointmentSlots.length; y++) {
                var currSlot = currCandidate.appointmentSlots[y];
                var newSlotTreeItem = {};
                newSlotTreeItem.label = moment(this.dateGMTHelper(currSlot.slotStart)).format('llll');
                newSlotTreeItem.name = currCandidate.serviceResourceId + "|" + 
                                       this.dateGMTHelper(currSlot.slotStart) + "|" +
                                       currCandidate.serviceResourceType + "|" +
                                       currCandidate.serviceResourceCrewId  + "|" +
                                       currCandidate.serviceResourceTeamSize;
                newSlotTreeItem.expanded = false;
                newTreeItem.items.push(newSlotTreeItem);
            }
            candidatesTree.push(newTreeItem);
        }
        console.log('newTreeItem',newTreeItem);
        return candidatesTree;
    },
    showInfo : function(component, mesg, timer) {
        component.set("v.showInfo", true);
        component.set("v.infoMesg", mesg);
        component.set("v.infoTimer", timer);
        component.find("appInfo").startTimer();

    },
    // helper function to convert dates returned by the FSL Engine back to GMT
    // this is required to format dates correctly in Javascript as the GMT
    // offset of the local timezone re-adds the GMT offset, in effect doubling
    // the GMT offset for each date/time returned
    dateGMTHelper : function(dateString) {
        
        // get Javascript date representation (moment converts to milliseconds by default)
        var origDate = moment(dateString);
        // console.log("***[ServiceAppointmentManagerHelper.dateGMTHelper] moment.js parsed date: "+ origDate);
        // get the timezone offset in milliseconds
        var origDateOffsetMs = moment().utcOffset()*60000;
        // console.log("***[ServiceAppointmentManagerHelper.dateGMTHelper] moment.js utc date offset: "+ origDateOffsetMs);
        // calculate the original GMT Date
        var convertedDate = moment(origDate - origDateOffsetMs);
        
        return convertedDate;
    },
    showEditScreen : function(component) {
        component.set("v.isEditing", true);
        component.set("v.isScheduling", false);
        component.set("v.isScheduled", false);
        component.set("v.isTempScheduling", false);

    },
    showScheduleScreen : function(component) {
        component.set("v.isEditing", false);
        component.set("v.isScheduling", true);
        component.set("v.isScheduled", false);
        component.set("v.isTempScheduling", false);

    },
    showTempScheduleScreen : function(component) {
        component.set("v.isEditing", false);
        component.set("v.isScheduling", false);
        component.set("v.isTempScheduling", true);

        component.set("v.isScheduled", false);
    },
    showScheduleSuccess : function(component) {
        component.set("v.isTempScheduling", false);
        component.set("v.isEditing", false);
        component.set("v.isScheduling", false);
        component.set("v.isScheduled", true);
    },
    displayError : function(component, title, messg) {
        component.set("v.showError", true);
        component.set("v.errorTitle", title);
        component.set("v.errorMesg", messg);
    },
    clearError : function(component) {
        component.set("v.showError", false);
        component.set("v.errorTitle", "");
        component.set("v.errorMesg", "");

    },
    displaySuccess : function(component, title, messg) {
        component.set("v.showSuccess", true);
        component.set("v.successTitle", title);
        component.set("v.successMesg", messg);
    },
    // helper method to schedule the Service Appointment
    // once candidate has been selected
    scheduleAppointmentCandidate : function(component) {
        // display the spinner
        component.set("v.isProcessing",true);
        var that = this;
        var currSelection = component.get("v.currSelectedCandidate");
        var currApptId = component.get("v.srvApptId");
        
        // extract out the scheduling components from selection
        var selectedComponents = currSelection.split("|");
        console.log('selectedComponents :' + selectedComponents);
        var srvResourceId = selectedComponents[0];
        var srvTimeStart = selectedComponents[1];
        var srvResourceType;
        var srvResourceCrewId;
        var srvResourceTeamSize;
        
        if(selectedComponents[2]=='undefined'){
            srvResourceType = null;
        }else{            
            srvResourceType = selectedComponents[2];
        }
       
        if(selectedComponents[3]=='undefined'){            
            srvResourceCrewId = null;
        }else{
            srvResourceCrewId = selectedComponents[3];
        }
        
        if(selectedComponents[4]=='undefined'){
            srvResourceTeamSize = null;            
        }else{
            srvResourceTeamSize =  selectedComponents[4];
        }
        
        var scheduleAppointmentCandidate = component.get("c.scheduleAppointmentForCandidate");
        console.log(currApptId+'----'+srvResourceId+'----'+srvTimeStart+'----'+srvResourceType+'----'+srvResourceCrewId+'----'+srvResourceTeamSize);
        scheduleAppointmentCandidate.setParams(
            {
                "srvAppointmentId": currApptId,
                "srvResourceId": srvResourceId,
                "apptStartTime": srvTimeStart,
                "srvResourceType":srvResourceType,
                "srvResourceCrewId":srvResourceCrewId,
                "srvResourceTeamSize":srvResourceTeamSize
            }
        );
        scheduleAppointmentCandidate.setCallback(this, function(response){
            var state = response.getState();
            console.log('state',state);
            if(component.isValid() && state === "SUCCESS"){
                // fire an event to indicate a service appointment has been scheduled
                // Get the application event by using the
                // e.<namespace>.<event> syntax
                var appEvent = $A.get("e.c:SvcApptMgrScheduledEvent");
                appEvent.setParams({
                    "messageType" : "APPOINTMENT_SCHEDULED"
                });
                appEvent.fire();
                that.showScheduleSuccess(component);

            }else{
                console.log("***[ServiceAppointmentManagerHelper.scheduleAppointmentCandidate] Failed with state: "+ state);
            }
            // set reponse property on Component
            component.set("v.schedulingReponseMessage", response.getReturnValue());
            var scheduleresponse=component.get("v.schedulingReponseMessage.schedulingMessage");
            if (scheduleresponse.includes("Work Order Authority")){
                component.set("v.schedulingReponseMessage.schedulingMessage", 'Work Order Authority cannot be Empty');
            }
           
            console.log('schedulingReponseMessage ',response.getReturnValue());
            // navigate to Scheduling Complete Screen
            // hide the spinner
            component.set("v.isProcessing",false);
        });
        $A.enqueueAction(scheduleAppointmentCandidate);
    },
    // helper method to schedule the Service Appointment
    // once candidate has been selected
    updateAppointment : function(component, isFindCandidates) {
        // display the spinner
        component.set("v.isProcessing",true);
        var that = this;
        // set up and execute call to Apex Controller
        var currAppt = component.get("v.currSrvAppointment");
        console.log("***[ServiceAppointmentManagerHelper.updateAppointment] Updating Appt Id: " + currAppt.Id);
        var updateAppointment = component.get("c.updateSADetails");
        updateAppointment.setParams(
            {
                "srvAppointmentId": currAppt.Id,
                "srvSubject": currAppt.subject,
                "srvEarlyStart": currAppt.earliestStart,
                "srvDueDate": currAppt.dueDate
            }
        );
        
        updateAppointment.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
               component.set("v.currSrvAppointment", response.getReturnValue().updatedAppointment);
                // fire an event to indicate a service appointment has been scheduled
                // Get the application event by using the
                // e.<namespace>.<event> syntax
                // Note this should be a separate event type in a
                // Production implementation
                if(response.getReturnValue().updateStatus == "ERROR"){
                    component.set("v.currSrvAppointment", currAppt);
                    that.displayError(component,"Error", "Please check the due date, it can't be so close to the Earliest Start Time.");
                    that.showEditScreen(component);
                }else{
                    var servAppWrap = response.getReturnValue();
                    console.log('DisableFindCandidate updateAppointment'+ servAppWrap.disableFindCandidate);
                    console.log('DisableTier2Candidate updateAppointment'+ servAppWrap.disableTier2Candidate);
                    var appEvent = $A.get("e.c:SvcApptMgrScheduledEvent");
                    appEvent.setParams({
                        "messageType" : "APPOINTMENT_SCHEDULED"
                    });
                    appEvent.fire();
                    if(isFindCandidates === true) {
                        that.showScheduleScreen(component);
                        that.getAppointmentCandidates(component, false);
                    } else {
                        that.showTempScheduleScreen(component);
                        that.getTempMetadata(component);
                    }
                }
                   
            }else{
                console.log("***[ServiceAppointmentManagerHelper.updateAppointment] Failed with state: "+ state);
            }
            // hide the spinner
            component.set("v.isProcessing",false);
        });
        $A.enqueueAction(updateAppointment);
    }
})