({
    init : function(component, event, helper) {
        console.log('***[ServiceAppointmentManagerController.init] Initialising Service Manager Component...');
        //helper.populateServiceAppointment(component);
    },
    // get Candidates for an Appointment
    getCandidates : function(component, event, helper) {
        let isValid = helper.validateAppointment(component);
        if(isValid === true) {
            helper.updateAppointment(component, true);
        }
    },
    handleSaveAlert : function(component, event, helper) {
      helper.showInfo(component, "Please click on save button!!", 5000);
    }, 
    // get Temp Candidates for an Appointment
    getTempCandidates : function(component, event, helper) {
        let isValid = helper.validateAppointment(component);
        if(isValid === true) {
            helper.updateAppointment(component, false);   
        }
    },
    handleIsTimeBoundChange : function(component, event, helper) {
        helper.showInfo(component, "Please scroll down & click on save button to save changes.", 30000);
        let commStartDefault = component.get("v.currSrvAppointment.committedStartTimeDefault");
        let commEndDefault = component.get("v.currSrvAppointment.committedFinishTimeDefault");
        component.set("v.currSrvAppointment.committedStartTime", commStartDefault);
        component.set("v.currSrvAppointment.committedFinishTime", commEndDefault);
    },
    // handle selection of a candidate in tree
    handleCandidateSelect: function (component, event) {
        event.preventDefault();
        var treeSelection = event.getParam('name');
        var isCandidateSelected = treeSelection.includes("|");
        if (isCandidateSelected == true) {
            component.set("v.currSelectedCandidate", treeSelection);
            component.set("v.isSchedBtnDisabled",false);
        } else {
            component.set("v.currSelectedCandidate", '');
            component.set("v.isSchedBtnDisabled",true);
        }
    },
    // schedule an appointment after a candidate has been selected
    scheduleAppointment: function(component, event, helper) {
        helper.scheduleAppointmentCandidate(component);
    },
    // verification script to fire after moment.js library loaded
    afterMomentScriptsLoaded: function(component, event, helper) {
        var testDate = moment("2018-08-09T12:00:00.000Z");
    },
    handleSaveInstruction : function (component, event, helper) {
        helper.clearError(component);
        helper.saveInstruction(component);
    },
    // Close Scheduling Finished Screen
    closeSchedulingScreen : function(component, event, helper) {
        component.set("v.isEditing", true);
        component.set("v.isScheduling", false);
        component.set("v.isScheduled", false);
        component.set("v.schedulingReponseMessage",{});
        helper.populateServiceAppointment(component);
    },
    // handle appointment selection in another component
    handleClaimSrvApptManagerSelectionEvent: function(component, event, helper) {
        var eventMessageType = event.getParam("messageType");
        if (eventMessageType === "APPOINTMENT_SELECTED") {
            var srvApptSelectedId = event.getParam("selectedAppt");
            // to do: reset current Service Appointment on Component and call helper.populateServiceAppointment(component);
            component.set("v.isApptSelected", true);
            component.set("v.srvApptId", srvApptSelectedId);
            helper.showEditScreen(component);
            helper.populateServiceAppointment(component);
            helper.clearError(component);
        } else if (eventMessageType === "NON_APPOINTMENT_SELECTED") {
        } else {
        }
    },
    handleSvcApptMgrScheduledEvent : function(component, event, helper) {
        let srvApptId = component.get("v.srvApptId");
        if(!$A.util.isEmpty(srvApptId)) {
            helper.showEditScreen(component);
            helper.populateServiceAppointment(component);
        }
    },
    // close Candidates Screen
    backFromCandidates: function(component, event, helper) {
        helper.showEditScreen(component);
    },
    updateAppt: function(component, event, helper) {
        console.log('***[ServiceAppointmentManagerController.updateAppt] Updating Appointment...');
    },
    handleSearchAccounts : function(component, event, helper) {
        component.set("v.isTempSelected", false);
        helper.searchAccounts(component);
    },
    handleTempSelect : function(component, event, helper) {
        component.set("v.isTempSelected", true);
        var accId = event.getParam("name");
        component.set("v.selTempCandidate", accId);

    },
    handleScheduleTempCandidates : function(component, event, helper) {
        
        let scheStart = component.get("v.currSrvAppointment.scheduledStartTime");
        let scheEnd = component.get("v.currSrvAppointment.scheduledFinishTime");
        let today = $A.localizationService.formatDateTime(new Date());  
        let scheStartDate =  $A.localizationService.formatDate(component.get("v.currSrvAppointment.scheduledStartTime"));     
        let todayDate = $A.localizationService.formatDate(new Date());

        /*if(scheStartDate < todayDate) {
            helper.displayError(component,"Error", "Schedule start date should be greater than today.");
        } */
        if(scheStart > scheEnd) {
            helper.displayError(component,"Error", "Schedule end date should be greater than schedule start date.");
        } else {
            helper.clearError(component);
            helper.scheduleTempCandidate(component);
        }
    }
})