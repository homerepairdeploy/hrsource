({

    // function call on component Load
    doInit: function (component, event, helper) {
        var action = component.get("c.getRoomRecords");
        action.setParams({ "StagingBAID": component.get("v.recordId") });
        // alert('StagingBAID'+component.get("v.recordId"));
        action.setCallback(this, function (response) {
            var state = response.getState(); //Checking response status
            console.log("rooms... " + JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS") {
                component.set("v.RoomsList", response.getReturnValue());
            }
            else {
                helper.createObjectData(component, event);
            }
        }); $A.enqueueAction(action);
    },

    onCheck: function (component, event, helper) {
        var checkCmp = component.find("rowID");
        var index;
        var tempIDs = [];
        var roomvaluee;
        const CHK = (checkCmp.length == null) ? [checkCmp] : checkCmp;
        var count = 0;
        let checkBoxState = event.getSource().get("v.checked");
        component.find("disableenable").set("v.disabled", !checkBoxState);
        //component.find("RowID").set("v.disablecheckbox", !checkBoxState);

        // play a for loop and check every checkbox values 
        // if value is checked(true) then add those Id (stored in value attribute on checkbox) in tempIDs var.
        for (var i = 0; i < CHK.length; i++) {
            if (CHK[i].get("v.checked") == true) {
                count++;
                tempIDs.push(CHK[i].get("v.value"));
                index = CHK[i].get("v.name");
                roomvaluee = true;
            }
        }

        var RoomsList = component.get("v.RoomsList");
        var RoomRecord = RoomsList[index];

        if (count >= 2) {
            component.set("v.showMessage", true);
        }

        component.set("v.roomValue", roomvaluee);
        component.set("v.roomId", tempIDs[0]);
        component.set("v.tempId", tempIDs);

        helper.validateRoomName(component, event,helper);
        helper.saveRooomData(component, event, RoomsList);
        helper.fetchRoomItems(component, event, tempIDs);
        helper.fetchWorkTypeNames(component, event, helper);
    },

    // function for create new object Row in Rooms List 
    addNewRow: function (component, event, helper) {
        var StagingBAID = component.get("v.recordId");
        var RoomsList = component.get("v.RoomsList");
        helper.validateRoomName(component, event,helper);
        helper.saveRooomData(component, event, RoomsList);
        helper.addRooms(component, event, StagingBAID);
    },

    saveDraft: function (component, event, helper) {
        var RoomsList = component.get("v.RoomsList");
        helper.validateRoomName(component, event,helper);
        helper.saveRooomData(component, event, RoomsList);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The rooms have been saved successfully.",
            "type": "success"
        });
        toastEvent.fire();
    },

    // function duplicate selected room 
    duplicateRoom: function (component, event, helper) {
        var checkCmp = component.find("rowID");
        var tempIDs = [];
        const CHK = (checkCmp.length == null) ? [checkCmp] : checkCmp;
        // play a for loop and check every checkbox values 
        // if value is checked(true) then add those Id (stored in value attribute on checkbox) in tempIDs var.
        for (var i = 0; i < CHK.length; i++) {
            if (CHK[i].get("v.checked") == true) {
                tempIDs.push(CHK[i].get("v.value"));
            }
        }
        //This is to disable the duplicate room button
        let checkBoxState = event.getSource().get("v.checked");
        component.find("disableenable").set("v.disabled", !checkBoxState);
        helper.duplicateRooms(component, event, tempIDs);
    },

    // function for delete the row 
    removeDeletedRow: function (component, event, helper) {

        var StagingBAID = component.get("v.recordId");
        //var RoomID = event.currentTarget.dataset.record;
        var RoomID = event.getSource().get("v.name");
        var action = component.get("c.deleteRooms");
        action.setParams({ "RoomID": RoomID });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                component.set("v.showRoomDeletePopUp", false);
                helper.fetchExistingRooms(component, event, StagingBAID);
            }
        });

        //This is to disable the duplicate room button
        component.find("disableenable").set("v.disabled", true);
        $A.enqueueAction(action);
    },

    removeTradeItemRow: function (component, event, helper) {
        // var TradeItemID = event.currentTarget.dataset.record;
        var TradeItemID = event.getSource().get("v.name");
        //alert('ID---'+TradeItemID);

        var tempIDs = [];
        tempIDs = component.get("v.tempId");
        //alert('RoomIDs--'+tempIDs);
        var action = component.get("c.deleteTradeItems");
        action.setParams({ "TradeItemID": TradeItemID });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                component.set("v.showTradeItemDeletePopUp", false);
                helper.fetchRoomItems(component, event, tempIDs);
            }
        });
        $A.enqueueAction(action);
    },

    updateTradeItemRow: function (component, event, helper) {
        var index = event.currentTarget.dataset.index;
        var TradeItemsList = component.get("v.TradeItemsList");
        var tiRecord = TradeItemsList[index];

        helper.saveTradeItemRow(component, event, tiRecord);

    },

    confirmRoomDelete: function (component, event, helper) {
        component.set("v.showRoomDeletePopUp", true);
        component.set("v.selectedRoomId", event.currentTarget.getAttribute("data-record"));
    },

    cancelDelete: function (component, event, helper) {
        component.set("v.showRoomDeletePopUp", false);
    },

    confirmTradeItemDelete: function (component, event, helper) {
        component.set("v.showTradeItemDeletePopUp", true);
        component.set("v.selectedTradeItemId", event.currentTarget.getAttribute("data-item"));
    },

    cancelItemDelete: function (component, event, helper) {
        component.set("v.showTradeItemDeletePopUp", false);
    },

    updateItemValue: function (component, event, helper) {
        helper.updateItemValues(component, event, helper);
        //component.set("v.updateTradeItemIconEnabled", true);
    },


    //actions occuring on 'Show details' button
    openModel: function (component, event, helper) {
        // Set isModalOpen attribute to true
        component.set("v.isModalOpen", true);
        helper.productDetails(component, event, helper);
    },

    closeModel: function (component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModalOpen", false);
    },

    closeModal: function (component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModelOpen", false);
    },

    closeError: function (component, event, helper) {
        // Set showError attribute to false  
        component.set("v.showError", false);
    },

    closeMessage: function (component, event, helper) {
        // Set showError attribute to false  
        component.set("v.showMessage", false);
    },

    closeRoomNameMessage: function (component, event, helper) {
        // Set showError attribute to false  
        component.set("v.showRoomNameError", false);
    },


    submitDetails: function (component, event, helper) {
        // Set isModalOpen attribute to false
        //Add your code to call apex method or do some processing
        component.set("v.isModalOpen", false);
    },

    //All picklistvalue changes related to Product2 and Pricebooks
    onWorkTypeChange: function (component, event, helper) {
        helper.fetchWorkCodeGroupsCatagory(component, event, helper);
    },

    onCatagoryChange: function (component, event, helper) {
        helper.fetchWorkCodeGroupItemDescription(component, event, helper);
    },

    onItemDescripChange: function (component, event, helper) {
        helper.fetchWorkCodeGroupDimensions(component, event, helper);
    },

    onDimensionChange: function (component, event, helper) {
        helper.fetchWorkCodeGroupRepairMethod(component, event, helper);
    },

    onRepairChange: function (component, event, helper) {
        helper.fetchWorkCodeGroupAdditionalNotes(component, event, helper);
    },

    orderChange: function (component, event, helper) {
        var inputfield = event.getSource();
        var inputvalue = event.getSource().get("v.value");
        var indexvar = event.getSource().get("v.name");
        var TradeItemsList = component.get("v.TradeItemsList");
        var TradeItemsOldList = component.get("v.TradeItemsList");
        var Ids = TradeItemsList[indexvar].Id;
        var existingTradeMap = component.get("v.tradeItemsmap");  //Map of reciD Site_Visit_Number__c    
        var prevSiteVisitNumber = existingTradeMap.get(Ids);
        if (inputvalue - prevSiteVisitNumber > 1 || inputvalue < prevSiteVisitNumber) {
            inputfield.setCustomValidity("Site Visit Number should be Sequential");
        }
        else {
            inputfield.setCustomValidity("");
           // var tiRecord = TradeItemsList[indexvar];
           // helper.saveTradeItemRow(component, event, tiRecord);
            helper.updateItemValues(component, event, helper);
        }
    },

    addTradeItem: function (component, event, helper) {
        helper.addTradeItems(component, event, helper);
    },
})