({
    fetchExistingRooms: function (component, event, StagingBAID) {
        var action = component.get("c.getRoomRecords");
        action.setParams({ "StagingBAID": StagingBAID });
        action.setCallback(this, function (response) {
            var state = response.getState(); //Checking response status
            console.log("rooms... " + JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS") {
                component.set("v.TradeItemsList", []);
                component.set("v.RoomsList", response.getReturnValue());  // Adding values in Aura attribute variable.   
            }
        });
        $A.enqueueAction(action);
    },

    addRooms: function (component, event, StagingBAID) {
        var action = component.get('c.insertRooms');
        action.setParams({ "StagingBAID": StagingBAID });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // display SUCCESS message
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The rooms have been added successfully.",
                    "type": "success"
                });
                toastEvent.fire();
                this.fetchExistingRooms(component, event, StagingBAID);
            }
        });
        $A.enqueueAction(action);
    },


    duplicateRooms: function (component, event, AllRoomIds) {
        var StagingBAID = component.get("v.recordId");
        // var roomID = component.get("v.roomId");
        //alert(AllRoomIds[0]);
        var action = component.get('c.duplicateRoomWithTradeItems');
        action.setParams({ "RoomID": AllRoomIds[0] });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                // display SUCCESS message
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The room has been duplicated successfully.",
                    "type": "success"
                });
                toastEvent.fire();
                this.fetchExistingRooms(component, event, StagingBAID);

            }
            else {
                alert('some problem---' + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },

    validateRoomName: function (component, event, helper) {
        var RoomNumber;
        var RoomsList = component.get("v.RoomsList");
        for (var i = 0; i < RoomsList.length; i++) {
            if (RoomsList[i].Name === "Room Name?") {
                RoomNumber = i + 1;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Please enter a valid Room Name on Room number: " + RoomNumber
                });
                toastEvent.fire();
            } 
        }
    },


    saveRooomData: function (component, event, RoomsList) {
        var action = component.get("c.autoSaveRoomRecords");
        action.setParams({ "StagingRooms": JSON.stringify(RoomsList) });
        action.setCallback(this, function (response) {
            var state = response.getState(); //Checking response status
            if (state === "SUCCESS") {
                console.log("State in save--" + state);
            }
        });
        $A.enqueueAction(action);

    },
    
    
    
   updateItemValues: function (component, event, tiRecord) {
        var indexvar = event.getSource().get("v.name");
        var TradeItemsList = component.get("v.TradeItemsList");
        var newtradeItemList = [];
        console.log('TradeItemsList=', TradeItemsList);
        for (let i = 0; i < TradeItemsList.length; i++) {
            TradeItemsList[i].checkedIcon = false;
            newtradeItemList.push(TradeItemsList[i]);
        }
        newtradeItemList[indexvar].checkedIcon = true;
        component.set("v.TradeItemsList", newtradeItemList);
    },


    saveTradeItemRow: function (component, event, tiRecord) {
        var tempIds = [];
        var action = component.get("c.saveTradeItems");
        action.setParams({ "StagingRoomItem": JSON.stringify(tiRecord) });
        action.setCallback(this, function (response) {
            var state = response.getState(); //Checking response status
            if (state === "SUCCESS") {
                console.log("State in save--" + state);
                // display SUCCESS message
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "The room Item has been Updated successfully."
                });
                toastEvent.fire();
            }
               tempIds = component.get("v.tempId");
                this.fetchRoomItems(component, event, tempIds);
        });
        $A.enqueueAction(action);
    },


    addTradeItems: function (component, event, helper) {
        var action = component.get("c.insertRoomItems");
        var tempIds = [];
        var hours = component.find("hours").get("v.value");
        var minutes = component.find("minutes").get("v.value");
        var time = hours * 60 + parseInt(minutes);
        var quantity = component.find("quantity").get("v.value");
        var qtyPrice = component.find("qtyPrice").get("v.value");
        var cost = component.find("cost").get("v.value");
        action.setParams({
            "productId": component.get("v.productId"),
            "roomId": component.get("v.roomId"),
            "minute": time,
            "quantity": quantity,
            "qtyPrice": qtyPrice,
            "labourCost": cost
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.selectedWorkTypes","");
                component.set("v.selectedWorkCodeGroupsCatagory","");
                component.set("v.selectedWorkCodeGroupsItemDescription","");
                component.set("v.selectedWorkCodeGroupsDimensions","");
                component.set("v.selectedWorkCodeGroupsRepairMethod","");
                component.set("v.selectedWorkCodeGroupsAdditionalNotes","");

                // display SUCCESS message
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "The room Items have been added successfully."
                });
                toastEvent.fire();
                component.set("v.isModalOpen", false);
                tempIds = component.get("v.tempId");
                this.fetchRoomItems(component, event, tempIds);
                // commented by Vidhya $A.get('e.force:refreshView').fire();
            }
            else {
                alert('some problem' + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },

    fetchRoomItems: function (component, event, AllRoomsIds) {
        var action = component.get('c.getRoomItemRecords');
        action.setParams({ "RoomId": AllRoomsIds[0] });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                const tradeItemsmap = new Map();
                const tradeItemsListMap = new Map();
                var tradeList = response.getReturnValue();

                for (var i = 0; i < tradeList.length; i++) {
                    tradeItemsmap.set(tradeList[i].Id, tradeList[i].Site_Visit_Number__c);
                    tradeItemsListMap.set(tradeList[i].Id, true);
                    tradeList[i].checkedIcon = false;
                }
                component.set("v.tradeItemsmap", tradeItemsmap);
                component.set("v.TradeItemsList", tradeList);
            }
            else {
                component.set("v.TradeItemsList", []);
            }
        });
        $A.enqueueAction(action);
    },


    fetchWorkTypeNames: function (component, event, helper) {
        var action = component.get("c.fetchWorkTypeNames");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                component.set("v.workTypes", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    fetchWorkCodeGroupsCatagory: function (component, event, helper) {
        var action = component.get("c.fetchWorkCodeGroupCatagory");
        action.setParams({
            'workTypeId': component.get("v.selectedWorkTypes")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                component.set("v.workCodeGroupsCatagory", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    fetchWorkCodeGroupItemDescription: function (component, event, helper) {
        var action = component.get("c.fetchWorkCodeGroupItemDescription");
        action.setParams({
            'worktypeId': component.get("v.selectedWorkTypes"),
            'catagoryId': component.get("v.selectedWorkCodeGroupsCatagory")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                component.set("v.workCodeGroupsItemDescription", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    fetchWorkCodeGroupDimensions: function (component, event, helper) {
        var action = component.get("c.fetchWorkCodeGroupDimensions");
        action.setParams({
            'worktypeId': component.get("v.selectedWorkTypes"),
            'catagoryId': component.get("v.selectedWorkCodeGroupsCatagory"),
            'itemDescId': component.get("v.selectedWorkCodeGroupsItemDescription")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                component.set("v.workCodeGroupsDimensions", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    fetchWorkCodeGroupRepairMethod: function (component, event, helper) {
        var action = component.get("c.fetchWorkCodeGroupRepairMethod");
        action.setParams({
            'worktypeId': component.get("v.selectedWorkTypes"),
            'catagoryId': component.get("v.selectedWorkCodeGroupsCatagory"),
            'itemDescId': component.get("v.selectedWorkCodeGroupsItemDescription"),
            'dimensionId': component.get("v.selectedWorkCodeGroupsDimensions")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                component.set("v.workCodeGroupsRepairMethod", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    fetchWorkCodeGroupAdditionalNotes: function (component, event, helper) {
        var action = component.get("c.fetchWorkCodeGroupAdditionalNotes");
        action.setParams({
            'worktypeId': component.get("v.selectedWorkTypes"),
            'catagoryId': component.get("v.selectedWorkCodeGroupsCatagory"),
            'itemDescId': component.get("v.selectedWorkCodeGroupsItemDescription"),
            'dimensionId': component.get("v.selectedWorkCodeGroupsDimensions"),
            'repairId': component.get("v.selectedWorkCodeGroupsRepairMethod")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                component.set("v.workCodeGroupsAdditionalNotes", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },


    productDetails: function (component, event, helper) {
        var action = component.get("c.fetchProduct");
        action.setParams({
            'selectedWorkTypes': component.get("v.selectedWorkTypes"),
            'selectedWorkCodeGroupsCatagory': component.get("v.selectedWorkCodeGroupsCatagory"),
            'selectedWorkCodeGroupsItemDescription': component.get("v.selectedWorkCodeGroupsItemDescription"),
            'selectedWorkCodeGroupsDimensions': component.get("v.selectedWorkCodeGroupsDimensions"),
            'selectedWorkCodeGroupsRepairMethod': component.get("v.selectedWorkCodeGroupsRepairMethod"),
            'selectedWorkCodeGroupsAdditionalNotes': component.get("v.selectedWorkCodeGroupsAdditionalNotes"),
            "StagingBAID": component.get("v.recordId")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                component.set("v.productWrapp", response.getReturnValue());
                var labourTime = component.get("v.productWrapp.labourMinute");
                var hours = Math.trunc(labourTime / 60);
                var minutes = labourTime % 60;
                component.set("v.labourTime", labourTime);
                component.set("v.productWrapp.labourHour", hours);
                component.set("v.productWrapp.labourMinute", minutes);
                component.set("v.materialPrice", component.get("v.productWrapp.materialPrice"));
                component.set("v.orgMaterialPrice", component.get("v.productWrapp.materialPrice"));
                component.set("v.labourPrice", component.get("v.productWrapp.labourPrice"));
                component.set("v.orgLabourPrice", component.get("v.productWrapp.labourPrice"));
                component.set("v.productId", component.get("v.productWrapp.productId"));
            }
            else{
                component.set("v.productError", true);
            }
        });
        $A.enqueueAction(action);
    },
})