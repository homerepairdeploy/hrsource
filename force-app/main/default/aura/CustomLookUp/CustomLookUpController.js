({
    onfocus : function(component,event,helper){
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        helper.searchHelper(component,event,'');
	},
    doInit : function(component, event, helper) {
		        
        helper.clearLookup(component);        
    },
    keyPressController : function(component, event, helper) {
        // get the search Input keyword   
        var getInputkeyWord = component.get("v.SearchKeyWord");
        // check if getInputKeyWord size id more then 0 then open the lookup result List and 
        // call the helper 
        // else close the lookup result List part.   
        if( getInputkeyWord.length > 0 ){
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
            component.set("v.listOfSearchRecords", null ); 
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
        
    },
    onblur : function(component,event,helper){       
        component.set("v.listOfSearchRecords", null );
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    createRoom : function(component, event, helper){
        component.set("v.showCreateRoom" , true); 
        component.set("v.showCreateRoom" , true); 
    },
    // this function invoked when HRHideCreateRoomComp event if triggered.
    handleHideRoomEvent : function(component,event,helper){
        console.log('roomCreatedFlag ' + JSON.stringify(event.getParam("room")));
        var roomCreatedFlag = event.getParam("roomCreatedFlag");
        var selectedRoomFromEvent=event.getParam("room");
        
        
        var toastEvent = $A.get("e.force:showToast");
        // make showCreateRoom attribute to false to hide create room fields/comp.
        if(roomCreatedFlag == 'Yes'){ 
            component.set("v.selectedRecord" , selectedRoomFromEvent);
            
            var forclose = component.find("lookup-pill");
            $A.util.addClass(forclose, 'slds-show');
            $A.util.removeClass(forclose, 'slds-hide');
            
            
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
            
            var lookUpTarget = component.find("lookupField");
            $A.util.addClass(lookUpTarget, 'slds-hide');
            $A.util.removeClass(lookUpTarget, 'slds-show'); 
            
            
            component.set("v.showCreateRoom" , false);
            toastEvent.setParams({
                "title": "Success!",
                "type" : "success",
                "message": "Room created successfully."
            });
            toastEvent.fire();            
        }else{
            component.set("v.showCreateRoom" , false);
             toastEvent.setParams({
                "title": "Error!",
                "type" : "error",
                "message": "Room creation Cancelled or Unsuccessfull."
            });
            toastEvent.fire();
        }            
    },
    // function for clear the Record Selection 
    clear :function(component,event,helper){
        console.log('in...');
        helper.clearLookup(component);
    },
    
    // This function call when the end User Select any record from the result list.   
    handleComponentEvent : function(component, event, helper) {
        
        // get the selected Account record from the COMPONETN event 	 
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        console.log('selectedAccountGetFromEvent >>> '+ JSON.stringify(selectedAccountGetFromEvent));
        component.set("v.selectedRecord" , selectedAccountGetFromEvent); 
        component.set("v.selectedRecordId", selectedAccountGetFromEvent.Id);
        
        component.set("v.selectedRecord" , selectedAccountGetFromEvent); 
        
        var forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
        
        var lookUpTarget = component.find("lookupField");
        $A.util.addClass(lookUpTarget, 'slds-hide');
        $A.util.removeClass(lookUpTarget, 'slds-show');  
        // call the event   
        var compEvent = component.getEvent("updateTimevaluesEventFired");
        // set the clearance flag to the event attribute.  
        compEvent.setParams({"objectName" : component.get("v.objectAPIName")});  
        // fire the event  
        compEvent.fire();
    },
    // automatically call when the component is done waiting for a response to a server request.  
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : false });
        evt.fire();  
    },
    // automatically call when the component is waiting for a response to a server request.
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : true });
        evt.fire();    
    },
})