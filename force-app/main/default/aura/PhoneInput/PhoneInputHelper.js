({
	validatePhone : function(phoneVal) {
        if(!phoneVal) {
            return true;
        }
        if (phoneVal.length != 10){
            return false;
        }
        var phoneno = /(^1300\d{6}$)|(^1800|1900|1902\d{6}$)|(^0[2|3|7|8]{1}[0-9]{8}$)|(^13\d{4}$)|(^04\d{2,3}\d{6}$)/;
        return phoneVal.match(phoneno);
	},
    addErrorMessage: function(message) {
        var messages = [];
        messages.push({"message": message});
        return messages;
    }
})