({
	callApexFunction: function(component, functionName, paramsObj) {
        var that = this;
        var promise = new Promise(function(resolve, reject) {
            var action = component.get(functionName);
            if (paramsObj !== undefined) {
                action.setParams(paramsObj);
            }
            action.setCallback(that, function(response) {
                var state = response.getState();
                if (component.isValid() && state === 'SUCCESS') {
                    resolve(response.getReturnValue());
                } else {
                    reject(Error(that.getErrorMsg(response)));
                }
            });
            $A.enqueueAction(action);
        });
        return promise;
    },
    callApexStorable: function(component, functionName, paramsObj, successFn,errorFn) {
        var that = this;
        var action = component.get(functionName);
        action.setStorable();
        if (paramsObj !== undefined && paramsObj !== null) {
            action.setParams(paramsObj);
        }
        action.setCallback(that, function(response) {
            var state = response.getState();
            if (component.isValid() && state === 'SUCCESS') {
                if(successFn) successFn(response.getReturnValue());
            } else {
                if(errorFn) errorFn(Error(that.getErrorMsg(response)));
            }
        });
        $A.enqueueAction(action);
    },
    createAuraComponent : function(componentName, params){
        var that = this;
        var promise = new Promise(function(resolve, reject) {
            $A.createComponent(
                componentName,
                params,
                function(newComp, status, errorMessage){
                    if (status === "SUCCESS") {
                        resolve(newComp);
                    } else if (status === "INCOMPLETE" || status === "ERROR") {
                        reject(errorMessage);
                    }
                }
            )
        });
        return promise;
    },
    getErrorMsg : function(response) { // returns relevant errors for apex method callback
        if (response.getState() === "ERROR") {
            var errors = response.getError();
            ////this.console.log('errors',errors);
            if (errors) {
                if (errors[0] && errors[0].message) {
                    return errors[0].message;
                }
            }
        }
        return 'Failed with State - ' + response;
    },
    showLoader : function(component) {
        component.set("v.showLoader", true);
    },
    hideLoader : function(component) {
        component.set("v.showLoader", false);
    }, 
    showToast : function(type, title, message,  duration) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({"title": title,
                              "message": message,
                              "type": type,
                              "duration": duration});
        toastEvent.fire();
    },
    navigateToUrl : function(navUrl) {
        let urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({ "url": navUrl });
        urlEvent.fire();
    }
})