({
	doInit : function(component, event, helper) {
	    component.set('v.showLoadingSpinner', true); 
        var action = component.get("c.getClaimRecord");
        action.setParams({            
            'ClaimID' : component.get("v.recordId")
        });

        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null){
             component.set('v.showLoadingSpinner', false); 
            component.set("v.caseWrapper",response.getReturnValue());
            // alert('<<<'+JSON.stringify(component.get("v.caseWrapper")));
            }
        });
       
       $A.enqueueAction(action);
    },
      
      openPds :function(component,event,helper){
        var action = component.get("c.getPdsDetails");
            
    	action.setParams({
    	'ClaimID' : component.get("v.recordId")   
    	 });
      	action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
            var pdsurl = response.getReturnValue();
            var eurl =  encodeURI(pdsurl);
            component.set("v.pdsurl",eurl);
            return eurl;
            }
        });
     
       $A.enqueueAction(action); 
      }

})