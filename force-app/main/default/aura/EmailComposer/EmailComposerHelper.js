({
    getEmailTempaltes : function(component, event) {
        var action = component.get("c.getTemplates");
        var RecID   = component.get("v.recordId");
        action.setParams({"RecID":RecID});  
        action.setCallback(this,function(response){
            var loadResponse = response.getReturnValue();
            console.log('templates..!',loadResponse);            
            if(!$A.util.isEmpty(loadResponse)){                
                component.set('v.templates',loadResponse);                 
            }
        });
        $A.enqueueAction(action);
    },
    getTemplate : function(component, event) { 
        var templId = component.get("v.selTemplate");
        if(!$A.util.isEmpty(templId)) {            
            var action = component.get("c.getTemplateDetails");
            action.setParams({"templteId":templId});            
            action.setCallback(this,function(response){
                component.set("v.showLoader", false);
                var responseVal = response.getReturnValue();
                if(!$A.util.isEmpty(responseVal)){
                    component.set("v.templDetail",responseVal);
                    component.set("v.subjTxt",responseVal.Subject);
                }
            });
            $A.enqueueAction(action);
        } else {
            component.set("v.templDetail", null );
            component.set("v.subjTxt","");
            component.set("v.bodyContent", "");
        }
    },
    clearEmailComposer: function(component, event) {
    	component.set("v.shwSucesMsg", false);
        component.set("v.shoMsg", false);
        component.set("v.selectedLookUpRecords", []);
        component.set("v.selTemplate",'');
        component.set("v.templDetail",[]);
        component.set("v.subjTxt","");
        component.set("v.toExternalEmail","");
        component.set("v.bodyContent","");
        component.find("attachmentModal").find("attId").set("v.selectedRows", []);
	},
    sendEmails : function(component, event) {
        console.log('sendEmails In.. ');  
        var templateId ;
        component.set("v.shoMsg", false);
        component.set("v.message", "");
        var subjMatter = component.get("v.subjTxt");
        var bodyContent = component.get("v.bodyContent");
        var templDetail = component.get("v.templDetail");
       
        if(templDetail) {
            templateId = templDetail.Id;
        } 
        
        var contacts=component.get("v.contacts");
        var listSelectedItems=component.get("v.selectedLookUpRecords");
        var listSelectedItemsForCC=component.get("v.selectedLookUpRecordsForCC");
        console.log(JSON.stringify(listSelectedItemsForCC));
        
        var listExternalEmail=component.get("v.toExternalEmail");
        console.log(listExternalEmail);
        listExternalEmail=listExternalEmail.split(';');
        console.log(listExternalEmail);
        
        if(!$A.util.isEmpty(contacts) || !$A.util.isEmpty(listSelectedItems) || !$A.util.isEmpty(listExternalEmail)){
            console.log("contacts", contacts);            
            var conIdsList=[];
            var emailList=[];
            var emailCCList=[];
            var externalEmailList=[];
            contacts.forEach(function(contact){
               conIdsList.push(contact.Id );
            });
            console.log(JSON.stringify(listSelectedItems));            
            listSelectedItems.forEach(function(emailAdd){
               emailList.push(emailAdd.Email);
            });  
            listExternalEmail.forEach(function(emailAdd){
               externalEmailList.push(emailAdd);
            }); 
            listSelectedItemsForCC.forEach(function(emailAdd){
               emailCCList.push(emailAdd.Email);
            }); 
            console.log(emailList);
            
            if($A.util.isEmpty(templateId) && $A.util.isEmpty(subjMatter) && $A.util.isEmpty(bodyContent)){
            	component.set("v.shoMsg", true);
            	component.set("v.message", "Please provide email Body and Subject");
            }else{
                var selectdAttachments 
                	= component.find("attachmentModal").find("attId").get("v.selectedRows");
				
                console.log('---accIdStr--- ', conIdsList);                
                var action = component.get("c.sendAnEmailMsg");
                action.setParams({"templateId":templateId,
                                  "idList":conIdsList,
                                  "emailList":emailList,
                                  "emailCCList":emailCCList,
                                  "externalEmailList":externalEmailList,
                                  "subj" : !$A.util.isEmpty(subjMatter) ? subjMatter : '',
                                  "contentBody" : !$A.util.isEmpty(bodyContent) ? bodyContent : '',
                                  "attIds" : selectdAttachments,
                                  "claimId" : component.get("v.recordId")
                                  });
                action.setCallback(this,function(response){                    
                    var emailMsgResp = response.getReturnValue();
                    console.log('--emailMsgResp--', emailMsgResp); //isSuccess  errMsg                    
                    if(emailMsgResp.isSuccess){
                      
                        component.set("v.shwSucesMsg", false);
                        component.set("v.shoMsg", false);
                        component.set("v.contacts", []);
                        component.set("v.selectedLookUpRecords", []);
                        component.find("attachmentModal").find("attId").set("v.selectedRows", []);
                        var modal = component.find("FileModal");
                        $A.util.addClass(modal, 'slds-hide');
                        // Success
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "type" : "success",
                            "message":"Email has been sent successfully!"
                        });
                        toastEvent.fire();  
                        $A.get('e.force:refreshView').fire();
                    }
                    else {
                        component.set("v.shoMsg", true);
                        component.set("v.message", emailMsgResp.errMsg);
                    }                    
                });
                $A.enqueueAction(action);
            }
        }else {
            component.set("v.shoMsg", true);
            component.set("v.message", "The message must have at least one To recipient");
        }
        
    }
})