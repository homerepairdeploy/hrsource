({
	CreateCreditMemo : function(component, event, helper) {  
        var spinner = component.find("mySpinner");
		$A.util.toggleClass(spinner, "slds-hide");
        
    	var action = component.get("c.createCreditMemo");        
        action.setParams({            
            'supplierNumber' : component.get('v.supplierInvoiceNo'),
            'recordId' : component.get('v.recordId'),
            'creditMemoDetails' : component.get('v.creditMemoDetails'),
            'isPartialCreditNote' : component.get('v.isPartialCreditNote'),
            'labourAmount' : component.get('v.labourAmount'),
            'materialAmount' : component.get('v.materialAmount')
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                
                if(response.getReturnValue().includes('Success')) {
                    var array = response.getReturnValue().split(":");
                    helper.showToast('success', 'Credit Memo created Successfully');
                    $A.get('e.force:refreshView').fire();
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                      "recordId": array[1]                      
                    });
                    navEvt.fire();

                }
                else {
                    helper.showToast('error', response.getReturnValue());
                    alert(response.getReturnValue());
                }
            }            
            $A.util.toggleClass(spinner, "slds-hide");
        });        
        $A.enqueueAction(action);
	},
	setAmounts : function(component, event, helper) {
	    var materialAmount = component.get('v.materialAmount');
	    var labourAmount = component.get('v.labourAmount');

        var gstAmount = (parseFloat(materialAmount) + parseFloat(labourAmount)) * 0.1;
        var totalAmount = (parseFloat(materialAmount) + parseFloat(labourAmount)) + gstAmount;

        component.set('v.gstAmount', gstAmount);
        component.set('v.totalAmount', totalAmount);
    }
})