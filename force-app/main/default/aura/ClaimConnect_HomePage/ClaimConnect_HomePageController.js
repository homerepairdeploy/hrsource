({
    doInit : function(component, event, helper) {     
        // Delete temporary content records created for the user in view assessment/previous assessment      // 
        //var sPageURL = decodeURIComponent(window.location.search.substring(1)); 
        //alert('url:' + sPageURL);
        helper.delAWSContentDocs(component,event,helper);              
        component.set('v.showMyServAppointments',"true");
    },
    
    handleMenuSelect : function(component, event, helper) {
        var selectedMenuItemValue = event.getParam("value");
        component.set("v.label",selectedMenuItemValue);        
        component.set('v.showMyServAppointments',"false");
        component.set('v.showMyDrftAssessments',"false");
        component.set('v.showMySubAssessments',"false");
        
        component.set('v.searchValue','');
        if(selectedMenuItemValue =='My Service Appointments')
        {
            component.set('v.showMyServAppointments',"true");
            
        }
        else if(selectedMenuItemValue =='My Draft Assessments')
        {            
            component.set('v.showMyDrftAssessments',"true");  
        }
            else if(selectedMenuItemValue =='My Submitted Assessments')
            {
                component.set('v.showMySubAssessments',"true");
                
            }        
    },  
    
    searchKeyChange : function(component, event, helper)
    { 
        var searchKey = component.find("searchKey").get("v.value");
        if((searchKey == '') || (searchKey == null ))
        { component.set("v.searchVal",false);}
        else
        {component.set("v.searchVal",true);}
        
          
        if (component.get('v.showMyServAppointments') =='true')
        {         
          var servAppListComponent = component.find('servAppList'); 
          const Cmp1 = (servAppListComponent.length == null) ? [servAppListComponent] : servAppListComponent;
          Cmp1[0].searchKeyMethod(searchKey);
          //servAppListComponent.searchKeyMethod1(searchKey);   
        }
         else if (component.get('v.showMyDrftAssessments')=='true')
        {
          var drftListComponent = component.find('draftList');
          const Cmp2 = (drftListComponent.length == null) ? [drftListComponent] : drftListComponent;
          Cmp2[0].searchKeyMethod(searchKey);   
        }
        else
        {
          var subAsmtListComponent = component.find('subAssmntList');
          const Cmp3 = (subAsmtListComponent.length == null) ? [subAsmtListComponent] : subAsmtListComponent;
           Cmp3[0].searchKeyMethod(searchKey);   
        }    
          
    },
    
    navigate : function(component, event, helper) {
        var ClmId = event.getParam("ClmRecId");   
        component.set("v.ClaimId",ClmId);        
        var BAId = event.getParam("BARecId");    
        component.set("v.BARecordId",BAId);
        var SApptId = event.getParam("SAppRecId");         
        component.set("v.ServApptId",SApptId);
        
        var viewName = component.get("v.label");      
        
        
        switch (viewName){
            case "My Service Appointments":   
                // Create New record in Staging BA              
                
                helper.createSBARecordandNavigate (component,event, helper);                              
                break;
                
            case "My Draft Assessments" :                               
                var navigateEvent = $A.get("e.force:navigateToComponent");
                navigateEvent.setParams({
                    componentDef: "c:ClaimConnect_AssessmentPage",                     
                    componentAttributes :{'recordId':ClmId, 'BARecordId':BAId,'ServApptId':SApptId}
                });                 
                navigateEvent.fire();                 
                break;
                
            case "My Submitted Assessments":  
                var navigateEvent = $A.get("e.force:navigateToComponent");
                navigateEvent.setParams({
                    componentDef: "c:ClaimConnect_ViewAssessment",                     
                    componentAttributes :{'recordId':ClmId, 'BARecordId':BAId,'ServApptId':SApptId}
                });                 
                navigateEvent.fire();                                 
                break;
        }
    },
})