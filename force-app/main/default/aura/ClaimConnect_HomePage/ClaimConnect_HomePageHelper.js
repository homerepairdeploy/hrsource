({
	delAWSContentDocs : function(component,helper)
    {
        var action = component.get("c.deleteTempContentDocs");   
      
        action.setCallback(this, function(response) 
        {
          var state = response.getState();
          
          if (state == "SUCCESS") { 
           console.log("Content documents created for AWS file deleted for the user");
            
          }
          else
          {
              console.log("Couldn't delete Content documents created for AWS file for the user");
          }
    });
        $A.enqueueAction(action);                 
        
    },
    
     createSBARecordandNavigate: function(component, event, helper) {
        // Create New record in Staging BA    
       
        var ClaimId = component.get("v.ClaimId"); 
        var ServApptId = component.get("v.ServApptId");          
        
        var action = component.get("c.createBARecord");          
        action.setParams({            
            'ClaimId' : ClaimId,
            'ServAppt': ServApptId
        });        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS" || state === "DRAFT") {                      
                var sBA = response.getReturnValue();                  
                var RecId = sBA.Id;                        
                
                component.set("v.BARecordId",RecId);   
                helper.loadSunCorpRoomDetails(component,event,helper);                 
                
               var navigateEvent = $A.get("e.force:navigateToComponent");
                navigateEvent.setParams({
                    componentDef: "c:ClaimConnect_AssessmentPage",                     
                    componentAttributes :{'recordId':ClaimId, 'BARecordId':RecId,'ServApptId':ServApptId}
                });                 
                navigateEvent.fire();  
            }
            else {
                // record is not saved
                 var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Failure",
                    "message": "Assessment Record Initilization Failed" 
                });
                resultsToast.fire();           
            }
        })
        $A.enqueueAction(action); 
    },

     loadSunCorpRoomDetails: function(component, event, helper) {   
        
        var ClaimId = component.get("v.ClaimId"); 
        var BARecordId = component.get("v.BARecordId");         
        
        var action = component.get("c.populateRoomDetails");
        action.setParams({ 'ClaimId' : ClaimId,
                          'BARecordId': BARecordId });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Sun Corp Rooms have been populated successfully!" );                
            }
        }) 
        $A.enqueueAction(action);             
    },
})