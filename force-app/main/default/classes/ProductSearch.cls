/*
 * 
 * 
 This class holds the method implemenaitons for the Base class ProductSearch.
History
<Date>                       <Authors Name>        <Brief Description of Change>
May-26-2022                  Madhukar Reddy(Congnizant)       SD-19 - Required Product Search due to limits on returned values in SalesForce.
------------------------------------------------------------------------------------*/


public class ProductSearch {
     @AuraEnabled(cacheable=true)
        public static List<PricebookEntry> findProducts(String searchKey) {
            ID pBId=[SELECT Id, Name, IsActive FROM PriceBook2 WHERE IsStandard = True AND Name='Standard Price Book' LIMIT 1].Id;

            if(String.isNotBlank(searchKey)){
            String key = '%' + searchKey + '%';
            return [
              SELECT Name,ProductCode FROM PricebookEntry where  isQualified__c=True AND Pricebook2Id =:pBId
                        AND Name LIKE :key LIMIT 100];

        }
            else{
                return [
               SELECT Name,ProductCode FROM PricebookEntry where  isQualified__c=True AND Pricebook2Id =:pBId
                         LIMIT 100];
            }
        }
    }