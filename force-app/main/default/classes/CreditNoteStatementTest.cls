@IsTest
private class CreditNoteStatementTest {
    @IsTest
    static void CreditNotePDF(){
	
	    Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Test');   
        //create a Claim Authority(Added by CRMIT)
        case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id);
        AP_Invoice__c invoice = HomeRepairTestDataFactory.createAPInvoiceWithWorkOrder(wo.id);

        
        
        Test.startTest();
        CreditNoteStatement.CreditNoteStatement(new List<String>{invoice.Id});
        Test.stopTest();

        List<ContentVersion> cv = [SELECT Id FROM ContentVersion];

        system.assertEquals(1, cv.size(), 'CreditNote PDF is Generated');
        
    }
}