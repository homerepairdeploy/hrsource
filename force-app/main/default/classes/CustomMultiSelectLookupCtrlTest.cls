/* =====================================================================================
Type:       Test class
Purpose:    Test cases for CustomMultiSelectLookupCtrl(For LightningFileUpload Component )
========================================================================================*/
@isTest
private class CustomMultiSelectLookupCtrlTest{
    static testMethod void fetchCustomMultiSelectLookupCtrlTest() {
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        List <sObject> returnList = new List <sObject>();
        CustomMultiSelectLookupCtrl.fetchLookUpValuesObject('Claim','Contact',returnList);
        CustomMultiSelectLookupCtrl.fetchLookUpValues('CJ','case',ca.Id);
    }
}