/***************************************************************** 
Purpose: Test class for generating Assessment Report                                                        
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            HomeRepair    23/06/2020      Created      Home Repair Claim System  
*******************************************************************/
@istest 
public class GenerateAssessReportTest { 
   
    
    static testmethod void testBatchWithContactAccountProcess() {  
       GenerateAssessReport.checkTest();
       UploadAssessmentPhotos.checkTest();
    }
    
  
}