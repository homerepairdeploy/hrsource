/**
 * Test class for API Logging Utilities class.
 *
 * @date    2019-06-03
 * @author  Nikhil Jaitly
 */
@isTest
public class NJ_APILoggingTest {

    /**
	 * @method 	apilogUtils_customSettingEnabled_01
	 * @case   	When the logging util is called for first time, and custom setting is not set
     *          by default its set to true and creates a debug log.
	 */
    @isTest static void apilogUtils_customSettingDefault_01() {        
        
        NJ_APILogging.LogDetail logDetails = new NJ_APILogging.LogDetail('apilogUtils_customSettingEnabled', null);
        
        Test.startTest();
            NJ_APILogging.log(logDetails);
        Test.stopTest();
        
        System.assertEquals(1, [Select Id from Mobile_App_Logging__c].size(), 'API Debug Log record not created.');
    }
    /**
	 * @method 	log
	 * @case   	When custom settings is enabled for logging, 
     *          it should create a debug log record.
	 */
     
    @isTest static void apilogUtils_customSettingsEnabled_02() {        
        
        insert new API_Logging_Settings__c(Name='Enable API Logs', Enable_Debug_Log_Messages__c=true);
        NJ_APILogging.LogDetail logDetails = new NJ_APILogging.LogDetail('apilogUtils_customSettingEnabled', null);
        
        Test.startTest();
            NJ_APILogging.log(logDetails);
        Test.stopTest();

        System.assertEquals(true, API_Logging_Settings__c.getInstance().Enable_Debug_Log_Messages__c, 'Logging Custom Settings not set.');
        System.assertEquals(1, [Select Id from Mobile_App_Logging__c].size(), 'API Debug Log record not created.');
    }
    /**
	 * @method 	apilogUtils_customSettingsDisabled_03
	 * @case   	When custom settings is disabled for logging, 
     *          it should NOT create a debug log record.
	 */     
    @isTest static void apilogUtils_customSettingsDisabled_03() {        
        
        insert new API_Logging_Settings__c(Name='Enable API Logs', Enable_Debug_Log_Messages__c=false);
        NJ_APILogging.LogDetail logDetails = new NJ_APILogging.LogDetail('apilogUtils_customSettingEnabled', null);
        
        Test.startTest();
            NJ_APILogging.log(logDetails);
        Test.stopTest();

        System.assertEquals(false, API_Logging_Settings__c.getInstance().Enable_Debug_Log_Messages__c, 'Logging Custom Settings not set.');
        System.assertEquals(0, [Select Id from Mobile_App_Logging__c].size(), 'API Debug Log record created when it shouldnt.');
    }

    /**
	 * @method 	apilogUtils_validateLogFieldValues_04
	 * @case   	When custom settings is disabled for logging, 
     *          it should NOT create a debug log record.
	 */     
    @isTest static void apilogUtils_validateLogFieldValues_04() {        
        RestRequest req = new RestRequest();
        req.requestURI = '/services/apexrest/DemoUrl';  //Request URL
        req.httpMethod = 'PUT';//HTTP Request Type
        req.requestBody = Blob.valueof('{"Key": "Value"}');

        String methodName = 'apilogUtils_customSettingEnabled';
        NJ_APILogging.LogDetail logDetails = new NJ_APILogging.LogDetail(methodName, req);
        
        Test.startTest();
            NJ_APILogging.log(logDetails);
        Test.stopTest();

        List<Mobile_App_Logging__c> logList = [SELECT 
                                                        API_Method_Name__c,
                                                        API_VERB__c,
                                                        Assessment_Appointment__c,
                                                        Assessor__c,
                                                        Claim__c,
                                                        Id,
                                                        Incoming_JSON__c,
                                                        Mobile_App_Version__c,                                                        
                                                        Outgoing_JSON__c,
                                                        Request_Finish_Time__c,
                                                        Request_Start_TIme__c,
                                                        Response_Code__c,
                                                        Response_Error__c,
                                                        ServiceAppointment__c 
                                                        FROM 
                                                    Mobile_App_Logging__c];
        
        System.assertEquals(1, logList.size(), 'API Debug Log record is not created');
        System.assert(logList[0].Incoming_JSON__c!=null, 'Incoming Json is not set');
        //System.assertEquals(UserInfo.getUserId(), logList[0].Assessor__c, 'Incoming Json is not set');
        System.assertEquals(methodName, logList[0].API_Method_Name__c, 'Incorrect method Name set');
    }

    /**
	 * @method 	apilogUtils_validateLogFieldValues_04
	 * @case   	When custom settings is disabled for logging, 
     *          it should NOT create a debug log record.
	 */     
    @isTest static void apilogUtils_validateWithoutRequestBody_05() {        
        RestRequest req = new RestRequest();
        req.requestURI = '/services/apexrest/DemoUrl';  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type        
        String methodName = 'apilogUtils_customSettingEnabled';
        NJ_APILogging.LogDetail logDetails = new NJ_APILogging.LogDetail(methodName, req);
        
        Test.startTest();
            NJ_APILogging.log(logDetails);
        Test.stopTest();

        List<Mobile_App_Logging__c> logList = [SELECT 
                                                        Incoming_JSON__c
                                                FROM 
                                                    Mobile_App_Logging__c];
        
        System.assertEquals(1, logList.size(), 'API Debug Log record is not created');
        System.assert(logList[0].Incoming_JSON__c!=null, 'Incoming Json is not set');
    }
    
}