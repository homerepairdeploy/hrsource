/*********************************************************************************************************** 
Purpose: Perform download of all requires photo's needing resizing via HTTP AWS calls                                                     
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            HomeRepair    17/07/2020      Created      RepairForce Assessment Generation
***********************************************************************************************************/
public class DownloadAndResizeAllImages implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {  
    private String bucketName {get;set;}
    private String hostName {get;set;}
    private String rootPath {get;set;}
    private String fileResolution {get;set;}
    private String fileSite {get;set;}
    private String originalFilePath {get;set;}
    private Map<String, Blob> allImages {get;set;}
    private Map<String, String> allKeys {get;set;}
    public List<S3_FileStore__c> fileStore =new List<S3_FileStore__c>();
    public List<Blob> allImageContent = new List<Blob>();
    
    Public DownloadAndResizeAllImages()
    {
        // Build config
        getConfig();
        this.allImages = New Map<String, Blob>();
        this.allKeys = New Map<String, String>();
        //String transformedFileName;
        
        //For (S3_FileStore__c s3FSObj:s3FileStores)
        //{
        //    system.debug(' File Object: ' + s3FSObj.S3ServerUrl__c);
        //    getResizedAWSImage(s3FSObj);
        //}
        
        //If (this.allImages.size() > 0 && this.allKeys.size() > 0)
        //{
        //    For (String aKey:allKeys.keySet())
        //    {
        //        transformedFileName = aKey.split('&&')[0];
        //        system.debug('** Filename: ' + this.fileResolution + ' - ' + aKey);
        //        createContentVersionForImage(allImages.get(aKey), transformedFileName, this.allKeys.get(aKey));
        //    }
        //}
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Description__c,FileExtension__c,FileName__c,Id,ObjectId__c,OwnerId,Requires_Resizing__c,S3Server_URL_New__c,UploadedBy__c,Uploaded_date__c FROM S3_FileStore__c WHERE Requires_Resizing__c = true ORDER BY CreatedDate ASC NULLS FIRST';
        return Database.getQueryLocator(query);
    }
    
    Private Void getConfig()
    {
        // Get AWS Configuration
        List<AWS_Credential__mdt> lstAws=[Select Bucket_Name__c ,Host_Name__c,Root_Path__c  from AWS_Credential__mdt];
        this.bucketName = lstAws.get(0).Bucket_Name__c;
        this.hostName = lstAws.get(0).Host_Name__c;
        this.rootPath = lstAws.get(0).Root_Path__c;    
        
        this.fileResolution = System.Label.AssessmentPhotoResolution;
        this.fileSite = System.Label.AssessmentPhotoSite;
        
        If (this.fileResolution.length() == 0)
        {
            this.fileResolution='800x800';
        }
        
        If (this.bucketName.length() > 0 && this.hostName.length() > 0)
        {
            this.originalFilePath = 'https://' + this.bucketName + '.' + this.hostName + '/';
            system.debug('originalFilePath: ' + this.originalFilePath);
        } else
        {
            this.originalFilePath = 'error';
        }
    }
    
    //Public Void getResizedAWSImage(S3_FileStore__c fileStoreObj)
    public void execute(Database.BatchableContext BC, List<S3_FileStore__c> scope) 
    {
        
        for(S3_FileStore__c fileStoreObj:scope)
        {
            String url = buildS3ResizeURL(fileStoreObj);
            // Get and clean filename
            String fileName = fileStoreObj.FileName__c;
            fileName = fileName.replace('%20', ' ');
            fileName = fileName.replace('+', ' ');
            
            String objId = fileStoreObj.ObjectId__c;
            
            If (url.length()>0)   
            {
                HttpRequest req = new HttpRequest();
                HttpResponse imageRes = New HttpResponse();
                
                url = url.replace(' ', '+');
                url = url.replace('%20', '+');
                url=url+'&mode=dev';
                req.setEndpoint(url);
                req.setMethod('GET');
                
                Http http = new Http();
                HttpResponse res= http.send(req);

                system.debug(' Response Location: ' + res.getHeader('location'));
                
                // Send secondary request if able to pull down image blob
                If(res.getStatusCode() == 301 || res.getStatusCode() == 302)
                {    
                    req.setEndpoint(res.getHeader('location'));
                    
                    imageRes = New Http().send(req);
                }
                
                blob responseBody = imageRes.getBodyAsBlob();
                system.debug('Header:' + imageRes.getHeader('Content-Type'));
                
                If (responseBody.size() > 0){
                    system.debug('** FOUND: ' + fileName);
                    this.allImages.put(fileName + '&&' + fileStoreObj.Id, responseBody);
                    this.allKeys.put(fileName + '&&' + fileStoreObj.Id, objId);  
                    createContentVersionForImage(responseBody, fileName, objId);
                }           
                fileStoreObj.Requires_Resizing__c = false;
                fileStore.add(fileStoreObj);
            }
            
        }
        
        if (fileStore.size() > 0){
            update fileStore;     
        } 
    }
    
    Private Void createContentVersionForImage(Blob imageContent, String FileName, String objId){
        
  
        // Perform checks
        // 
        String caseId;
        
        List<Case> caseObj = [SELECT Id FROM Case WHERE Id = :objId LIMIT 1];
        If (caseObj.size() == 0)
        {
            List<Room__c> roomObj = [SELECT Claim__r.Id FROM Room__c WHERE Id = :objId LIMIT 1];    
            If (roomObj.size() > 0)
            {
                caseId = roomObj[0].Claim__r.Id;
            }
        } else
        {
            caseId = caseObj[0].Id;
        }
        
        ContentVersion imageVer = New ContentVersion();
        
        String imageTitle = FileName.Replace('.jpg', '');
        
        imageVer.ContentLocation = 'S';
        imageVer.Description = 'Assessment_Image';
        imageVer.PathOnClient = fileResolution + '-' + FileName;
        imageVer.Title = fileResolution + '-' + FileName;
        imageVer.VersionData = imageContent;

        
        insert imageVer;
        
        Id imageVerId = imageVer.Id;        
        imageVer = [Select ContentDocumentId FROM ContentVersion WHERE Id = :imageVerId];
        
        ContentDocumentLink imageLink = New ContentDocumentLink();
        
        imageLink.ContentDocumentId = imageVer.ContentDocumentId;
        imageLink.LinkedEntityId = caseId;
        imageLink.ShareType = 'V';      
        insert imageLink;
    } 
    
    Private String buildS3ResizeURL(S3_FileStore__c s3FSObj)
    {
        String originalUrl=s3FSObj.S3Server_URL_New__c; 
        String s3URL; String baseFileLocation; String urlFileResolution;
        
        originalFilePath = 'https://' + this.bucketName + '.' + this.hostName + '/';
        
        System.debug('Site Setting: ' + this.fileSite);
        System.debug('Resolution Setting: ' + this.fileResolution);
        //System.debug('Original URL: ' + originalUrl);
        
        urlFileResolution='Site_ImageResize?path=' + this.fileResolution;
        
        baseFileLocation = originalUrl.replace(this.originalFilePath, '');
        s3URL = this.fileSite + urlFileResolution + '/' + baseFileLocation;
        s3URL = s3URL.replace('%20', '+');
        
        system.debug('New URL: ' + s3URL);
        
        return s3URL;
    }
    
    public void finish(Database.BatchableContext bc)
    {
        system.debug('finished Download/Resize');
        if(! Test.isRunningTest() ){
            //Database.executeBatch(new MarkPhotoCompleteAndDeleteDraft(),1);
            Database.executeBatch(new GenerateAssessReport(),1);
        }
    }
   
        
    public static void checkTest(){
     Integer i=0;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
         i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
       
       
       
        
    
    
    
    }    
}