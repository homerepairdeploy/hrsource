/**
 * This class is handler class for variation trigger
 * @CreatedDate : 01/02/2019
 * @Author : Nikhil
 */
public without sharing class VariationHandler implements ITrigger {

    public void bulkBefore() {
        if (Trigger.isUpdate) {
            Map<Id, Variation__c> mapNewVariation = (Map<Id, Variation__c>)Trigger.newMap;
            Map<Id, Variation__c> mapOldVariation = (Map<Id, Variation__c>)Trigger.oldMap;
            List<Variation__c> varAdditionalScope = new List<Variation__c>();
            List<Variation__c> varWOLI = new List<Variation__c>();
            for (Variation__c var : mapNewVariation.values()) {
                if (var.Variation_Status__c != mapOldVariation.get(var.Id).Variation_Status__c && var.Variation_Status__c == NJ_Constants.VARIATION_STATUS_APPROVED) {
                    varWOLI.add(var);
               // commented as part of HRD-603
               /*     if (var.Variation_Reason__c == NJ_Constants.VARIATION_REASON_ADDITIONALSCOPE) {
                        varAdditionalScope.add(var);
                    }*/
                }
            }
            if (!varWOLI.isEmpty()) {
                VariationHelper.createWorkOrderLineItem(varWOLI);
            }
            /*if (!varAdditionalScope.isEmpty()) {
                VariationHelper.createServiceAppointment(varAdditionalScope);
            }*/
        }

    }
    public void bulkAfter() {
        Map<Id, Variation__c> mapOldVariation = (Map<Id, Variation__c>)Trigger.oldMap;
        Map<Id, Variation__c> mapNewVariation = (Map<Id, Variation__c>)Trigger.newMap;
        List<Variation__c> varWithWO = new List<Variation__c>();
        List<Variation__c> varWithSA = new List<Variation__c>();
        Variation__c oldVariation, newVariation;
        if (Trigger.isInsert) {
            System.debug('------>hereworkOrder ' + newVariation);

            if (!mapNewVariation.isEmpty()) {
                VariationHelper.submitVariationForApproval(mapNewVariation.values());
                for (Variation__c var : mapNewVariation.values()) {
                    if (var.Work_Order__c != null) {
                        varWithWO.add(var);
                    }
                    if (var.Service_Appointment__c != null) {
                        varWithSA.add(var);
                    }
                }
               /* if (!varWithWO.isEmpty()) {
                    VariationHelper.lockUnlocaWOAndSA(varWithWO, VariationHelper.LockUnlockOperation.LOCK);
                }
                System.debug('Nikhil varWithSA ' + varWithSA);
                if (!varWithSA.isEmpty()) {
                    VariationHelper.lockUnlocaWOAndSA(varWithSA,  VariationHelper.LockUnlockOperation.LOCK);
                }*/
            }
        }
        if (Trigger.isUpdate) {
            List<Variation__c> varAdditionalScope = new List<Variation__c>();
            
            //  HRD-104 – Work Order Invoice Fields Start
            if (!mapNewVariation.isEmpty()) {
                VariationHelper.updateVariationAmount(mapNewVariation.values());
            } 
            //  HRD-104 – Work Order Invoice Fields End
            
            for (Variation__c var : mapNewVariation.values()) {
                oldVariation = mapOldVariation.get(var.Id);
                newVariation = var;
                if (newVariation.Variation_Status__c != oldVariation.Variation_Status__c) {
                    if (newVariation.Variation_Status__c == NJ_Constants.VARIATION_STATUS_APPROVED || newVariation.Variation_Status__c == NJ_Constants.VARIATION_STATUS_REJECTED) {
                        if (newVariation.Variation_Status__c == NJ_Constants.VARIATION_STATUS_APPROVED && newVariation.Variation_Reason__c == NJ_Constants.VARIATION_REASON_ADDITIONALSCOPE) {
                            varAdditionalScope.add(var);
                        }
                        if (var.Work_Order__c != null) {
                            varWithWO.add(var);
                        }
                        if (var.Service_Appointment__c != null) {
                            varWithSA.add(var);
                        }

                    }
                }
            } 
           /* if (!varWithWO.isEmpty()) {
                VariationHelper.lockUnlocaWOAndSA(varWithWO, VariationHelper.LockUnlockOperation.UNLOCK);
            }

            if (!varWithSA.isEmpty()) {
                VariationHelper.lockUnlocaWOAndSA(varWithSA,  VariationHelper.LockUnlockOperation.UNLOCK);
            }
            VariationHelper.createTaskForApprovedRejectVariation(mapNewVariation.values(),  mapOldVariation);  */
        }

    }
    public void beforeInsert(SObject so) {}
    public void afterInsert(SObject so) {
    }
    public void beforeUpdate(SObject oldSo, SObject so) {}
    public void beforeUnDelete(SObject so) {}
    public void afterUpdate(SObject oldSo, SObject so) {
    }
    public void beforeDelete(SObject so) {}
    public void afterDelete(SObject so) {}
    public void afterUnDelete(SObject so) {}
    public void andFinally() {}
}