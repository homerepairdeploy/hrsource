/***************************************************************** 
Purpose: Upload Documents                                                       
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            HomeRepair    23/06/2020      Created      Home Repair Claim System  
*******************************************************************/
public class UploadAssessmentPhotos implements Database.Batchable<sObject>, Database.allowsCallOuts {
    
    private String bucketName {get;set;}
    private String methodName {get;set;}
    private String hostName {get;set;}
    private String rootPath {get;set;}
    private Set<Id> BAIdSet=new Set<Id>();
    private Set<Id> SRIdSet=new Set<Id>();
    private Set<Id> contentIdset=new Set<Id>();
    private Map<Id,Id> contentLinkEntMap=new Map<Id,Id>();
    private Map<Id,Id> BAClaimIdMap=new Map<Id,Id>();
    private Map<Id,string> SRIdNameMap=new Map<Id,String>();
    private Map<Id,Id> SRIdCaseMap=new Map<Id,Id>();
    private Map<string,Id> RoomsMap = new Map<string,Id>();
    public List<S3_FileStore__c> fileStore =new List<S3_FileStore__c>();  
    
    public UploadAssessmentPhotos(){
        
        set<Id> SRBAIdset=new set<Id>();
        set<Id> caseIdset=new set<Id>();
        List<AWS_Credential__mdt> lstAws=[Select Bucket_Name__c ,Host_Name__c,Root_Path__c  from AWS_Credential__mdt];
        bucketName=lstAws.get(0).Bucket_Name__c;
        hostName=lstAws.get(0).Host_Name__c;
        rootPath=lstAws.get(0).Root_Path__c;
        methodName='PUT';
        Datetime currTimeMinusFive = Datetime.now().addMinutes(-5);
        list<AssessmentReports__c> lstAssrRepts=new list<AssessmentReports__c>();
        
        // JP Modified : Added predicate to not gather Assessment processing holders when the Staging_Assessor_BA__c is null
        if( Test.isRunningTest() )
            lstAssrRepts=[select id,Status__c,Case__c,Service_Appointment__c,ErrorMsg__c,Staging_Assessor_BA__c,PhotosUploaded__c from AssessmentReports__c where PhotosUploaded__c=false AND Staging_Assessor_BA__c != null];
        else
            lstAssrRepts=[select id,Status__c,Case__c,Service_Appointment__c,ErrorMsg__c,Staging_Assessor_BA__c,PhotosUploaded__c from AssessmentReports__c where PhotosUploaded__c=false AND CreatedDate <= :currTimeMinusFive AND Staging_Assessor_BA__c != null];
        
        for(AssessmentReports__c  asrc:lstAssrRepts){
            BAIdSet.add(asrc.Staging_Assessor_BA__c);
            BAClaimIdMap.put(asrc.Staging_Assessor_BA__c,asrc.case__c);
            caseIdset.add(asrc.case__c);
        }
        
        system.debug('BAIdset in start ' +BAIdset);
        
        for(Room__c r:[Select Name,Id from Room__c where Claim__c =:caseIdset]){
            RoomsMap.put(r.Name,r.id);
        }
        
        for(Staging_Assessor_sow_rooms__c StagingRooms : [Select id,Name,UniqueRoomId__c,Staging_Assessor_BA__r.Case__c from Staging_Assessor_sow_rooms__c where Staging_Assessor_BA__c=:BAIdSet]){
            SRIdSet.add(StagingRooms.id);
            SRIdNameMap.put(StagingRooms.id,StagingRooms.name);
            SRIdCaseMap.put(StagingRooms.id,StagingRooms.Staging_Assessor_BA__r.Case__c);
        }
        SRBAIdset.addAll(BAIdset);
        SRBAIdset.addAll(SRIdset);  
        If (!SRBAIdset.isEmpty()){  
            for(ContentDocumentLink link:[SELECT ContentDocumentId,LinkedEntityId FROM ContentDocumentLink where LinkedEntityId IN :SRBAIdset]){
                contentIdSet.add(link.ContentDocumentId);
                contentLinkEntMap.put(link.ContentDocumentId,link.LinkedEntityId); 
            } 
        }    
    }    
    
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query;
        If (!Test.IsRunningTest()){
            query='SELECT VersionData,Title,ContentDocumentId,ContentSize,Description,CreatedById,FileExtension FROM ContentVersion WHERE ContentDocumentId = :contentIdSet AND IsLatest = true and AWSUploaded__c=false' ;
        } else {
            query='SELECT VersionData,Title,ContentDocumentId,ContentSize,Description,CreatedById,FileExtension FROM ContentVersion WHERE ContentDocumentId = :contentIdSet AND IsLatest = true and AWSUploaded__c=false LIMIT 1' ;
        }
        return Database.getQueryLocator(query);
    }
    
    
    public void execute(Database.BatchableContext BC, List<ContentVersion > scope) {
        list<ContentVersion> updateContentVersionlst=new list<ContentVersion>();
        system.debug('hostname'+hostname);
        set<Id> contentIdlst=new set<Id>();
        for(ContentVersion attach:scope)
        {                 
            contentIdlst.add(attach.contentdocumentId);
            String filename=attach.Title;
            string objectName;string parentId;string rName;Boolean isFileUploaded;
            If (BAIdSet.contains(contentLinkEntMap.get(attach.ContentDocumentId))){
                objectName='Case';
                parentId=BAClaimIdMap.get(contentLinkEntMap.get(attach.ContentDocumentId));
                rName='UnKnown';
                
            }
            else if (SRIdSet.contains(contentLinkEntMap.get(attach.ContentDocumentId))){
                objectName='Room__c';
                parentId=RoomsMap.get(SRIdNameMap.get(contentLinkEntMap.get(attach.ContentDocumentId)));
                rName=SRIdNameMap.get(contentLinkEntMap.get(attach.ContentDocumentId));
                
            }
            system.debug('ParentId'+ParentId);
            system.debug('ObjectName'+ObjectName);
            system.debug('rName'+rName);
            filename=filename.replaceAll('[^a-zA-Z0-9]','%20');
            rName= rName.replaceAll('[^a-zA-Z0-9]','%20');
            String fileextension=attach.FileExtension;
            String fileID = attach.ContentDocumentId;            
            String fullfilename=String.valueOf(UserInfo.getUserId()).substring(0, 15)+'%20-%20'+UserInfo.getFirstName()+'%20'+UserInfo.getLastName()+'%20-%20'+filename.toLowerCase()+ '.'+fileextension.toLowerCase();           
            
            string filepath= 'https://' + this.bucketName + '.' + this.hostName + '/' +  this.rootPath +'/'+Objectname+'/'+String.valueOf(parentId).substring(0, 15)+'%20-%20'+rName+'/'+fullfilename;             
            
            ClaimConnect_AWSConnector connector=new ClaimConnect_AWSConnector();   
            S3_FileStore__c fStore=new S3_FileStore__c();
            fStore.ContentSize__c = attach.ContentSize;                
            fStore.Description__c = attach.Description;
            fStore.FileExtension__c = attach.FileExtension.toLowerCase();
            fStore.FileName__c= filename + '.' + fileextension;
            fStore.ObjectId__c=parentId;  
            fStore.S3ServerUrl__c=filepath;
            fStore.Uploaded_date__c = Date.today();
            fStore.UploadedBy__c = userInfo.getUserId();
            fileStore.add(fStore); 
            if (!Test.isRunningTest()){
                isFileUploaded = connector.uploadFileToAWS(parentId, objectName,rName, this.rootPath, attach); 
            } 
            else isFileUploaded=true;
            if (isFileUploaded){
                ContentVersion cv=new ContentVersion();
                cv.id=attach.id;
                cv.AWSUploaded__c=true;
                updateContentVersionlst.add(cv);
                
            }                 
        }
        
        if (updateContentVersionlst.size()> 0) update updateContentVersionlst; 
        if (fileStore.size() > 0){
            insert fileStore;     
        } 
    }
    
    public void finish(Database.BatchableContext bc){ 
        
        if(!Test.isRunningTest())
        {
        	Database.executeBatch(new DownloadAndResizeAllImages(), 1);
        }
        //Database.executeBatch(new MarkPhotoCompleteAndDeleteDraft(),1);
        //Database.executeBatch(new GenerateAssessReport(),1);
        
    }
    
    
    
    
    
    
    
}