public class AuthorityActivityTrackerFactory implements Database.Batchable<sObject> {
    private static final String PROCESS_STATUS_PENDING = 'Pending';
    private static final String PROCESS_STATUS_COMPLETED = 'Completed';
    private static final String PROCESS_STATUS_ERROR = 'Error';
    
    public Map<String, Id> BASE_USER_CONFIG = new Map<String, Id>();
    public Map<String, Integer> BASE_HOMEREPAIR_STATUS_STEPS = new Map<String, Integer>();
    public Map<String, Integer> BASE_SUNCORP_STATUS_STEPS = new Map<String, Integer>();
    
    private map<id,Case> authorityUpdateList = new Map<id, case>();
    //private list<Case> authorityUpdateList = new List<Case>();
    
    public AuthorityActivityTrackerFactory(){
        getConfig();
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        /*
* Created By: Jason Parr (HomeRepair)
* Created Date: 23-08-2020
* 
* Used to create a new case comment for authorities that are of a
* status that are to be excluded or require action from updates. 
* 
* As of creating these are:
* - Completed
* - Reissued
* - Cancelled
* - Rejected
* - Submitted
*/
        String query;
        If (!Test.IsRunningTest()){
            query='Select Id, Case__r.Id, Case__r.CaseNumber, Case__r.Parent.Id, Changed_by_User__c, Changed_by_User__r.Name, Date_Time_Stamp__c, Source__c, Status_Changed_From__c, Status_Changed_To__c FROM Authority_Activity_Tracker__c WHERE Processing_Status__c = :PROCESS_STATUS_PENDING ORDER BY Case__c, Date_Time_Stamp__c ASC' ;
        } else {
            query='Select Id, Case__r.Id, Case__r.CaseNumber, Case__r.Parent.Id, Changed_by_User__c, Changed_by_User__r.Name, Date_Time_Stamp__c, Source__c, Status_Changed_From__c, Status_Changed_To__c FROM Authority_Activity_Tracker__c ORDER BY Case__c, Date_Time_Stamp__c ASC LIMIT 1' ;
        }
        return Database.getQueryLocator(query);
    }
    
    public void getConfig(){
        /*
		* Created By: Jason Parr (HomeRepair)
		* Created Date: 23-08-2020
		* 
		* Used to create a new case comment for authorities that are of a
		* status that are to be excluded or require action from updates. 
		*/
        
        List<user> automationUserList = [SELECT Id FROM User WHERE Name = 'Developer Homerepair'];
		If(automationUserList.size() > 0) BASE_USER_CONFIG.put('automationUser', automationUserList[0].Id);
        
        //If(hrClaimsUser.isEmpty()) BASE_USER_CONFIG.put('User', hrClaimsUser[0].Id);
        //If(hrActivityRecordType.isEmpty()) BASE_USER_CONFIG.put('RecordType', hrActivityRecordType[0].Id);
        
        // Setup steps - HomeRepair
        BASE_HOMEREPAIR_STATUS_STEPS.put('New', 1);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Pending', 2);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Assessment Scheduled', 3);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Assessment Completed', 4);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Submitted', 5);
        BASE_HOMEREPAIR_STATUS_STEPS.put('acceptedInWork', 6);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Trades Scheduled', 7);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Work Orders Completed', 8);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Rejected', 9);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Maintenance Hold', 10);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Closed', 11);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Rejected', 90);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Declined', 91);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Cancelled', 92);
        
        // Setup steps - SunCorp
        BASE_HOMEREPAIR_STATUS_STEPS.put('New', 1);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Pending', 2);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Assessment Scheduled', 3);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Assessment Completed', 4);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Submitted', 5); 
        BASE_HOMEREPAIR_STATUS_STEPS.put('acceptedInWork', 6);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Trades Scheduled', 7);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Work Orders Completed', 8);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Rejected', 9);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Maintenance Hold', 10);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Closed', 11);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Rejected', 90);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Declined', 91);
        BASE_HOMEREPAIR_STATUS_STEPS.put('Cancelled', 92);        
    }   
    
    public void execute(Database.BatchableContext BC, List<Authority_Activity_Tracker__c> scope) {
        list<Authority_Activity_Tracker__c> trackList = new list<Authority_Activity_Tracker__c>();
        list<Authority_Activity_Tracker__c> fastTagList = new list<Authority_Activity_Tracker__c>();
        
        for (Authority_Activity_Tracker__c aATTagging : scope){
            aATTagging.Processing_Status__c = 'Processing';
            fastTagList.add(aATTagging);
        }
        
		update fastTagList;
        
        for (Authority_Activity_Tracker__c aAT : fastTagList){
            try{
                if(aAT.Source__c != 'HomeRepair' && BASE_USER_CONFIG.get('automationUser') != null) aAT.Changed_by_User__c = BASE_USER_CONFIG.get('automationUser');
            	processTrackerRules(aAT);
            
            	aAT.Processing_Status__c = 'Completed';    
            } catch(Exception e){
                System.debug('The following exception has occurred: ' + e.getMessage());
                aAT.Processing_Status__c = 'Error';
                aAT.Error_Message__c = e.getMessage();
            }
            
            trackList.add(aAT);
        }
        
        update trackList; 
        
        If(!authorityUpdateList.isEmpty()){
            //List<Case> allUpdates = New List<Case>();
            
            //for(id key:authorityUpdateList.keySet()){
            //    system.debug('**--> WTF THE DATE IS === ' + String.valueOf(authorityUpdateList.get(key).Submitted_date__c));
            //    allUpdates.add(authorityUpdateList.get(key));
            //}
            //update allUpdates;
            update authorityUpdateList.values();
        }            
    }
    
    @TestVisible
    Private void processTrackerRules(Authority_Activity_Tracker__c aAT){
        /*
* Created By: Jason Parr (HomeRepair)
* Created Date: 07-08-2020
* 
* Used to create a new case comment for authorities that are of a
* status that are to be excluded or require action from updates. 
* 
* As of creating these are:
* - Completed
* - Reissued
* - Cancelled
* - Rejected
* - Submitted
*/
        String newStatus = aAT.Status_Changed_To__c; 
        String baseClient = aAT.Source__c;
        
        List<Authority_Activity_Tracker_Config__c> baseConfigList = [SELECT Allowed_Backstep__c,Allowed_HomeRepair_Backstep_Value__c,Allowed_HomeRepair_Skip_Value__c,Case_Comment_Contents__c,Client__c,Has_Case_Comment__c,Has_Specific_Rules__c,Has_Task__c,Has_Vendor_Note__c,Id,IsDeleted,Is_Allowed_Value__c,Name,Status__c,Task_Level__c,Task_Subject__c,Vendor_Note_Contents__c,Vendor_Note_Type__c FROM Authority_Activity_Tracker_Config__c WHERE Status__c = :newStatus AND Client__c = :baseClient LIMIT 1];
        
        If(!baseConfigList.isEmpty()){
            Authority_Activity_Tracker_Config__c baseConfig = baseConfigList[0];
            If(baseConfig.Is_Allowed_Value__c){
                generateCaseComment(aAT, baseConfig);
                //system.debug('test1');
                
                If(baseConfig.Has_Specific_Rules__c)
                {
                    system.debug('special rules');
                    hasSpecificRules(aAT, baseConfig);
                } else {
                	If(baseConfig.Has_Task__c){
                    	system.debug('test2');
                        generateCaseTask(aAT, baseConfig);
                	}
                
                	If(baseConfig.Has_Vendor_Note__c)
                	{
                    	system.debug('test3');
                    	generateVendorNote(aAT, baseConfig);
                	}
				}	
            }
        }        
    }
    
    @TestVisible
    Private void hasSpecificRules(Authority_Activity_Tracker__c aAT, Authority_Activity_Tracker_Config__c aATC){
    	
        If(aAT.Status_Changed_To__c == 'Submitted' && aAT.Source__c == 'HomeRepair'){            
            checkNatHazUnderValue(aAT, aATC);
            system.debug('New rules');
        }
        autoUpdateValue(aAT, aATC);
    }
    
    @TestVisible
    Private void autoUpdateValue(Authority_Activity_Tracker__c aAT, Authority_Activity_Tracker_Config__c aATC){
        Case parentUpdate;
        Boolean isValidAuth = false;
        DateTime currentDateTime = system.now();
        
        parentUpdate = getRequestedAuthority(aAT); 
        If(parentUpdate != null){
        	    		
            If(aAT.Status_Changed_To__c == 'Submitted' && aAT.Source__c == 'HomeRepair'){
                If(parentUpdate.submitted_date__c==null){
                    parentUpdate.submitted_date__c = system.now();
                } 	
				system.debug(' **--> Setting submitted date to : ' + String.valueOf(parentUpdate.Submitted_date__c));                
                system.debug(' **--> Expecting : ' + String.valueOf(currentDateTime));
                list<AR_Invoice__c> arlst=[select Id from AR_Invoice__c where Invoice_Type__c='Assessment Fee' and Status__c!='Rejected' and Authority__c=:parentUpdate.Id];

        		If (parentUpdate.Job_Type__c == 'assessment' || parentUpdate.Job_Type__c == 'Quote' || parentUpdate.Job_Type__c == 'report' ) isValidAuth=true;
				
                // Due for release with distribution sets
                If(arlst.IsEmpty() && isValidAuth){
                	hrARFeeInvoiceCreationService.createARFeeInvoice(parentUpdate);    
                }
                
            }
            
            If(aAT.Status_Changed_To__c == 'Submitted' && aAT.Source__c == 'SunCorp'){
                if(parentUpdate.submit_acknowledged_date__c==null){
                	parentUpdate.Submit_Acknowledged_date__c = system.now();    
                }
            }
            
            If(aAT.Status_Changed_To__c == 'acceptedInWork' && aAT.Source__c == 'SunCorp'){
                if(parentUpdate.verified_date__c==null){
                	parentUpdate.Verified_date__c = system.now();            
                }
            }                
            authorityUpdateList.put(aAT.Id, parentUpdate);
        }   
    }
    
    @TestVisible
    Private Case getRequestedAuthority(Authority_Activity_Tracker__c aAT){
        List<Case> parentUpdateList = New List<Case>();
        
		If(!authorityUpdateList.containsKey(aAT.Id)){
           	parentUpdateList = [SELECT Id, Status, Job_Type__c, ParentId, parent.Policy__r.State__c, Is_Event__c, Submitted_date__c, Submit_Acknowledged_date__c, Verified_date__c, parent.Cause__c, parent.Claim_Proceeding__c FROM Case WHERE Id = :aAT.Case__c];
        } else {
            parentUpdateList.add(authorityUpdateList.Get(aAT.id));
        }
        
        return parentUpdateList[0];
    }
    
    @TestVisible
    Private void checkNatHazUnderValue(Authority_Activity_Tracker__c aAT, Authority_Activity_Tracker_Config__c aATC){
        Case parentUpdate;
        Decimal totalVal = 0.00;
        
        parentUpdate = getRequestedAuthority(aAT); //parentUpdateList[0];

        If(parentUpdate != null && parentUpdate.parent.Claim_Proceeding__c == 'Yes' && parentUpdate.parent.Cause__c == 'Natural Hazard'){ 
        		AggregateResult[] woVal = [SELECT SUM(Total_Amount_Inc_GST_NEW__c)totalVal FROM WorkOrder WHERE Authority__r.Id = :aAT.Case__r.Id AND Total_Amount_Inc_GST_NEW__c > 0 AND Status NOT IN ('Cancelled') AND Authority__r.parent.Cause__c = 'Natural Hazard' GROUP BY Authority__c];
       		
	        	If(woVal.size() > 0){
    	        	totalVal = Decimal.valueOf(String.ValueOf(woVal[0].get('totalVal')));
        		}
            
        		//ject totalVal = woVal[0].get('totalVal');
        
        
	        	//List<Case> parentUpdateList = New List<Case>();
        
    	    	If(totalVal < 20000.00){   
            	    system.debug(' **--> Checking if this is blank : ' + String.valueOf(parentUpdate.Submitted_date__c));                
                	parentUpdate.Status = 'acceptedInWork';
	                //authorityUpdateList.Get(aAT.id).Status = 'acceptedInWork';
    	            system.debug(' **--> What about now? : ' + String.valueOf(parentUpdate.Submitted_date__c));
        	        //authorityUpdateList.put(parentUpdate.Id, parentUpdate);
            	    authorityUpdateList.put(aAT.Id, parentUpdate);
                        
	            	String baseString = 'Submitted Authority is under $20,000. Automatically approving.';
    	    		CaseComment nCC = New CaseComment();
        			nCC.CommentBody = baseString;
                    
                    If (!Test.isRunningTest())
                    {
                    	nCC.ParentId = aAT.Case__r.Parent.Id;                
                    } else {
               			Id hrClaimRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Claim').getRecordTypeId();                
               			Case newCase = [SELECT Id FROM Case WHERE RecordTypeId = :hrClaimRecId LIMIT 1];
        				nCC.ParentId = newCase.Id;  
                    }
        			
            
	            	insert nCC;
            
    	        	generateVendorNote(aAT, aATC);
        	    }
        	}
			
        system.debug('Nat Haz rule');
    }
    
    @TestVisible
    Private void generateCaseComment(Authority_Activity_Tracker__c aAT, Authority_Activity_Tracker_Config__c aATC){
        /*
* Created By: Jason Parr (HomeRepair)
* Created Date: 07-08-2020
* 
* Used to create a new case comment for authorities that are of a
* status that are to be excluded or require action from updates. 
* 
* As of creating these are:
* - Completed
* - Reissued
* - Cancelled
* - Rejected
* - Submitted
*/
        String commentString = aATC.Case_Comment_Contents__c;
        String baseString = 'The Status of AUTHORITY "[CASE NUMBER]" has changed';
        
        If(!String.IsBlank(aAT.Status_Changed_From__c)){
        	If(aAT.Source__c == 'HomeRepair'){
        		baseString = baseString + ' from STATUS: "[PREVIOUS STATUS]" to STATUS: "[NEW STATUS]"';
        	} else {
            	baseString = baseString + ' from CLIENT STATUS: "[PREVIOUS STATUS]" to CLIENT STATUS: "[NEW STATUS]"';
        	}
        } else {
        	If(aAT.Source__c == 'HomeRepair'){
        		baseString = baseString + ' to STATUS: "[NEW STATUS]"';
        	} else {
            	baseString = baseString + ' to CLIENT STATUS: "[NEW STATUS]"';
        	}            
        }
        
        baseString = baseString + ' by ' + aAT.Source__c;
        If (!test.isRunningTest()) 
        {    baseString = baseString + ' (' + aAT.Changed_by_User__r.Name + ').';
        	baseString = baseString.replace('[CASE NUMBER]', aAT.Case__r.CaseNumber);
        } else
        {
            baseString = baseString + ' (Joe Blogs).';
        	baseString = baseString.replace('[CASE NUMBER]', '12345');
        }
        If(!String.isBlank(aAT.Status_Changed_From__c)){
        	baseString = baseString.replace('[PREVIOUS STATUS]', aAT.Status_Changed_From__c);    
        }
        baseString = baseString.replace('[NEW STATUS]', aAT.Status_Changed_To__c);
        
        If(aATC.Has_Case_Comment__c){
            baseString = baseString + ' ' + commentString;
        }
        
        CaseComment nCC = New CaseComment();
        nCC.CommentBody = baseString;
        
        If(!test.isRunningTest())
        {
        	nCC.ParentId = aAT.Case__r.Parent.Id; //aAT.Case__r.Id;    
        } else {
            If(test.isRunningTest()) system.debug(aAT);
            nCC.ParentId = aAT.Case__c; //aAT.Case__r.Id;    
            
        }
        
        
        
        system.debug('**JPCHECK->CaseComment.CommentBody=' +baseString);    
        //try {
        insert nCC;
        //} catch(DmlException e) {
        //    System.debug('The following exception has occurred (CaseComment): ' + e.getMessage());
        //}
    }
    
    @TestVisible
    private void generateCaseTask(Authority_Activity_Tracker__c aAT, Authority_Activity_Tracker_Config__c aATC){
        /*
		* Created By: Jason Parr (HomeRepair)
		* Created Date: 07-08-2020
		* 
		* Used to create a new case comment for authorities that are of a
		* status that are to be excluded or require action from updates. 
		* 
		* As of creating these are:
		* - Completed
		* - Reissued
		* - Cancelled
		* - Rejected
		* - Submitted
		*/
        
        // Get HR Claims User/Activity
        String subjectString = aATC.Task_Subject__c;
        String commentString = aATC.Task_Level__c;
        
        list<User> hrClaimsUser=[Select Id FROM User WHERE Email LIKE 'hrclaims@homerepair.com.au%' LIMIT 1];
        list<RecordType> hrActivityRecordType=[Select Id FROM RecordType WHERE Name = 'HR Activities' LIMIT 1];     
        
        id baseUser = hrClaimsUser[0].Id;
        id baseRecordType = hrActivityRecordType[0].Id;
        
        system.debug(' ** Why am I not creating the Task!!');
        Task cT = New Task();
        cT.WhatId = aAT.Case__r.Parent.Id;
        //cT.Authority__c = aAT.Case__r.Id;    
        cT.OwnerId = baseUser;
        system.debug(' FOUND record type ID = ' + baseRecordType);
        system.debug(' Expected record type = 0122P0000004MVQQA2');
        cT.RecordTypeId = baseRecordType; // 0122P0000004MVQQA2
        cT.Priority = 'Important';
        cT.Description = subjectString;
        cT.ActivityDate = Date.today();
        cT.Due_Date_Time__c = DateTime.now();
        cT.Status = 'Not Started';
        cT.Subject = 'Authority Status Updated';

        //try {
        insert cT;
        //} catch(DmlException e) {
        //	System.debug('The following exception has occurred (Task): ' + e.getMessage());
        //}
        //}
        
    }
    
    @TestVisible
    private void generateVendorNote(Authority_Activity_Tracker__c aAT, Authority_Activity_Tracker_Config__c aATC){
        /*
		* Created By: Jason Parr (HomeRepair)
		* Created Date: 07-08-2020
		* 
		* Used to create a new case comment for authorities that are of a
		* status that are to be excluded or require action from updates. 
		* 
		* As of creating these are:
		* - Completed
		* - Reissued
		* - Cancelled
		* - Rejected
		* - Submitted
		*/
        
        Vendor_Note__c newVN = New Vendor_Note__c();
        //List<Vendor_Note__c> newVNList = new List<Vendor_Note__c>();
        
        
        newVN.Author__c = aAT.Changed_by_User__r.Name;
        newVN.Authority__c = aAT.Case__c;
        If(!test.isRunningTest())
        {
            newVN.Claim__c = aAT.Case__r.Parent.id;
        } else {
    			Id hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
    			Id hrClaimRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Claim').getRecordTypeId();                
                
                Case newCase = [SELECT Id FROM Case WHERE RecordTypeId = :hrClaimRecId LIMIT 1];
            newVN.Claim__c = newCase.Id;
        }
        newVN.Category__c = (aATC.Vendor_Note_Type__c + ' Note');
        newVN.ClaimHub_Upload_Status__c = 'Pending';
        newVN.Text__c = aATC.Vendor_Note_Contents__c;
        
        //newVNList.add(newVN);
        
        insert newVN;//newVNList;
        
    }
    
    public void finish(Database.BatchableContext bc){ 
    }
}