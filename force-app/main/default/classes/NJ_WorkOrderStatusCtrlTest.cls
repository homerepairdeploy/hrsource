@isTest
public class NJ_WorkOrderStatusCtrlTest {
  @isTest static void test_NJ_WorkOrderStatusCtrl(){
    Case testCase = FSL_TestDataFactory.createClaim();
    workOrder testWO = FSL_TestDataFactory.HRTradeWorkOrderWithCase(testCase.Id);
    testWO.Status = 'Accepted WO';
    update testWO;
    NJ_WorkOrderStatusCtrl.initWorkOrderInfo(testWO.Id);
    NJ_WorkOrderStatusCtrl.saveWorkOrder(testWO);
  }
        
}