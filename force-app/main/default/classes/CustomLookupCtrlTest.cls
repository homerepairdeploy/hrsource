/* =====================================================================================
Type:       Test class
Purpose:    Test cases for CustomLookupCtrlTest(For LightningFileUpload Component )
========================================================================================*/
@isTest
private class CustomLookupCtrlTest{
    public static final Id hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
    
    @testSetup 
    static void setup(){
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id);       
        List<WorkOrderLineItem> woliList=HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1);
        woliList.addAll(HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1));
        Room__c rm=HomeRepairTestDataFactory.createRoom(cs.id,woliList);
        HomeRepairTestDataFactory.createProductForWorkType('Test',wt.Id,1,'Test');
    }
    static testMethod void fetchCustomLookupCtrlTestTest() {
      
        
        Case cs=[Select id from Case limit 1];
          //For caseauthority
        Case ca=[Select id from Case where recordtypeId =: hrAuthorityRecId limit 1];
        CustomLookupCtrl.fetchLookUpValues('CJ','case',ca.Id);
        CustomLookupCtrl.fetchLookUpValues('','case',ca.Id);
         //For WorkType
        worktype wt=[Select id from worktype limit 1];
        CustomLookupCtrl.fetchLookUpValues('CJ','worktype',wt.Id);
        CustomLookupCtrl.fetchLookUpValues('','worktype',wt.Id);
        
        //For WorkOrder
        WorkOrder wo=[Select id from WorkOrder limit 1];
        CustomLookupCtrl.fetchLookUpValues('CJ','WorkOrder',wo.Id);
        CustomLookupCtrl.fetchLookUpValues('','WorkOrder',wo.Id);
        
        //For Room 
        CustomLookupCtrl.fetchLookUpValues('CJ','Room__c',wo.Id);
        CustomLookupCtrl.fetchLookUpValues('','Room__c',wo.Id);
        
        //For ServiceAppointment
        ServiceAppointment testServ = FSL_TestDataFactory.createNewServiceAppointment(wo.Id, 'Tentative', cs.Id);
        CustomLookupCtrl.fetchLookUpValues('CJ','ServiceAppointment',testServ.Id);
        CustomLookupCtrl.fetchLookUpValues('','ServiceAppointment',testServ.Id);
        
        //For PricebookEntry
        PricebookEntry pbe = [Select ID,Product2Id FROM PricebookEntry limit 1];
        CustomLookupCtrl.fetchLookUpValues('CJ','PricebookEntry',wo.Id);
        CustomLookupCtrl.fetchLookUpValues('','PricebookEntry',wo.Id);
    }
}