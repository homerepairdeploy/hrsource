/************************************************************************************************************
Name: NJ_TradeTypeEmailTemplateController
=============================================================================================================
Purpose: Controller of VF Component TradeTypeEmailComponent 
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL       DESCRIPTION
1.0        Nikhil         			         Created     NJ_TradeTypeEmailTemplateController
*************************************************************************************************************/
public class NJ_TradeTypeEmailTemplateController {
    public String leadId {get;set;}
    
    public List<Trade_Type__c> getTradeTypeLst(){
        List<Trade_Type__c> tradetypes;
        tradetypes = [SELECT Id, Name, Status__c,Work_Type__r.Name, Reject_Reason__c FROM Trade_Type__c WHERE Lead__c =: leadId Order By Status__c];
        return tradetypes;
    }
}