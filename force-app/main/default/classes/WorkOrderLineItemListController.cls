/********************************************************
* Name: WorkOrderLineItemListController
* Created By: Vasu Gorakati
* Date: 10 th December 2018
* Description: Display work order line items related list on Trade portal
* by excluding cash settled items
*********************************************************/
public class WorkOrderLineItemListController {
	@AuraEnabled
	public static List<WorkOrderLineItem> fetchWorkOrderLineItems(String objectId) {
		List<WorkOrderLineItem> WorkOrderLineItemList = new List<WorkOrderLineItem>();
		WorkOrderLineItemList = [Select Id, PricebookEntry.Name, LineItemNumber, Product_Description__c, WorkType.Name from WorkOrderLineItem
		                         where WorkOrderId = : objectId and Cash_Settled__c = false order by CreatedDate desc];
		return WorkOrderLineItemList;
	}
}