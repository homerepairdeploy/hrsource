/***************************************************************** 
Purpose: Retreives Invoice and Created ContentVersion Record                                                      
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            Vasu          02/08/2018      Created      Home Repair Claim System  
*******************************************************************/   
Global class ARCMFeesInvoiceStatement{
    /***************************************************************** 
    Purpose: ARCMFeesInvoiceStatement method to fetch Invoice from Process Builder
    Parameters: invoices(Invoice)
    Returns: none
    Throws [Exceptions]: none                                                        
    History                                                             
    --------                                                            
    VERSION        AUTHOR         DATE           DETAIL   Description 
    1.0            Vasu          02/08/2018     Created  Home Repair Claim system 
    *******************************************************************/
    @InvocableMethod
    public static void ARCMFeesInvoiceStatement(List<String> invoices) {
        generateARCMFeesInvoice(Invoices);
    }
    /***************************************************************** 
    Purpose: generateARCMFeesInvoice method to fetch Invoice for generate ContentVersion Record and Update Invoice Generate_Invoice_Statement__c field
    Parameters: invoices(Invoice)
    Returns: none
    Throws [Exceptions]: none                                                        
    History                                                             
    --------                                                            
    VERSION        AUTHOR         DATE           DETAIL   Description 
    1.0            Vasu          02/08/2018     Created  Home Repair Claim system 
    *******************************************************************/
    
    @future(callout=true)
    public static void generateARCMFeesInvoice(List<String> invoices){
        List<AR_Invoice__c> invoiceList=[Select id,Name,Invoice_Type__c,Generate_Invoice_Statement__c 
                                      from AR_Invoice__c where id IN : invoices];
        //List<Attachment> attList=new List<Attachment>();
        List<ContentVersion> attachmentList = new List<ContentVersion>();
        List<ContentDocumentLink> links = new List<ContentDocumentLink>();
        set<string> invIdSet = new set<string>();
        for(AR_Invoice__c inv: invoiceList) {
            PageReference pdfPage = new PageReference('/apex/ARCMFeesInvoiceStatement');
            pdfPage.getParameters().put('id',inv.Id);
                        
            ContentVersion cVersion = new ContentVersion();
            cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.            
            cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
            cVersion.OwnerId = UserInfo.getUserId();//Owner of the file                        
                        
            if(!Test.isRunningTest()){
                cVersion.VersionData = pdfPage.getContentAsPdf();
            }else{
                cVersion.VersionData = Blob.valueOf('Test Data');
            }            
            
            //at.ParentId = inv.Id;
            attachmentList.add(cVersion);         
            if (inv.Invoice_Type__c == 'CM Fees') {
                cVersion.PathOnClient = cVersion.Title = 'CM-Fees-'+inv.Name+'-'+System.now().format('yyyy-dd-mm HH:mm:ss')+'.pdf';               
            }else if (inv.Invoice_Type__c == 'Customer'){
                cVersion.PathOnClient = cVersion.Title = 'AR-Invoice-'+inv.Name+'-'+System.now().format('yyyy-dd-mm HH:mm:ss')+'.pdf';                
            }
            else if (inv.Invoice_Type__c == 'Customer Credit Memo'){
                cVersion.PathOnClient = cVersion.Title = 'Customer-CreditMemo-'+inv.Name+'-'+System.now().format('yyyy-dd-mm HH:mm:ss')+'.pdf';                
            }
            else if (inv.Invoice_Type__c == 'CM Fees Credit Memo'){
                cVersion.PathOnClient = cVersion.Title = 'CM-Fees-CreditMemo-'+inv.Name+'-'+System.now().format('yyyy-dd-mm HH:mm:ss')+'.pdf';                
            }
            else if (inv.Invoice_Type__c == 'Assessment Fee Credit Memo'){
                cVersion.PathOnClient = cVersion.Title = 'Assessment-Fees-CreditMemo-'+inv.Name+'-'+System.now().format('yyyy-dd-mm HH:mm:ss')+'.pdf';  
            }
            else if (inv.Invoice_Type__c == 'Assessment Fee'){
                cVersion.PathOnClient = cVersion.Title = 'Assessment-Fees-'+inv.Name+'-'+System.now().format('yyyy-dd-mm HH:mm:ss')+'.pdf';  
            }
			
            invIdSet.add(inv.Id);   
            //Insert Content Version to the invoice     
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.LinkedEntityId = inv.Id;
            cDocLink.ShareType = 'I';
            cDocLink.Visibility = 'InternalUsers';
            links.add(cDocLink);          
        }
        if(!attachmentList.isEmpty()) {
            insert attachmentList;
            Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:attachmentList[0].Id].ContentDocumentId;
            links[0].ContentDocumentId = conDocument;
            insert links;
        }
        return;
    }
}