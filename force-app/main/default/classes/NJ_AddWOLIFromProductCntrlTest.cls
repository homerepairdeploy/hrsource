@isTest
public class NJ_AddWOLIFromProductCntrlTest {
    @testSetup 
    static void setup(){
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        HomeRepairTestDataFactory.createProductForWorkcode('Test','100',1,'Test');
        HomeRepairTestDataFactory.createProductWithPricebookEntry('Test2');
        HomeRepairTestDataFactory.createProductWithPricebookEntryAndWorkCodes('Test2');        
        WorkType wt2 = [SELECT Id 
                       FROM WorkType
                       LIMIT 1];
        List<Product2> prods = new List<Product2>();
        for(Product2 pd : [SELECT Id, Name, Level_1__c, Level_2__c,
                                  Level_3__c, Level_4__c,
                                  Level_5__c, Work_Type__c
                           FROM Product2]) {
          //pd.Level_1__c = pd.Level_2__c ;
          //= pd.Level_3__c = pd.Level_4__c = pd.Level_5__c;
          pd.Work_Type__c = wt2.Id;
          prods.add(pd);
        }
        update prods;
        //changed claimjob to Authority by CRMIT
        case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id);       
        HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1);
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
    }
    @isTest static void testcreateWorkOrderLineItemCtrl(){
        test.startTest();
            
            Id pbId = Test.getStandardPricebookId();
            //string sPricebookEntryId = [Select ID,Product2Id FROM PricebookEntry limit 1].Id;
            string sPricebookEntryId = pbId;    
            string sProduct2Id = [Select ID,Product2Id FROM PricebookEntry limit 1].Product2Id;
            HomeRepairTestDataFactory.createPriceBookEntry(1,'Standard Price Book',sProduct2Id,true);
            String workOrderId=[Select Id from WorkOrder limit 1].Id;
            WorkOrderLineItem woli=[Select WorkOrderId,Labour_Time__c,Quantity,PricebookEntryId from WorkOrderLineItem limit 1];
            WorkOrderLineItem woliNew=new WorkOrderLineItem();
            woliNew.PricebookEntryId=woli.PricebookEntryId;
            woliNew.Quantity=woli.Quantity;
            woliNew.WorkOrderId=woli.WorkOrderId;
            //woliNew.PricebookEntryId=sPricebookEntryId;
            NJ_AddWOLIFromProductCntrl.initAddWOLIFromProduct(workOrderId);
            NJ_AddWOLIFromProductCntrl.createWorkOrderLineItem(woliNew);
            NJ_AddWOLIFromProductCntrl.getPriceBookEntry('test', 'Delhi', 'Level 1','Level 1','Level 1','Level 1','Level 1');
        test.stopTest();
    }
}