/*****************************************************************
Purpose: Generates assessment report by calling Conga api
--------
VERSION        AUTHOR         DATE           DETAIL       Description
1.0            Vasu          12/05/2018      Created      Home Repair Claim System
*******************************************************************/
@RestResource(urlMapping = '/homerepair/api/v1/congaassessmentreport/*')
global with sharing class RESTCongaComposerAssessmentReportAPI {
    /*****************************************************************
Purpose: Get method to Generate PDF Via Conga Composer
Parameters: claimId (CaseId)
Returns: RESTCongaAttachmentResponseHandler
Throws [Exceptions]: Rest exception
History
--------
VERSION        AUTHOR         DATE           DETAIL   Description
1.0            Vasu          12/05/2018     Created  Home Repair Claim system
*******************************************************************/
    @HttpGet
    global static RESTCongaAttachmentResponseHandler GET() {
        system.debug('Entering GET');
        // Initialise request context to get request parameters
        // and response handler to return results
        Set<String> contentDocumentIds = new Set<String>();
        RestRequest reqParam = RestContext.request;
        RESTCongaAttachmentResponseHandler response = new RESTCongaAttachmentResponseHandler();
        //get claim Id
        String claimId = reqParam.params.get('claimId');
        //get appointment Id
        String appointmentId = reqParam.params.get('apptId');        
        System.Debug('*** param assessment  appointmentId = ' +  appointmentId);
        System.Debug('***Request Context = ' + reqParam);
        //Read Claim
        Case claim = new Case();
        RESTCongaAttachmentWrapper rcaw;
        ContentVersion [] cv;
        claim = [Select id, CaseNumber, Assessment_Report_Created__c from case where id = :claimId];
        //get isGenerateReport flag
        Boolean isGenerateReport = Boolean.valueOf(reqParam.params.get('isGenerateReport'));        
        System.Debug('*** param isGenerateReport = ' +  isGenerateReport);
        
        
        
        Boolean isNCReport;      
        if (reqParam.params.get('isNCReport') == null) {
              isNCReport=false;
        }else{
            //get isNCReport flag
            isNCReport = Boolean.valueOf(reqParam.params.get('isNCReport'));  
        }
        System.Debug('*** param isNCReport= ' +  isNCReport);
        if(isGenerateReport == false){
            List<ContentDocumentLink> cdlList=[Select id,ContentDocumentId from ContentDocumentLink where LinkedEntityId=:claimId and (ContentDocument.LatestPublishedVersion.Title like : NJ_CONSTANTS.WORK_TYPE_REASSESSMENT+'%' OR ContentDocument.LatestPublishedVersion.Title like : NJ_CONSTANTS.WORK_TYPE_ASSESSMENT+'%')  order by ContentDocument.LastModifiedDate desc limit 1];
            if(cdlList.size() > 0){
                List<ContentVersion> cvList = [select id , contentDocumentId, VersionData from ContentVersion where ContentDocumentId=:cdlList[0].ContentDocumentId  limit 1];
                rcaw = new RESTCongaAttachmentWrapper(cvList[0]);
            }
        }else{
            ServiceAppointment serveApp = new ServiceAppointment();
            //Find the parent Id and parent type of SA.
            try {
                serveApp = [SELECT ID,WorkType.Name
                            FROM ServiceAppointment
                            WHERE ID = : appointmentId LIMIT 1];
            }Catch (StringException se) {
                system.debug('appointmentId: '+appointmentId);
                throw new RESTCustomException('No Service Appointment record found');
            }
            
            String reportName;
            
            system.debug('serveApp.WorkType.Name :' + serveApp.WorkType.Name );
            
            if(isNCReport){
                if (serveApp.WorkType.Name == NJ_CONSTANTS.WORK_TYPE_ASSESSMENT){
                    reportName = NJ_CONSTANTS.WORK_TYPE_ASSESSMENT + 'Report' +'-' + claim.CaseNumber + '-' +  date.today().format();
                }else{
                    reportName = NJ_CONSTANTS.WORK_TYPE_REASSESSMENT + 'Report' + '-' +claim.CaseNumber + '-' +  date.today().format();
                }
            }else{
                if (serveApp.WorkType.Name == NJ_CONSTANTS.WORK_TYPE_ASSESSMENT){
                    reportName = NJ_CONSTANTS.WORK_TYPE_ASSESSMENT + 'EstimateReport' + '-' + claim.CaseNumber + '-' +  date.today().format();
                }else{
                    reportName = NJ_CONSTANTS.WORK_TYPE_REASSESSMENT + 'EstimateReport' + '-' + claim.CaseNumber + '-' +  date.today().format();
                }
            }
            System.Debug(LoggingLevel.DEBUG, '***Request Context = ' + reqParam);
            String sessId = UserInfo.getSessionId();
            String servUrl = Url.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/29.0/' + UserInfo.getOrganizationId();
            String url2;
            GeneralSettings__c genSetting = GeneralSettings__c.getValues('Environment');
            system.debug('environment: ' + genSetting.Value__c);
            if (genSetting.Value__c == 'prod') {
                url2 = 'https://composer.congamerge.com/composer8/index.html?' +
                    'sessionId=' + sessId + '&serverUrl=' + EncodingUtil.urlEncode(servUrl, 'UTF-8') +
                    '&defaultpdf=1&ds7=3&fp0=1&solmgr=1' +
                    '&OFN=' + reportName +
                    '&SC0=0&SC1=SalesforceFile' +
                    '&Id=' + claimId;
                if(isNCReport){
                    url2 +='&QueryId=[Cash]a0s2P0000005t1M,[ClaimWO]a0s2P0000005t1K,[Cause]a0s2P0000005t1F,' +
                        '[Decline]a0s2P0000005t1H,[WOlines]a0s2P0000005t1N,[Policy]a0s2P0000005t1G,[Risks]a0s2P0000005t1J,' +
                        '[WOSR]a0s2P0000005t1P,[Accomm]a0s2P0000005t1D,[Asbestos]a0s2P0000005t1I,[WOAssess]a0s2P0000005t1E' +
                        '&TemplateId=a102P0000086v4t';
                }else{
                    url2 +='&QueryId=[WOlines]a0s2P0000005t1N,[WOAssess]a0s2P0000005t1E,[Accomm]a0s2P0000005t1D,' +
                        '[Asbestos]a0s2P0000005t1I,[Decline]a0s2P0000005t1H,[Policy]a0s2P0000005t1G,[Cause]a0s2P0000005t1F,' +
                        '[Risks]a0s2P0000005t1J,[RepairLink]a0s2P0000005t1O,[ClaimWO]a0s2P0000005t1K,[Heights]a0s2P0000005t1L,' +
                        '[Cash]a0s2P0000005t1M,[WOSR]a0s2P0000005t1P&TemplateId=a102P0000086v4u';
                }
                url2 +='&APIMode=1&targetorigin=' + EncodingUtil.urlEncode(Url.getSalesforceBaseUrl().toExternalForm(), 'UTF-8');
                
            } else if (genSetting.Value__c == 'hrdev') {  
                url2 = 'https://composer.congamerge.com/composer8/index.html?' +
                    'sessionId=' + sessId + '&serverUrl=' + EncodingUtil.urlEncode(servUrl, 'UTF-8') +
                    '&defaultpdf=1&ds7=3&fp0=1&solmgr=1' +
                    '&OFN=' + reportName +
                    '&SC0=0&SC1=SalesforceFile' +
                    '&Id=' + claimId +
                    '&QueryId=[WOlines]a042O000001DfAy,[WOAssess]a042O000001DfAr,[Accomm]a042O000001DfB2,' +
                    '[Asbestos]a042O000001DfAz,[Decline]a042O000001DfAx,[Policy]a042O000001DfAs,[Cause]a042O000001DfAw,' +
                    '[Risks]a042O000001DfAv,[RepairLink]a042O000001DfB0,[ClaimWO]a042O000001DfB1,[Heights]a042O000001DfB4,' +
                    '[Cash]a042O000001DfAt,[WOSR]a042O000001DfAq&';
                if(isNCReport){
                    url2 +='TemplateId=a0C2O0000006CfRUAU';
                }else{
                    url2 +='TemplateId=a0C2O0000005wx1';
                }
                url2 +='&APIMode=1&targetorigin=' + EncodingUtil.urlEncode(Url.getSalesforceBaseUrl().toExternalForm(), 'UTF-8');
                
            } else if (genSetting.Value__c == 'hruat') {
                url2 = 'https://composer.congamerge.com/composer8/index.html?' +
                    'sessionId=' + sessId + '&serverUrl=' + EncodingUtil.urlEncode(servUrl, 'UTF-8') +
                    '&defaultpdf=1&ds7=3&fp0=1&solmgr=1' +
                    '&OFN=' + reportName +
                    '&SC0=0&SC1=SalesforceFile' +
                    '&Id=' + claimId;
                if(isNCReport){
                    url2 +='&QueryId=[WOlines]a0k2N0000002FSk,[WOAssess]a0k2N0000002FSY,[Accomm]a0k2N0000002FSj,' +
                        '[Asbestos]a0k2N0000002FSg,[Decline]a0k2N0000002FSe,[Policy]a0k2N0000002FSZ,[Cause]a0k2N0000002FSd,' +
                        '[Risks]a0k2N0000002FSc,[ClaimWO]a0k2N0000002FSi,' +
                        '[Cash]a0k2N0000002FSa,[WOSR]a0k2N0000002FSX&TemplateId=a0s2N0000004VTe';
                }else{
                    url2 +='&QueryId=[WOlines]a0k2N0000002FSf,[WOAssess]a0k2N0000002FSY,[Accomm]a0k2N0000002FSj,' +
                        '[Asbestos]a0k2N0000002FSg,[Decline]a0k2N0000002FSe,[Policy]a0k2N0000002FSZ,[Cause]a0k2N0000002FSd,' +
                        '[Risks]a0k2N0000002FSc,[RepairLink]a0k2N0000002FSh,[ClaimWO]a0k2N0000002FSi,[Heights]a0k2N0000002FSl,' +
                        '[Cash]a0k2N0000002FSa,[WOSR]a0k2N0000002FSX&TemplateId=a0s2N0000004MH9';
                }
                
                url2 +='&APIMode=1&targetorigin=' + EncodingUtil.urlEncode(Url.getSalesforceBaseUrl().toExternalForm(), 'UTF-8');
            } else if (genSetting.Value__c == 'systemtest') {
                url2 = 'https://composer.congamerge.com/composer8/index.html?' +
                    'sessionId=' + sessId + '&serverUrl=' + EncodingUtil.urlEncode(servUrl, 'UTF-8') +
                    '&defaultpdf=1&ds7=3&fp0=1&solmgr=1' +
                    '&OFN=' + reportName +
                    '&SC0=0&SC1=SalesforceFile' +
                    '&Id=' + claimId;
                if(isNCReport){
                    url2 +='&QueryId=[WOlines]a042O000001DfB3,[WOAssess]a042O000001DfAr,[Accomm]a042O000001DfB2,' +
                        '[Asbestos]a042O000001DfAz,[Decline]a042O000001DfAx,[Policy]a042O000001DfAs,[Cause]a042O000001DfAw,' +
                        '[Risks]a042O000001DfAv,[ClaimWO]a042O000001DfB1,' +
                        '[Cash]a042O000001DfAt,[WOSR]a042O000001DfAq&TemplateId=a0C2O0000006K8g';
                }else{
                    url2 +='&QueryId=[WOlines]a042O000001DfB3,[WOAssess]a042O000001DfAr,[Accomm]a042O000001DfB2,' +
                        '[Asbestos]a042O000001DfAz,[Decline]a042O000001DfAx,[Policy]a042O000001DfAs,[Cause]a042O000001DfAw,' +
                        '[Risks]a042O000001DfAv,[RepairLink]a042O000001DfB0,[ClaimWO]a042O000001DfB1,[Heights]a042O000001DfB4,' +
                        '[Cash]a042O000001DfAt,[WOSR]a042O000001DfAq&TemplateId=a0C2O0000005wx1';
                }
                url2 +='&APIMode=1&targetorigin=' + EncodingUtil.urlEncode(Url.getSalesforceBaseUrl().toExternalForm(), 'UTF-8');
            } else {
                url2 = 'https://composer.congamerge.com/composer8/index.html?' +
                    'sessionId=' + sessId + '&serverUrl=' + EncodingUtil.urlEncode(servUrl, 'UTF-8') +
                    '&defaultpdf=1&ds7=3&fp0=1&solmgr=1' +
                    '&OFN=' + reportName +
                    '&SC0=0&SC1=SalesforceFile' +
                    '&Id=' + claimId + '&QueryId=[Heights]a0rN0000003HtsM,[Decline]a0rN0000003HtsP,[Policy]a0rN0000003HtsQ,[WOAssess]a0rN0000003HtsK,[TradeType]a0rN0000003HtsH,[WOlines]a0rN0000003HtsI,[ClaimWO]a0rN0000003HtsY,[Accomm]a0rN0000003HtsL,[Asbestos]a0rN0000003HtsO,[RepairLink]a0rN0000003HtsX,[Cause]a0rN0000003HtsV,[Risks]a0rN0000003HtsW,[Cash]a0rN0000003HuBW,&TemplateId=a195D000004aJXD' +
                    '&APIMode=1&targetorigin=' + EncodingUtil.urlEncode(Url.getSalesforceBaseUrl().toExternalForm(), 'UTF-8');
            }
            
            system.debug('congaURL: ' + url2);
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url2);
            req.setMethod('GET');
            req.setTimeout(60000);
            HttpResponse res;
            
            if (!Test.isRunningTest()) {
                // Send the request, and return a response
                res = http.send(req);
                system.debug('Repsonse status ' + res.getStatus());
                system.debug('Response status code' + res.getStatusCode());
                System.debug('Repsonse body ' + res.getBody());
                cv = [select id , contentDocumentId, VersionData from ContentVersion where id = :res.getBody() limit 1];
            } else {
                cv = [select id , contentDocumentId, VersionData from ContentVersion limit 1];
            }
            
            
            // validate the request parameters
            try {
                if (cv.size() > 0) {
                    rcaw = new RESTCongaAttachmentWrapper(cv[0]);
                    contentDocumentIds.add(cv[0].contentDocumentId);
                    //delete report if assessment is Preview only
                    if (claim.Assessment_Report_Created__c == false) {
                        response.Message = HomeRepairUtil.deleteContentVersion(contentDocumentIds);
                    }
                } else {
                    response.Status = 'Error';
                    response.ErrorCode = 'Error';
                    response.Message = 'Assessment Report not generated. Please contact HomeRepair customer support team';
                    return response;
                    
                }
            } catch (RESTCustomException re) {
                response.Status = 'Error';
                response.ErrorCode = 'DML_ERROR';
                response.Message = 'Assessment Report not generated. Please contact HomeRepair customer support team';
                return response;
            }
        }
        system.debug('Exiting Get method : ' + rcaw);
        response.Status = 'OK';
        response.Message = 'Success';
        response.Data = rcaw;
        return response;
    }
}