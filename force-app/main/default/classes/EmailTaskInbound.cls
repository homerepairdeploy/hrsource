public class EmailTaskInbound implements Messaging.InboundEmailHandler{
    public Messaging.InboundEmailResult handleInboundEmail
        (Messaging.inboundEmail email,Messaging.InboundEnvelope envelope){
            Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
            
            system.debug('Subject'+email.subject);
            system.debug('Subject'+email.plainTextBody);
            string subbj = email.subject;
            List<WorkOrder>  WoList   = new List<WorkOrder>();
            
            List<EmailMessage> emailMessageList = [SELECT Id, TextBody,ToAddress FROM EmailMessage where ToAddress =: email.fromAddress and createdDate >:Datetime.now().addMinutes(-2)];
            if(emailMessageList.size() > 0){
                system.debug('Hello here');                    
                result.success = true;
            }else{
                List<Case> caseList = [SELECT Id, CaseNumber FROM Case WHERE CaseNumber = :email.subject
                                       or Claim_Number_No_Leading_Zeros__c = :email.subject
                                       or claim_number__c = :email.subject LIMIT 1]; 
                system.debug('caseList: ' + caseList);
                if(caseList.size() > 0 ){
                    return result=createTaskAndAttachment(email,caseList[0].Id,null,null,null,null);
                }
                if(caseList.size()==0){  
                    if (email.subject.toLowerCase().contains('claim#')){   System.debug('CaseNumber: '+ extractCaseThreadRef(email.subject));
                        if(extractCaseThreadRef(email.subject) != null){
                            System.debug('CaseNumber: '+ extractCaseThreadRef(email.subject));
                            caseList = [SELECT Id, CaseNumber FROM Case WHERE CaseNumber = :extractCaseThreadRef(email.subject)
                                        or Claim_Number_No_Leading_Zeros__c = :extractCaseThreadRef(email.subject)
                                        or claim_number__c = :extractCaseThreadRef(email.subject) LIMIT 1];
                            if(caseList.size() > 0){  
                                result=createTaskAndAttachment(email,caseList[0].Id,null,null,null,null); 
                            }else{
                                result.success = true;
                                sendClaimNotFoundEmail(email);
                            }
                            
                        }else{
                            result.success = true;
                            sendClaimNotFoundEmail(email);
                        }
                        
                    }else{
                        result.success = true;
                        if(email.subject.contains('WO#'))
                        { 
                            string woNum = extractWOThreadRef(email.subject);
                            string woSubj =null;
                            If(email.subject.contains('Quote'))
                            {
                              woSubj = 'Quote Update Received';  
                            }else {
                              woSubj = 'Check & Measure Update Received';
                            }
                            
                            if(woNum != null){ 
                                woList = [SELECT ID , caseID FROM workOrder where workOrderNumber = :woNum Limit 1];
                            }
                            if(woList.size() > 0)
                              return result=createTaskAndAttachment(email, null, woList[0].id, woList[0].caseID , woSubj, woNum );                        
                            
                        }else {
                            sendClaimNotFoundEmail(email); 
                        }
                    }
                }else{
                    result.success = true;
                    sendClaimNotFoundEmail(email);
                }
            }
            return result;                                                             
        }
    private void sendClaimNotFoundEmail(Messaging.inboundEmail emailCase){
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName='HomeRepair Customer Suppport'];
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        List<String> toAddresses = new List<String>(); 
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        if ( owea.size() > 0 ) {
            email.setOrgWideEmailAddressId(owea.get(0).Id);
        }  
        toAddresses.add(emailCase.fromAddress);
        email.subject = 'We are unable to locate your claim details in our system';
        email.plainTextBody = 'In order to direct your enquiry to the appropriate support team, please enter a valid Claim number in the subject field of your email. \n\nIf you are a customer, please call 1300 836 229 for all general claim queries.If you are a trade working for HomeRepair, please call 03 7003 7525 for all trade queries” instead.\n\nRegards\nThe HomeRepair Team \n\nThe contents of this email are strictly confidential. If you are not the intended recipient, any use, disclosure or copying of this email (including any attachments) is unauthorised and prohibited. If you have received this email in error, please notify Home Repair immediately by return email and then delete the message from your system. Home Repair Pty Ltd ABN 51 603 320 314. If the disclaimer can not be applied, attach the message to a new disclaimer message.';          
        email.setToAddresses(toAddresses);
        emailList.add(email);
        if(emailList.size() > 0){
            if(!Test.isRunningTest()){ 
                Messaging.sendEmail(emailList);
            }
        } 
    }
    private Messaging.InboundEmailResult createTaskAndAttachment(Messaging.inboundEmail email,String caseId, String woID, string claimID, string woSubj, string woNum){
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        EmailMessage incomingEmail = new EmailMessage();
        if(caseId != null)
            incomingEmail.ParentId = caseId;
        if(WoID !=null)
            incomingEmail.RelatedToId  = WoID;
        incomingEmail.FromAddress = email.fromAddress;
        incomingEmail.Subject = email.subject;
        incomingEmail.HtmlBody = email.htmlBody;
        incomingEmail.FromName = email.fromName;
        incomingEmail.Status = '0';
        incomingEmail.ToAddress = email.toAddresses[0];        
        incomingEmail.Incoming = true;
        
        try{
            Insert incomingEmail;
        } catch(Exception e){
            String errorMessage = e.getMessage();
            result.message = errorMessage;
            result.success = false;            
        }
        List<HomeRepairUtil.contentWrapper> attachmentList = new List<HomeRepairUtil.contentWrapper> ();
        if(email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
            system.debug('H1');
            for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
                HomeRepairUtil.contentWrapper att = new HomeRepairUtil.contentWrapper(
                    EncodingUtil.base64Encode(email.binaryAttachments[i].body),
                    email.binaryAttachments[i].filename,null);   
                attachmentList.add(att);                     
            }
        }
        
        if(email.textAttachments != null){ system.debug('H2');
            for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments) {
                HomeRepairUtil.contentWrapper att = new HomeRepairUtil.contentWrapper(
                    tAttachment.body,
                    tAttachment.fileName,null);   
                attachmentList.add(att);   
            }
        }
        
        if(!attachmentList.isEmpty()){
            system.debug('Kemoji - att' + attachmentList  + 'case ID : ' + caseId);
            HomeRepairUtil.doUploadContent(caseId , woID, attachmentList);
            
        }
        // New Task object to be created
        Task[] newTask = new Task[0];
        try{
            // Add a new Task to the case record we just found above.
            
            List<User> userList=[Select Id,Name,Email 
                                 FROM User
                                 WHERE Name = 'HR Claims'
                                 Limit 1];
            
            newTask.add(new Task(Description =  woNum != null ?  woNum : email.plainTextBody,
                                 priority = 'Important',
                                 status = 'Not Started',
                                 subject = claimID != null ?  woSubj : 'Review Correspondence' ,
                                 IsReminderSet = true,
                                 Type = 'Email',
                                 OwnerId = userList[0].id,
                                 ActivityDate=System.Today()+1,
                                 ReminderDateTime = System.now()+1,
                                 WhatId =  claimID != null ? claimID : caseId));
            
            // Insert the new Task 
            insert newTask; 
            result.success = true;
        } catch(Exception e){
            String errorMessage = e.getMessage();
            result.message = errorMessage;
            result.success = false;            
        }
        
        
        return result;
    }
    private String extractCaseThreadRef(String emailSubject){
        String target = emailSubject.toLowerCase();
 //       String caseRef = target.substringAfter('claim #');
        String caseRef   = target.substringBetween('claim#', '#');
        system.debug('caseRef: ' + caseRef); 
        return caseRef; 
    }
    private String extractWOThreadRef(String emailSubject){
        String target = emailSubject.toLowerCase();
        String woRef = target.substringBetween('wo#' , '#');
        system.debug('woRef: ' + woRef  +  ' Target ' + Target); 
        return woRef; 
    }
}