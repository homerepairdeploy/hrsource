public  class TradeTypeController {

    public class PicklistOption {
        public PicklistOption( String label) {
            this.label = label;
        }
        public PicklistOption(String value, String label) {
            this.value = value;
            this.label = label;
        }
        public Boolean hideChildren = true;
        public String value;
        public String label;
        public List<PicklistOption> children;
    }

    public class FieldEntry {
        public String objName {set; get;}
        public String apiName {set; get;}
        public String dependentFieldName {set; get;}
        public List<PicklistOption> options {set; get;}
        public Map<String, List<PicklistOption>> dependentOptions {set; get;}
    }
    @AuraEnabled
    public static List<WorkType> getWorkTypeNames(List<String> wtIds) {
        List<WorkType> wtNames = new List<WorkType>();
        for(WorkType wt : [SELECT Id,Name
                           FROM WorkType
                           WHERE Id IN :wtIds 
                           ORDER BY NAME ASC NULLS LAST	]) {
            wtNames.add(wt);
        }
        return wtNames;
    }
    @AuraEnabled
    public static String getOptions() {
        String WORKTYPE = 'Lead.Work_Type__c';
        String SERVICEAREAS = 'Lead.Service_Areas__c';
        return getOptions(WORKTYPE, SERVICEAREAS);
    }

    public static String getOptions(String WORKTYPE, String SERVICEAREAS) {
        List<FieldEntry> entries = new List<FieldEntry>();
        List<WorkType> workTypes = WorkTypeSelector.getWorkTypes();
        List<TradeTypeController.PicklistOption> workTypeOptions
            = (new WorkTypeDomain(workTypes)).getWorkTypeOptions();
        FieldEntry workTypeEntry  = new FieldEntry();
        workTypeEntry.apiName = WORKTYPE;
        workTypeEntry.options = workTypeOptions;
        entries.add(workTypeEntry);


        FieldEntry serviceAreaEntry  = new FieldEntry();
        serviceAreaEntry.apiName = SERVICEAREAS;
        serviceAreaEntry.options = new List<TradeTypeController.PicklistOption> {ServiceTerritoryService.getServiceTerritories()};
        entries.add(serviceAreaEntry);
        return JSON.serialize(entries);
    }

    @AuraEnabled
    public static String getTradeComplianceForm() {
        String TRADE_COMPLIANCE_FORM = 'Trade Compliance Form';
        Wizard_Setting__mdt setting
            = WizardSettingService.getWizardSettingByName(TRADE_COMPLIANCE_FORM);
        return JSON.serialize(setting);
    }
    @AuraEnabled
    public static String getOptions(String entryStr) {
        return JSON.serialize(FieldUtils.fillOptions(entryStr));
    }
    @AuraEnabled
    public static String upsertTradeCompliance(String tradeComplianceStr) {
        List<Trade_Type__c> tradeCompliances
            = (List<Trade_Type__c>)JSON.deserialize(tradeComplianceStr, List<Trade_Type__c>.class);
        return JSON.serialize(tradeCompliances);
    }
    @AuraEnabled
    public static String deleteTradeCompliance(String tradeId) {
        List<Trade_Type__c> ttList=[Select id from Trade_Type__c where Id = : tradeId];
        delete ttList;
        
        return 'Success';
    }
    @AuraEnabled
    public static String getDocuments(String tradeComplianceStr) {
        List<Trade_Type__c> tradeCompliances
            = (List<Trade_Type__c>)JSON.deserialize(tradeComplianceStr, List<Trade_Type__c>.class);
        List<String> parentIds = new List<String>();
        for (Trade_Type__c tradeCompliance : tradeCompliances) {
            if (tradeCompliance.Id != null )
                parentIds.add(tradeCompliance.Id);
        }
        List<ContentDocumentLink> links = [Select id, ContentDocumentId,
                                           ContentDocument.LatestPublishedVersion.Title, ContentDocument.LatestPublishedVersion.Description,
                                           LinkedEntityId FROM ContentDocumentLink where LinkedEntityId in: parentIds];
        return JSON.serialize(links);
    }
	@AuraEnabled
     public static List < String > getselectOptions() {
     
      List < String > allOpts = new list < String > ();
      // Get the object type of the SObject.
      Schema.sObjectType objType = Trade_Type__c.getSObjectType();
     
      // Describe the SObject using its object type.
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
     
      // Get a map of fields for the SObject
      map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
     
      // Get the list of picklist values for this field.
      list < Schema.PicklistEntry > values =
       fieldMap.get('Trade_Type__c').getDescribe().getPickListValues();
      // Add these values to the selectoption list.
      for (Schema.PicklistEntry a: values) {
          if(a.isActive()){
              allOpts.add(a.getValue());
          }       
      }
      system.debug('allOpts ---->' + allOpts);
      allOpts.sort();
      return allOpts;
     }

}