/***************************************************************** 
Purpose: Test Class for WorkOrderTrigger 
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            -          22/03/2019      Created      Home Repair Claim System  
*******************************************************************/
@istest
public class WorkOrderTriggerTest {
    
    @TestSetup
    static void TestdataCreateMethod() {
        //Create the Account Record.
        Account acc = HomeRepairTestDataFactory.createAccounts('Test WorkOrder Account')[0];        
        //Create the Contact Record.
        contact conVar = HomeRepairTestDataFactory.createTradeContact('test WO Contatct',acc.Id);        
        
        Home_Repairs_Trigger_Switch__c hrts = HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        BypassWOValidation__c  byps = HomeRepairTestDataFactory.createBypassWOValidation(false);
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);                
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        
        //Commented by CRMIT to create a child case instead of Claim_Job__c
        //Claim_Job__c cj=HomeRepairTestDataFactory.createClaimJob(cs.Id);
       	//Added by CRMIT to create a child case for parent case
         Case childCaseObj = HomeRepairTestDataFactory.createChildClaimAuthority(cs.Id);
         system.debug('======childCaseObj' +childCaseObj);

        //Commented by CRMIT
        //WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,cj.id);
        //Added by CRMIT for create workorders for case and child case
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrdersWithCase('Home Repair Trades',cs.id,wt.id,childCaseObj.id);
        List<WorkOrderLineItem> woliList=HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1);
        
    }

    private static testMethod void testUpdateWorkOrder() {
        Test.startTest();
        Case cs=[Select id from Case limit 1];
        WorkOrder wo = [Select id,CaseId,WorkTypeId,Authority__c from WorkOrder limit 1];
        WorkOrder woNew=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',wo.CaseId,wo.WorkTypeId,wo.Authority__c);
        wo.Invoice_Type__c=NJ_CONSTANTS.LINE_ITEM_CREDIT_MEMO;
        wo.ParentWorkOrderId=woNew.id;
        wo.Variation_Approval_Status__c = 'Approved';
        wo.Active_Variation_Exists__c   = true;
        wo.Status =NJ_CONSTANTS.WO_STATUS_JOBCOMPLETE;
        update wo;
        
        System.debug('Wo '+[Select id,Invoice_Type__c,Status,ParentWorkOrderId from WorkOrder where id=:wo.id]);
        Test.stopTest();
    }
    
      private static testMethod void testUpdateWorkOrderLineItem() {
        WorkOrderLineItem woli = [Select id,Material_Rate__c from WorkOrderLineItem limit 1];
        woli.Material_Rate__c = 20;
        Test.startTest();
        update woli;
        Test.stopTest();
    }
    
    private static testMethod void testServiceAppointmentWorkOrder() {
           Test.startTest();
        Case cs=[Select id from Case limit 1];
        WorkOrder wo = [Select id,CaseId,WorkTypeId,Authority__c from WorkOrder limit 1];
        wo.Status =NJ_CONSTANTS.WO_STATUS_ACCEPTED;
     
        ServiceAppointment testServ = FSL_TestDataFactory.createNewServiceAppointment(wo.Id, 'Tentative', wo.CaseId);
        update wo;        
        System.debug('Wo '+[Select id,Invoice_Type__c,Status,ParentWorkOrderId from WorkOrder where id=:wo.id]);
        Test.stopTest();
    }
    private static testMethod void testRejectWorkOrderId() {
        Case cs=[Select id from Case limit 1];
        WorkOrder wo = [Select id,CaseId,WorkTypeId,Authority__c, Status, StatusCategory from WorkOrder limit 1];
        wo.Status=NJ_Constants.WO_STATUS_REJECTED ;
        wo.Rejection_Reason__c='Test Reject'; 
        Test.startTest();        
        update wo;
        ServiceAppointment testServ = FSL_TestDataFactory.createNewServiceAppointment(wo.Id, null, wo.CaseId);
        wo.Status=NJ_CONSTANTS.WO_STATUS_REJECTED ;
        update wo;
                        
        Map<Id, WorkOrder> woMap = new Map<Id, WorkOrder>([Select id,Invoice_Type__c,Quality_Assurance_Text__c, CaseId, Status,ParentWorkOrderId from WorkOrder where id=:wo.id]);
        WorkOrderHelper.updateServiceAppointments(woMap);
        WorkOrderHelper.addQualityAssuranceTextTask(woMap.values()); 
        WorkOrderHandler wo2 = new WorkOrderHandler();
        wo2.beforeUnDelete(wo);
        wo2.afterDelete(wo);
        wo2.afterUnDelete(wo);
        wo2.beforeDelete(wo);
        wo2.andFinally();
        Test.stopTest();
    }
    
    public static testMethod void testRejectWorkOrderId1(){
      Test.startTest();
      WorkOrder woauth=[ Select id,CaseId,WorkTypeId,Authority__c,Work_Order_Authority__c,ParentWorkOrderId from WorkOrder limit 1]; 
	  woauth.Work_Order_Authority__c ='Make Safe';
      update woauth;
      Test.stopTest();

    }
    @IsTest
    Public static void RunTest1(){
     	WorkOrderHandler.checkTest();   
    }                           
}