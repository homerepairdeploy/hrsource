global class OnceoffGenerateARInvoicePDF implements Database.Batchable<sObject> {
    
    list<string> namelist=new list<string>();
    global OnceoffGenerateARInvoicePDF(){
    
      for(HRInvoiceTemp__c hrinvoicetemp:[select id,name from HRInvoiceTemp__c]){
          namelist.add(hrinvoicetemp.name);
      }
      
    
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
        String query='select id from AR_Invoice__c where name in :namelist';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<AR_Invoice__c> scope) {
    
       list<string> invoices=new list<string>();
       for(AR_Invoice__c arinvc:scope){
          invoices.add(arinvc.id); 
          
       }
       generateARCMFeesInvoice(invoices);
        
        
    }
    
    public static void generateARCMFeesInvoice(List<String> invoices){
        List<AR_Invoice__c> invoiceList=[Select id,Name,Invoice_Type__c,Generate_Invoice_Statement__c 
                                      from AR_Invoice__c where id IN : invoices];
        //List<Attachment> attList=new List<Attachment>();
        List<ContentVersion> attachmentList = new List<ContentVersion>();
        List<ContentDocumentLink> links = new List<ContentDocumentLink>();
        set<string> invIdSet = new set<string>();
        for(AR_Invoice__c inv: invoiceList) {
            PageReference pdfPage = new PageReference('/apex/ARCMFeesInvoiceStatement');
            pdfPage.getParameters().put('id',inv.Id);
                        
            ContentVersion cVersion = new ContentVersion();
            cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.            
            cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
            cVersion.OwnerId = UserInfo.getUserId();//Owner of the file                        
                        
            if(!Test.isRunningTest()){
                cVersion.VersionData = pdfPage.getContentAsPdf();
            }else{
                cVersion.VersionData = Blob.valueOf('Test Data');
            }            
            
            //at.ParentId = inv.Id;
            attachmentList.add(cVersion);         
            if (inv.Invoice_Type__c == 'CM Fees') {
                cVersion.PathOnClient = cVersion.Title = 'CM-Fees-'+inv.Name+'-'+System.now().format('yyyy-dd-mm HH:mm:ss')+'.pdf';               
            }else if (inv.Invoice_Type__c == 'Customer'){
                cVersion.PathOnClient = cVersion.Title = 'AR-Invoice-'+inv.Name+'-'+System.now().format('yyyy-dd-mm HH:mm:ss')+'.pdf';                
            }
            else if (inv.Invoice_Type__c == 'Customer Credit Memo'){
                cVersion.PathOnClient = cVersion.Title = 'Customer-CreditMemo-'+inv.Name+'-'+System.now().format('yyyy-dd-mm HH:mm:ss')+'.pdf';                
            }
            if (inv.Invoice_Type__c == 'CM Fees Credit Memo'){
                cVersion.PathOnClient = cVersion.Title = 'CM-Fees-CreditMemo-'+inv.Name+'-'+System.now().format('yyyy-dd-mm HH:mm:ss')+'.pdf';                
            }
            invIdSet.add(inv.Id);   
            //Insert Content Version to the invoice     
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.LinkedEntityId = inv.Id;
            cDocLink.ShareType = 'I';
            cDocLink.Visibility = 'InternalUsers';
            links.add(cDocLink);          
        }
        if(!attachmentList.isEmpty()) {
            insert attachmentList;
            Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:attachmentList[0].Id].ContentDocumentId;
            links[0].ContentDocumentId = conDocument;
            insert links;
        }
        return;
    }
    
    global void finish(Database.BatchableContext bc){ 
        
    }
    
}