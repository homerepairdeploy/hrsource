/***************************************************************** 
Purpose: This class is to surface files to Trade Platform
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            Vasu          11/07/2018      Created      Home Repair Claim System  
*******************************************************************/
public with sharing class ContentFileListController {
    @AuraEnabled
    public static List<ContentDocumentLink> fetchContentDocument(String objectId) {
        List<ContentDocumentLink> ContentDocumentLinkList=new List<ContentDocumentLink>();
             
        List<AP_Invoice__c> apInvoiceList=[Select Id from AP_Invoice__c where Work_Order__c =: objectId];
        if(apInvoiceList.size() > 0){
            Set<String> invoiceIds=new Set<String>();
            for(AP_Invoice__c ap:apInvoiceList){
                invoiceIds.add(ap.Id);                
            }
            ContentDocumentLinkList=HomeRepairUtil.contentDocumentList(invoiceIds);
        }
        return ContentDocumentLinkList;
    }
    @AuraEnabled
    public static List<ContentDocumentLink> fetchWorkOrderContentDocument(String objectId) {
        List<ContentDocumentLink> ContentDocumentLinkList=new List<ContentDocumentLink>();
        Set<String> workOrderIds=new Set<String>();
        workOrderIds.add(objectId);
        ContentDocumentLinkList=HomeRepairUtil.contentDocumentList(workOrderIds);
        return ContentDocumentLinkList;
    }

    public class WorkOrderInfo {
        @AuraEnabled
        public Boolean displayComponent;
        @AuraEnabled
        public String fieldParent;
    }
    @AuraEnabled
    public static WorkOrderInfo fetchParentId(String objectId,string fieldOfParent) {
        WorkOrderInfo woi = new WorkOrderInfo();
        User usr = NJ_Utilities.getUserInfo(UserInfo.getUserId());
        WorkOrder workOrderList = Database.query('SELECT '+fieldOfParent+' , Invoicing_Complete__c FROM WorkOrder WHERE id = \''+ objectId + '\' limit 1');
        woi.fieldParent = (string)workOrderList.get(fieldOfParent);
        woi.displayComponent = usr.Profile.Name != NJ_Constants.COMMUNITY_PROFILE_NAME ||
                               usr.Profile.Name == NJ_Constants.COMMUNITY_PROFILE_NAME && !(Boolean)workOrderList.get('Invoicing_Complete__c');
        return woi;
    }
    @AuraEnabled
    public static List<ContentDocumentLink> fetchClaimContentDocument(String objectId) {
        List<ContentDocumentLink> ContentDocumentLinkList=new List<ContentDocumentLink>();
        Set<String> caseIds=new Set<String>();
        List<WorkOrder> workOrderList=[Select id,CaseId from WorkOrder where id =: objectId];
        system.debug(workOrderList[0]);
        if(workOrderList.size() > 0){
            system.debug(workOrderList[0]);
            if(workOrderList[0].CaseId != null){
                caseIds.add(workOrderList[0].CaseId);
            for(ContentDocumentLink cdl : HomeRepairUtil.contentDocumentList(caseIds)) {
                //if(!(cdl.ContentDocument.LatestPublishedVersion.Title.startsWith('AssessmentEstimateReport-') || 
                       //cdl.ContentDocument.LatestPublishedVersion.Title.startsWith('ReassessmentEstimateReport-'))) 
                       if((cdl.ContentDocument.Description == 'Assessment Report') || ((!(cdl.ContentDocument.LatestPublishedVersion.Title.startsWith('AssessmentEstimateReport-') || 
                       	cdl.ContentDocument.LatestPublishedVersion.Title.startsWith('ReassessmentEstimateReport-'))) &&
                          (cdl.ContentDocument.LatestPublishedVersion.Title.startsWith('AssessmentReport-') || cdl.ContentDocument.LatestPublishedVersion.Title.startsWith('ReassessmentReport-'))) ){
                        ContentDocumentLinkList.add(cdl);
                    }
                }
            }          
        }
        return ContentDocumentLinkList;
    }
}