@isTest
public class OnceOffUpdateTasksTest {
    @testSetup 
    static void setup(){
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        Claim_Job__c cj=HomeRepairTestDataFactory.createClaimJob(cs.Id);   
        
    }  
    public static testmethod void testBatchOnceOffUpdateTasks() { 
            case c=[select id,cjid__c from case limit 1];
            claim_job__c cj=[select id from claim_job__c limit 1];
            c.CJId__c=cj.id;
            update c;
            Task t=new Task();
            t.priority = 'Normal';
            t.status = 'Not Started';
            t.subject ='Test description';
            t.Claim_Job__c=cj.id;
            t.WhatId =  c.id;
            insert t;
            Test.startTest(); 
               
                database.executeBatch(new OnceOffUpdateTasks(),1);
               
            Test.stopTest();
        
    }
    
}