@isTest
public class OnceOffCreateAuthorityUpdtWOTest {
    @TestSetup
    public static void setupTestData() {
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        Claim_Job__c cj=HomeRepairTestDataFactory.createClaimJob(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,cj.id);        
        AP_Invoice__c invoice = HomeRepairTestDataFactory.createAPInvoiceWithWorkOrder(wo.id);
        AR_Invoice__c ARinvoice = HomeRepairTestDataFactory.createARInvoiceCMFee(cj.id);
        AR_Invoice__c ARinvoicecust = HomeRepairTestDataFactory.createARInvoiceCustomer(cj.id);
    }
    public static testmethod void testBatchOnceoffCreateAuthFalse() {  
            Test.startTest(); 
                database.executeBatch(new OnceOffCreateAuthorityUpdtWO(false),1);
               
            Test.stopTest();
        
    }
    
}