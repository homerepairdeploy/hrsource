/* =====================================================================================
Type:       Test class
Purpose:    Test cases for homeRepPersonDetailAURA
========================================================================================*/
@isTest
private class homeRepPersonDetailAURATest{
    @TestSetup
    static void TestdataCreateMethod() {
        
        Account acc = HomeRepairTestDataFactory.createAccounts('Test Trade Account')[0];
        contact con = HomeRepairTestDataFactory.createTradeContact('TestTrade Contatct',acc.Id);
        User user = HomeRepairTestDataFactory.createTradeUser(con.Id);
        ServiceResource sr = HomeRepairTestDataFactory.createNewServiceResource(user.Id);
        
    }
    static testMethod void homeRepPersonDetailAURAAllTest() {
        homeRepPersonDetailAURA.getContactId();
        homeRepPersonDetailAURA.getServiceResources();
        ServiceResource sr = [SELECT Id FROM ServiceResource LIMIT 1];
        homeRepPersonDetailAURA.getServiceResourceId(sr.Id);
    }
}