@isTest
public class ClaimJobServiceTest {
    @TestSetup
    static void TestdataCreateMethod() {
        //Create the Account Record.
        Account acc = HomeRepairTestDataFactory.createAccounts('Test WorkOrder Account')[0];        
        //Create the Contact Record.
        contact conVar = HomeRepairTestDataFactory.createTradeContact('test WO Contatct',acc.Id);        
        
        Home_Repairs_Trigger_Switch__c hrts = HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        BypassWOValidation__c  byps = HomeRepairTestDataFactory.createBypassWOValidation(false);
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
                
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        
        //Claim_Job__c cj=HomeRepairTestDataFactory.createClaimJob(cs.Id);
        
        //WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,cj.id); 
        
    }
    public static testMethod void CompletionCallTaskDCJobTest() {
        Test.startTest();
            Map<Id, Claim_Job__c> jobList = new Map<Id, Claim_Job__c>([Select Id, Claim__c FROM Claim_Job__c ]);   
            for(Claim_Job__c job : jobList.values()) {
                job.Job_Type__c = NJ_CONSTANTS.JOB_TYPE_DOANDCHARGE;
            }
            update jobList.values();
            
            List<WorkOrder> orderList = [SELECT Id FROM WorkOrder];
            update orderList;
        
            ClaimJobService.CompletionCallTaskDCJob(jobList.values());
            Case cse = [SELECT Id FROM Case Limit 1];
            ClaimJobService.createTask('Description', cse.Id, UserInfo.getUserId()); 
            /*Task t = [Select Id FROM Task Limit 1];
            t.Status = 'Completed';
            t.Claim_Job__c = [Select Id, Claim__c FROM Claim_Job__c Limit 1].Id;
            t.Case_Comments_Added__c = true;
            update t;*/
            
            //TaskService.createInvoiceRecord([SELECT Id, Claim_Job__c FROM WorkOrder]);
            //TaskService.createInvoiceRecord(jobList.keySet());
        Test.stopTest(); 
    }
}