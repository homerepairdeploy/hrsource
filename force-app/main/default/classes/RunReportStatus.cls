global class RunReportStatus implements Database.Batchable<sObject>, Database.RaisesPlatformEvents {
    
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
    
                
        String query='select id from case where recordtype.name like \'%'+'claim'+'%\'';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<case> scope) {
    
       for(case c:scope){
       
          Flow.Interview flow = new Flow.Interview.Calculate_Claim_Report_Statuses(new map<String,Object> 
                                                {'CaseId' => c.id});     
          flow.start();
       
       }
       
      
    }
    
    global void finish(Database.BatchableContext bc){ 
        
    }
    
    
           
}