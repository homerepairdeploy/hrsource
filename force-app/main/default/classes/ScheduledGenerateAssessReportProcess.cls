/***************************************************************** 
Purpose: Scheduledclass for generating Assessment Report                                                  
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            HomeRepair    23/06/2020      Created      Home Repair Claim System  
*******************************************************************/
global class ScheduledGenerateAssessReportProcess implements Schedulable {
  global void execute(SchedulableContext sc) { 
    GenerateAssessReport objClaim = new GenerateAssessReport();
    Database.executeBatch(objClaim,1);       
  }
}