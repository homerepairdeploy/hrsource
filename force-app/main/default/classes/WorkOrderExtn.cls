public class WorkOrderExtn {
 public WorkOrderExtn() {          
    }
    public list<string> rooms  {get;set;}
    public Map<string,List<WorkOrderLineItem>> mapWOLI  {get;set;}
    public String pageurl {get;set;}
     
    public Account acc{         
        get {
            
            List<Account> accList=[Select id,Name,Accounts_Email_Address__c,Postal_Street__c,
                                   ABN__c,Phone,BillingStreet, BillingCity, BillingState, Preferred_Business_Name__c, Website, Alternate_Phone__c, 
                                   BillingPostalCode, BillingCountry from Account 
                                   where Name='HomeRepair.Net.Au Pty Ltd' LIMIT 1];
            if(accList.size() == 0){
                acc=new Account();
            }else{
                acc=accList[0];              
            }
            return acc;
        } set;
    }
    
        public WorkOrderExtn(ApexPages.StandardController controller) {  
            GeneralSettings__c genSetting = GeneralSettings__c.getValues('Environment');
            system.debug('environment: ' + genSetting.Value__c);
			String durableId = [SELECT Id, Name, Status FROM Site WHERE Name = 'RepairForce_Trade_Platform' AND Status = 'Active' LIMIT 1].Id;
            String baseURL = [SELECT id, SecureUrl from SiteDetail where DurableId = :durableId LIMIT 1].SecureUrl;
            pageurl = baseURL+'/apex/WorkOrder_SOWReport_PDF?id=' +apexpages.currentpage().getparameters().get('id');   
            //pageurl = 'https://homerepair--' + genSetting.Value__c + '--c.visualforce.com/apex/WorkOrder_SOWReport_PDF?id=' +apexpages.currentpage().getparameters().get('id');            
            system.debug('pageurl: ' + pageurl);
            
            mapWOLI = new Map<string,List<WorkOrderLineItem>> ();
            rooms = new list<string>();
            system.debug('Work Order ID' + apexpages.currentpage().getparameters().get('id'));
            for(WorkOrderLineItem woli:[Select room__r.Name,WorkType.Name,Product2.Name from WorkOrderLineItem WHERE WorkOrderId= : apexpages.currentpage().getparameters().get('id')])
            {
               
                 if (mapWOLI.containsKey(woli.room__r.Name)){
                     mapWOLI.get(woli.room__r.Name).add(woli);
                     system.debug('WOLI1' + woli.WorkType.Name);
                 }
                else
                {
                    mapWOLI.put(woli.room__r.Name, new  List <WorkOrderLineItem> { woli });
                    rooms.add(woli.room__r.Name);
                    system.debug('Room Name' + woli.room__r.Name);
                    system.debug('WOLI2' + woli.WorkType.Name);
                } 

            }
        }
    
 /*   public PageReference gotoPage() {
       GeneralSettings__c genSetting = GeneralSettings__c.getValues('Environment');
       system.debug('environment: ' + genSetting.Value__c);
       String pageurl = 'https://homerepair--' + genSetting.Value__c + '--c.visualforce.com/apex/WorkOrder_SOWReport_PDF';
       PageReference p = new PageReference(pageurl);
       p.setRedirect(true);
       return p;
       
    }*/
	
	 public static void checkTest(){
     Integer i=0;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    
    }
}