public class PickListController {
    @AuraEnabled        
    public static List<String> getPickListValuesIntoList(String objectType, String selectedField){
        List<String> pickListValuesList = new List<String>();
        List<String> excludedPickListValuesList=new list<String>();
        Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectType);
        Schema.DescribeSObjectResult res = convertToObj.getDescribe();
        Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(selectedField).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        If (objectType == 'Task' && selectedField == 'Subject') {
            for (Excluded_Manual_Task_Matrix__mdt ExManTask:[SELECT DeveloperName,Task_Name__c FROM Excluded_Manual_Task_Matrix__mdt order by Task_Name__c asc ]){
                excludedPickListValuesList.add(ExManTask.Task_Name__c);
            } 
        }    
        for( Schema.PicklistEntry pickListVal : ple){
            If (!excludedPickListValuesList.contains(pickListVal.getLabel()))   pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList;
    }
}