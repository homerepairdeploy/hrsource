/*================================================================================================
Type:       Test class
Purpose:    Test cases for BatchSetSuppressedFromScheduling and BatchSetSuppressedFromSchedulingSch
=================================================================================================*/
@isTest
private class BatchSetSuppressedFromSchedulingTest{
    @TestSetup
    static void TestdataCreateMethod() {
        Account acc = HomeRepairTestDataFactory.createAccounts('Test Trade Account')[0];     
    }
    static @isTest void testExpiryDate() {
        Account acc = [Select Id from Account limit 1];
        acc.Public_Liability_Expiry__c = Date.Today().addMonths(-4);
        update acc;

        //RUN THE Scheduler Class
        String CRON_EXP = '0 0 23 * * ?';
        BatchSetSuppressedFromSchedulingSch scheObj = new BatchSetSuppressedFromSchedulingSch();
        system.schedule('Test Scheduler Job', CRON_EXP, scheObj);
        
        //run the batch class
        Id z = Database.ExecuteBatch(new BatchSetSuppressedFromScheduling(), 200);

    }
}