public class ClaimJobHandler implements ITrigger {
 
    public static final String CLAIM_JOB_STATUS_COMPLETED = 'Completed';
    public static final String CLAIM_JOB_STATUS_ACCEPTED = 'acceptedInWork';
    public static final String CLAIM_HUB_STATUS_UPDATE = 'Pending';
    Public List<Claim_Job__c> claimJobList = new List<Claim_Job__c>();
    public ClaimJobHandler() {}
    
    public void bulkBefore() {
        if(Trigger.IsInsert){          
            for(Sobject claimJobTemp : trigger.new){
              // This is to set trigger Claim Hub status update as part of Jitterbit integration
              Claim_Job__c claimJobRec = (Claim_Job__c) claimJobTemp;                
              if(claimJobRec.Status__c == CLAIM_JOB_STATUS_COMPLETED|| claimJobRec.Status__c == CLAIM_JOB_STATUS_ACCEPTED){
                claimJobRec.ClaimHub_Status_Update__c=CLAIM_HUB_STATUS_UPDATE;
              }
                  
            }        
         }
        if(Trigger.IsUpdate){         
            List<Job_Margin__mdt> marginList = [SELECT MasterLabel, QualifiedApiName, Margin__c, DeveloperName FROM Job_Margin__mdt];       
            Decimal dAndCMargin;
            Decimal contentsMargin;
            map<string, decimal> jobMarginMap = new map<string, decimal>();
            for(Job_Margin__mdt margin : marginList) {
                jobMarginMap.put(margin.DeveloperName, margin.Margin__c);
            }
            for(Sobject claimJobTemp : trigger.new) { 
                  //This is to set trigger Claim Hub status update as part of Jitterbit integration
                  Claim_Job__c claimJobRec = (Claim_Job__c) claimJobTemp;
                  Claim_Job__c oldClaimJob = (Claim_Job__c) Trigger.OldMap.get(claimJobTemp.Id);  
                  if(claimJobRec.Status__c != oldClaimJob.Status__c){
                      if(claimJobRec.Status__c == CLAIM_JOB_STATUS_COMPLETED){
                        claimJobRec.ClaimHub_Status_Update__c=CLAIM_HUB_STATUS_UPDATE;
                      }
                  }
                  Decimal margin = 0;
                  if(jobMarginMap.containsKey(claimJobRec.Job_Type__c))
                      margin = jobMarginMap.get(claimJobRec.Job_Type__c);
                  else
                      margin = 14.5;
                  system.debug('margin==>'+margin);
                  claimJobRec.Non_cash_settled_margin__c = (claimJobRec.Total_Labour_NonCashSettled__c + claimJobRec.Total_Material_NonCashSettled__c)*(margin/100);
                  claimJobRec.Cash_settled_margin__c = (claimJobRec.Total_Labour_CashSettled__c + claimJobRec.Total_Material_CashSettled__c)*(margin/100);
            }        
         }
    }
    public void bulkAfter() {
        if(Trigger.IsUpdate){          
            for(Sobject claimJobTemp : trigger.new){ 
                  Claim_Job__c claimJobRec = (Claim_Job__c) claimJobTemp;   
                  claimJobList.add(claimJobRec);
            }        
         }   
         if(Trigger.IsInsert) {
             system.debug('update claim status to new when new claim job created.');
             List<Id> caseIdList = new List<Id>();
             for(Sobject claimJobTemp : trigger.new){ 
                 caseIdList.add((Id)claimJobTemp.get('Claim__c'));                
             }
             List<case> caseList = [SELECT Id, Status From Case WHERE Id IN: caseIdList and Status =: NJ_CONSTANTS.CASE_STATUS_CLOSED];
             for(Case claim: caseList ) {
                 claim.status = NJ_CONSTANTS.SA_STATUS_NEW; 
             }
             system.debug('caseList==>'+caseList);
             update caseList; 
         }
                                             
    }
    public void beforeInsert(SObject so) {
    } 
    public void afterInsert(SObject so) {
    } 
    public void beforeUpdate(SObject oldSo, SObject so) {

    }
    public void beforeUnDelete(SObject so) {} 
    
    public void afterUpdate(SObject oldSo, SObject so) {} 
    public void beforeDelete(SObject so){} 
    public void afterDelete(SObject so) {} 
    public void afterUnDelete(SObject so) {} 
    public void andFinally() {
    }               
}