/************************************************************************************************************
Name: ClaimJobHandler
=============================================================================================================
Purpose: Class for Claim Job Trigger as Handler.
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL       DESCRIPTION
1.0        Vasu         20/02/2019       Created       Home Repair Claim System  
*************************************************************************************************************/
public with sharing class ClaimJobService {    
    
    /*********************************************
* Create completion call ta for Do and Charge job
**********************************************/     
    public static Void CompletionCallTaskDCJob(List<Claim_Job__c> claimJobList) {      
        List<Task> taskList=new List<Task>();
        Set<String> caseIds=new Set<String>();
        if (claimJobList.size() > 0 ){
            for(Claim_Job__c cj : claimJobList){
                if(cj.Claim__c != null){
                    caseIds.add(cj.Claim__c);               
                }           
            }
        }
        if(caseIds.size() > 0){           
            List<Case> caseNoList =[Select ID,CaseNumber,(select id from workOrders where Claim_Job_Type__c = 'doAndCharge' 
                                                          AND Status NOT IN('Job Complete','Closed', 'Job Complete Awaiting Pricing')) FROM Case where id IN : caseIds];
            
            List<User> userList=[Select Id FROM User
                                 WHERE Name = 'HR Claims'
                                 Limit 1];
            if(!caseNoList.isEmpty()){
                for(Case cs : caseNoList){
                    if(cs.workOrders.size() == 0){
                        taskList.add(createTask('Completion Call',cs.Id,userList[0].id));
                    }
                }
                
            }
            If (!taskList.isEmpty()){
                insert taskList;
            }                     
        }
    }    
    public static Task createTask(string taskDescription,string claimId, string userId){
        // New Task object to be created
        Task[] newTask = new Task[0];
        newTask.add(new Task(priority = 'Normal',
                             status = 'Not Started',
                             subject =taskDescription,
                             IsReminderSet = true,
                             Description=taskDescription,
                             OwnerId = userId,
                             ActivityDate=System.Today()+1,
                             ReminderDateTime = System.now()+1,
                             WhatId =  claimId));
        return newTask[0]; 
    }
}