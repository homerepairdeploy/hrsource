global class OnceOffUpdateVendorNote implements Database.Batchable<sObject> {
    
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query='select id,Authority__c, Claim_Job__c from Vendor_Note__c  where Claim_Job__c!=null';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<Vendor_Note__c> scope) {
        set<Id> CJIDset=new set<Id>();
        for(Vendor_Note__c t: scope){
            CJIDset.add(t.Claim_Job__c);
        }
        map<Id,Id> CJAuthMap=new map<Id,Id>();
        for(case c:[select id,CJID__c from case where CJID__c=:CJIDset]){
           CJAuthMap.put(c.CJID__c,c.id);
        }
        list<Vendor_Note__c> Vlst=new list<Vendor_Note__c>();
        for (Vendor_Note__c tk:scope){
             If (CJAuthMap.containskey(tk.Claim_Job__c)){
                 Vendor_Note__c tkitem=new Vendor_Note__c();
                 tkitem.Id=tk.Id;
                 tkitem.Authority__c=CJAuthMap.get(tk.Claim_Job__c); 
                 Vlst.add(tkitem);
             }
             
                         
             
        } 
        update Vlst;
       
      
    }
    
    global void finish(Database.BatchableContext bc){ 
        
    }
    
    
           
}