@isTest
public class CreateWorkOrderLineItemCtrlTest {
    @testSetup 
    static void setup(){
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        HomeRepairTestDataFactory.createProductForWorkcode('Test','100',1,'Test');
        //Create a claim Authority Record(Added by CRMIT)
        case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id);       
        HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1);
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
    }
    @isTest static void testcreateWorkOrderLineItemCtrl(){
        test.startTest();
            
        	Id pbId = Test.getStandardPricebookId();
        	//string sPricebookEntryId = [Select ID,Product2Id FROM PricebookEntry limit 1].Id;
        	string sPricebookEntryId = pbId;	
            string sProduct2Id = [Select ID,Product2Id FROM PricebookEntry limit 1].Product2Id;
            HomeRepairTestDataFactory.createPriceBookEntry(1,'Standard Price Book',sProduct2Id,true);
            String workOrderId=[Select Id from WorkOrder limit 1].Id;
            WorkOrderLineItem woli=[Select WorkOrderId,Labour_Time__c,Quantity,PricebookEntryId from WorkOrderLineItem limit 1];
            WorkOrderLineItem woliNew=new WorkOrderLineItem();
            woliNew.PricebookEntryId=woli.PricebookEntryId;
            woliNew.Quantity=woli.Quantity;
            woliNew.WorkOrderId=woli.WorkOrderId;
            //woliNew.PricebookEntryId=sPricebookEntryId;
            createWorkOrderLineItemCtrl.getWorkOrderDetails(workOrderId);
            createWorkOrderLineItemCtrl.createWorkOrderLineItem(woliNew);
           WorkOrderLineItem woliNew1=new WorkOrderLineItem();
                    createWorkOrderLineItemCtrl.createWorkOrderLineItem(woliNew1);

        test.stopTest();
    }
    @isTest static void testdeleteWorkOrderLineItem(){
                   
        WorkOrderLineItem woli=[Select WorkOrderId,Labour_Time__c,Quantity,PricebookEntryId from WorkOrderLineItem limit 1];            
        Test.startTest();
            delete woli;
        Test.stopTest();
        
    }
}