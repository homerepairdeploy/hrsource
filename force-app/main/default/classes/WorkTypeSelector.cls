public with sharing class WorkTypeSelector {
    public static  List<WorkType> getWorkTypes () {
        return [Select Name, Id, DurationType, RecordType.Name from WorkType where RecordType.Name='Repair Items' or RecordType.Name='MakeSafe Items' order by Name];
    }
}