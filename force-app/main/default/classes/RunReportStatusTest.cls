@isTest
public class RunReportStatusTest {
    @testSetup 
    static void setup(){
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
       
    }  
    public static testmethod void testReportStatus() { 
            case c=[select id,cjid__c from case limit 1];
                   
            Test.startTest(); 
               
                database.executeBatch(new RunReportStatus(),1);
				database.executeBatch(new RunAuthReportStatus(),1);
               
            Test.stopTest();
        
    }
    
}