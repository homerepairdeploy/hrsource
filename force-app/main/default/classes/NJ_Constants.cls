/**
 * Constants Class 
 * @CreatedDate : 06/11/2018
 * @CreatedBy : Nikhil Jaitly
 */
public with sharing class NJ_Constants {
    public static final String CLAIM_STATUS_ASSESSMENT_SCHEDULED = 'Assessment Scheduled';
    public static final String CLAIM_STATUS_WO_SCHEDULED = 'Work Order Scheduled';
    public static final String SA_STATUS_TENTATIVE = 'Tentative';
    public static final String SA_STATUS_NEW = 'New';
    public static final String SA_STATUS_CONFIRMED = 'Confirmed';
    public static final String SA_STATUS_AWAITINGCONFIRMATION = 'Awaiting Confirmation';
    public static final String SA_STATUS_COMPLETED = 'Completed';
    public static final String SA_STATUS_CANCELLED = 'Cancelled';
    public static final String SA_STATUS_CANNOTCOMPLETE = 'Cannot Complete';

    public static final String WO_STATUS_ASSIGNED = 'Work Order Assigned';
    public static final String WO_STATUS_CANCELLED = 'Cancelled';
    public static final String WO_STATUS_SCHEDULED = 'Work Order Scheduled';
    public static final String WO_STATUS_ASSESSMENT_SCHEDULED = 'Assessment Scheduled';
    public static final String WO_STATUS_ACCEPTED = 'Accepted WO';
    public static final String WO_STATUS_REJECTED = 'Rejected WO';
    public static final String WO_STATUS_JOBCOMPLETE = 'Job Complete';
    public static final String WO_STATUS_NEW = 'New';
	public static final String WO_PRICED = 'Priced Work Order';																		 

    public static final Set<String> UPTOAMOUNTS = new Set<String> { NJ_Constants.WORK_TYPE_ASSESSMENT,
                                                                    NJ_Constants.WORK_TYPE_REASSESSMENT,
                                                                    NJ_Constants.WORK_TYPE_QUALITY_ASSURANCE,
                                                                    NJ_Constants.WORK_TYPE_INTERNALASSESSORS
                                                                  };
    public static final String VARIATION_STATUS_NEW = 'New';
    public static final String VARIATION_REASON_ADDITIONALSCOPE = 'Additional Scope';

    public static final String WO_STATUS_WORKCOMMENCED = 'Work Commenced';
    public static final String WO_STATUS_WORKCOMPLETE = 'Work Complete';
    public static final String WO_STATUS_CLOSED = 'Closed';
    public static final String STANDARD_PRICEBOOK_NAME = 'Standard Price Book';

    public static final String VARIATION_STATUS_APPROVED = 'Approved';
    public static final String VARIATION_STATUS_REJECTED = 'Rejected';
    public static final String WORKORDERLINEITEM_RT_VARIATION = 'Variation';
    public static final String SERVICE_APPOINTMENT_RT_GENERAL = 'Service_Appointment_General';
    public static final String WORK_ORDER_RT_HOMEREPAIRTRADES = 'Home_Repair_Trades';
    public static final String WORKORDER_LOCKED_RT_DEVELOPERNAME = 'Home_Repair_Trades_Locked';
    public static final String SERVICEAPPOINTMENT_LOCKED_RT_DEVELOPERNAME = 'SA_Locked';
    
    //Jitendra 
    public static final String ARINVOICE_STATUS_APPROVED = 'Approved';
    Public static final string ARINVOICE_STATUS_SUBMITTED = 'Submitted';
    Public static final string AR_CM_FEE_CREDIT_MEMO = 'CM Fees Credit Memo';
    Public static final string AR_CM_FEE = 'CM Fees';
	Public static final string AR_ASSESSMENT_FEE_CREDIT_MEMO = 'Assessment Fee Credit Memo';
    Public static final string AR_ASSESSMENT_FEE = 'Assessment Fee';																					 
	Public static final string AR_CUSTOMER_CREDIT_MEMO = 'Customer Credit Memo';
    Public static final string AR_CUSTOMER = 'Customer';
    public static final String CLAIM_JOB_STATUS_COMPLETED = 'Completed';
    public static final String ARINVOICE_STATUS_REJECTED = 'Rejected';    
    public static final String HR_CLAIM = 'HR Claims';
    public static final string TASK_PRIORITY_NORMAL = 'Normal';
    public static final string TASK_PRIORITY_IMPORTANT = 'Important';
    public static final string TASK_STATUS_NOT_STARTED = 'Not Started';
    public static final string TASK_STATUS_COMPLETED = 'Completed';
    public static final string INVOICE_REJECTED = 'AR Invoice Rejected';

    public static final String COMMUNITY_PROFILE_NAME = 'Trade Platfrom Community Plus User';
    
    public static final string TASK_FOLLOWER_SUBJECT = 'New follow up on closed claim'; 
    public static final string TASK_FOLLOWER_SUBJECT_CLOSED = 'Closed Claim followup';
    Public static final string TASK_COMPLETION_CALL_SUBJECT = 'Completion Call'; 
    //Task Intro Call
    //Public static final string TASK_INTRO_CALL_SUBJECT = 'Intro Call'; 
    Public static final string FINAL_CONTACT_CALL_SUBJECT = 'Final Contact Customer Attempt';
    public static final string CASE_STATUS_CLOSED = 'Closed';
    Public static final string JOB_TYPE_ASSESMENT = 'assessment';
	Public static final string JOB_TYPE_REPORT = 'report';	
    Public static final string JOB_TYPE_QUOTE = 'Quote';	
    Public static final string JOB_TYPE_DOANDCHARGE = 'doAndCharge';
    Public static final string JOB_TYPE_CONTENT = 'Contents';
    Public static final String LINE_ITEM_CREDIT_MEMO ='Credit Memo';
    Public static final string INVOICE_DRAFT = 'Draft';
    Public static final string INVOICE_CUSTOMER = 'Customer';
    Public static final string INVOICE_CMFEES = 'CM Fees';
    Public static final string INVOICE_CREDIT_MEMO_CUSTOMER = 'Customer Credit Memo';

    public static final string AP_INVOICE_APPROVALS = 'AP_Invoice_Approvals'; 
    public static final string AP_QUEUE = 'AP'; 
    public static final String DOCUMENT_TYPE_QUOTE = 'Quote';
    public static final string WORK_TYPE_ASSESSMENT = 'Assessment';
    public static final string WORK_TYPE_REASSESSMENT = 'Reassessment';
    public static final string WORK_TYPE_QUALITY_ASSURANCE = 'Quality Assurance';
    public static final string WORK_TYPE_COORD_ASSESSMENT = 'Coordinator Assessment';
    public static final string WORK_TYPE_INTERNALASSESSORS = 'Internal/Assessors';

    public static final String WORK_TYPE_STARTS_WITH_MAKESAFE = 'MS';
    public static final String WORK_TYPE_STARTS_WITH_HOMEASSIST = 'HA';
    public static final String TRADE_UPDATE_TEXT='Trade Update';
    public static final String PL_INSURANCE_TEXT='Public Liability Insurance Expired';
    public static final String WC_INSURANCE_TEXT='Work Cover Insurance Expired';
    public static final String WC_INSURANCE_TYPE='WC';
    public static final String PL_INSURANCE_TYPE='PL';
    //Work Authority type constants and Template constants
    public static final String HOMEASSIST='Home Assist';
    public static final String MAKESAFE='Make Safe';
    public static final String LIABILITY='Liability Quote Required';
    public static final String PRICEDWO='Priced Work Order';
    public static final String RECTIFICATION='Rectification';
    public static final String QUOTEAPPR='Quote Approved';
    public static final String REPQUOT='Report and Quote';
    public static final String DOCHG='Do and Charge';
    public static final String GWORKS = 'General Works';
    
    public static final String HOMEASSISTTEMP='HomeAssistAuthTemp';
    public static final String MAKESAFETEMP='MakeSafeGeneralAuthTemp';
    public static final String LIABILITYTEMP='LiabilityQuoteRequiredAuthTemp';
    public static final String PRICEDWOTEMP='PricedWOAuthTemp';
    public static final String RECTIFICATIONTEMP='RectificationAuthTemp';
    public static final String QUOTEAPPRTEMP='QuoteApprovedAuthTemp';
    public static final String REPQUOTTEMP='RQGeneralAuthTemp';
    public static final String DOCHGTEMP='CustomDoCharge';
    
    public static final String MSASBESTOS='MSAsbestos';
    public static final String MSASBESTOSAUTHTEMP='MakeSafeAsbestosAuthTemp';
    public static final String MSTREELOOPER='MSTreeLooper';
    public static final String MSTREELOOPERAUTHTEMP='MakeSafeTreeLooperAuthTemp';
    public static final String MSROOFERMETAL='MSRooferMetal';
    public static final String MSROOFERTILE='MSRooferTile';
    public static final String MSROOFERAUTHTEMP='MakeSafeRooferAuthTemp';
    public static final String MSRESTORER='MSRestorer';
    public static final String MSRESTORERAUTHTEMP='MakeSafeRestorerAuthTemp';
    public static final String MSTEMPFENCING='MSTempFencing';
    public static final String MSTEMPFENCINGAUTHTEMP='MakeSafeTempFencingAuthTemp';
    public static final String RQLEAKDETECTION='RQLeakDetection';
    public static final String RQLEAKAUTHTEMP='ReportQuoteLeakAuthTemp';
    public static final String RQMETAL='RQMetal';
    public static final String RQPOLYCARB='RQPolycarb';
    public static final String RQTILES='RQTiles';
    public static final String RQROOFERAUTHTEMP='ReportQuoteRooferAuthTemp';
    public static final String RQENGINEER='RQEngineer';
    public static final String RQENGINEERAUTHTEMP='ReportQuoteEngineerAuthTemp';
    
    public static final Set<String> ASSREASSSET = new Set<String> { NJ_Constants.WORK_TYPE_ASSESSMENT,NJ_Constants.WORK_TYPE_REASSESSMENT };
    //Email Template Developer Names
    public static final String X7DAYS_PL_EXPIRY='X7_days_from_Public_Liability_Expiry';
    public static final String X7DAYS_WC_EXPIRY='X7_days_from_Work_Cover_Expiry';
    //Added assessment types to update status
    public static final Set<String> ASSESSTYPES = new Set<String> { NJ_Constants.WORK_TYPE_ASSESSMENT,
                                      NJ_Constants.WORK_TYPE_REASSESSMENT,
                                      NJ_Constants.WORK_TYPE_COORD_ASSESSMENT,
                                      NJ_Constants.WORK_TYPE_INTERNALASSESSORS};
    Public static final string WORK_TYPE_INTERNAL = 'Internal/Assessors';

    Public static final string TRADE_INVOICE = 'Invoice';
    Public static final string AP_CREDIT_MEMO = 'Supplier Credit Memo';
    Public static final string AP_INVOICE_STATUS_SUBMITTED = 'Submitted';
    Public static final string AP_INVOICE_STATUS_APPROVED = 'Approved';
    Public static final string AP_ORACLE_PROCESSING_STATUS_PENDING = 'Pending';
    
    Public static final string QUALITY_ASSURANCE_TASK = 'Quality Assurance Review';
   
}