public class APInvoiceCreditMemo_cc {
    @auraEnabled
    public static string createCreditMemo(String supplierNumber, String recordId, string creditMemoDetails, Boolean isPartialCreditNote, Decimal labourAmount, Decimal materialAmount) {
        Savepoint sp;
        try {
            sp = Database.setSavepoint();
            RecordType rt = [Select Id FROM RecordType WHERE DeveloperName = 'AP_Invoice'];
            List<AggregateResult> summaryResult = [SELECT SUM(Labour_ex_GST__c) labourWithoutGST,
                                                        SUM(Material_ex_GST__c) materialWithoutGST, SUM(Total_ex_GST__c) total
                                                        FROM AP_Invoice__c WHERE Parent_Invoice__c =: recordId AND Invoice_Type__c =: NJ_CONSTANTS.AP_CREDIT_MEMO AND Status__c !=: NJ_CONSTANTS.ARINVOICE_STATUS_REJECTED];
            system.debug('summaryResult Init==>'+summaryResult);
            String queryString = 'Select '+getAllFields('AP_Invoice__c');
            string lineItemString = ', Work_Order__r.Total_Labour_Amount_New__c, Work_Order__r.Total_Material_Amount_New__c, Work_Order__r.Service_Resource_Company__c, (SELECT '+getAllFields('AP_SOW__c')+' FROM AP_SOWs__r ) ';
            queryString += lineItemString+' From AP_Invoice__c Where Id=\''+String.escapeSingleQuotes(recordId)+'\' Limit 1';
            
            system.debug(queryString);
            AP_Invoice__c invoice= (AP_Invoice__c)Database.query(queryString);
            
            if(invoice != null){
                AP_Invoice__c newInvoice = invoice.clone();
                newInvoice.Partial_Credit_Note__c = isPartialCreditNote;
                newInvoice.Status__c = NJ_CONSTANTS.AP_INVOICE_STATUS_SUBMITTED; 
                newInvoice.Supplier_Invoice_Ref__c = supplierNumber;
                newInvoice.Credit_Memo_Details__c = creditMemoDetails;  
                newInvoice.Parent_Invoice__c = recordId;
                newInvoice.Invoice_Type__c = NJ_CONSTANTS.AP_CREDIT_MEMO;  

                if (isPartialCreditNote) {
                    newInvoice.Labour_ex_GST__c = labourAmount;
                    newInvoice.Material_ex_GST__c = materialAmount;
                }
                else {
                    newInvoice.Labour_ex_GST__c = -invoice.Labour_ex_GST__c;
                    newInvoice.Material_ex_GST__c = -invoice.Material_ex_GST__c;
                }

                //newInvoice.Oracle_Process_Status__c = NJ_CONSTANTS.AP_ORACLE_PROCESSING_STATUS_PENDING ;
                newInvoice.Oracle_Process_Status__c = '';
                newInvoice.New_Trade_Account__c = invoice.Work_Order__r.Service_Resource_Company__c;
                //Accounting Date is set to null when new Credit memo is created
                //Accounting Date will be approval date of Credit memo - AP Invoice Automation
                newInvoice.Accounting_Date__c = Null;
                if(summaryResult.size() > 0) {
                    newInvoice.Adjustment_Amount_Labour__c = (decimal)summaryResult[0].get('labourWithoutGST');
                    newInvoice.Adjustment_Amount_Materials__c = +(decimal)summaryResult[0].get('materialWithoutGST'); 
                }
                if(newInvoice.Adjustment_Amount_Labour__c < 0) 
                    newInvoice.Adjustment_Amount_Labour__c = newInvoice.Adjustment_Amount_Labour__c * -1;
                if(newInvoice.Adjustment_Amount_Materials__c < 0)
                    newInvoice.Adjustment_Amount_Materials__c = newInvoice.Adjustment_Amount_Materials__c * -1;
                system.debug('newInvoice.Adjustment_Amount_Labour__c ==>'+newInvoice.Adjustment_Amount_Labour__c+' newInvoice.Adjustment_Amount_Materials__c==>'+newInvoice.Adjustment_Amount_Materials__c);
                newInvoice.RecordTypeId = rt.Id;
                insert newInvoice;
                
                Map<Id, AP_SOW__c> lineItemMap;
                List<AP_SOW__c> lineItemList = new List<AP_SOW__c>();
                if(!invoice.AP_SOWs__r.isEmpty()) {
                    lineItemMap = new Map<Id, AP_SOW__c>(invoice.AP_SOWs__r);    
                    lineItemList = invoice.AP_SOWs__r.clone();
                    
                    for(AP_SOW__c item : lineItemList) {                
                        item.Id = null;
                        item.AP_Invoice__c = newInvoice.Id;                                  
                    }
                }
            
             
                system.debug('lineItemList==>'+lineItemList);
                if(!lineItemList.isEmpty())
                    insert lineItemList;
                
                return 'Success:'+newInvoice.Id+'';
            }
            return null;
        }
        catch (System.DmlException e) { 
            Database.rollback(sp);
            system.debug('Exception Occured==>'+e.getMessage()+' :: StackTrace==>'+e.getStackTraceString());
            List<String> errorMessageList = new List<String>();
            errorMessageList.add(e.getDmlMessage(0));            
            String errorMessage = String.join(errorMessageList,',');
            return errorMessage;
        }
        catch (Exception e) {
            Database.rollback(sp);
            system.debug('Exception Occured==>'+e.getMessage()+' :: StackTrace==>'+e.getStackTraceString());
            return e.getMessage();
        }
    } 
    private static String getAllFields(String objectName){
        Schema.SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult r = objectType.getDescribe();
        List<String>apiNames =  new list<String>();
        for(string apiName : r.fields.getMap().keySet()){
            apiNames.add(apiName);
        }
        return string.join(apiNames,',');
    }

    @AuraEnabled
    public static string createCreditMemoAR(ARInvoiceLWCWrapper arinvoicewrapper) {
       // Savepoint sp;
       system.debug('arinvoicewrapper ' +arinvoicewrapper);
        try {
         //   sp = Database.setSavepoint();
            String queryString = 'Select '+getAllFields('AR_Invoice__c');
            string lineItemString = ',(SELECT '+getAllFields('AR_SOW__c')+' FROM AR_SOWs__r )';
            queryString += lineItemString+' From AR_Invoice__c Where Id=\''+String.escapeSingleQuotes(arinvoicewrapper.recordId)+'\' Limit 1';
           
            system.debug(queryString);
            AR_Invoice__c invoice= (AR_Invoice__c)Database.query(queryString);
                      
            if(invoice != null){
                AR_Invoice__c newInvoice = invoice.clone();
                newInvoice.Status__c = NJ_CONSTANTS.AP_INVOICE_STATUS_SUBMITTED; 
                newInvoice.Credit_Memo_Details__c = arinvoicewrapper.creditMemoDetails;
                newInvoice.ParentInvoice__c = arinvoicewrapper.recordId;
                If (invoice.Invoice_Type__c == NJ_CONSTANTS.AR_CM_FEE){
                    newInvoice.Invoice_Type__c = NJ_CONSTANTS.AR_CM_FEE_CREDIT_MEMO;
                    newInvoice.Labour_ex_GST__c = -invoice.Labour_ex_GST__c;
                    newInvoice.Material_ex_GST__c = -invoice.Material_ex_GST__c;
                }
                else{
                   newInvoice.Invoice_Type__c = NJ_CONSTANTS.AR_CUSTOMER_CREDIT_MEMO;  
                    If  (arinvoicewrapper.partialAmount){
                        newInvoice.Labour_ex_GST__c = -arinvoicewrapper.labourAmtExGST;
                        newInvoice.Material_ex_GST__c = -arinvoicewrapper.materialAmtExGST;
                    }
                    else{
                        newInvoice.Labour_ex_GST__c = -invoice.Labour_ex_GST__c;
                        newInvoice.Material_ex_GST__c = -invoice.Material_ex_GST__c;
                    }       
                }   

                newInvoice.ClaimHub_Upload_Status__c='';
                newInvoice.ClaimHub_Upload_Error_Message__c='';
                newInvoice.Payment_Terms__c='';
                //newInvoice.Oracle_Process_Status__c = NJ_CONSTANTS.AP_ORACLE_PROCESSING_STATUS_PENDING ;
                newInvoice.Oracle_Process_Status__c = '';
                //Accounting Date is set to null when new Credit memo is created
                //Accounting Date will be approval date of Credit memo - AP Invoice Automation
                newInvoice.Accounting_Date__c = Null;
                insert newInvoice;
                
                Map<Id, AR_SOW__c> lineItemMap;
                List<AR_SOW__c> lineItemList = new List<AR_SOW__c>();
                if(!invoice.AR_SOWs__r.isEmpty()) {
                    lineItemMap = new Map<Id, AR_SOW__c>(invoice.AR_SOWs__r);    
                    lineItemList = invoice.AR_SOWs__r.clone();
                    
                    for(AR_SOW__c item : lineItemList) {                
                        item.Id = null;
                        item.AR_Invoice__c = newInvoice.Id;                                  
                    }
                }
            
             
                system.debug('lineItemList==>'+lineItemList);
                if(!lineItemList.isEmpty())
                    insert lineItemList;
                
                return newInvoice.Id;
            }
            return null;
         }
        catch (System.DmlException e) { 
           // Database.rollback(sp);
            system.debug('Exception Occured==>'+e.getMessage()+' :: StackTrace==>'+e.getStackTraceString());
            List<String> errorMessageList = new List<String>();
            errorMessageList.add(e.getDmlMessage(0));            
            String errorMessage = String.join(errorMessageList,',');
            return errorMessage;
        }
        catch (Exception e) {
          //  Database.rollback(sp);
            system.debug('Exception Occured==>'+e.getMessage()+' :: StackTrace==>'+e.getStackTraceString());
            return e.getMessage();
        }    

        
    }
}