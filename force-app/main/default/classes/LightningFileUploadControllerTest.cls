/* =====================================================================================
Type:       Test class
Purpose:    Test cases for LightningFileUploadController (For LightningFileUpload Component )
========================================================================================*/
@isTest
private class LightningFileUploadControllerTest{
    static testMethod void fetchContentDocumentAnduploadFileTest() {
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);  
        HomeRepairTestDataFactory.addContentVersionToParent(cs.Id);
        ContentVersion cnt = [SELECT Id, ContentDocumentId FROM ContentVersion limit 1]; 
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        String base64Data= EncodingUtil.base64encode(bodyBlob);
         //Create the claim Authority(Added by CRMIT)l.
        case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id); 
        LightningFileUploadController.updateFileDescription(wo.id,'Invoice',false,true,cs.id,cnt.ContentDocumentId,ca.id);
        LightningFileUploadController.fetchCustomMetaDataValues('Assessment Report');
        LightningFileUploadController.fetchCustomMetaDataValues(cs.id);
        //Below line of code added by CRMIT
        LightningFileUploadController.fetchContentDocument(cs.id);
    }
}