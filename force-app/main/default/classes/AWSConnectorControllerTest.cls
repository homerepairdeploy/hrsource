@isTest
public class AWSConnectorControllerTest
{

public static testMethod void testCall()
{
  Test.startTest();
   RestRequest req= new RestRequest();
      Map<String,String> headers = new Map<String,String> {
        'region' => 'ap-southeast-2',
        'service' => 's3',
        'host' => 'hr-clouddata-dev.s3.amazonaws.com',
        'path' => '/hrdev1/Case/test.png',
        'method' => 'GET'
           
       
    };
     String reqJson = JSON.serialize(headers );
   req.addParameter('region', 'ap-southeast-2');
   req.addParameter('service', 's3');
   req.addParameter('host', 'hr-clouddata-dev.s3.amazonaws.com');
   req.addParameter('path', '/hrdev1/Case/test.png');
   req.addParameter('method', 'GET');
  
   req.requestBody = Blob.valueof(reqJson );
   
   RestContext.request = req;
  
   AWSConnectorController.JsonWrap  jsWrap = AWSConnectorController.doPost();
    String myJSON = JSON.serialize(jsWrap );
   System.assertEquals(true,myJSON.contains('canonicalRequest'));
   Test.stopTest();
}

}