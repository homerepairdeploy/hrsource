@isTest
public class homeRepairSupplierInvCreationServiceTest {
    @TestSetup
    static void TestdataCreateMethod() {
        //Create the Account Record.
        Account acc = HomeRepairTestDataFactory.createAccounts('Test WorkOrder Account')[0];        
        //Create the Contact Record.
        contact conVar = HomeRepairTestDataFactory.createTradeContact('test WO Contatct',acc.Id);        
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);                
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        Case childCaseObj = HomeRepairTestDataFactory.createChildClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrdersWithCase('Home Repair Trades',cs.id,wt.id,childCaseObj.id);
        List<WorkOrderLineItem> woliList=HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1);
    }
    @isTest
    static void createInvoiceRecordTest(){
        WorkOrder wo = [Select id,CaseId,WorkTypeId,status,Authority__c from WorkOrder];
        Map<Id,String> workorderMap = new Map<Id,String>();
        workorderMap.put(wo.Id, wo.status); 
        homeRepairSupplierInvCreationService.createInvoiceRecord(workorderMap);
        
    }
}