@RestResource(urlMapping='/AWSConnect')
global class AWSConnectorController
{
    @HttpPost
    global static JsonWrap doPost() {

    String region = '';
    String service = '';
    String host = '';
    String path = '';
    String method = '';
    String querystring = '';
    String additionalHeaders = '';
    
    Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(
        RestContext.request.requestBody.toString()
    );

    region = String.valueOf(params.get('region'));
    service = String.valueOf(params.get('service'));
    host = String.valueOf(params.get('host'));
    path = String.valueOf(params.get('path'));
    method = String.valueOf(params.get('method'));
    querystring = String.valueOf(params.get('query'));
    additionalHeaders = String.valueOf(params.get('headers'));
        
    Datetime now = Datetime.now();
    String iso8601time = now.formatGmt('YYYYMMdd\'T\'HHmmss\'Z\'');
    String iso8601date = now.formatGmt('YYYYMMdd');
    String termination = 'aws4_request';
    String accessKey  = 'AKIAXXXPPWJB5ML4ULW7'; // AWS Access ID
    String secret = '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'; // AWS Secret key
    String credentialScope = iso8601date + '/' + region + '/' + service + '/' + termination; 
    
    // Default headers
    Map<String,String> headers = new Map<String,String> {
        'x-amz-date' => iso8601time,
        'Host' => host,
        'x-amz-content-sha256' => 'UNSIGNED-PAYLOAD'
    };
       
    // Get passed in headers and add them to list    
    if (!String.isBlank(additionalHeaders)) {
        List<String> additionalHeaderStrs = additionalHeaders.split(';');
        for(String headerStr : additionalHeaderStrs)
        {
            String[] parts = headerStr.split(':');
            headers.put(parts[0], parts[1]);
        }
    }
    
    List<String> headersList = new List<String>(headers.keySet());
    for (Integer i = 0; i < headersList.size(); i++) {
        headersList[i] = headersList[i].toLowerCase();
    }
    headersList.sort();
    
    //Task 1: Create a Canonical Request for Signature Version 4
    String canonicalRequest = createCanonicalRequest(method, path, querystring, host, headers, 'UNSIGNED-PAYLOAD');
    
    //Task 2: Create a String to Sign for Signature Version 4
    String stringToSign = getStringToSign(canonicalRequest, iso8601time, iso8601date, region, service);
    
    //Task 3: Calculate the AWS Signature Version 4
    String sign = signature(secret, iso8601date, region, service, stringToSign);
    
    //Task 4: Add the Signing Information to the Request
    String authorization = 'AWS4-HMAC-SHA256'
    + ' ' + 'Credential=' + accessKey  + '/' + credentialScope
    + ', ' + 'SignedHeaders=' + String.join(headersList, ';')
    + ', ' + 'Signature=' + sign;
            
    headers.put('Authorization', authorization);
    JsonWrap jsWrap = new JsonWrap(canonicalRequest, stringToSign, sign, authorization, headers);

    return jsWrap;
}
  
public static String createCanonicalRequest(String method, String path, String querystring,String host,Map<String, String> headers, String payload){
    String canonicalRequest = method + '\n';
    canonicalRequest += path + '\n'; 
    if (!String.isBlank(querystring)){
      canonicalRequest += queryString + '\n';
    } else {
      canonicalRequest += '\n';
    }
    List<String> headersList = new List<String>(headers.keySet());
    headersList.sort();

    for (String header : headersList){
        String headerContent = header.toLowerCase() + ':' + headers.get(header).trim();
        canonicalRequest += headerContent + '\n';
    }
  canonicalRequest += '\n';
    
    for (String header : headersList){
        canonicalRequest += header.toLowerCase() + ';';
    }

    canonicalRequest = canonicalRequest.removeEnd(';');
    canonicalRequest += '\n';
    canonicalRequest += 'UNSIGNED-PAYLOAD';

    return canonicalRequest;
}

public static  String getStringToSign(String canonicalRequest, String requestdatetime, String requestdate, String region, String service){
    String stringToSign = 'AWS4-HMAC-SHA256' + '\n';
    stringToSign += requestdatetime + '\n';
    stringToSign += requestdate + '/' + region + '/' + service + '/aws4_request\n'; 
    stringToSign += EncodingUtil.convertToHex(Crypto.generateDigest('SHA-256', Blob.valueOf(canonicalRequest))).toLowerCase();

    return stringToSign;
}

public static  String signature(String secret, String iso8601date, String region, String service, String stringToSign){
    Blob kDate = Crypto.generateMac('HmacSHA256', Blob.valueOf(iso8601date), Blob.valueOf('AWS4'+secret));
  Blob kRegion = Crypto.generateMac('HmacSHA256', Blob.valueOf(region), kDate);
  Blob kService = Crypto.generateMac('HmacSHA256', Blob.valueOf(service), kRegion);
  Blob kSigning = Crypto.generateMac('HmacSHA256', Blob.valueOf('aws4_request'), kService);
  String strSignature = EncodingUtil.convertToHex(Crypto.generateMac('HmacSHA256', Blob.valueOf(stringToSign), kSigning));
  strSignature = strSignature.toLowerCase();
  
    return strSignature;
}

global class JsonWrap{
    public String Authorization;
    public String canonicalRequest;
    public String stringToSign;
    public String signature;
    public Map<String,String> headers;
          
    public JsonWrap(String c, String s, String sign, String auth, Map<String,String> h) {
        this.canonicalRequest= c;
        this.stringToSign= s;
        this.signature= sign;
        this.Authorization = auth;
        this.headers= h;
    }
}
}