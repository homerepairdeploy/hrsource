@IsTest
private class EmailRCTITest {
    @IsTest
    static void EmailRCTI(){
        
        EmailTemplate validEmailTemplate = new EmailTemplate();
        
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'RCTI_PDF_Invoice_Email';
        validEmailTemplate.DeveloperName = 'RCTI_PDF_Invoice_Email';
        validEmailTemplate.TemplateType = 'text';
        validEmailTemplate.FolderId = UserInfo.getUserId();
        validEmailTemplate.Subject = 'test';
        validEmailTemplate.Body = 'test';
        
        insert validEmailTemplate;

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {

            Account acc = new Account(Name = 'Test Acc',RCTI_NEW__c = true, Trade_RCTI_Date__c = System.today());
            insert acc;

            WorkType workStatusObj = new WorkType(Name = 'Cabinetmaker', EstimatedDuration = 4, DurationType = 'Hours');//[SELECT Id FROM WorkType where Name = 'Cabinetmaker' limit 1];
            insert workStatusObj;

            Id caseRecId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Authority').getRecordTypeId();

            case testCase = new Case(Assessment_Count__c = 2, Status = 'New');
            insert testCase;

            case testCase2 = new Case(Assessment_Count__c = 2, Status = 'New', RecordTypeId = caseRecId, ParentId = testCase.Id);
            insert testCase2;

            workOrder testWo = new workOrder();
            testWo.Accepted_WO_Date__c = System.today().addDays(2);
            testWo.Service_Resource_Company__c = acc.Id;
            testWo.WorkTypeId = workStatusObj.Id;
            testWo.CaseId = testCase.Id;
            testWo.Authority__c = testCase2.Id;
            insert testWo;
            
            AP_Invoice__c ap = new AP_Invoice__c();
            ap.Work_Order__c = testWo.Id;
            ap.Account_Email__c = 'test@test.com';
            ap.Invoice_Type__c = 'Supplier' ;
            ap.Oracle_Process_Status__c = 'Processed';
            ap.RCTI_PDF_Trade_Status__c = 'Pending';

            insert ap;

            Test.startTest();

            BatchEmailTaxInvoiceStatement batch = new BatchEmailTaxInvoiceStatement();
            Database.executeBatch(batch);

            Test.stopTest();

            AP_Invoice__c updatedAP = [SELECT Id, RCTI_PDF_Trade_Status__c FROM AP_Invoice__c WHERE Id = :ap.Id LIMIT 1];
            
            system.assertEquals('Sent', updatedAP.RCTI_PDF_Trade_Status__c, 'RCTI PDF Email is sent');
            
        }

    }
    
    @IsTest
    static void NoEmailRCTI(){
        
        EmailTemplate validEmailTemplate = new EmailTemplate();
        
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'RCTI_PDF_Invoice_Email';
        validEmailTemplate.DeveloperName = 'RCTI_PDF_Invoice_Email';
        validEmailTemplate.TemplateType = 'text';
        validEmailTemplate.FolderId = UserInfo.getUserId();
        validEmailTemplate.Subject = 'test';
        validEmailTemplate.Body = 'test';
        
        insert validEmailTemplate;

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {

            Account acc = new Account(Name = 'Test Acc',RCTI_NEW__c = true, Trade_RCTI_Date__c = System.today());
            insert acc;

            WorkType workStatusObj = new WorkType(Name = 'Cabinetmaker', EstimatedDuration = 4, DurationType = 'Hours');//[SELECT Id FROM WorkType where Name = 'Cabinetmaker' limit 1];
            insert workStatusObj;

            Id caseRecId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Authority').getRecordTypeId();

            case testCase = new Case(Assessment_Count__c = 2, Status = 'New');
            insert testCase;

            case testCase2 = new Case(Assessment_Count__c = 2, Status = 'New', RecordTypeId = caseRecId, ParentId = testCase.Id);
            insert testCase2;

            workOrder testWo = new workOrder();
            testWo.Accepted_WO_Date__c = System.today().addDays(2);
            testWo.Service_Resource_Company__c = acc.Id;
            testWo.WorkTypeId = workStatusObj.Id;
            testWo.CaseId = testCase.Id;
            testWo.Authority__c = testCase2.Id;
            insert testWo;

            AP_Invoice__c ap = new AP_Invoice__c();
            ap.Work_Order__c = testWo.Id;
            //ap.Account_Email__c = 'test@test.com';
            ap.Invoice_Type__c = 'Supplier' ;
            ap.Oracle_Process_Status__c = 'Processed';
            ap.RCTI_PDF_Trade_Status__c = 'Pending';
            insert ap;
            
            try
            {
                Test.startTest();

                BatchEmailTaxInvoiceStatement batch = new BatchEmailTaxInvoiceStatement();
                Database.executeBatch(batch);

                Test.stopTest();
            }
            catch (Exception e)
            {
                
            }
            
            
        }

    }

    @IsTest
    static void testSchedulale() {

        Test.startTest();
        ScheduledEmailTaxInvoiceStatement schedule = new ScheduledEmailTaxInvoiceStatement();
        schedule.execute(null);
        Test.stopTest();

    }
}