/***************************************************************** 
Purpose: Batch job to set Suppressed from Scheduling if Public Liability
Expiry or Work Cover Expiry has passed on an Account

Test class     : BatchSetSuppressedFromSchedulingTest
Scheduler class: BatchSetSuppressedFromSchedulingSch

History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            Harpreet       6/08/2019      Created        
*******************************************************************/

global class BatchSetSuppressedFromScheduling implements Database.Batchable<sObject>, Database.Stateful 
{

    /************************START METHOD*************************/
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
      String query = 'Select Id, Public_Liability_Expiry__c, Work_Cover_Expiry__c, Name from Account '
                   +' where Public_Liability_Expiry__c < Today or Work_Cover_Expiry__c < Today';
      
      System.debug('query '+query); 
      return Database.getQueryLocator(query);
    }
    
    
    /****************************EXECUTE METHOD**********************************/
    global void execute(Database.BatchableContext BC, List<Account> scope) 
    {
        
        //the list of Account records to update
        List<Account>  accounts_to_update =  new List<Account>();
        List<Task> tasks_to_create = new List<Task>();
        //iterate over all Accounts returned in query
        for (Account acc : scope) 
        {
            //set account's Suppressed_from_Scheduling__c = true
            acc.Suppressed_from_Scheduling__c = true;

            //add the account in the list to be updated
            accounts_to_update.add(acc);
            
            //Check which licence(s) have expired and create a string accordingly. 
            String sub = acc.Public_Liability_Expiry__c < Date.Today() ? 'Public Liability' : '';
            sub+= acc.Work_Cover_Expiry__c < Date.Today() && acc.Public_Liability_Expiry__c < Date.Today() ? ' and Work Cover' : 'Work Cover';
            
            //Create a new task for Account and assign it to Procurement Queue
            Task tsk               = new Task(
                priority           = 'Normal',
                status             = 'Not Started',
                subject            = sub + ' Licence has expired',
                IsReminderSet      = true,
                Description        = sub + ' Licence has expired for Account: ' + acc.Name,
                OwnerId            = System.Label.Procurement_Tasks_Owner,
                ActivityDate       = System.Today()+1,
                ReminderDateTime   = System.now()+1,
                WhatId             =  acc.Id
            );
            tasks_to_create.add(tsk);
            
        }//end of the acc for loop
        
        //update the Accounts if the list isn't empty
        if(!accounts_to_update.IsEmpty())
        {
            update accounts_to_update;
            insert tasks_to_create;
        }
    }
    
    /*****************FINISH METHOD******************/
    global void finish(Database.BatchableContext BC) {  
        
    }
}