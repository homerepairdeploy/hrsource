public class InvoiceStatementExtn{
    
    public InvoiceStatementExtn() {  
        
    }
    public AR_Invoice__c invoice{get;set;}
    public String        AddlCommts {get;set;}
    public Account acc{         
        get {
            
            List<Account> accList=[Select id,Name,Accounts_Email_Address__c,Postal_Street__c,
                                   ABN__c,Phone,BillingStreet, BillingCity, BillingState, Preferred_Business_Name__c, Website, Alternate_Phone__c, 
                                   BillingPostalCode, BillingCountry from Account 
                                   where Name='HomeRepair.Net.Au Pty Ltd' LIMIT 1];
            if(accList.size() == 0){
                acc=new Account();
            }else{
                acc=accList[0];              
            }
            return acc;
        } set;
    }
    public InvoiceStatementExtn(ApexPages.StandardController controller) {  
        
        List<AR_Invoice__c > invoices = [SELECT Id,Authority__r.CreatedDate,Authority__r.Date_Commenced__c,Authority__r.Date_Completed__c,(SELECT Id, Room__c, Scope_Of_Work__c , Additional_Comments__c FROM AR_SOWs__r ORDER BY Room__c)
                    FROM AR_Invoice__c WHERE Id= : apexpages.currentpage().getparameters().get('id')];
        
        for(AR_Invoice__c arI : invoices)
        {     AddlCommts = 'No';
            for(AR_SOW__c arSw : arI.AR_SOWs__r)
            {
                if(arSw.Additional_Comments__c != null && arSw.Additional_Comments__c != ''){ 
                    AddlCommts = 'Yes';
                    break;
                }
                    
            }
            
        }
        if(invoices.size() > 0)
            invoice = invoices[0]; 
    }  
}