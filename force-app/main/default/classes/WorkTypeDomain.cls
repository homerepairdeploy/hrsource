public class WorkTypeDomain extends SObjectDomain {
    public WorkTypeDomain(List<WorkType> workTypes) {
        super(workTypes);
    }
   
    public List<TradeTypeController.PicklistOption> getWorkTypeOptions() {
        List<TradeTypeController.PicklistOption> workTypes 
            = new List<TradeTypeController.PicklistOption>();
        for(Sobject record: records) {
            WorkType workT = (WorkType) record;
            workTypes.add(new TradeTypeController.PicklistOption(workT.Name, 
                workT.Name));
        }
        return workTypes;
    }
}