public class CustomLookupCtrl {
    public static final String hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
    public static final Id hrClaimRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Claim').getRecordTypeId();
    
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String objectName,String recordId) {
        String sQuery;
        String searchKey = '%' + searchKeyWord + '%';
        String authorityRecType ='\''+hrAuthorityRecId+'\'';
        String searchRecordId ='\''+recordId+'\'';
        system.debug('searchRecordId :' + searchRecordId);
        List < sObject > returnList = new List < sObject >();
        
        if(ObjectName == 'PricebookEntry'){
            WorkOrder woRec = new Workorder();     
            woRec = [Select ID,WorkType.Name, Risk_State__c FROM WorkOrder WHERE Id =: recordId Limit 1];
            String workTypeName ='\''+woRec.WorkType.Name+'\'';
            String riskState ='\''+woRec.Risk_State__c+'\'';
            if(searchKeyWord == '' || searchKeyWord == ' '){
                sQuery =  'select id, Name, ProductCode,Product2.UOM__c,Labour_Price__c, Material_Price__c,Labour_TIME_mins__c from ' +ObjectName + ' where WorkType__c = '+ workTypeName +' AND State__c =' +riskState+ ' order by createdDate DESC limit 5'; 
            }else{
                sQuery =  'select id, Name, ProductCode,Product2.UOM__c,Labour_Price__c, Material_Price__c,Labour_TIME_mins__c from ' +ObjectName + ' where (Name LIKE: searchKey OR ProductCode LIKE: searchKey) AND WorkType__c = '+ workTypeName +' AND State__c =' +riskState+ ' order by createdDate DESC limit 50';  
            }            
        }
        else if(ObjectName == 'User'){
            if(searchKeyWord == '' || searchKeyWord == ' '){
                sQuery =  'select id, name from ' +ObjectName + ' order by createdDate DESC limit 5';   
            }else{
                sQuery =  'select id, name from ' +ObjectName + ' where name LIKE: searchKey order by createdDate DESC limit 5'; 
            }
        }
        else if(ObjectName == 'WorkOrder'){
            if(searchKeyWord == '' || searchKeyWord == ' '){
                sQuery =  'select id, WorkOrderNumber from ' +ObjectName + ' order by createdDate DESC limit 5';   
            }else{
                sQuery =  'select id, WorkOrderNumber from ' +ObjectName + ' where WorkOrderNumber LIKE: searchKey order by createdDate DESC limit 5'; 
            }
        }
        else if(ObjectName == 'ServiceAppointment'){
            if(searchKeyWord == '' || searchKeyWord == ' '){
                sQuery =  'select id, AppointmentNumber from ' +ObjectName + ' order by createdDate DESC limit 5'; 
            }else{
                sQuery =  'select id, AppointmentNumber from ' +ObjectName + ' where AppointmentNumber LIKE: searchKey order by createdDate DESC limit 5';  
            }
        }
        else if(ObjectName == 'Room__c'){
            WorkOrder woRec = new Workorder(); 
            woRec = [Select ID,Case.Id FROM WorkOrder WHERE Id =: recordId Limit 1];
            searchRecordId = '\''+woRec.Case.Id+'\'';
            if(searchKeyWord == '' || searchKeyWord == ' '){
                sQuery =  'select id,Name from ' +objectName + ' where Claim__c='+searchRecordId  +' order by createdDate DESC limit 5';
            }else{
                sQuery =  'select id,Name from ' +objectName + ' where Name LIKE: searchKey AND Claim__c='+searchRecordId  +' order by createdDate DESC limit 100';
            }
        }
        /* else if(ObjectName == 'Claim_Job__c'){
if(searchKeyWord == '' || searchKeyWord == ' '){
sQuery =  'select id,Name from ' +objectName + ' where Claim__c='+searchRecordId +' order by createdDate DESC limit 5'; 
}else{
sQuery =  'select id,Name from ' +objectName + ' where Name LIKE: searchKey AND Claim__c='+searchRecordId +' order by createdDate DESC limit 100';
} */
        //This is for case authority(Added by CRMIT)
        else if(ObjectName == 'case'){
            case c = [select id,RecordTypeId from case where Id =: recordId];
            if(c.RecordTypeId == hrClaimRecId)
            {
                if(searchKeyWord == '' || searchKeyWord == ' '){
                    sQuery =  'select id,CaseNumber,status from ' +objectName + ' where CaseNumber LIKE: searchKey AND parentId='+searchRecordId +' AND RecordTypeId='+authorityRecType +' order by createdDate DESC limit 100';
                    System.debug('sQuery========' +sQuery);
                    //sQuery =  'select id,Name,Action_Type__c from ' +objectName + ' where Id='+searchRecordId +' order by createdDate DESC limit 5'; 
                }else{
                    sQuery =  'select id,CaseNumber,status  from ' +objectName + ' where CaseNumber LIKE: searchKey AND parentId='+searchRecordId +' AND RecordTypeId='+authorityRecType +' order by createdDate DESC limit 100';
                }
            }else{
                if(searchKeyWord == '' || searchKeyWord == ' '){
                    sQuery =  'select id,CaseNumber,status from ' +objectName + ' where CaseNumber LIKE: searchKey AND Id='+searchRecordId +' AND RecordTypeId='+authorityRecType +' order by createdDate DESC limit 100';
                    System.debug('sQuery========' +sQuery);
                    //sQuery =  'select id,Name,Action_Type__c from ' +objectName + ' where Id='+searchRecordId +' order by createdDate DESC limit 5'; 
                }else{
                    sQuery =  'select id,CaseNumber,status  from ' +objectName + ' where CaseNumber LIKE: searchKey AND Id='+searchRecordId +' AND RecordTypeId='+authorityRecType +' order by createdDate DESC limit 100';
                }
            }
            
        } else if(ObjectName == 'WorkType'){
            if(searchKeyWord == '' || searchKeyWord == ' '){
                sQuery =  'select id,Name from ' +objectName + ' order by createdDate DESC limit 5'; 
            }else{
                sQuery =  'select id,Name from ' +objectName + ' where Name LIKE: searchKey order by createdDate DESC limit 100';
            }
        } else if(ObjectName == 'Trade_Type__c') {
            if(searchKeyWord == '' || searchKeyWord == ' '){
                sQuery = 'select id,Name from ' + objectName + ' order by createdDate DESC limit 5'; 
            }else{
                sQuery = 'select id,Name from ' + objectName + ' where Name LIKE: searchKey order by createdDate DESC limit 100';
            }
        }
        else if(ObjectName == 'Account') {
            sQuery = 'select id,Name from ' + objectName + ' where Name LIKE: searchKey AND RecordType.Name = \'Insurance Provider\' order by createdDate DESC limit 100';
        }
        
        system.debug('sQuery :'+sQuery);
        List < sObject > lstOfRecords = Database.query(sQuery);
        system.debug('lstOfRecords :'+lstOfRecords);
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
}