public class CustomMultiSelectLookupCtrl {
    @AuraEnabled
    public static List < sObject > fetchLookUpValuesObject(String searchKeyWord, String ObjectName, List<sObject> ExcludeitemsList) {
        String searchKey = '%' + searchKeyWord + '%';
        List < sObject > returnList = new List < sObject > ();
        
        List<string> lstExcludeitems = new List<string>();
        for(sObject item : ExcludeitemsList ){
            lstExcludeitems.add(item.id);
        }
        //changed logic by CRMIT
        String sQuery;
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5 and exclude already selected records  
        if(ObjectName == 'case'){
            sQuery =  'select id,CaseNumber from ' +ObjectName + ' where CaseNumber LIKE: searchKey AND Id NOT IN : lstExcludeitems order by createdDate DESC limit 100';   
        }else{
            sQuery =  'select id, Email,Name from ' +ObjectName + ' where Name LIKE: searchKey AND Id NOT IN : lstExcludeitems order by createdDate DESC limit 100';
        }
        system.debug('sQuery :'+sQuery);
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String objectName,String parentId) {
        String searchKey = '%' + searchKeyWord + '%';
        String caseId ='\''+parentId+'\'';
        List < sObject > returnList = new List < sObject >();
        String sQuery;
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5 and exclude already selected records  AND Claim__c='+caseId +'
        //changed logic by CRMIT
        if(ObjectName == 'case'){
            sQuery =  'select id,CaseNumber from ' +objectName + ' where CaseNumber LIKE: searchKey AND ParentId='+caseId +' order by createdDate DESC limit 100';
        }else{
            sQuery =  'select id,Name from ' +objectName + ' where Name LIKE: searchKey AND Claim__c='+caseId +' order by createdDate DESC limit 100';
        }
        system.debug('sQuery :'+sQuery);
        List < sObject > lstOfRecords = Database.query(sQuery);
        system.debug('lstOfRecords :'+lstOfRecords);
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
}