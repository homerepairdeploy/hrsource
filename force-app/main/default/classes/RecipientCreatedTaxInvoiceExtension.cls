public without sharing class RecipientCreatedTaxInvoiceExtension {

    public List<WorkOrderLineItem> woliList{get;set;}

    public RecipientCreatedTaxInvoiceExtension(ApexPages.StandardController controller) {  

        List<AP_Invoice__c> ap = [SELECT Id, Work_Order__c, Name FROM AP_Invoice__c WHERE Id= : apexpages.currentpage().getparameters().get('id') ];

        if (ap.size() > 0 && String.isNotBlank(ap[0].Work_Order__c)) {
            woliList = [SELECT PricebookEntry.Name, Room__r.Name, Description FROM WorkOrderLineItem WHERE WorkOrderId = :ap[0].Work_Order__c AND Cash_Settled__c = False];
        }

    }  

}