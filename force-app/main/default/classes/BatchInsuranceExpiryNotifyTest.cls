@isTest
public class BatchInsuranceExpiryNotifyTest{

   @TestSetup
    public static void setupTestData() {
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Account acc=HomeRepairTestDataFactory.createTradeAccountWithInsuranceExpiry();
             
    }
  
    public static testMethod void test7daysEmailReminderNotify() {
         
        
        Test.startTest();
            BatchInsuranceExpiryNotificationProcess InsurExpNotify = new BatchInsuranceExpiryNotificationProcess();
            Database.executeBatch(InsurExpNotify);    
            
        Test.stopTest();        
    }
}