global class RESTRepairItemProductPriceBookWrapper {
  //Product fields
  public String state; 
  public double labourPrice;
  public double materialPrice;
  public double labourTime;
  public RESTRepairItemProductPriceBookWrapper(PricebookEntry pbe){
        this.state = pbe.State__c;
        this.labourPrice = pbe.Labour_Price__c;
        this.materialPrice= pbe.Material_Price__c;
        this.labourTime = pbe.Labour_TIME_mins__c;
  }
}