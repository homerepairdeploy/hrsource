/*
Apex Trigger to ensure that Appropriate Assessment Report Items
Are created when user interacts with the mobile App.
*/
/*To Do
1. Identify the fields on case which relates to Assessment Items in the background.
At the moment we have the following fields :
Appoint_Repair_Link_Assessor__c(If yes)
Cash_Settlement_Reason__c
Temporary_Accommodation_Reason__c
Asbestos__c
Safety_Repair_Working_Heights__c
Potential_Risks_To_Trades__c
Report_Type__c
Claim_Proceeding__c [Maintenance to be completed]
Claim_Proceeding__c (Decline)

*/

public class CaseHandler implements ITrigger {
    public static final String CLAIM_JOB_STATUS_COMPLETED = 'Completed';
    public static final String CLAIM_JOB_STATUS_ACCEPTED = 'acceptedInWork';
    public static final String CLAIM_HUB_STATUS_UPDATE = 'Pending';
     public static final Id hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
        public static final Id hrClaimRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Claim').getRecordTypeId();
    public List<Assessment_Report_Item__c> assessmentReportItems =  new List<Assessment_Report_Item__c>(); 
    public List<Assessment_Report_Item__c> delassessmentReportItems =  new List<Assessment_Report_Item__c>(); 
    public List<String>caseIds;
    public List<Case> claimList =  new List<Case>();
    public List<Case> claimAssessmentList =  new List<Case>();
    public Map<String,List<Assessment_Report_Item__c>>caseIdToReportItemsMap;
    Set<string> caseforTaskSet = new Set<string>();
    Set<ID> InscaseIDs = new Set<ID>();
       
    public CaseHandler() {
        
    }
    
    public void bulkBefore() {  
        //Modified Code by CRMIT on 18/02
        //Migrated Logic from claimhandler to casehandler with HR authority record type
        List<Job_Margin__mdt> marginList = [SELECT MasterLabel, QualifiedApiName, Margin__c, DeveloperName FROM Job_Margin__mdt];       
        Decimal dAndCMargin;
        Decimal contentsMargin;
        map<string, Job_Margin__mdt> jobMarginMap = new map<string, Job_Margin__mdt>();
        for(Job_Margin__mdt margin : marginList) {
            jobMarginMap.put(margin.DeveloperName, margin);
        }
        Job_Margin__mdt margin =  jobMarginMap.get('doAndCharge');      
        system.debug('margin==>'+margin);
        

 
        if(Trigger.IsInsert){   
            for(Sobject claimTemp : trigger.new){
                // This is to set trigger Claim Hub status update as part of Jitterbit integration
                Case claimRec = (Case) claimTemp;    
                if(claimRec.RecordTypeId == hrAuthorityRecId)
                {
                    if(claimRec.Status == CLAIM_JOB_STATUS_COMPLETED|| claimRec.Status == CLAIM_JOB_STATUS_ACCEPTED){
                        claimRec.ClaimHub_Status_Update__c=CLAIM_HUB_STATUS_UPDATE;
                    }
				} 
            }        
        }
        if(Trigger.IsUpdate){
            List<Id> closedCase = new List<Id>();
            List<Case> newList = (List<case>)Trigger.new;
            Map<Id, Case> oldMap = (Map<Id, Case>) Trigger.oldMap;
            //Id automationUser;

            List<Authority_Activity_Tracker__c> aATList = New List<Authority_Activity_Tracker__c>();            
            if(checkRecursive.runOnceClaimUpdate()){            
            	User automationUser = [SELECT Id FROM User WHERE Email LIKE '%hrclaims@homerepair.com.au%' LIMIT 1];
                Id currentUser = UserInfo.getUserId();
                
                If(currentUser==null) currentUser = automationUser.Id;
                    
                system.debug(' **-- Checking the newList: ' + newList);    
            	for(Case claim : newList){
                    //system.debug(' **-- Got a case: ' + claim);
                    //system.debug(' **-- claim.RecordTypeId=' + claim.RecordTypeId);
                    //system.debug(' **-- hrAuthorityRecId=' + hrAuthorityRecId);
                	if(claim.RecordTypeId == hrAuthorityRecId)
                    {
                        system.debug(' **-- Im an auth too!!');
                    	//List<RecordType> AuthorityRecordList = [SELECT Id FROM RecordType WHERE DeveloperName = 'HR_Authority']; //Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
                		//If(AuthorityRecordList.size() > 0) AuthorityRecordId = AuthorityRecordList[0].Id;
                
                    	if(claim.Status != oldMap.get(claim.Id).Status){
                        	if(claim.Status == CLAIM_JOB_STATUS_COMPLETED|| claim.Status == CLAIM_JOB_STATUS_ACCEPTED)
                            { //** JP Modified 04-08-2020
                            	claim.ClaimHub_Status_Update__c=CLAIM_HUB_STATUS_UPDATE;
                        	} 	
                    	}
						Decimal marginValue = 0;
                    	Boolean defualtMargin=false;
                   		if(jobMarginMap.containsKey(claim.Job_Type__c))
                        {
                        	If(claim.Job_Type__c == 'doAndCharge' || claim.Job_Type__c == 'contents' || claim.Job_Type__c == 'Quote')
                        	{
                        		If (String.isNotBlank(claim.Source_Channel__c) )
                            	{
                            		If (claim.Source_Channel__c == 'SCRO')
                                	{
                                		marginValue = 16.6;
                              		} else 
                                	{
                                    	defualtMargin=true; 
                                	}               
                           		} else  
                            	{
                                	defualtMargin=true; 
                            	}                    
                        	} else 
                            {
                                defualtMargin=true;
                            }
                    	}  
                    	if (defualtMargin)  marginValue = jobMarginMap.get(claim.Job_Type__c).Margin__c; else marginValue=14.5;
                    	//else
                    	//    marginValue = 14.5;
                    	system.debug('margin==>'+marginValue);
                    	claim.Non_Cash_Settle_Margin__c = (claim.Total_Labour_NonCashSettled__c + claim.Total_Material_NonCashSettled__c)*(marginValue/100);
                    	claim.Cash_Settle_Margin__c = (claim.Total_Labour_CashSettled__c + claim.Total_Material_CashSettled__c)*(marginValue/100);
                    
                    	Case oldCase = (Case) Trigger.OldMap.get(claim.Id);
                    	system.debug('1. Checking the update');
                    	//system.debug('** JPCHECK OldStatus: ' + oldCase.Status);
                        //system.debug('** JPCHECK NewStatus: ' + claim.Status);
                    	if(claim.Status.toLowerCase() != oldCase.Status.toLowerCase())
                        {
							system.debug('2. Checking the update');
                    		// Add authority activity tracker
                    		Authority_Activity_Tracker__c aATHR = New Authority_Activity_Tracker__c();
                    		aATHR.Case__c = claim.Id;
                    		aATHR.Date_Time_Stamp__c = system.now();
                    		aATHR.Processing_Status__c = 'Pending';
                    		aATHR.Status_Changed_From__c = oldCase.Status;
                    		aATHR.Status_Changed_To__c = claim.Status;
                    		aATHR.Changed_by_User__c = currentUser;
                       		aATHR.Source__c = 'HomeRepair';    
                        
                        	aATList.add(aATHR);
                   		}
                    
                    	system.debug('**--> Checking 1');
                    	//system.debug('**--> String is blank : ' + String.IsBlank(oldCase.Client_Status__c));
                    	//system.debug('**--> (claim.Client_Status__c.toLowerCase() != oldCase.Client_Status__c.toLowerCase()) = ' + (claim.Client_Status__c.toLowerCase() != oldCase.Client_Status__c.toLowerCase()));
                    	//system.debug('**--> automationUser.Id = ' + automationUser.Id);
                    	//system.debug('**--> Is new string blank : ' + String.IsBlank(claim.Client_Status__c));

						//system.debug('** JPCHECK OldClientStatus: ' + oldCase.Client_Status__c);
                        //system.debug('** JPCHECK NewClientStatus: ' + claim.Client_Status__c);
                        If(claim.Client_Status__c != oldCase.Client_Status__c)
                        {
                       		system.debug('3. Checking the update');
							// Add authority activity tracker
                    		Authority_Activity_Tracker__c aATCL = New Authority_Activity_Tracker__c();
                    		aATCL.Case__c = claim.Id;
                    		aATCL.Date_Time_Stamp__c = system.now();
                    		aATCL.Processing_Status__c = 'Pending';
                    		aATCL.Status_Changed_From__c = oldCase.Client_Status__c;
                    		aATCL.Status_Changed_To__c = claim.Client_Status__c;
                    		aATCL.Changed_by_User__c = automationUser.Id;
                    		aATCL.Source__c = 'SunCorp';    

                            aATList.add(aATCL);
    	                } 
                    	system.debug('**--> Checking 2');
            		}
            	}
            
            	if(!closedCase.isEmpty()) {
              		List<case> closedCaseList = [SELECT Id, 
                   		                         (SELECT Id FROM Tasks WHERE Subject !=: NJ_CONSTANTS.TASK_FOLLOWER_SUBJECT_CLOSED AND Status !=: NJ_CONSTANTS.TASK_STATUS_COMPLETED) 
                       		                     FROM Case 
                           			                 WHERE Id IN : closedCase];
               		system.debug('closedCaseList==>'+closedCaseList );                             
               		for(Case claim : closedCaseList) {
                	   	if(!claim.tasks.isEmpty()) 
                    	{
                       		Trigger.newMap.get(claim.Id).addError('Please complete all related tasks before closing this claim');
            	   		}
               		}
           		}
				If (aATList.size() > 0){
               		system.debug('5. Checking the update');
					insert aATList;
				}
            }
       	}
    }
    
    public void bulkAfter() {       
         //Modified Code by CRMIT on 18/02
        //Migrated Logic from claimhandler to casehandler with HR authority record type
        List<Authority_Activity_Tracker__c> aATList = New List<Authority_Activity_Tracker__c>();
        if(Trigger.IsInsert){
            system.debug('update claim status to new when new claim HR Authority is created.');
            List<Id> caseIdList = new List<Id>();
            List<case> caseList = new List<case>();
            for(Sobject claimTemp : trigger.new){
                if(claimTemp.get('RecordTypeId') == hrAuthorityRecId)
                {
                	if((Id)claimTemp.get('ParentId') != null)
                	{
                    	caseIdList.add((Id)claimTemp.get('ParentId'));
                	}
                }
            }
            if(caseIdList.size()>0)
            {
                caseList = [SELECT Id, Status From Case WHERE Id IN: caseIdList and Status =: NJ_CONSTANTS.CASE_STATUS_CLOSED];
            }
            if(caseList.size()>0)
            {
                for(Case claim: caseList ) {
                    claim.status = NJ_CONSTANTS.SA_STATUS_NEW; 
                }
                //system.debug('caseList==>'+caseList);
                update caseList; 
            }
            
            if(checkRecursive.runOnceClaimInsert()){
                claimList  = new List<Case>();
            	//
        		user automationUser;

         
        		//List<user> automationUserList = [SELECT Id FROM User WHERE Email LIKE '%hrclaims@homerepair.com.au%'];
				//If(automationUserList.size() > 0) automationUser = automationUserList[0];
        		// 
                
                for(Sobject claimTemp : trigger.new){ 
                    Case claimRec = (Case)claimTemp;
                    claimList.add(claimRec);                  

                	//if(claimTemp.get('RecordTypeId') == hrAuthorityRecId)
                	//{
                    //	if(claimRec.Status == CLAIM_JOB_STATUS_COMPLETED|| claimRec.Status == CLAIM_JOB_STATUS_ACCEPTED){
                    //    	claimRec.ClaimHub_Status_Update__c=CLAIM_HUB_STATUS_UPDATE;
                    //	}
                    //}
                    	// Add authority activity tracker
                    	//Authority_Activity_Tracker__c aATHR = New Authority_Activity_Tracker__c();
                    	//aATHR.Case__c = Id.valueOf(String.valueOf(claimTemp.get('Id')));
                    	//aATHR.Date_Time_Stamp__c = system.now();
                    	//aATHR.Processing_Status__c = 'Pending';
                    	//aATHR.Status_Changed_To__c = String.valueOf(claimTemp.get('Status'));
                    	//aATHR.Changed_by_User__c = automationUser.Id;
                    	//aATHR.Source__c = 'HomeRepair';    
                        
                    	//aATList.add(aATHR);
                    
						// Add authority activity tracker
                    	//Authority_Activity_Tracker__c aATCL = New Authority_Activity_Tracker__c();
                    	//aATCL.Case__c = Id.valueOf(String.valueOf(claimTemp.get('Id')));
                    	//aATCL.Date_Time_Stamp__c = system.now();
                    	//aATCL.Processing_Status__c = 'Pending';
                    	//aATCL.Status_Changed_To__c = String.valueOf(claimTemp.get('Status'));
                    	//aATCL.Changed_by_User__c = automationUser.Id;
                    	//aATCL.Source__c = 'SunCorp';    

                    	//aATList.add(aATCL);
                    

					//}                     
                }          
			}
        }

        if(Trigger.IsUpdate){               
            if(checkRecursive.runOnceClaimUpdate()){
                claimList  = new List<Case>();
                system.debug(' Kemoji in Update');          

                for(Sobject claimTemp : trigger.new){ 
                    Case claimRec = (Case)claimTemp;
                    claimAssessmentList.add(claimRec);
                    Case oldCase = (Case) Trigger.OldMap.get(claimRec.Id);
                    
                    //system.debug(' **-- BULK-AFTER - Got a case: ' + claimRec);
                    //system.debug(' **-- BULK-AFTER - claim.RecordTypeId=' + claimRec.RecordTypeId);
                    //system.debug(' **-- BULK-AFTER - hrAuthorityRecId=' + hrAuthorityRecId);
                    
                    if(claimRec.Assessment_Report_Created__c == true) { 
                        if(claimRec.insurance_Purchased__c == true  && (claimRec.Total_Estimate__c > oldCase.Total_Estimate__c ))
                        {
                            system.debug(claimRec);
                            String ID_Desc   = claimRec.ID + 'Purchase Additional Insurance';
                            caseforTaskSet.add(ID_Desc);
                            InscaseIDs.add(claimRec.ID);
                            system.debug(' addtionall ');
                            
                        }else{ 
                            
                            if(claimRec.State1__c != null){
                                if (claimRec.insurance_Purchased__c == false  && (claimRec.Total_Estimate__c > claimRec.State_Wise_Insurance_Thrsld__c) && 
                                    claimRec.State_Wise_Insurance_Thrsld__c > 0 ){
                                        String ID_Desc   = claimRec.ID + 'Purchase Insurance';
                                        caseforTaskSet.add(ID_Desc);
                                        InscaseIDs.add(claimRec.ID);
                                        system.debug('not in addi');
                                    }
                            }
                            
                        }
                    }      
                }

            }
            
            /* if(checkRecursive.runOnceClaimUpdate()){
List<Case> newList = (List<Case>)Trigger.new;
Map<Id, Case> CaseoldMap = (Map<Id, Case>) Trigger.oldMap;
for(Case cse : newList)
{   
if(cse.insurance_Purchased__c == true  && (cse.Total_Estimate__c > CaseoldMap.get(cse.ID).Total_Estimate__c ))
{
String ID_Desc   = cse.ID + 'Insurance coverage exceeded, additional insurance required';
caseforTaskSet.add(ID_Desc);
system.debug(' addtionall ');

}else{ 

if(cse.State1__c != null){
if(cse.Total_Estimate__c  != CaseoldMap.get(cse.ID).Total_Estimate__c &&
cse.Total_Estimate__c > cse.State_Wise_Insurance_Thrsld__c) {
String ID_Desc   = cse.ID + 'The estimate is above $' + cse.State_Wise_Insurance_Thrsld__c +', insurance must be purchased'  ; 
caseforTaskSet.add(ID_Desc);
system.debug('not in addi');
}
}

}

}

} */
        }
        //If (aATList.size() > 0){
        //	insert aATList;
		//}
    }
    public void beforeInsert(SObject so) {
    } 
    public void afterInsert(SObject so) {
        
    } 
    public void beforeUpdate(SObject oldSo, SObject so) {
        Case newcase = (Case)so;
        Case oldcase=(Case)oldSo;
        If (!System.isFuture() && !System.isBatch()){
            
            //phone validation
            If ((newcase.Home_Phone__c!=null && newcase.Home_Phone__c!=oldcase.Home_Phone__c)||
                (newcase.Customer_Mobile_Phone__c!=null && newcase.Customer_Mobile_Phone__c!=oldcase.Customer_Mobile_Phone__c)||
                (newcase.Customer_Contact_Phone__c!=null && newcase.Customer_Contact_Phone__c!=oldcase.Customer_Contact_Phone__c)){
                    
                    newcase.addError('Phone Numbers can not be updated at the claim level. Please update on the contact to reflect the same');
                    
                    
                }    
            
            
        }
        
        
    }
    public void beforeUnDelete(SObject so) {} 
    
    public void afterUpdate(SObject oldSo, SObject so) {
        
    }  
    public void beforeDelete(SObject so){} 
    public void afterDelete(SObject so) {} 
    public void afterUnDelete(SObject so) {} 
    public void andFinally() {
        //system.debug('claimList :'+claimList);
        //system.debug('claimAssessmentList :'+claimAssessmentList);
        if(Trigger.IsAfter){
            if (!claimList.isEmpty()||Trigger.IsInsert){
                CaseService.triggerClaimAssignmentRule(claimList); 
            } 
            if(!claimAssessmentList.isEmpty()){
                CaseService caseServ = new CaseService();
                caseServ.generateClaimAssessmentReportItems(claimAssessmentList,Trigger.OldMap);
                CaseService.updateClaimDeclineClause(claimAssessmentList,Trigger.OldMap);
                system.debug(claimAssessmentList);
                 system.debug(Trigger.OldMap);
            }
            if(!caseforTaskSet.isEmpty())
            {    
                //CaseService caseServ1 = new CaseService(); 
                system.debug('caseforTaskSet  Handler' + caseforTaskSet);
                  system.debug('InscaseIDs  Handler' + InscaseIDs);
                CaseService.createTasknWO(caseforTaskSet, InscaseIDs);
            }
        } 
    }  
    public static void checkTest(){
     Integer i=0;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
         i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;

     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;        
    }    
}