public class CreateTaskCtrl {
    @AuraEnabled        
    public static List<Task_Matrix__mdt> getTaskMatrixList(){
        List<Task_Matrix__mdt> taskMatricList = new List<Task_Matrix__mdt>();
        taskMatricList = [SELECT DeveloperName,Due_Days__c,Max_Due_Days__c,Priority__c,Task_Name__c FROM Task_Matrix__mdt];    
        return taskMatricList;
    }
    @AuraEnabled        
    public static sObject getDefaultUser(){
        List <sObject> lstOfRecords = [SELECT Id,Name FROM User where Name='HR Claims' ]; 
        return lstOfRecords[0];
    }
    @AuraEnabled        
    public static string createTask(Task task){
        Savepoint sp; 
        try{
            sp = Database.setSavepoint();
            insert task;
            return 'Success:'+task.Id+'';
        }
        catch (System.DmlException e) {
            Database.rollback(sp);
            system.debug('Exception Occured==>'+e.getMessage()+' :: StackTrace==>'+e.getStackTraceString());
            List<String> errorMessageList = new List<String>();
            errorMessageList.add(e.getDmlMessage(0));            
            String errorMessage = String.join(errorMessageList,',');
            return errorMessage;
        }
        catch (Exception e) {
            Database.rollback(sp);
            system.debug('Exception Occured==>'+e.getMessage()+' :: StackTrace==>'+e.getStackTraceString());
            return e.getMessage();
        }
    }
}