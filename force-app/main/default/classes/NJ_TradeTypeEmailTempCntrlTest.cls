/************************************************************************************************************
Name: NJ_TradeTypeEmailTempCntrlTest
=============================================================================================================
Purpose: Test class of Controller of VF Component TradeTypeEmailComponent  
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL       DESCRIPTION
1.0        Nikhil                        Created     NJ_TradeTypeEmailTempCntrlTest
*************************************************************************************************************/
@isTest
private class NJ_TradeTypeEmailTempCntrlTest {
  
  @isTest static void getTradeTypeLst_Test() {
    // Implement test code

    List<Lead> lstLead = HomeRepairTestDataFactory.createLead('test Comany', 'Test1', 'Test1');
    Lead ld = lstLead[0];
    
    WorkType testWT = homeRepairTestDataFactory.createWorkType('MakeSafe Items', 'testWorkType');
    
    //set service areas for trade account
        List<String> TestServiceAreas = new List<String>();
        TestServiceAreas.add('VIC - Melbourne');
        TestServiceAreas.add('VIC - Melbourne CBD');
        TestServiceAreas.add('VIC - Melbourne - E / SE');
        TestServiceAreas.add('VIC - Melbourne - E / SE');
        TestServiceAreas.add('VIC - Melbourne NE');
        TestServiceAreas.add('VIC - Melbourne NW');
        TestServiceAreas.add('VIC - Melbourne - W');

    //create Trade Account
        Account testTier2 = FSL_TestDataFactory.createServiceResource('testTier2', TestServiceAreas);
        
        //trade compliance
        Trade_Type__c testCompliance = FSL_TestDataFactory.createTradeCompliance(testTier2.Id, testWT.Id);
        testCompliance.Lead__c = ld.Id;
        update testCompliance;

    Test.startTest();
    NJ_TradeTypeEmailTemplateController ttEmailTempCntrl = new NJ_TradeTypeEmailTemplateController();
    ttEmailTempCntrl.leadId = ld.Id;
    List<Trade_Type__c> ttList = ttEmailTempCntrl.getTradeTypeLst();
    Test.stopTest();

    system.assertEquals(true, ttList.size()> 0 );
  }
}