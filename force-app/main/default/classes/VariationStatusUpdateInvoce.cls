public class VariationStatusUpdateInvoce {
        public static Boolean runOnce = true;

    @InvocableMethod
    
    public static void updateVariationStatus(List<workOrder> woIDs)
    {
        system.debug('!!!!!!!!!!!!!!    Tquila VariationStatusUpdateInvoce started         !!!!!!!!!!!!!!!!!' + runOnce);
        set<ID> wrkIDs = new set<ID>();
        Map<ID, variation__c> varMap = new Map<ID, variation__c>();
        List<workOrder> woList = new list<WorkOrder>();
        List<variation__c> vrList = new list<variation__c>();
        
        
        for(workOrder w : woIDs)
        {
            wrkIDs.add(w.ID); 
        }
        
        if(runOnce) {
            
            for(WorkOrder wo : [SELECT Id, Variation_Approval_Status__c, Total_Variations_Amounts_Ex_Rejected__c, Up_To_Amount__c, Total_Amount__c, Variation_Total__c, 
                                   (SELECT Additional_Labour_Amount__c, Additional_Materials_Amount__c, Variation_Status__c
                                           FROM Variations__r WHERE Work_Order__c IN :wrkIDs AND Variation_Status__c = 'Requested' )
                                    FROM WorkOrder WHERE Id IN :wrkIDs  ]){
                                    
                                    workOrder wo1 = new workOrder();
                                    wo1.Total_Variations_Amounts_Ex_Rejected__c = 0.0;
                                    for(Variation__c vr : wo.Variations__r)
                                    { 
                                        variation__c  vt1 = new variation__c(); 
                                        
                                        if(vr.Variation_Status__c  == 'Requested')
                                        {
                                            vt1.Variation_Status__c = wo.Variation_Approval_Status__c;
                                            vt1.Id                  = vr.id;
                                            vrList.add(vt1);
                                        }
                                        
                                    }
                                        
                                        if(wo.Variation_Approval_Status__c == 'Rejected'){
                                             wo1.Total_Variations_Amounts_Ex_Rejected__c = wo.Total_Variations_Amounts_Ex_Rejected__c - wo.Variation_Total__c;
                                             wo1.ID  = wo.id;
                                             woList.add(wo1);
                                        }
                                    
                                }
            if(vrList.size() > 0)
                update vrList;
            if(woList.size() > 0)
                update woList;
                runOnce = false;

        }
    }
}