/* =====================================================================================
Type:       Test class
Purpose:    Test cases for AuthorityActivityFactory
========================================================================================*/
@isTest
public class AuthorityActivityTrackerFactoryTest {
    //public static case internalAuthority;
    //HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
    
    public static final Id hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
    public static final Id hrClaimRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Claim').getRecordTypeId();
    
    @testSetup
    public static void testSetup(){
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        User us = HomeRepairTestDataFactory.createSystemAdminUser('Developer', 'HomeRepair');
        us.email = 'hrclaims@homerepair.com.au';
        
        User us2 = HomeRepairTestDataFactory.createStandardUser('Ima', 'TestBot');
        us2.email = 'ima.testbot@testing.com.au';
        
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        //case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        
        Case internalAuthority = HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        internalAuthority.ParentId = cs.Id;
        // Create configs
        list<Authority_Activity_Tracker_Config__c> aATCList = New list<Authority_Activity_Tracker_Config__c>();
        
        Authority_Activity_Tracker_Config__c aATC1 = New Authority_Activity_Tracker_Config__c();
        aATC1.Client__c = 'HomeRepair';
        aATC1.Status__c = 'New';
        aATC1.Allowed_Backstep__c = FALSE;
        aATC1.Allowed_HomeRepair_Backstep_Value__c = 0;
        aATC1.Allowed_HomeRepair_Skip_Value__c = 2;
        aATC1.Is_Allowed_Value__c = FALSE;
        aATC1.Has_Case_Comment__c = FALSE;
        aATC1.Case_Comment_Contents__c = null;
        aATC1.Has_Task__c = FALSE;
        aATC1.Task_Level__c = null;
        aATC1.Task_Subject__c = null;
        aATC1.Has_Vendor_Note__c = FALSE;
        aATC1.Vendor_Note_Type__c = null;
        aATC1.Vendor_Note_Contents__c = null;
        aATC1.Has_Specific_Rules__c = FALSE;
        
        aATCList.Add(aATC1);

        //
        Authority_Activity_Tracker_Config__c aATC2 = New Authority_Activity_Tracker_Config__c();
        aATC2.Client__c = 'HomeRepair';
        aATC2.Status__c = 'Pending';
        aATC2.Allowed_Backstep__c = FALSE;
        aATC2.Allowed_HomeRepair_Backstep_Value__c = 0;
        aATC2.Allowed_HomeRepair_Skip_Value__c = 0;
        aATC2.Is_Allowed_Value__c = TRUE;
        aATC2.Has_Case_Comment__c = FALSE;
        aATC2.Case_Comment_Contents__c = null;
        aATC2.Has_Task__c = FALSE;
        aATC2.Task_Level__c = null;
        aATC2.Task_Subject__c = null;
        aATC2.Has_Vendor_Note__c = FALSE;
        aATC2.Vendor_Note_Type__c = null;
        aATC2.Vendor_Note_Contents__c = null;
        aATC2.Has_Specific_Rules__c = FALSE;
        
        aATCList.Add(aATC2);

		//
		Authority_Activity_Tracker_Config__c aATC3 = New Authority_Activity_Tracker_Config__c();
        aATC3.Client__c = 'HomeRepair';
        aATC3.Status__c = 'Submitted';
        aATC3.Allowed_Backstep__c = FALSE;
        aATC3.Allowed_HomeRepair_Backstep_Value__c = 0;
        aATC3.Allowed_HomeRepair_Skip_Value__c = 6;
        aATC3.Is_Allowed_Value__c = TRUE;
        aATC3.Has_Case_Comment__c = TRUE;
        aATC3.Case_Comment_Contents__c = 'ABC123';
        aATC3.Has_Task__c = TRUE;
        aATC3.Task_Level__c = null;
        aATC3.Task_Subject__c = 'ABC123';
        aATC3.Has_Vendor_Note__c = TRUE;
        aATC3.Vendor_Note_Type__c = 'Passive';
        aATC3.Vendor_Note_Contents__c = 'Current Authority falls within HomeRepair "Under $20,000/Auto Approval" rules. Please review and action Authority accordingly.';
        aATC3.Has_Specific_Rules__c = TRUE;
        
        aATCList.Add(aATC3);
        
        //
		Authority_Activity_Tracker_Config__c aATC4 = New Authority_Activity_Tracker_Config__c();
        aATC4.Client__c = 'HomeRepair';
        aATC4.Status__c = 'acceptedInWork';
        aATC4.Allowed_Backstep__c = FALSE;
        aATC4.Allowed_HomeRepair_Backstep_Value__c = 0;
        aATC4.Allowed_HomeRepair_Skip_Value__c = 0;
        aATC4.Is_Allowed_Value__c = TRUE;
        aATC4.Has_Case_Comment__c = FALSE;
        aATC4.Case_Comment_Contents__c = null;
        aATC4.Has_Task__c = FALSE;
        aATC4.Task_Level__c = null;
        aATC4.Task_Subject__c = null;
        aATC4.Has_Vendor_Note__c = FALSE;
        aATC4.Vendor_Note_Type__c = 'Passive';
        aATC4.Vendor_Note_Contents__c = null;
        aATC4.Has_Specific_Rules__c = FALSE;
        
        aATCList.Add(aATC4);    
        
        insert aATCList;
    }
    
    @isTest
    public static void testStatusUpdateAuthority(){
        User us = HomeRepairTestDataFactory.createSystemAdminUser('Developer', 'HomeRepair');
		us.email = 'hrclaims@homerepair.com.au';
        Case currentAuth;
        
        System.runAs(us)
        {        
	       	Test.startTest();
        
    	   	//Case currentAuth = [select id, Status from case where RecordType.Id = :hrAuthorityRecId limit 1];
       		currentAuth = [select id, Status from case where RecordTypeId = :hrAuthorityRecId limit 1];
    	
        	system.debug('Is this here ' + currentAuth.status);        
        	/*currentAuth.Status = 'New';
        
        	update currentAuth;
        
        	currentAuth = [select id, Status from case where Id = :currentAuth.Id limit 1];*/
        	checkRecursive.claimUpdateRun=true;
       		currentAuth.Status = 'Pending';
        
        	update currentAuth;
 			
       		Test.stopTest();
        }
        //System.AssertEquals(1, [SELECT id FROM Authority_Activity_Tracker__c WHERE Id = :currentAuth.Id AND Status_Changed_To__c = 'Pending' LIMIT 1].size()); 
    }  
    
    @isTest
    public static void testConfig()
    {
        AuthorityActivityTrackerFactory aATF = New AuthorityActivityTrackerFactory();
        Integer i = aATF.BASE_HOMEREPAIR_STATUS_STEPS.get('New');
        system.assertEquals(i, 1);
        
    }
    
    @isTest
    public static void testStatusChangeHandling()
    {
        User us = HomeRepairTestDataFactory.createSystemAdminUser('Developer', 'HomeRepair');
		us.email = 'hrclaims@homerepair.com.au';
        Case currentAuth;
        
        System.runAs(us)
        {
        	Test.startTest();
        	currentAuth = [select id, Status from case where RecordTypeId = :hrAuthorityRecId limit 1];
        	//Case newAuth = currentAuth;
        
        	checkRecursive.claimUpdateRun=true;
        	currentAuth.Client_Status__c = 'Submitted';
        
        	update currentAuth;
        
        	Test.stopTest();
        }
        //System.AssertEquals(1, [SELECT id FROM Authority_Activity_Tracker__c WHERE Id = :currentAuth.Id AND Status_Changed_To__c = 'Submitted' LIMIT 1].size()); 
    }
    
    @isTest
    public static void testGetRequestedAuthority()
    {
        Authority_Activity_Tracker__c aAT = New Authority_Activity_Tracker__c();
        AuthorityActivityTrackerFactory aATF = New AuthorityActivityTrackerFactory();
        Id AuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
        system.debug('**-- Running test "testGetRequestedAuthority()"');
        //Test.startTest();
        Case currentAuth = [select id, Status from case where RecordTypeId = :AuthorityRecId limit 1];
        //User testUser = [Select Id FROM User WHERE email = 'ima.testbot@testing.com.au' limit 1];
        User us2 = HomeRepairTestDataFactory.createStandardUser('Ima', 'TestBot');
        us2.email = 'ima.testbot@testing.com.au';
        
        system.debug('**-- Am I doing anything???"');
        system.debug('**-- CurrentAuth = ' + currentAuth);
        system.debug('**-- testUser = ' + us2);
        
        aAT.Case__c = currentAuth.Id;
        aAT.Changed_by_User__c = us2.Id;
        aAT.Date_Time_Stamp__c = system.now();
        aAT.Processing_Status__c = 'Pending';
        aAT.Status_Changed_To__c = 'New';
        aAT.Source__c = 'HomeRepair';
        
        List<Case> baseReturnAuth = New List<Case>(); 
        baseReturnAuth.add(aATF.getRequestedAuthority(aAT));
        //Test.stopTest();
        //system.assertEquals(1, baseReturnAuth.size());
    }
    
    @isTest
    Public static void testProcessActivityTracker()
    {      
        User us = HomeRepairTestDataFactory.createSystemAdminUser('Developer', 'HomeRepair');
		us.email = 'hrclaims@homerepair.com.au';
                
        System.runAs(us)
        {
        	test.startTest();
        	Id AuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
        
            Case currentAuth = [select id, Status from case where RecordTypeId = :AuthorityRecId limit 1];        
            checkRecursive.claimUpdateRun=true;
            currentAuth.Status = 'New';
            
            //Authority_Activity_Tracker__c aAT = [SELECT Id FROM Authority_Activity_Tracker__c LIMIT 1];
            //system.debug('**-- aAT = ' + aAT);
        	Authority_Activity_Tracker__c aAT = New Authority_Activity_Tracker__c();
        	aAT.Case__c = currentAuth.Id;
        	aAT.Changed_by_User__c = us.Id;
        	aAT.Date_Time_Stamp__c = system.now();
        	aAT.Processing_Status__c = 'Pending';
        	aAT.Status_Changed_To__c = 'New';
            aAT.Source__c = 'HomeRepair';
            
            insert aAT;
            
            AuthorityActivityTrackerFactory objAuthority = new AuthorityActivityTrackerFactory();
    		Database.executeBatch(objAuthority,1);    
        	
            test.stopTest();
        }
    }

    @isTest
    Public static void testRulesProcessing()
    { 
        User us = HomeRepairTestDataFactory.createSystemAdminUser('Developer', 'HomeRepair');
		us.email = 'hrclaims@homerepair.com.au';
                        
        //System.runAs(us)
        //{
        	test.startTest();
            
            Authority_Activity_Tracker_Config__c aATC3 = New Authority_Activity_Tracker_Config__c();
        	aATC3.Client__c = 'HomeRepair';
        	aATC3.Status__c = 'Submitted';
        	aATC3.Allowed_Backstep__c = FALSE;
        	aATC3.Allowed_HomeRepair_Backstep_Value__c = 0;
        	aATC3.Allowed_HomeRepair_Skip_Value__c = 6;
        	aATC3.Is_Allowed_Value__c = TRUE;
        	aATC3.Has_Case_Comment__c = TRUE;
        	aATC3.Case_Comment_Contents__c = 'ABC123';
        	aATC3.Has_Task__c = TRUE;
        	aATC3.Task_Level__c = '0';
        	aATC3.Task_Subject__c = 'ABC123';
        	aATC3.Has_Vendor_Note__c = TRUE;
        	aATC3.Vendor_Note_Type__c = 'Passive';
        	aATC3.Vendor_Note_Contents__c = 'Current Authority falls within HomeRepair "Under $20,000/Auto Approval" rules. Please review and action Authority accordingly.';
        	aATC3.Has_Specific_Rules__c = TRUE;
            
            insert aATC3;
        	Id AuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
        
            Case currentAuth = [select id, Status, ParentId, Parent.Id from case where RecordTypeId = :AuthorityRecId limit 1];        
        	Case currentCase = [select id, Status, ParentId, Parent.Id from case where RecordTypeId != :AuthorityRecId limit 1];     
            checkRecursive.claimUpdateRun=true;
            currentAuth.Status = 'New';

        	currentAuth.Parent = currentCase;
        	update currentAuth;
        
        	Authority_Activity_Tracker__c aAT = New Authority_Activity_Tracker__c();
        	aAT.Case__c = currentAuth.Id;
        	aAT.Changed_by_User__c = us.Id;
        	aAT.Date_Time_Stamp__c = system.now();
        	aAT.Processing_Status__c = 'Pending';
        	aAT.Status_Changed_From__c = 'New';
        	aAT.Status_Changed_To__c = 'Submitted';
            aAT.Source__c = 'HomeRepair';
            
            database.SaveResult aATsr = database.insert(aAT);			            
            
            system.debug('**-- ID RESULT: ' + aATsr);
            AuthorityActivityTrackerFactory aATF = New AuthorityActivityTrackerFactory();
            //aATF.processTrackerRules(aAT);
            aATF.generateCaseTask(aAT, aATC3);
            aATF.generateCaseComment(aAT, aATC3);
            aATF.generateVendorNote(aAT, aATC3);
            aATF.hasSpecificRules(aAT, aATC3);
            aATF.autoUpdateValue(aAT, aATC3);
        //}
    }
    
    /* @isTest
    Public static void testCaseComments()
    { 
        User us = HomeRepairTestDataFactory.createSystemAdminUser('Developer', 'HomeRepair');
		us.email = 'hrclaims@homerepair.com.au';
                        
        System.runAs(us)
        {
        	test.startTest();
            
            Authority_Activity_Tracker_Config__c aATC3 = New Authority_Activity_Tracker_Config__c();
        	aATC3.Client__c = 'HomeRepair';
        	aATC3.Status__c = 'Submitted';
        	aATC3.Allowed_Backstep__c = FALSE;
        	aATC3.Allowed_HomeRepair_Backstep_Value__c = 0;
        	aATC3.Allowed_HomeRepair_Skip_Value__c = 6;
        	aATC3.Is_Allowed_Value__c = TRUE;
        	aATC3.Has_Case_Comment__c = TRUE;
        	aATC3.Case_Comment_Contents__c = 'ABC123';
        	aATC3.Has_Task__c = TRUE;
        	aATC3.Task_Level__c = '0';
        	aATC3.Task_Subject__c = 'ABC123';
        	aATC3.Has_Vendor_Note__c = TRUE;
        	aATC3.Vendor_Note_Type__c = 'Passive';
        	aATC3.Vendor_Note_Contents__c = 'Current Authority falls within HomeRepair "Under $20,000/Auto Approval" rules. Please review and action Authority accordingly.';
        	aATC3.Has_Specific_Rules__c = TRUE;
            
            insert aATC3;
        	Id AuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
        
            Case currentAuth = [select id, Status from case where RecordTypeId = :AuthorityRecId limit 1];        
            checkRecursive.claimUpdateRun=true;
            currentAuth.Status = 'New';

        	Authority_Activity_Tracker__c aAT = New Authority_Activity_Tracker__c();
        	aAT.Case__c = currentAuth.Id;
        	aAT.Changed_by_User__c = us.Id;
        	aAT.Date_Time_Stamp__c = system.now();
        	aAT.Processing_Status__c = 'Pending';
        	aAT.Status_Changed_To__c = 'Submitted';
            aAT.Source__c = 'HomeRepair';
            
            database.SaveResult aATsr = database.insert(aAT);			            
            
            system.debug('**-- ID RESULT: ' + aATsr);
            AuthorityActivityTrackerFactory aATF = New AuthorityActivityTrackerFactory();
            aATF.generateCaseComment(aAT, aATC3);
        }
    }
    
    @isTest
    Public static void testVendorNotes()
    { 
        User us = HomeRepairTestDataFactory.createSystemAdminUser('Developer', 'HomeRepair');
		us.email = 'hrclaims@homerepair.com.au';
                        
        System.runAs(us)
        {
        	test.startTest();
            
            Authority_Activity_Tracker_Config__c aATC3 = New Authority_Activity_Tracker_Config__c();
        	aATC3.Client__c = 'HomeRepair';
        	aATC3.Status__c = 'Submitted';
        	aATC3.Allowed_Backstep__c = FALSE;
        	aATC3.Allowed_HomeRepair_Backstep_Value__c = 0;
        	aATC3.Allowed_HomeRepair_Skip_Value__c = 6;
        	aATC3.Is_Allowed_Value__c = TRUE;
        	aATC3.Has_Case_Comment__c = TRUE;
        	aATC3.Case_Comment_Contents__c = 'ABC123';
        	aATC3.Has_Task__c = TRUE;
        	aATC3.Task_Level__c = '0';
        	aATC3.Task_Subject__c = 'ABC123';
        	aATC3.Has_Vendor_Note__c = TRUE;
        	aATC3.Vendor_Note_Type__c = 'Passive';
        	aATC3.Vendor_Note_Contents__c = 'Current Authority falls within HomeRepair "Under $20,000/Auto Approval" rules. Please review and action Authority accordingly.';
        	aATC3.Has_Specific_Rules__c = TRUE;
            
            insert aATC3;
        	Id AuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
        
            Case currentAuth = [select id, Status from case where RecordTypeId = :AuthorityRecId limit 1];        
            checkRecursive.claimUpdateRun=true;
            currentAuth.Status = 'New';

        	Authority_Activity_Tracker__c aAT = New Authority_Activity_Tracker__c();
        	aAT.Case__c = currentAuth.Id;
        	aAT.Changed_by_User__c = us.Id;
        	aAT.Date_Time_Stamp__c = system.now();
        	aAT.Processing_Status__c = 'Pending';
        	aAT.Status_Changed_To__c = 'Submitted';
            aAT.Source__c = 'HomeRepair';
            
            database.SaveResult aATsr = database.insert(aAT);			            
            
            system.debug('**-- ID RESULT: ' + aATsr);
            AuthorityActivityTrackerFactory aATF = New AuthorityActivityTrackerFactory();
            aATF.generateVendorNote(aAT, aATC3);
        }
    }
    
    @isTest
    Public static void testCaseTask()
    { 
        User us = HomeRepairTestDataFactory.createSystemAdminUser('Developer', 'HomeRepair');
		us.email = 'hrclaims@homerepair.com.au';
                        
        System.runAs(us)
        {
        	test.startTest();
            
            Authority_Activity_Tracker_Config__c aATC3 = New Authority_Activity_Tracker_Config__c();
        	aATC3.Client__c = 'HomeRepair';
        	aATC3.Status__c = 'Submitted';
        	aATC3.Allowed_Backstep__c = FALSE;
        	aATC3.Allowed_HomeRepair_Backstep_Value__c = 0;
        	aATC3.Allowed_HomeRepair_Skip_Value__c = 6;
        	aATC3.Is_Allowed_Value__c = TRUE;
        	aATC3.Has_Case_Comment__c = TRUE;
        	aATC3.Case_Comment_Contents__c = 'ABC123';
        	aATC3.Has_Task__c = TRUE;
        	aATC3.Task_Level__c = '0';
        	aATC3.Task_Subject__c = 'ABC123';
        	aATC3.Has_Vendor_Note__c = TRUE;
        	aATC3.Vendor_Note_Type__c = 'Passive';
        	aATC3.Vendor_Note_Contents__c = 'Current Authority falls within HomeRepair "Under $20,000/Auto Approval" rules. Please review and action Authority accordingly.';
        	aATC3.Has_Specific_Rules__c = TRUE;
            
            insert aATC3;
        	Id AuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
        
            Case currentAuth = [select id, Status from case where RecordTypeId = :AuthorityRecId limit 1];        
            checkRecursive.claimUpdateRun=true;
            currentAuth.Status = 'New';

        	Authority_Activity_Tracker__c aAT = New Authority_Activity_Tracker__c();
        	aAT.Case__c = currentAuth.Id;
        	aAT.Changed_by_User__c = us.Id;
        	aAT.Date_Time_Stamp__c = system.now();
        	aAT.Processing_Status__c = 'Pending';
        	aAT.Status_Changed_To__c = 'Submitted';
            aAT.Source__c = 'HomeRepair';
            
            database.SaveResult aATsr = database.insert(aAT);			            
            
            system.debug('**-- ID RESULT: ' + aATsr);
            AuthorityActivityTrackerFactory aATF = New AuthorityActivityTrackerFactory();
            aATF.generateCaseTask(aAT, aATC3);
        }
    } */
    
    Public static void basicTest()
    {
        AuthorityActivityTrackerProcess.checkTest();
    }
    
    Public static void basicCashHandlerTest()
    {
        CaseHandler.checkTest();
    }
    
    Public static void basicCheckRecursiveTest()
    {
        CheckRecursive.checkTest();
    }
}