/************************************************************************************************************
Name:ServiceAppointmentService
=============================================================================================================
Purpose: Service Appointment Service which is called by triiger class.It assigns fields to service appointment 
and updates workorder with Trade user details
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL       DESCRIPTION
1.0        Vasu         4/07/2018        Created       Home Repair Claim System  
*************************************************************************************************************/

public without Sharing class ServiceAppointmentService {    
    public static void assignFieldsServiceAppointment(List<ServiceAppointment> serviceAppointmentList){
        system.debug('Entering assignFieldsServiceAppointment method with ServiceAppointment : '+serviceAppointmentList);
        Set<String> woIds = new Set<String>();
        Map<String,WorkOrder> workOrderMap = new Map<String,WorkOrder>();
        Map<String,WorkOrderLineItem> workOrderLineItemMap = new Map<String,WorkOrderLineItem>();
        
        For(ServiceAppointment sa:serviceAppointmentList){
            woIds.add(sa.ParentRecordId);
        }
        
        List<All_Service_Appointments__c> allServiceAppointmentsList=new List<All_Service_Appointments__c>();
        List<WorkOrder> woUpdate = new List<WorkOrder>();
        for(WorkOrder wo : [Select Id,CaseId,Work_Type_Name__c,Sort_Order__c,WorkOrderNumber,Description from workOrder where Id IN : woIds]){
            workOrderMap.put(wo.Id,wo);
        }
        For (ServiceAppointment sa:ServiceAppointmentList){
            if(workOrderMap.containsKey(sa.ParentRecordId)) {                                      
                sa.Claim__c=workOrderMap.get(sa.ParentRecordId).CaseId; 
                sa.Work_Type_Name__c=workOrderMap.get(sa.ParentRecordId).Work_Type_Name__c;
                //Changes Status to Confirmed
                system.debug('sa.Work_Type_Name__c: ' + sa.Work_Type_Name__c);
                //Added Assessment worktypes
                if(sa.Work_Type_Name__c != null){ 
                    if((sa.Work_Type_Name__c==NJ_CONSTANTS.WORK_TYPE_ASSESSMENT || sa.Work_Type_Name__c== NJ_CONSTANTS.WORK_TYPE_REASSESSMENT
                        || sa.Work_Type_Name__c==NJ_CONSTANTS.WORK_TYPE_INTERNALASSESSORS || sa.Work_Type_Name__c== NJ_CONSTANTS.WORK_TYPE_COORD_ASSESSMENT
                        || sa.Work_Type_Name__c.startsWith(NJ_CONSTANTS.WORK_TYPE_STARTS_WITH_HOMEASSIST)
                        || sa.Work_Type_Name__c.startsWith(NJ_CONSTANTS.WORK_TYPE_STARTS_WITH_MAKESAFE))){
                            if(sa.Status == NJ_CONSTANTS.SA_STATUS_TENTATIVE){
                                sa.Status=NJ_CONSTANTS.SA_STATUS_CONFIRMED;
                                woUpdate.add(new WorkOrder(Id = sa.ParentRecordId, Status = NJ_Constants.WO_STATUS_ASSESSMENT_SCHEDULED));
                            }
                        }                               
                }
            }
        }
        if(!woUpdate.isEmpty()) {
            update woUpdate;
        }
        system.debug('Exiting assignFieldsServiceAppointment method');
    }
    
    public static void upsertAllServiceAppointments(List<ServiceAppointment> serviceAppointmentList,Map<Id,SObject> oldAppointments,Boolean isUpdate){
        system.debug('Entering upsertAllServiceAppointments : '+serviceAppointmentList);
        Set<String> woIds = new Set<String>();
        Map<String,WorkOrder> workOrderMap = new Map<String,WorkOrder>();
        Map<String,WorkOrderLineItem> workOrderLineItemMap = new Map<String,WorkOrderLineItem>();
        
        For(ServiceAppointment sa:serviceAppointmentList){
            woIds.add(sa.ParentRecordId);
        }
        
        List<All_Service_Appointments__c> allServiceAppointmentsList=new List<All_Service_Appointments__c>();
        
        for(WorkOrder wo : [Select Id,CaseId,Work_Type_Name__c,Trade_User__c,Sort_Order__c,WorkOrderNumber,Description from workOrder where Id IN : woIds]){
            workOrderMap.put(wo.Id,wo);
        }
        for(WorkOrderLineItem wili : [Select Id,LineItemNumber,Description from WorkOrderLineItem where WorkOrderId IN : woIds]){
            workOrderLineItemMap.put(wili.Id,wili);
        }
        
        For(ServiceAppointment sa : serviceAppointmentList){
            
            
            All_Service_Appointments__c asa = new All_Service_Appointments__c();
            asa.Schedule_StartDate__c=sa.SchedStartTime;
            asa.Schedule_EndDate__c=sa.SchedEndTime;
            
            If(workOrderMap.containsKey(sa.ParentRecordId)) {
                asa.Claim__c = workOrderMap.get(sa.ParentRecordId).CaseId;
                asa.Work_Type__c = workOrderMap.get(sa.ParentRecordId).Work_Type_Name__c;
                asa.Work_Order__c=workOrderMap.get(sa.ParentRecordId).Id;
                asa.Work_Order_Description__c=workOrderMap.get(sa.ParentRecordId).Description;
                asa.Service_Resource__c=workOrderMap.get(sa.ParentRecordId).Trade_User__c;
            }
            
            If(workOrderLineItemMap.containsKey(sa.ParentRecordId)) {
                asa.Claim__c=sa.Claim__c;
                asa.Work_Order_Line_Item__c=workOrderLineItemMap.get(sa.ParentRecordId).LineItemNumber;
                asa.Work_Order_Line_Item_Desc__c=workOrderLineItemMap.get(sa.ParentRecordId).Description;
            }         
            asa.External_ID__c=sa.ParentRecordId+''+sa.Id;
            
            
            allServiceAppointmentsList.add(asa);           
        }
        
        
        List<Database.upsertResult> asaResults = Database.upsert(allServiceAppointmentsList, 
                                                                 All_Service_Appointments__c.External_ID__c, false);
        system.debug('Exiting upsertAllServiceAppointments method');
    }
    
    Public static void UpdateClaimStatus(List<ServiceAppointment> serviceAppointmentList){
        system.debug('Entering UpdateClaimStatus : '+serviceAppointmentList);
        Set<String> woIds = new Set<String>();
        Map<String,WorkOrder> workOrderMap = new Map<String,WorkOrder>();
        List<WorkOrder> workOrderList=new List<WorkOrder>();
        List<Case> caseList=new List<Case>();
        
        For(ServiceAppointment sa:serviceAppointmentList){
            woIds.add(sa.ParentRecordId);
        }
        for(WorkOrder wo : [Select Id,Status,CaseId,Rectification__c,Work_Type_Name__c,Trade_User__c,Sort_Order__c,WorkOrderNumber,Description from workOrder where Id IN : woIds]){
            workOrderMap.put(wo.Id,wo);
        }
        
        For(ServiceAppointment sa : serviceAppointmentList){
            system.debug('Work_Type_Name__c: ' +sa.Work_Type_Name__c);
            system.debug('sa.Work_Type_Assessment__c: ' + sa.Work_Type_Assessment__c);
            if(sa.Work_Type_Name__c != null){
                If(workOrderMap.containsKey(sa.ParentRecordId)) {
                    WorkOrder wo=workOrderMap.get(sa.ParentRecordId);
                    If (sa.Work_Type_Name__c.startsWith('MS-') && wo.Status == NJ_CONSTANTS.WO_STATUS_NEW){
                        Case cs=new Case();
                        cs.Id=workOrderMap.get(sa.ParentRecordId).CaseId;
                        cs.Status = NJ_CONSTANTS.CLAIM_STATUS_WO_SCHEDULED ;
                        caseList.add(cs);
                    }
                    If ((sa.Work_Type_Name__c==NJ_CONSTANTS.WORK_TYPE_ASSESSMENT||sa.Work_Type_Name__c== NJ_CONSTANTS.WORK_TYPE_REASSESSMENT)&& wo.Status == NJ_CONSTANTS.WO_STATUS_NEW){
                        Case cs=new Case();
                        cs.Id=workOrderMap.get(sa.ParentRecordId).CaseId;
                        cs.Assessment_Report_Created__c=false;
                        //Claim status should not be updated to Assessment scheduled
                        //cs.Status = NJ_CONSTANTS.CLAIM_STATUS_ASSESSMENT_SCHEDULED;
                        caseList.add(cs);
                    }
                }
            }             
        }
        
        if(caseList.size() > 0){
            update caseList;
        }
        
        system.debug('Exiting UpdateClaimStatus method');
        
    }
    
    public static void updateWorkOrderTradeUserDetails(Map<Id,User> serviceAppointmentUserMap){
        system.debug('Entering updateWorkOrderTradeUserDetails: '+ 'serviceAppointmentUserMap : ' + serviceAppointmentUserMap);
        List<WorkOrder> workOrderList = new List<WorkOrder>();
        
        workOrderList = [select Id,CaseId,Status,Trade_User__c,Work_Type_Name__c,First_Assessment__c,Resource_Email__c,AccountId from WorkOrder where id in :serviceAppointmentUserMap.keySet()];
        
        Map<Id,ServiceAppointment> serviceAppointmentMap=new Map<Id,ServiceAppointment>();
        for(ServiceAppointment  sa : [select ParentRecordId,Id,Service_Resource__c,Tier_2_Trade_Account__c,Service_Resource__r.IsCapacityBased from ServiceAppointment 
                                      where (Service_Resource__c != null or Tier_2_Trade_Account__c != null) and ParentRecordId IN :serviceAppointmentUserMap.keySet()]){
                                          serviceAppointmentMap.put(sa.ParentRecordId,sa);
                                      }
        
        if (!workOrderList.IsEmpty()){
            User u;  
            // Update work order details with status, service resource company and Trade user
            for(WorkOrder wo : workOrderList){
         
                If ((wo.Work_Type_Name__c==NJ_CONSTANTS.WORK_TYPE_ASSESSMENT||wo.Work_Type_Name__c== NJ_CONSTANTS.WORK_TYPE_REASSESSMENT|| 
                     wo.Work_Type_Name__c==NJ_CONSTANTS.WORK_TYPE_QUALITY_ASSURANCE || wo.Work_Type_Name__c==NJ_CONSTANTS.WORK_TYPE_COORD_ASSESSMENT)&& wo.Status == NJ_CONSTANTS.WO_STATUS_NEW){
                         //Added Coordinator Assessment 
                         if (wo.Work_Type_Name__c==NJ_CONSTANTS.WORK_TYPE_ASSESSMENT || wo.Work_Type_Name__c==NJ_CONSTANTS.WORK_TYPE_COORD_ASSESSMENT){
                              wo.First_Assessment__c = true;
                         }else{
                              wo.First_Assessment__c = false;
                         }
                 }
                if (serviceAppointmentUserMap.ContainsKey(wo.Id)){
                    u = serviceAppointmentUserMap.get(wo.Id);
                }
                if (u != null){
                    wo.Trade_User__c = u.id;
                    if (u.contact != null){
                        wo.Resource_Email__c = u.contact.account.Job_Email_Address__c;
                    }else{
                        wo.Resource_Email__c = u.Email ;
                    }
                    System.debug('Service Resource Company'+u.AccountId);
                    wo.Service_Resource_Company__c = u.accountId;
                    if (serviceAppointmentMap.ContainsKey(wo.Id)){
                        if(serviceAppointmentMap.get(wo.Id).Service_Resource__c != null){
                            wo.Capacity_Based__c=serviceAppointmentMap.get(wo.Id).Service_Resource__r.IsCapacityBased;
                            wo.Service_Resource__c=serviceAppointmentMap.get(wo.Id).Service_Resource__c;
                        }
                        //If no service resource means and it is assigned to Tier2 Trade. If so set capacity to true
                        if(serviceAppointmentMap.get(wo.Id).Tier_2_Trade_Account__c != null){
                            wo.Capacity_Based__c=true;
                        }
                    }
                }
            }
            Update workOrderList;
        }
        system.debug('Exiting updateWorkOrderTradeUserDetails: '+workOrderList);
    } 
}