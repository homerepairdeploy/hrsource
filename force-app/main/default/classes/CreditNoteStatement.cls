Global class CreditNoteStatement {
    @InvocableMethod
    public static void CreditNoteStatement(List<String> invoices) {
        generateCreditNote(Invoices);
    }

    @future(callout=true)
    public static void generateCreditNote(List<String> invoices){
        List<AP_Invoice__c> invoiceList=[Select id,Name,Generate_Invoice_Statement__c from AP_Invoice__c where id IN : invoices];
        List<ContentVersion> attachmentList = new List<ContentVersion>();
        List<ContentDocumentLink> links = new List<ContentDocumentLink>();
        set<string> invIdSet = new set<string>();
        for(AP_Invoice__c inv: invoiceList) {
            PageReference pdfPage = new PageReference('/apex/CreditNoteStatement');
            pdfPage.getParameters().put('id',inv.Id);
                        
            ContentVersion cVersion = new ContentVersion();
            cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.            
            cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
            cVersion.OwnerId = UserInfo.getUserId();//Owner of the file                        
                        
            if(!Test.isRunningTest()){
                cVersion.VersionData = pdfPage.getContentAsPdf();
            }else{
                cVersion.VersionData = Blob.valueOf('Test Data');
            }            
            
            attachmentList.add(cVersion);
            cVersion.PathOnClient = cVersion.Title = 'CreditNote-'+inv.Name+'-'+System.today().format()+'.pdf';    
            invIdSet.add(inv.Id);   
            //Insert Content Version to the invoice     
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.LinkedEntityId = inv.Id;
            cDocLink.ShareType = 'I';
            cDocLink.Visibility = 'InternalUsers';
            links.add(cDocLink);          
        }
        if(!attachmentList.isEmpty()) {
            insert attachmentList;
            Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:attachmentList[0].Id].ContentDocumentId;
            links[0].ContentDocumentId = conDocument;
            insert links;
        }
        return;
    }
}