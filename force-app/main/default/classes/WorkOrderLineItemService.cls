/************************************************************************************************************
Name: WorkOrderLineItemService
=============================================================================================================
Purpose: Class with re-usable methods for WorkOrderLineItem trigger.
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL       DESCRIPTION
1.0        Vasu         10/10/2018        Created       Home Repair Claim System  
*************************************************************************************************************/
public without sharing class WorkOrderLineItemService { 
    private static Map<String,List<ServiceAppointment>> workOrderLineItemMap;
    
    public static void isWorkOrderAmended(Set<String> workOrderSet){
        List<WorkOrder> workOrderList=[SELECT id,Work_Order_Amended__c from WorkOrder where id IN : workOrderSet]; 
        for(WorkOrder wo : workOrderList){
            wo.Work_Order_Amended__c=true;
        }
        update workOrderList;
    }
    
    /*********************************************
* Create service appointments based on site visit
**********************************************/ 
    
    public static void createServiceAppointment(List<WorkOrderLineItem> workOrderLineItemsList){
        List<Id> workOrderIds= new List<Id>();
        for (WorkOrderLineItem wli : workOrderLineItemsList){
            workOrderIds.add(wli.WorkOrderId);
        }
        List<ServiceAppointment> serviceAppointmentList=new List<ServiceAppointment>();
        Map<String,Integer> siteVisitCountMap=new Map<String,Integer>();
        workOrderLineItemMap=new Map<String, List<ServiceAppointment>>();
        Map<String,VisitNumberInfo> visitNoMap = new Map<String,VisitNumberInfo>();
        list<aggregateResult> aggResults = [SELECT MAX(Site_Visit_Number__c)visitCount,WorkOrderId from WorkOrderLineItem where WorkOrderId IN: workOrderIds group by WorkOrderId ];
        String WorkOrderId;
        Integer visitCount;
        for(Integer i=0;i<aggResults.size();i++){  
            WorkOrderId=String.valueOf(aggResults[i].get('WorkOrderId'));
            visitCount=Integer.valueOf(aggResults[i].get('visitCount'));
            siteVisitCountMap.put(WorkOrderId,visitCount);
        }
        String mapKey;
        VisitNumberInfo tempInfo;
        Boolean canCreateServiceApppointment;
        for(WorkOrder wo : [Select id, WorkType.Name,(Select id from ServiceAppointments),
                            (Select Id,Site_Visit_Number__c,Service_Appointment__c,
                             Cash_Settled__c, Labour_Time__c, Sort_Order__c,
                             Product2.Level_5_Description__c,Product2.Team__c,Product2.Team_Size__c
                             from WorkOrderLineItems) 
                            from WorkOrder 
                            where id IN: workOrderIds]){
                                
                                if(wo.WorkOrderLineItems.size() > 0){
                                    for(WorkOrderLineItem woli: wo.WorkOrderLineItems){
                                        if (woli.Site_Visit_Number__c != null){
                                            ServiceAppointment sa=new ServiceAppointment();
                                            sa.Id=woli.Service_Appointment__c;
                                            mapKey = String.valueOf(woli.Site_Visit_Number__c) + wo.Id;
                                            if(!visitNoMap.containsKey(mapKey)) {
                                                visitNoMap.put(mapKey, new VisitNumberInfo());
                                            }
                                            tempInfo = visitNoMap.get(mapKey);
                                            if(woli.Labour_Time__c != null && !woli.Cash_Settled__c) {
                                                tempInfo.totalDuration += woli.Labour_Time__c;
                                            }
                                            if(woli.Sort_Order__c != null) {
                                                tempInfo.sortOrder = woli.Sort_Order__c;
                                            }
                                            tempInfo.team = woli.product2.team__c;
                                            tempInfo.teamSize = woli.product2.team_Size__c;
                                            //tempInfo.canCreateSA = tempInfo.canCreateSA || !woli.Cash_Settled__c; 
                                            if(woli.Cash_Settled__c){
                                                tempInfo.canCreateSA = false;
                                            }
                                            else {
                                                tempInfo.canCreateSA = true;
                                            }
                                            System.debug('CanCreateSA'+tempInfo.canCreateSA);
                                            visitNoMap.put(mapKey, tempInfo); 
                                        }
                                        
                                    }
                                    if(wo.ServiceAppointments.size() == siteVisitCountMap.get(wo.Id)){                
                                    }else {
                                        if(wo.ServiceAppointments.size() == 0){
                                            for(Integer i=0;i<siteVisitCountMap.get(wo.Id);i++){  
                                                mapKey = String.valueOf(i+1)+wo.Id;
                                                if(visitNoMap.containsKey(mapKey)) {
                                                    SAAppointments app = getServiceAppointments(mapKey,wo, visitNoMap.get(mapKey));
                                                    if(!app.sAppointments.isEmpty()) {
                                                        serviceAppointmentList.addAll(app.sAppointments);
                                                    }
                                                }
                                                
                                            }
                                        }else{
                                            if (siteVisitCountMap.containsKey(wo.Id)){
                                                for(Integer i=0;i< (siteVisitCountMap.get(wo.Id)-wo.ServiceAppointments.size());i++){
                                                    mapKey = String.valueOf(i+1)+wo.Id;
                                                    if(visitNoMap.containsKey(mapKey)) {
                                                        SAAppointments app = getServiceAppointments(mapKey,wo, visitNoMap.get(mapKey));
                                                        if(!app.sAppointments.isEmpty()) {
                                                            serviceAppointmentList.addAll(app.sAppointments);
                                                        }
                                                        
                                                    }
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                            }
        system.debug('serviceAppointmentList : '+serviceAppointmentList);
        if(!serviceAppointmentList.IsEmpty()){
            try{
                System.debug('serviceAppointmentList size'+serviceAppointmentList.size());
                insert serviceAppointmentList; 
            }catch(DMLException e) {
                system.debug('Error while upserting WorkOrderShare- ' + e.getMessage());
            }           
        }
        
        List<WorkOrderLineItem> workOrderLineItemList=new List<WorkOrderLineItem>();               
        for(WorkOrderLineItem wili : [Select Id,Site_Visit_Number__c,Service_Appointment__c,WorkOrderId
                                      from WorkOrderLineItem where WorkOrderId IN : workOrderIds]){
                                          system.debug('workOrderLineItemMap1 :'+workOrderLineItemMap+'----'+wili.Id+'----'+wili.Site_Visit_Number__c);
                                          if (workOrderLineItemMap.containsKey(String.valueOf(wili.Site_Visit_Number__c)+wili.WorkOrderId)){    
                                              if(workOrderLineItemMap.get(String.valueOf(wili.Site_Visit_Number__c)+wili.WorkOrderId).size() == 1) {
                                                  system.debug('workOrderLineItemMap2 :'+workOrderLineItemMap+'----'+wili.Id+'----'+wili.Site_Visit_Number__c);
                                                  for(ServiceAppointment sa : workOrderLineItemMap.get(String.valueOf(wili.Site_Visit_Number__c)+wili.WorkOrderId))       {        
                                                      wili.Service_Appointment__c=sa.Id;
                                                  }
                                                  workOrderLineItemList.add(wili);
                                              }
                                          }
                                      }
        system.debug('workOrderLineItemList :'+workOrderLineItemList);
        if(!workOrderLineItemList.IsEmpty()){
            update workOrderLineItemList;
        } 
    }
     private static SAAppointments getServiceAppointments(String mapKey,WorkOrder wo, VisitNumberInfo tempInfo ) {
        SAAppointments servApp = new SAAppointments();
        Boolean canCreateServiceApppointment;
        List<ServiceAppointment> saAppTemp;
        ServiceAppointment sa=HomeRepairUtil.createServiceAppointment(wo.Id);  
        sa.RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByName().get('Service Appointment General').getRecordTypeId();
        sa.Duration = tempInfo.totalDuration / 60.00; 
        sa.Duration.setScale(2);
        sa.Sort_Order__c = tempInfo.sortOrder;
        sa.Team__c = tempInfo.team;
        sa.Team_Size__c = tempInfo.teamSize;
       /* if(sa.Duration > 8) {
            sa.FSL__IsMultiDay__c = true;
        }  */
        sa.Work_Type_Name__c = wo.WorkType.Name;
        canCreateServiceApppointment = tempInfo.canCreateSA;
        if(canCreateServiceApppointment) {
            servApp.sAppointments.add(sa);
        }
        if(!workOrderLineItemMap.containsKey(mapKey)) {
            workOrderLineItemMap.put(mapKey, new List<ServiceAppointment>());
        }
        saAppTemp = workOrderLineItemMap.get(mapKey);
        saAppTemp.add(sa);
        workOrderLineItemMap.put(mapKey,saAppTemp);
        return servApp;    
    }
    private class SAAppointments {
        List<ServiceAppointment> sAppointments;
        public SAAppointments() {
            sAppointments = new List<ServiceAppointment>();
        }
    }
    
    public class VisitNumberInfo {
        Decimal totalDuration;
        Decimal sortOrder;
        Boolean canCreateSA;
        Boolean team;
        Decimal teamSize;
        public VisitNumberInfo() {
            totalDuration = 0;
            canCreateSA = false;
            team = false;
            teamSize = 0;
        }
    }
}