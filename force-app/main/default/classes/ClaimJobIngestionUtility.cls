/**
 * Utility class for Claim Job Ingestion Batch class
 * @Author : Nikhil Jaitly
 * @CreatedDate : 21/07/2019
 */
public with sharing class ClaimJobIngestionUtility {
  /**
   * Filter claim job ingestion staging records
   * @Author : Nikhil Jaitly
   * @CreatedDate : 21/07/2019
   */
  
  public static Map<Id, Claim_Job_Ingestion_Staging__c> getIngestionForProcessing(List<Claim_Job_Ingestion_Staging__c> scope,
                                                                                   Map<String, Historical_Claim_Data__c> historicalClaimMap,
                                                                                   Map<String, Historical_Claim_Data__c> historicalClaimFalseMap){
                                                                                   
                                                                                   
    GeneralSettings__c gs = GeneralSettings__c.getValues('ClaimIngestionFilter');
    Map<Id, Claim_Job_Ingestion_Staging__c> toProcess = new Map<Id, Claim_Job_Ingestion_Staging__c>();
    Map<String, Historic_Claim_Job_Data__c> historicalClaimJobMap=new Map<String, Historic_Claim_Job_Data__c>();
   
     if(gs.Value__c == 'TRUE'){ 
      
      historicalClaimJobMap=fetchHistoricalClaimJobData(scope);
      historicalClaimMap=fetchHistoricalClaimData(scope,historicalClaimJobMap,true);
      system.debug('historicalClaimMap'+historicalClaimMap);
      
      system.debug('histroicalClaimJobMap'+historicalClaimJobMap);
      for (Claim_Job_Ingestion_Staging__c cjis : scope) {
		// Modified JP 15-10-2020 : HRD-800 - Ascertaining information via Historical Data Search
		// Excluding Claim Map as is now not required
		// 
		//if(!historicalClaimMap.containsKey(cjis.ClaimNumber__c)) {
		//Exclude if claim type is building and contents
          if (cjis.ClaimType__c == BatchClaimJobIngestionStagingProcess.CLAIM_TYPE_BUILDING_CONTENTS 
              && cjis.JobSubType__c == BatchClaimJobIngestionStagingProcess.JOB_SUB_TYPE_DOANDCHARGE){
              System.debug('Excluded building and content job: ' + cjis.InternalReferenceId__c);
            } else{  
                               
              toProcess.put(cjis.Id, cjis);
          }
        //}
      }
    } 
                                                                                                                                                                         
    return toProcess;
  }
  /**
   * Fetch historical claim records
   * @Author : Nikhil Jaitly
   * @CreatedDate : 21/07/2019
   */
  public static Map<String, Historical_Claim_Data__c> fetchHistoricalClaimData(List<Claim_Job_Ingestion_Staging__c> cjisList,Map<String, Historic_Claim_Job_Data__c> historicalClaimJobMap,Boolean claimExclusion ) {
    Map<String, Historical_Claim_Data__c> historicalClaimMap = new Map<String, Historical_Claim_Data__c>();
    Set<String> claimNumberSet=new Set<String>();
    for (Claim_Job_Ingestion_Staging__c cjis : cjisList) {
      if(cjis.ClaimNumber__c != null){
        claimNumberSet.add(cjis.ClaimNumber__c);
      }
    }
    set<string> claimNumberUniqueSet=new set<string>();
    if(!claimNumberSet.isEmpty()){
      List<Historical_Claim_Data__c> historicalClaimDataList = [SELECT Id,Claim_Number__c,Status__c,Legacy_Claim_ID__c  
                                                                FROM Historical_Claim_Data__c 
                                                                WHERE Claim_Number__c IN :claimNumberSet order by Claim_Number__c,Legacy_Claim_ID__c desc ];
      for(Historical_Claim_Data__c hcd : historicalClaimDataList){
      
         
          
          If (!claimNumberUniqueSet.contains(hcd.Claim_Number__c)){
          
              If (Integer.valueof(hcd.Legacy_Claim_ID__c) >= Integer.valueOf(system.label.LegacyClaimId)&& claimExclusion ){
                  If ((hcd.Status__c=='closed' && historicalClaimJobMap.containsKey(hcd.Claim_Number__c)) || hcd.Status__c=='Open')
                      historicalClaimMap.put(hcd.Claim_Number__c,hcd);
              }
              
              If (Integer.valueof(hcd.Legacy_Claim_ID__c) < Integer.valueOf(system.label.LegacyClaimId)&& claimExclusion ){
                  historicalClaimMap.put(hcd.Claim_Number__c,hcd);
              }
              
              If (Integer.valueof(hcd.Legacy_Claim_ID__c) < Integer.valueOf(system.label.LegacyClaimId)&& !claimExclusion ){
                  historicalClaimMap.put(hcd.Claim_Number__c,hcd);
              }
              
              claimNumberUniqueSet.add(hcd.Claim_Number__c);
              
           }  
           
           
              
      }
    }
    return historicalClaimMap;
  }  

   /**
   * Fetch historical claimJob Data records
   * @Author : Pardha
   * @CreatedDate : 3/12/2019
   */
  public static Map<String, Historic_Claim_Job_Data__c> fetchHistoricalClaimJobData(List<Claim_Job_Ingestion_Staging__c> cjisList) {
    Map<String, Historic_Claim_Job_Data__c> historicalClaimJobMap=new Map<String, Historic_Claim_Job_Data__c>();
    Set<String> claimNumberSet=new Set<String>();
    map<string,string> claimAuthMap=new map<string,string>();
    for (Claim_Job_Ingestion_Staging__c cjis : cjisList) {
      if(cjis.ClaimNumber__c != null){
        claimNumberSet.add(cjis.ClaimNumber__c);
        claimAuthMap.put(cjis.ClaimNumber__c,cjis.InternalReferenceId__c);
      }
    }
    if(!claimNumberSet.isEmpty()){
      List<Historic_Claim_Job_Data__c> historicalClaimJobDataList = [SELECT Id,Claim_Number__c,Authority_Number__c,Legacy_Claim_Id__c 
                                                                     FROM Historic_Claim_Job_Data__c 
                                                                      WHERE Claim_Number__c IN :claimNumberSet ];
      for(Historic_Claim_Job_Data__c hcd : historicalClaimJobDataList){
         
          If (claimAuthMap.get(hcd.Claim_Number__c)==hcd.Authority_Number__c) historicalClaimJobMap.put(hcd.Claim_Number__c,hcd); 
                 
      }
    }
    return historicalClaimJobMap;
  }    
  /**
   * Upsert accounts
   * @Author : Nikhil Jaitly
   * @CreatedDate : 21/07/2019
   */
  public static Map<String, Account> upsertAccounts(List<Claim_Job_Ingestion_Staging__c> claimJobProcessList,
                                                 Set<String> nameSet, Set<String> policyNumberSet) {
    List<Account> accountList = new List<Account>();
    Map<String, Account> accounts = new Map<String, Account>();
    Map<String, Account> existingAccountMap = new Map<String, Account>();
    Set<Account> upsertAccountSet = new Set<Account>();
    for (Account acct : [SELECT Id, Name,Policy_Number__c  
                         FROM Account 
                         WHERE Name IN : nameSet 
                         AND Policy_Number__c IN :policyNumberSet]) {
      existingAccountMap.put(acct.Policy_Number__c + acct.Name, acct);
    }
    for (Claim_Job_Ingestion_Staging__c cjis : claimJobProcessList) {
      accountList.add(getPersonAccount(cjis,existingAccountMap));
    }
    if (!accountList.isEmpty()) {
      upsertAccountSet.addAll(accountList);
      upsert new List<Account>(upsertAccountSet);
      for(Account acc : upsertAccountSet) {
        accounts.put(acc.Policy_Number__c + acc.Name, acc);
      }
      system.debug('accounts in utility clss ' + accounts);
    }      
    return accounts;
  }
  /**
   * Upsert contacts
   * @Author : Nikhil Jaitly
   * @CreatedDate : 21/07/2019
   */
  public static Map<String, Contact> upsertContacts(List<Claim_Job_Ingestion_Staging__c> claimJobProcessList,
                                                 Set<String> nameSet, Set<String> policyNumberSet,
                                                 Map<String,Account> accountMap) {
    Map<String, Contact> contacts = new Map<String, Contact>();
    Map<String, Contact> contactMap = new Map<String, Contact>();
    Set<Contact> upsertContactSet = new Set<Contact>();
    List<Contact> contactList = new List<Contact>();
    for(Contact con : [SELECT Id, Name, AccountId, Policy_Number__c,Phone,HomePhone, MobilePhone  
                       FROM Contact 
                       WHERE Name IN :NameSet 
                       AND Policy_Number__c IN :policyNumberSet]) {
      contactMap.put(con.Policy_Number__c + con.Name, con);
    }
    for (Claim_Job_Ingestion_Staging__c cjis : claimJobProcessList) {
        system.debug('contactMap ' + contactMap);
        system.debug('accountMap ' + accountMap);
      contactList.add(getContact(cjis, contactMap, accountMap));
    }        
    if(!contactList.isEmpty()) {
        system.debug('contactList ' + contactList);
      upsertContactSet.addAll(contactList);
        system.debug('upsertContactSet ' + upsertContactSet);
      upsert new List<Contact>(upsertContactSet);
      for(Contact con : upsertContactSet) {
          system.debug('con.Name ' + con.Name);
        contacts.put(con.Policy_Number__c + con.LastName, con);
      } 
      system.debug('main contactList in utility' + contactList);
  
    }
    return contacts;
  }
  /**
   * Upsert policy
   * @Author : Nikhil Jaitly
   * @CreatedDate : 21/07/2019
   */
  public static void upsertPolicy(List<Claim_Job_Ingestion_Staging__c> claimJobProcessList) {
    Set<String> policySet = new Set<String>();
    List<Policy__c> policyList = new List<Policy__c>();
    for (Claim_Job_Ingestion_Staging__c cjis : claimJobProcessList) {
      if(!policySet.contains(cjis.Policy_Number__c)) {
        policyList.add(getPolicy(cjis));
        policySet.add(cjis.Policy_Number__c);
      }
    }
    if(!policyList.isEmpty()) {
      upsert policyList Policy__c.Policy_Number__c;   
    }   
  }
  /**
   * Upsert claims
   * @Author : Nikhil Jaitly
   * @CreatedDate : 21/07/2019
   */
  public static Map<String,Id> upsertClaim(List<Claim_Job_Ingestion_Staging__c> claimJobProcessList, Map<String,Contact> contactMap,map<String,Id> claimsHistMap) {
      system.debug('contactMap -- CK ' + contactMap);
      Map<String,Id> claimMap = new Map<String,Id>();
    Map<String, Account> brandMap = new Map<String, Account>();
    List<Case> caseList = new List<Case>();
    for (Account acct : [SELECT Id, Name 
                             FROM Account 
                             WHERE Recordtype.Name  = :BatchClaimJobIngestionStagingProcess.IPACCOUNTRECORDTYPE]) {
      brandMap.put(acct.name.toLowerCase(), acct);
    }
    Set<String> claimSet = new Set<String>();
    for (Claim_Job_Ingestion_Staging__c cjis : claimJobProcessList) {
      if(!claimSet.contains(cjis.ClaimNumber__c)) {
        caseList.add(getCase(cjis, contactMap,brandMap,claimsHistMap));
        claimSet.add(cjis.ClaimNumber__c);
      }
      system.debug('case list in UPSERTCLAIM ' + caseList );
 
    }
    if(!caseList.isEmpty()) {

     // upsert caseList Case.Claim_Number__c; 
       upsert caseList Claim_Number_RecordType__c;
         for(case c : caseList){
          //Added by CRMIT to get the ClaimId to create child Case and returning to batch class
          claimMap.put(c.ClaimJob_Ingestion_Id_for_Claim__c,c.Id);
      }
      system.debug('case map from UPSERTCLAIM method ' + claimMap);
      system.debug('CAse list form UPSERTCLAIM method ' + caseList );
    }   
      return claimMap;
  }
  /**
   * Upsert claim job
   * @Author : Nikhil Jaitly
   * @CreatedDate : 21/07/2019
   */
   //Updated by CRMIT
   // ** JP Modified 21-08-2020 - Added functionality for creation of 
  public static Map<String,case> upsertClaimJob(List<Claim_Job_Ingestion_Staging__c> claimJobProcessList,Map<String,Id> claimMap) {
    Map<String,case> authorityMap = New Map<String,case>();
	List<Case> claimJobList = new List<Case>();
 	Case baseCase = new Case();
	//Map<String, Authority_Activity_Tracker__c> caseActivityList = New Map<String, Authority_Activity_Tracker__c>();
	//List<Authority_Activity_Tracker__c> activityList = New List<Authority_Activity_Tracker__c>();
    //Authority_Activity_Tracker__c baseActivity = New Authority_Activity_Tracker__c();
        
    for(Claim_Job_Ingestion_Staging__c cjis : claimJobProcessList) {
       //Added by CRMIT passing ClaimId(parent caseId) to the GetClaimJob Method(claimMap.get(cjis.ClaimNumber__c))
       //** JP Modified 07-08-2020 - Change to generate comments and tasks based on the status feed from client
       baseCase = getClaimJob(cjis,claimMap.get(cjis.Id));
		//If (cjis.Status__c != 'completed' || cjis.Status__c != 'reissued' || cjis.Status__c != 'cancelled' || cjis.Status__c != 'rejected'|| cjis.Status__c != 'declined'){
            //generateCaseComment(cjis, baseCase);
			//	generateCaseTask(cjis, baseCase);
        claimJobList.add(baseCase); //getClaimJob(cjis,claimMap.get(cjis.Id)));              	
        //} 		
 		//caseActivityList.put(cjis.JobNumber__c, buildActivityTracker(cjis));				                    
    } 
	
    
	system.debug('child case list in upsertClaimJob=====' +claimJobList);
    if(!claimJobList.isEmpty()) { 
        // ** JP Modified 06-08-2020
        // ** Changed to use Job_Number__c as the reference/external ID
      	//upsert claimJobList case.Source_Internal_Reference_Id__c;
      	upsert claimJobList case.Job_Number__c;
        for(case c : claimJobList){
        	authorityMap.put(c.ClaimJob_Ingestion_Id_for_Authority__c,c);  
            
            //If(caseActivityList.containsKey(c.Job_Number__c)){
            //	baseActivity = caseActivityList.get(c.Job_Number__c);    
            //    baseActivity.Case__c = c.id;
                
            //    activityList.add(baseActivity);
            //}
       	}
        //If(!caseActivityList.isEmpty()){
        //    insert activityList;
        //}
    }
    system.debug('child case list in upsertClaimJob11=====' +claimJobList);
 
    return authorityMap;
  }
    
    /* public static Authority_Activity_Tracker__c buildActivityTracker(Claim_Job_Ingestion_Staging__c caseUpdate){
        Authority_Activity_Tracker__c aAT = new Authority_Activity_Tracker__c();
		String jobNumber = caseUpdate.JobNumber__c;
        case existingCase = [SELECT Client_Status__c FROM Case WHERE Job_Number__c = :jobNumber];
        user automationUser = [SELECT Id FROM User WHERE Name = 'Developer Homerepair'];
                
        If(!String.isBlank(existingCase.Client_Status__c)){
            aAT.Status_Changed_From__c = existingCase.Client_Status__c;
        }
        
        aAT.Source__c = 'SunCorp';
        aAT.Changed_by_User__c = automationUser.Id;
        aAT.Status_Changed_To__c = caseUpdate.Status__c;
        aAT.Processing_Status__c = 'Pending';
        aAT.Date_Time_Stamp__c = DateTime.now();
        
        return aAT;
    } */
    

  /**
   * Upsert claim job item
   * @Author : Nikhil Jaitly
   * @CreatedDate : 21/07/2019
   */
  public static void upsertClaimJobItem(List<Claim_Job_Ingestion_Staging__c> claimJobProcessList,Map<String,case> authorityMap) {
    List<Claim_Job_Work_Item__c> AuthorityJobWorkItemList = new List<Claim_Job_Work_Item__c>();
    for (Claim_Job_Ingestion_Staging__c cjis : claimJobProcessList) {
      if(!cjis.Claim_Job_Work_Items_Staging__r.isEmpty()) {
        for(Claim_Job_Work_Items_Staging__c cjwi : cjis.Claim_Job_Work_Items_Staging__r){
          AuthorityJobWorkItemList.add(getClaimJobWorkItem(cjwi,authorityMap.get(cjis.Id)));
        }  
      }              
    }  
    if(!AuthorityJobWorkItemList.isEmpty()) { 
      upsert AuthorityJobWorkItemList;
       
    
    }
  }
  /**
   * get instance of  Claim_Job_Work_Item__c
   * @Author : Nikhil Jaitly
   * @CreatedDate : 21/07/2019
   */
  private static Claim_Job_Work_Item__c getClaimJobWorkItem(Claim_Job_Work_Items_Staging__c cjwis,Case authorityCase){  
    Claim_Job_Work_Item__c cjwi=new Claim_Job_Work_Item__c();
    //Added By CRMIT to replace Claim_Job with Case
    system.debug(cjwis.Job_Number__c);
    //Case claimJob = new Case(Job_Number__c= cjwis.Job_Number__c);
    //Added By CRMIT to replace  cjwi.Authority__c with cjwi.Claim_Job__r=claimJob;                     
    cjwi.Authority__c=authorityCase.Id;                     
    cjwi.Age__c=cjwis.Age__c;
    cjwi.Category__c=cjwis.Category__c;
    cjwi.Description__c=cjwis.Description__c;
    cjwi.Item__c=cjwis.Item__c;
    cjwi.Item_Location__c=cjwis.Item_Location__c;
    cjwi.Make_Model__c=cjwis.Make_Model__c;
    cjwi.Number_Of_Items__c=cjwis.Number_Of_Items__c;
    cjwi.Quantity__c=cjwis.Quantity__c;
    cjwi.Quote_Amount__c=cjwis.Quote_Amount__c;
    cjwi.Source_Internal_Reference_Id__c=cjwis.Source_Internal_Reference_Id__c;
    cjwi.Special_Instructions__c=cjwis.Special_Instructions__c;
    cjwi.Unit__c=cjwis.Unit__c;
    cjwi.RecordTypeId = NJ_Utilities.getRecordTypeId(Claim_Job_Work_Item__c.sObjectType, cjwis.Work_Item_Type__c);
      system.debug(cjwi.Authority__r);
    return cjwi;   
  }

  /**
   * get instance of  Claim_Job__c
   * @Author : Nikhil Jaitly
   * @CreatedDate : 21/07/2019
   */
  private static boolean isCliamJobOld(Claim_Job_Ingestion_Staging__c cjis) {
         string ClaimNumber=cjis.ClaimNumber__c.touppercase();
        //Added By CRMIT to replace with Claim_Job with Case
        list<Case> cjc=[select Id from Case where Job_Number__c=:cjis.JobNumber__c and Parent_Claim_Number__c=:ClaimNumber];
        if (cjc.size()>0) return true; else return false;
  }
  
    // ** JP Created 04-08-2020
    private static boolean isAuthorityQuoteNowAuthorised(Claim_Job_Ingestion_Staging__c cjis){
        string ClaimNumber=cjis.ClaimNumber__c.touppercase();
        boolean retValue = false;
        
        If ((cjis.JobSubType__c == 'assessment' || cjis.JobSubType__c == 'report'|| cjis.JobSubType__c == 'Quote' || cjis.JobSubType__c == 'authority') && (cjis.JobType__c == 'authority')){
        	list<Case> cjAuth=[select Id from Case where Job_Number__c=:cjis.JobNumber__c and Parent_Claim_Number__c=:ClaimNumber and Action_Type__c='quote'];
            
            If (cjAuth.size() > 0){
                retValue = true;
            }
        }
        
		return retValue;
    }

    // ** JP Created 04-08-2020
  	private static boolean isAuthorityStatusValid(Claim_Job_Ingestion_Staging__c cjis) {
        string caseStatus = cjis.Status__c.toUpperCase();
        boolean isValid = true;
        list<case> cjc=[select Status from Case where Source_Internal_Reference_Id__c=:cjis.InternalReferenceId__c LIMIT 1];
        
         if (cjc.size()>0 && (cjc[0].Status=='Completed' || cjc[0].Status=='Complete')){
				isValid=false;			                            
            
        } else {
	        if (
    	        caseStatus==BatchClaimJobIngestionStagingProcess.CLAIM_STATUS_CANCELLED.toUpperCase() ||
        		caseStatus==BatchClaimJobIngestionStagingProcess.CLAIM_STATUS_COMPLETED.toUpperCase() ||
            	caseStatus==BatchClaimJobIngestionStagingProcess.CLAIM_STATUS_REISSUED.toUpperCase() ||
            	caseStatus==BatchClaimJobIngestionStagingProcess.CLAIM_STATUS_REJECTED.toUpperCase() ||
            	caseStatus==BatchClaimJobIngestionStagingProcess.CLAIM_STATUS_DECLINED.toUpperCase() ||
        		caseStatus==BatchClaimJobIngestionStagingProcess.CLAIM_STATUS_COMPLETE.toUpperCase()) isValid = false;
            
        }
        
        return isValid;
	}
    
    // ** JP Created 04-08-2020
    private static boolean isAuthorityQuote(Claim_Job_Ingestion_Staging__c cjis) {
		string ClaimNumber=cjis.ClaimNumber__c.touppercase();
		list<Case> cjc=[select Id from Case where Job_Number__c=:cjis.JobNumber__c and Parent_Claim_Number__c=:ClaimNumber and Job_Type__c IN ('Quote', 'assessment', 'report')];
		if (cjc.size()>0) return true; else return false;
    }
  /**
   * get instance of  Claim_Job__c
   * @Author : Nikhil Jaitly
   * @CreatedDate : 21/07/2019
   */
  private static case getClaimJob(Claim_Job_Ingestion_Staging__c cjis,Id caseId) {
 
    case c = new case();
   
    //Added by CRMIT (Recordtype(Authority) for child case)
    c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
    c.Job_Number__c = cjis.JobNumber__c;
    c.Source_Internal_Reference_Id__c = cjis.InternalReferenceId__c;
    c.Vendor_Reference_Number__c = cjis.VendorReferenceNumber__c;
    c.Description = cjis.Job_Description__c;
    c.Date_Issued__c = cjis.DateIssued__c;
    //c.Claim_Number__c = cjis.ClaimNumber__c.touppercase();  
    c.Date_RespondBy__c = cjis.DateRespondBy__c;
    c.Reported_Date__c = cjis.DateReceived__c;
      c.ClaimJob_Ingestion_Id_for_Authority__c = cjis.id;
    system.debug('HomeRepairClamjobold'+isCliamJobOld(cjis));
    If (isAuthorityStatusValid(cjis)) c.Status = cjis.Status__c; // ** JP Modified 04-08-2020
    c.Client_Status__c = cjis.Status__c;
    If (isAuthorityQuoteNowAuthorised(cjis)) c.Verified_date__c = Datetime.now(); // ** JP Modified 04-08-2020
    c.Special_Instructions__c = cjis.SpecialInstructions__c;
    c.Authority_Claim_Type__c = cjis.ClaimType__c; 
    c.Is_Manual__c = cjis.Is_Manual__c ;
    c.Is_Event__c = cjis.Is_Event__c ;
    c.Source_Channel_Actual__c = cjis.Source_Channel_Actual__c ;
    if (cjis.HomeAssist__c == true) {
        c.Job_Type__c = BatchClaimJobIngestionStagingProcess.JOB_TYPE_HOME_ASSISST;
    } else {
      if ((cjis.ClaimType__c == BatchClaimJobIngestionStagingProcess.CLAIM_TYPE_REPAIR || cjis.ClaimType__c == BatchClaimJobIngestionStagingProcess.CLAIM_TYPE_REPLACE) && cjis.JobSubType__c == BatchClaimJobIngestionStagingProcess.JOB_SUB_TYPE_DOANDCHARGE){
        c.Job_Type__c = BatchClaimJobIngestionStagingProcess.JOB_TYPE_CONTENTS; 
      } else{
          If (!isAuthorityQuote(cjis))
          	{ 
          		c.Job_Type__c = cjis.JobSubType__c;} else 
            {
				c.Job_Type__c = 'assessment';// ** JP Modified 04-08-2020                
			}
       }
    }
    //if (cjis.JobSubType__c == 'assessment' || cjis.JobSubType__c == 'report'|| cjis.JobSubType__c == 'Quote')  c.Job_Type__c='Quote';
    c.Action_Type__c = cjis.JobType__c; 
    c.HomeAssist__c = cjis.HomeAssist__c;
      System.debug('CJIS.claimno '+cjis.ClaimNumber__c);
            System.debug('before '+caseId);

  if(cjis.ClaimNumber__c != null){
      //Case cs = new Case(Claim_Number__c= cjis.ClaimNumber__c.touppercase());
      //Added by CRMIT mapping childCase to parent case(Claim)
      If (String.isBlank(caseId)){
         Id claimRecordId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Claim').getRecordTypeId();
         caseId =[select id from case where RecordTypeId=:claimRecordId and Claim_Number__c=:cjis.ClaimNumber__c].Id;
      } 
      
      System.debug('After '+caseId);
      c.parentId = caseId;
    }
    c.Source_Channel__c = cjis.Source_Channel__c; 
    system.debug('case child in GETClaimJOB method =======' +c); 
    system.debug('case child in GETClaimJOB method parent id =======' +c.ParentId);   
    return c;
  }

  /**
   * get instance of  Account
   * @Author : Nikhil Jaitly
   * @CreatedDate : 21/07/2019
   */
  private static Account getPersonAccount(Claim_Job_Ingestion_Staging__c cjis,Map<String,Account> existingAccountMap) {
    return new Account(Id = existingAccountMap.containsKey(cjis.Policy_Number__c + cjis.PrimaryContact__c) 
                          ? existingAccountMap.get(cjis.Policy_Number__c + cjis.PrimaryContact__c).Id
                          : null,
                       RecordTypeId = NJ_Utilities.getRecordTypeId(Account.sObjectType, BatchClaimJobIngestionStagingProcess.PHACCOUNTRECORDTYPE),
                       Type = BatchClaimJobIngestionStagingProcess.PHACCOUNTRECORDTYPE,
                       Name = cjis.PrimaryContact__c,
                       Policy_Number__c = cjis.Policy_Number__c);
  }
  /**
   * get instance of  Contact
   * @Author : Nikhil Jaitly
   * @CreatedDate : 21/07/2019
   */
  private static Contact getContact(Claim_Job_Ingestion_Staging__c cjis, Map<String, Contact> contactMap, Map<String, Account> accountMap) {
    return new Contact( Id = contactMap.containsKey(cjis.Policy_Number__c + cjis.PrimaryContact__c) 
                           ? contactMap.get(cjis.Policy_Number__c + cjis.PrimaryContact__c).Id
                           : null,
                        AccountId = accountMap.containsKey(cjis.Policy_Number__c + cjis.PrimaryContact__c) 
                                  ? accountMap.get(cjis.Policy_Number__c + cjis.PrimaryContact__c).Id
                                  : null,
                        RecordTypeId = NJ_Utilities.getRecordTypeId(Contact.sObjectType, BatchClaimJobIngestionStagingProcess.PHCONTACTRECORDTYPE),
                        RelationshipToInsured__c = cjis.PrimaryContactRelationshipToInsured__c,
                        LastName = cjis.PrimaryContact__c,
                        HomePhone = cjis.PrimaryContactHomePhone__c!=null?CommonUtilityClass.formatNumber(cjis.PrimaryContactHomePhone__c):null,
                        Phone = cjis.PrimaryContactWorkPhone__c!=null?CommonUtilityClass.formatNumber(cjis.PrimaryContactWorkPhone__c):null,
                        Mobilephone = cjis.PrimaryContactMobilePhone__c!=null?CommonUtilityClass.formatNumber(cjis.PrimaryContactMobilePhone__c):null,
                        Email = cjis.PrimaryContactEmailAddress__c,
                        Policy_Number__c = cjis.Policy_Number__c);
    }
    /**
     * get instance of  Policy
     * @Author : Nikhil Jaitly
     * @CreatedDate : 21/07/2019
     */
    private static Policy__c getPolicy(Claim_Job_Ingestion_Staging__c cjis) {        
      return new Policy__c(AddressLine1__c = cjis.RiskAddressLine1__c, AddressLine2__c = cjis.RiskAddressLine2__c,
                           AddressLine3__c = cjis.RiskAddressLine3__c,Suburb__c = cjis.RiskAddressSuburb__c,
                           State__c = cjis.RiskAddressState__c,Insured__c = cjis.Insured__c,
                           Postcode__c = cjis.RiskAddressPostcode__c, Policy_Number__c = cjis.Policy_Number__c,
                           Occupancy_Type__c = cjis.OccupancyType__c, Storeys__c = cjis.Storeys__c,
                           Roof_Type__c = cjis.RoofType__c, Year_Built__c = cjis.HomeAge__c,
                           Excess__c = cjis.Excess__c, Construction_Type__c = cjis.Construction_Type__c,
                           Building_under_construction__c = cjis.Building_under_construction__c);
    }
    /**
     * get instance of  Case
     * @Author : Nikhil Jaitly
     * @CreatedDate : 21/07/2019
     */
    private static Case getCase(Claim_Job_Ingestion_Staging__c cjis, Map<string, Contact> contactMap,Map<String,Account> brandMap,map<String,Id> claimsHistMap) {
       System.debug('====INSIDE GETCASE METHOD======');
      Case cs = new Case();
      system.debug('cjis.Policy_Number__c + cjis.PrimaryContact__c ' + cjis.Policy_Number__c  + ' PrimaryContact__c ' + cjis.PrimaryContact__c);
      Policy__c pol = new Policy__c(Policy_Number__c = cjis.Policy_Number__c);
      cs.ClaimJob_Ingestion_Id_for_Claim__c = cjis.Id;
      cs.Policy__r = pol;
      cs.RecordTypeId = NJ_Utilities.getRecordTypeId(Case.sObjectType, 'Claim');
      if (brandMap.containsKey(cjis.Brand__c)){
        cs.Insurance_Provider__c = brandMap.get(cjis.Brand__c.toLowerCase()).Id;
      }
      if(contactMap.containsKey(cjis.Policy_Number__c + cjis.PrimaryContact__c)){
          system.debug('in the core');
        cs.ContactId = contactMap.get(cjis.Policy_Number__c + cjis.PrimaryContact__c).Id;
      }
      cs.Status = BatchClaimJobIngestionStagingProcess.CASE_STATUS;
      cs.Origin = BatchClaimJobIngestionStagingProcess.CASE_ORIGIN;
      if(cjis.ClaimNumber__c != null){
          
        cs.Claim_Number__c = cjis.ClaimNumber__c.touppercase();  
        cs.Claim_Number_RecordType__c = cjis.ClaimNumber__c.touppercase()+cs.RecordTypeId;
      }
      cs.Description = cjis.Claim_Description__c;
      if(cjis.Claim_Description__c != null && cjis.Claim_Description__c.length() > 250){
        cs.Subject = cjis.Claim_Description__c.subString(0,250);
      }else{
        cs.Subject = cjis.Claim_Description__c;
      }
      cs.Loss_Date__c = cjis.LossDate__c;
      cs.Authorised_Amount__c=cjis.AuthorisedAmount__c;
      cs.Loss_Cause__c = cjis.LossCause__c;
      cs.Asbestos_Present__c = cjis.Asbestos_Present__c;
      cs.Mould_Present__c = cjis.Mould_Present__c;
      cs.StatusUpdateDate__c = cjis.StatusUpdateDate__c;
        
      If (claimsHistMap.containsKey(cs.Claim_Number__c)) cs.Is_Legacy_Claim_Value__c=true;
      If (contactMap.get(cjis.Policy_Number__c + cjis.PrimaryContact__c).HomePhone!=null) cs.Home_Phone__c=contactMap.get(cjis.Policy_Number__c + cjis.PrimaryContact__c).HomePhone;
      If (contactMap.get(cjis.Policy_Number__c + cjis.PrimaryContact__c).MobilePhone!=null) cs.Customer_Mobile_Phone__c=contactMap.get(cjis.Policy_Number__c + cjis.PrimaryContact__c).MobilePhone;
      If (contactMap.get(cjis.Policy_Number__c + cjis.PrimaryContact__c).Phone!=null) cs.Customer_Contact_Phone__c=contactMap.get(cjis.Policy_Number__c + cjis.PrimaryContact__c).Phone;
        System.debug('==== get case method======' +cs);      
        return cs;
    }
    
    public static void checkTest(){
     Integer i=0;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    }
}