/********************************************************************************************** 
Name        : ClaimConnect_AWSService
Description : Class used in ClaimConnect_SummaryController and ClaimConnect_LightningFileUploadHandler
Purpose     : This class is used to upload photos to S3 server from Salesforce using REST API
                                                            
VERSION        AUTHOR               DATE           DETAIL      DESCRIPTION   
1.0            Vidhya Sivaperuman   25-05-2020     Created     ClaimConnect APP
/***********************************************************************************************/
public class ClaimConnect_AWSService {
    
    public string bucketName {get;set;}
    public string methodName {get;set;}
    public string hostName {get;set;}
    public string rootPath {get;set;}
    public List<S3_FileStore__c> fileStore {get;set;}   
   
    
    public ClaimConnect_AWSService(string bucket, string method, string host, string root)
    {
        bucketName=bucket;
        methodName=method;
        hostName=host;
        rootPath=root;
        fileStore=new List<S3_FileStore__c>();
    }
     
        
    public boolean UploadDocuments(string objectName,string parentId,string recordId,string rName)
    {
        system.debug('Upload documents objectName:'+objectName);
        system.debug('Upload documents parentId:'+parentId);
        system.debug('Upload documents recordId:'+recordId);                     
        
        Boolean isFileUploaded; 
        Set<Id> entityIds = new Set<Id>();
        entityIds.add(recordId);            
        
        List<ContentDocumentLink> links=[SELECT ContentDocumentId,LinkedEntityId FROM ContentDocumentLink where LinkedEntityId IN :entityIds];
        Set<Id> ids=new Set<Id>();
        system.debug('links'+links);
        for(ContentDocumentLink link:links)
        {
            ids.add(link.ContentDocumentId);
            system.debug('Upload documents ContentDocumentID:'+(link.ContentDocumentId));
        }                
        
        List<ContentVersion> versions=[SELECT VersionData,Title,ContentDocumentId,ContentSize,Description,CreatedById,FileExtension FROM ContentVersion WHERE ContentDocumentId = :ids AND IsLatest = true];
        ClaimConnect_AWSConnector connector=new ClaimConnect_AWSConnector();         
        
        for(ContentVersion attach:versions)
        {                 
           try
           {
            String filename=attach.Title;
            filename=filename.replaceAll('[^a-zA-Z0-9]','%20');
            rName= rName.replaceAll('[^a-zA-Z0-9]','%20');
            String fileextension=attach.FileExtension;
            String fileID = attach.ContentDocumentId;            
            String fullfilename=filename.toLowerCase()+ '.'+fileextension.toLowerCase();           

            string filepath= 'https://' + this.bucketName + '.' + this.hostName + '/' +  this.rootPath +'/'+Objectname+'/'+String.valueOf(parentId).substring(0, 15)+'%20-%20'+rName+'/'+fullfilename;             
               
            isFileUploaded = connector.uploadFileToAWS(parentId, objectName,rName, this.rootPath, attach);                                  
            if(!isFileUploaded)
            {
                return false;
            }
            
            S3_FileStore__c fStore=new S3_FileStore__c();
            fStore.ContentSize__c = attach.ContentSize;                
            fStore.Description__c = attach.Description;
            fStore.FileExtension__c = attach.FileExtension.toLowerCase();
            fStore.FileName__c= filename + '.' + fileextension;
            fStore.ObjectId__c=parentId;  
           // fStore.S3ServerUrl__c=filepath;
               //Added long text Area : Shilpy
            fStore.S3Server_URL_New__c = filepath;
            fStore.Uploaded_date__c = Date.today();
            fStore.UploadedBy__c = userInfo.getUserId();
            this.fileStore.add(fStore);                     
        }
            catch(Exception ex)
            {
                system.debug('Error uploading file to AWS. Please check with System administrator');                                                           
                return false;
            }            
        }        
        return true;
    }

    public void insertfilestoreRecord (){
      try
      {
      insert this.fileStore;
       }catch(Exception e){
             system.debug('Exception Occured==>'+e.getMessage());       }

     }   
    
    public FileData GetDocumentUsingFileId(string recordId)        
    {
        system.debug('GetDocument '+ recordId);
        FileData filedata=new FileData();
        List <S3_FileStore__c> files =[Select S3Server_URL_New__c,FileExtension__c,FileName__c from S3_FileStore__c where Id=:recordId];
        if(files.size()>0){
        S3_FileStore__c file=files[0];
            System.debug('file='+file);
        ClaimConnect_AWSConnector connector=new ClaimConnect_AWSConnector(); 
        try
        { 
            fileData = connector.downloadFileFromAWS(this.rootpath,file.S3Server_URL_New__c,file.FileName__c,file.FileExtension__c);                                  
        }
        catch(Exception ex)
        {
            system.debug('Error downloading file to AWS');            
        } 
        }
        return filedata; 
  }
}