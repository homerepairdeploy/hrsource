/**
* Util class
* @author : Nikhil Jaitly
* @Created Date : 24/10/2018
* */
public with sharing class NJ_Utilities {
  /**
   * Get  pick list values of field from an object
   * @author : Nikhil Jaitly
   * @Param : obj - Object
   * @Param : fld - Field Name
   * @Created Date : 24/10/2018
   * */
   public static List<NJ_SelectOptionLightning> getPicklistValues(SObjectType sObjectType, String fld){
      List<NJ_SelectOptionLightning> options = new List<NJ_SelectOptionLightning>();
      Schema.DescribeSObjectResult objDescribe = sObjectType.getDescribe();
      Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
      List<Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPickListValues();
      for (Schema.PicklistEntry a : values) {
        options.add(new NJ_SelectOptionLightning(a.getLabel(), a.getValue()));
      }
      return options;
    }

    /**
   * Get  Logged in user info
   * @author : Nikhil Jaitly
   * @Param : usrId - Id of the user
   * @return : Instance of the user
   * @Created Date : 15/05/2019
   * */
   public static User getUserInfo(String usrId){
      User usr;
      if(usrId != null) {
        usr = [SELECT Id, Profile.Name
               FROM User
               WHERE Id = :usrId
               LIMIT 1];
      }
      return usr;
    }
    /**
   * Get settings from custom metadata
   * @author : Nikhil Jaitly
   * @Created Date : 19/07/2019
   * */
   public static Map<String, String> getSettings(){
     Map<String,String> settings = new Map<String,String>();
     for(Setting__mdt sett : [SELECT Id, DeveloperName, Value__c
                              FROM Setting__mdt]) {
        settings.put(sett.DeveloperName, sett.Value__c);
      }     
     return settings;
   }
   /**
   * This method is used to get Record Type  Id by Name
   * @author : Nikhil Jaitly
   * @Created Date : 21/07/2019
   * */
   public static String getRecordTypeId(SObjectType sObjectType, String recordTypeName) {
      //Generate a map of tokens for all the Record Types for the desired object
      Map<String,Schema.RecordTypeInfo> recordTypeInfo = sObjectType.getDescribe().getRecordTypeInfosByName();
      if(!recordTypeInfo.containsKey(recordTypeName))
          return null;
      //Retrieve the record type id by name
      return recordTypeInfo.get(recordTypeName).getRecordTypeId();
   }

}