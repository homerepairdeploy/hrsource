/**
* Helper class for service appointment trigger
* @Author : Nikhil Jaitly
* @Createddate : 20/11/2018
*/
public without sharing class ServiceAppointmentHelper {
    private static Set<String> allowedReason = new Set<String> { 'Assessment required', 'Skills', 'Unable to Warrant' , 'Other'};
        /**
* This method update work order based on certain status
* @param woIds - list of work order ids
*/
       // @future
        public static void updateWorkOrder(Set<Id> woIds) {
            
            // Update custom setting BypassWOValidation__c.runValidations__c to false to bypass the status validation on WO
         /*    BypassWOValidation__c woValidation = BypassWOValidation__c.getOrgDefaults();
            woValidation.runValidations__c = false;
            update woValidation; */
            
            
            System.debug('Nikhil woIds ' + woIds);
            Map<Id, List<ServiceAppointment>> mapWoSA = new Map<Id, List<ServiceAppointment>>();
            List<ServiceAppointment> tempSA;
            for (ServiceAppointment sa : [SELECT Id, ParentRecordId, Service_Resource__r.RelatedRecordId,
                                          Service_Resource__r.RelatedRecord.Contact.AccountId, Tier_2_Trade_Account__c,
                                          Service_Resource__c, Status, Service_Resource__r.ResourceType, Service_Resource__r.IsCapacityBased, Service_Resource__r.ServiceCrew.Service_Crew_Company__r.Id,
                                          Cannot_Complete_Reason__c,ActualStartTime
                                          FROM ServiceAppointment
                                          WHERE ParentRecordId IN :woIds]) {
                                              if (!mapWoSA.containsKey(sa.ParentRecordId)) {
                                                  mapWoSA.put(sa.ParentRecordId, new List<ServiceAppointment>());
                                              }
                                              tempSA = mapWoSA.get(sa.ParentRecordId);
                                              tempSA.add(sa);
                                              mapWoSA.put(sa.ParentRecordId, tempSA);
                                          }
            Boolean updateAwaitConfirmation, updateConfirmed, updateCompleted, updateNew;
            Map<Id, WorkOrder> workOrderUpdate = new Map<Id, WorkOrder>();
            String accId;
            DateTime ActualStartTime;
            System.debug('Nikhil mapWoSA ' + mapWoSA);
            for (Id wo : mapWoSA.keySet()) {
                tempSA = mapWoSA.get(wo);
                updateAwaitConfirmation = updateConfirmed = updateCompleted = updateNew = true;
                accId = null;
                String servCrewCompanyName = null;
                for (ServiceAppointment sa : tempSA) {
                    
                    if (sa.Service_Resource__c != null && sa.Service_Resource__r.ResourceType == 'T') {
                        accId = sa.Service_Resource__r.RelatedRecord.Contact.AccountId;
                    } else if (sa.Service_Resource__c != null && sa.Service_Resource__r.ResourceType == 'C') {
                        accID = sa.Service_Resource__r.ServiceCrew.Service_Crew_Company__r.Id;
                    } else if (sa.Tier_2_Trade_Account__c != null) {
                        accId = sa.Tier_2_Trade_Account__c;
                    } else {
                        accId = null;
                    }
                    
                    if (sa.ActualStartTime != null){
                        ActualStartTime = sa.ActualStartTime;
                    }
                    
                    if (sa.Status != NJ_Constants.SA_STATUS_AWAITINGCONFIRMATION) {
                        updateAwaitConfirmation = false;
                    }
                    if (sa.Status != NJ_Constants.SA_STATUS_NEW) {
                        updateNew = false;
                    }
                    if (sa.Status != NJ_Constants.SA_STATUS_CONFIRMED || sa.Service_Resource__c == null || sa.Service_Resource__r.IsCapacityBased) {
                        updateConfirmed = false;
                    }
                    if (updateCompleted && (sa.Status == NJ_Constants.SA_STATUS_COMPLETED || (sa.Status == NJ_Constants.SA_STATUS_CANNOTCOMPLETE && allowedReason.contains(sa.Cannot_Complete_Reason__c) ))) {
                        updateCompleted = true;
                    }else{
                        updateCompleted = false;
                    }
                    
                    System.debug('Nik accId ' + accId);
                    System.debug('UpdateAwaitConf Flag ' + updateAwaitConfirmation);
                    System.debug('UpdateAwaitConf Flag ' + updateAwaitConfirmation);
                    System.debug('updateConfirmed Flag ' + updateConfirmed);
                    System.debug('updateCompleted Flag ' + updateCompleted);
                    
                    
                    if (updateAwaitConfirmation) {
                        workOrderUpdate.put(wo, new WorkOrder(Id = wo, Status = NJ_Constants.WO_STATUS_ASSIGNED, Service_Resource_Company__c = accId, Rejection_Reason__c = null));
                    }
                    if (updateConfirmed) {
                        workOrderUpdate.put(wo, new WorkOrder(Id = wo, Status = NJ_Constants.WO_STATUS_ACCEPTED, Service_Resource_Company__c = accId, Rejection_Reason__c = null));
                    }
                    if (updateCompleted) {
                        workOrderUpdate.put(wo, new WorkOrder(Id = wo,StartDate=ActualStartTime,Status = NJ_Constants.WO_STATUS_JOBCOMPLETE));
                    }
                    if (updateNew) {
                        workOrderUpdate.put(wo, new WorkOrder(Id = wo, Status = NJ_Constants.WO_STATUS_NEW, Service_Resource__c = null, Service_Resource_Company__c = null, Trade_User__c = null, Capacity_Based__c = false));
                    }
                }
            }
            
            System.debug('workOrderUpdate.values()' + workOrderUpdate.values());
            
            if (!workOrderUpdate.isEmpty()) {
                update workOrderUpdate.values();
            }
            
            // Update custom setting BypassWOValidation__c.runValidations__c to true after you have updated the status of the WO
          /*  BypassWOValidation__c woValidation1 = BypassWOValidation__c.getOrgDefaults();
            woValidation1.runValidations__c = true;
            update woValidation1;  */
            
        }
    /**
* This method is used to update WO status to 'Work Commenced' when Actual Start Date is changed on first SA of WO
* @param newList list of SA
* @param oldMap map of SA
* @Author : Nikhil
* @CreatedDate : 01/03/2019
*/
    /*
    public static void updateWOStatusToWorkCommencedForFirstSA(List<ServiceAppointment> newList, Map<Id, ServiceAppointment> oldMap) {
        Map<Id, List<ServiceAppointment>> mapWOIdToSAList = new Map<Id, List<ServiceAppointment>>();
        List<WorkOrder> lstWOToUpdate = new List<WorkOrder>();
        Set<Id> setSAIds = new Set<Id>();
        
        // Update custom setting BypassWOValidation__c.runValidations__c to false to bypass the status validation on WO
        BypassWOValidation__c woValidation = BypassWOValidation__c.getOrgDefaults();
        woValidation.runValidations__c = false;
        update woValidation;
        
        for (ServiceAppointment sa : newList) {
            if (sa.ActualStartTime != oldMap.get(sa.Id).ActualStartTime
                && !NJ_Constants.ASSREASSSET.contains(sa.Work_Type_Name__c)) {
                if (!mapWOIdToSAList.containsKey(sa.ParentRecordId)) {
                    mapWOIdToSAList.put(sa.ParentRecordId, new List<ServiceAppointment>());
                }
                setSAIds.add(sa.Id);
            }
        }
        
        
        for (ServiceAppointment sa : [SELECT Id, ParentRecordId, ActualStartTime FROM ServiceAppointment
                                      WHERE ParentRecordId IN : mapWOIdToSAList.keySet() AND Id NOT IN : setSAIds]) {
                                          mapWOIdToSAList.get(sa.ParentRecordId).add(sa);
                                      }
        
        for (Id woId : mapWOIdToSAList.keySet()) {
            Boolean isFirstSA = true;
            for (ServiceAppointment sa : mapWOIdToSAList.get(woId)) {
                if (sa.ActualStartTime != null) {
                    isFirstSA = false;
                    break;
                }
            }
            if (isFirstSA) {
                lstWOToUpdate.add(new WorkOrder(Id = woId, Status = NJ_Constants.WO_STATUS_WORKCOMMENCED));
            }
        }
        if (!lstWOToUpdate.isEmpty()) {
            update lstWOToUpdate;
        }
        
        // Update custom setting BypassWOValidation__c.runValidations__c to true after you have updated the status of the WO
        BypassWOValidation__c woValidation1 = BypassWOValidation__c.getOrgDefaults();
        woValidation1.runValidations__c = true;
        update woValidation1;
    }*/
    /**
     * This method is used to update the SA status to Confirmed and WO status to 'Accepted WO' when SR is set on SA for MS and HA WOs
     * @param newList list of SA
     * @param oldMap map of SA
     * @Author : Nikhil
     * @CreatedDate : 29 May 2019
     */
    public static void updateSAStatusWhenSAScheduled(List<ServiceAppointment> newList, Map<Id, ServiceAppointment> oldMap) {
        System.debug('updateSAStatusWhenSAScheduled >>> ');
        List<ServiceAppointment> lstSAToUpdate = new List<ServiceAppointment>();
        Map<Id,WorkOrder> mapIdToWO = new  Map<Id,WorkOrder>();

        // Update custom setting BypassWOValidation__c.runValidations__c to false to bypass the status validation on WO
        BypassWOValidation__c woValidation = BypassWOValidation__c.getOrgDefaults();
        woValidation.runValidations__c = false;
        update woValidation;

        for(ServiceAppointment sa : newList){
            if(sa.Service_Resource__c != oldMap.get(sa.Id).Service_Resource__c && sa.Service_Resource__c != null){
                if(sa.Work_Type_Name__c.startsWith('MS -') || sa.Work_Type_Name__c.startsWith('HA -')){
                    
                    lstSAToUpdate.add(new ServiceAppointment(Id = sa.Id, Status = 'Confirmed'));
                    mapIdToWO.put(sa.Work_Order__c, new WorkOrder(Id = sa.Work_Order__c, status = 'Accepted WO'));
                }
            }
        }
        System.debug('lstSAToUpdate >>> '+lstSAToUpdate.size());
        System.debug('mapIdToWO.values >>> '+mapIdToWO.values().size());
        if(!lstSAToUpdate.isEmpty()){
            update lstSAToUpdate;
        }
        
        List<ServiceAppointment> lstSA1 = [SELECT Id,Status FROM ServiceAppointment WHERE Id IN : lstSAToUpdate];
        System.debug('Updated SA1 >>> '+lstSA1);
        
        if(!mapIdToWO.values().isEmpty()){
            update mapIdToWO.values();
        }
        
        List<ServiceAppointment> lstSA2 = [SELECT Id,Status FROM ServiceAppointment WHERE Id IN : lstSAToUpdate];
        System.debug('Updated SA2 >>> '+lstSA2);

        // Update custom setting BypassWOValidation__c.runValidations__c to true after you have updated the status of the WO
        BypassWOValidation__c woValidation1 = BypassWOValidation__c.getOrgDefaults();
        woValidation1.runValidations__c = true;
        update woValidation1;
    }
}