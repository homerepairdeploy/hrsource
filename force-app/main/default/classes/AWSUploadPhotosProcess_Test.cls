/***************************************************************** 
Purpose: ScheduledTestclass for generating Assessment Report                                                       
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            HomeRepair    23/06/2020      Created      Home Repair Claim System  
*******************************************************************/
@isTest
public class AWSUploadPhotosProcess_Test {

    static testMethod void ScheduledClaimIngestionStaging_Test() { 
      
       try{
        Test.startTest();
        String CRON_EXP = '0 0 23 * * ?';
        AWSUploadPhotosProcess scheObj = new AWSUploadPhotosProcess();
        system.schedule('Test Scheduler Job', CRON_EXP, scheObj);
        Test.stopTest();
       }catch(exception e) {}

    }
}