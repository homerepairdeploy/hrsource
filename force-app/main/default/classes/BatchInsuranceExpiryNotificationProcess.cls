/***************************************************************** 
Purpose: Send Reminder Emails to Trades prior to 7 days work cover and public liability and create tasks under Trade Accounts
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            HRDeveloper          15/10/2019    Created      Home Repair Claim System  
*******************************************************************/

global class BatchInsuranceExpiryNotificationProcess implements Database.Batchable<sObject>, Database.Stateful,Schedulable{
    /***************************************************************** 
Purpose: Batch start method to fetch records from Account with 7 days due for either Work Cover/Public Liability 
Parameters:None
Returns: Query 
Throws [Exceptions]: None                                                          
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL   Description 
1.0            HRDeveloper        15/10/2019     Created  Home Repair Claim system 
*******************************************************************/
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
      String query = 'Select Id,Name,Public_Liability_Expiry__c,Work_Cover_Expiry__c,X7daysPriorWorkCoverExpiry__c,X7daysPriorPublicLiabilityExpiry__c,Job_Email_Address__c from Account where'  +
                    '(X7daysPriorPublicLiabilityExpiry__c = TODAY or X7daysPriorWorkCoverExpiry__c =TODAY)';
           
      System.debug('query '+query); 
      return Database.getQueryLocator(query);
    }
    
    /***************************************************************** 
Purpose: Batch execute method to process Insurances which are due for expiry
Parameters:List<Account>
Returns: none
Throws [Exceptions]: DML                                                          
*******************************************************************/   
    global void execute(Database.BatchableContext BC, List<Account> scope) {
        
        EmailTemplate PLInsuranceExpiryTemplate=[Select Name, Id, Body, HtmlValue,DeveloperName,  IsActive, Subject from EmailTemplate where DeveloperName='X7_days_from_Public_Liability_Expiry'];
        EmailTemplate WCInsuranceExpiryTemplate=[Select Name, Id, Body, HtmlValue,DeveloperName,  IsActive, Subject from EmailTemplate where DeveloperName='X7_days_from_Work_Cover_Expiry'];
        for(Account acc:scope){
           If (acc.X7daysPriorPublicLiabilityExpiry__c == Date.today()) createTaskSendEmail(PLInsuranceExpiryTemplate,acc,NJ_Constants.PL_INSURANCE_TYPE);
           If (acc.X7daysPriorWorkCoverExpiry__c == Date.today()) createTaskSendEmail(WCInsuranceExpiryTemplate,acc,NJ_Constants.WC_INSURANCE_TYPE);
        }      
       
    }

    
    public void createTaskSendEmail(EmailTemplate InsuranceExpiryTemplate, Account acc,string type){
     
      
      //Send Reminder PL/WC Insurance Email to Trade 
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	  for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) {
	  if(owa.Address.contains('trades')) mail.setOrgWideEmailAddressId(owa.id); }
      mail.setSubject( InsuranceExpiryTemplate.subject );
      mail.setPlainTextBody( InsuranceExpiryTemplate.body );
      mail.setToAddresses( new String[] { acc.Job_Email_Address__c } );
      Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );

      //create either PL or WC task
      User u=[select Id from user where Email=:System.label.ProcurementEmail limit 1];
      Task t=new Task(ActivityDate = Date.today()+1,Subject=InsuranceExpiryTemplate.subject, Priority='Important',Status='Not Started',WhatId=acc.Id,OwnerId = u.id);
      insert t;
        

    }
    
    global void execute(SchedulableContext sc) {
      BatchInsuranceExpiryNotificationProcess b = new BatchInsuranceExpiryNotificationProcess();  
      database.executebatch(b);
    } 
    
    global void finish(Database.BatchableContext BC) {  
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                              TotalJobItems, ApexClass.name, CreatedBy.Email, ExtendedStatus 
                              FROM AsyncApexJob WHERE Id = :bc.getJobId()];
     
     
     
    for(user uu : [select Id, Email from user where ID IN ( SELECT UserOrGroupId FROM GroupMember)])
     {
        
     // Send an email to the Apex job's submitter        //   notifying of job completion. 
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
       String[] toAddresses = new String[] {uu.Email};
       mail.setToAddresses(toAddresses);
       mail.setSenderDisplayName('Batch Execution Failure Notice');
       mail.setSubject('Batch: ' + a.ApexClass.name + ', Job Process ended with ' + a.NumberOfErrors  + ' Errors, Contact Support team');
        
       String Reason = ' Error Reason is --> ' ;
        Reason   =  Reason + a.ExtendedStatus;  
       if(a.ExtendedStatus == null )
       {
          Reason = ' ';
           String tt = ' This is for Test Coverage';
       }
        
    }
    }
    
    
}