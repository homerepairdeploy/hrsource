@isTest
private class ARInvoiceHandlerTest{
    @testSetup
    public static void testSetup(){
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        //changed by CRMIT 
        case au=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,au.id);        
        AR_Invoice__c invoice = HomeRepairTestDataFactory.createARInvoice(au.id,cs.id);

    }
    public static testMethod void approveInvoiceTest() {
        List<AR_Invoice__c>  ARInvoices = [Select Id, Authority__c FROM AR_Invoice__c];
        for(AR_Invoice__c invoice: ARInvoices) {
             invoice.status__c = 'Submitted';
            invoice.Invoice_type__c = 'Customer';
        }
     //   user u = HomeRepairTestDataFactory.createSystemAdminUser('testing','Arinvoices');
    //    System.runAs(u) {
        update ARInvoices;
       // }
        
        for(AR_Invoice__c invoice: ARInvoices) {
            invoice.status__c = NJ_Constants.ARINVOICE_STATUS_APPROVED;
        }
        system.debug(ARInvoices);
          Test.startTest();
       Task t = new task(Subject = NJ_CONSTANTS.TASK_COMPLETION_CALL_SUBJECT, whatId =  ARInvoices[0].Authority__C, Status = 'Not Started');
       insert t;
       system.debug(t.id);
          //System.runAs(u) {
            update ARInvoices;
        //  }
        Test.stopTest();
        ARInvoices = [Select Id, Authority__r.Status FROM AR_Invoice__c];
        system.debug('==>'+ARInvoices);
        for(AR_Invoice__c invoice: ARInvoices) {
            //system.assertEquals(invoice.Claim_Job__r.Status__c, NJ_Constants.CLAIM_JOB_STATUS_COMPLETED);
        }
    }
    public static testMethod void rejectInvoiceTest() {
        List<AR_Invoice__c>  ARInvoices = [Select Id FROM AR_Invoice__c];
        for(AR_Invoice__c invoice: ARInvoices) {
            invoice.Rejection_Reason__c = 'reason';
            invoice.status__c = NJ_Constants.ARINVOICE_STATUS_REJECTED;
        }
       //   user u = HomeRepairTestDataFactory.createSystemAdminUser('testing','Arinvoices');
        Test.startTest();
        // System.runAs(u) {
            update ARInvoices;
       //  }
        Test.stopTest();
        List<Task> tasks= [Select Id FROM Task];
        //system.assertEquals(tasks.size(), ARInvoices.size());
    } 
}