@isTest
public class DisplayActivitiesControllerTest {
    @TestSetup
    public static void setupTestData() {
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        case authorityCase = HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        Task t = new Task();
        t.Subject = 'test';
        t.priority = 'critical';
        t.ActivityDate = date.today();
        t.WhatId = authorityCase.id;
        insert t;
    }
    static testMethod void updateAPinvoice() {
        List<Task> task = [select id,subject,Description,ActivityDate,status,createddate,WhatId,Owner.Name from task];
        if(task.size()>0){
            DisplayActivitiesController.getActivities(task[0].WhatId);
            task[0].status = 'Completed';
            update task;
            DisplayActivitiesController.getActivities(task[0].WhatId);  
        }
        DisplayActivitiesController.getTaskPickLListValues();
        DisplayActivitiesController.updateTask(task[0],'New');
        DisplayActivitiesController.convertMonthTextToNumber(1);
        DisplayActivitiesController.convertMonthTextToNumber(2); 
        DisplayActivitiesController.convertMonthTextToNumber(3); 
        DisplayActivitiesController.convertMonthTextToNumber(4);
        DisplayActivitiesController.convertMonthTextToNumber(5);
        DisplayActivitiesController.convertMonthTextToNumber(6);
        DisplayActivitiesController.convertMonthTextToNumber(7);
        DisplayActivitiesController.convertMonthTextToNumber(8);
        DisplayActivitiesController.convertMonthTextToNumber(9);
        DisplayActivitiesController.convertMonthTextToNumber(10);
        DisplayActivitiesController.convertMonthTextToNumber(11);
        DisplayActivitiesController.convertMonthTextToNumber(12);
        
    }
}