/***************************************************************** 
Purpose:  Once off copy contact phones to claim phone fields and daily updates to claim 
History                                                             
--------                                                            
VERSION        AUTHOR          DATE           DETAIL       Description 
1.0            Pardha       17/01/2019     Created      Home Repair Claim System 
*******************************************************************/
global class AddExcludeResourceToWO implements Database.Batchable<sObject> {

     
    map<Id,ServiceResource> SRMap=new map<Id,ServiceResource>();
   
    public AddExcludeResourceToWO(map<Id,ServiceResource> SRMap){
         this.SRMap=SRMap;
    
    } 
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        String query='select Id from WorkOrder where status not in (\'closed\',\'Job complete\',\'cancelled\')';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<Workorder> WOlst) {
        map<Id,Id> WOSRMap=new map<Id,Id>();
        set<Id> WOIds=new set<Id>();
        for(Workorder wo:WOlst){
            WOIds.add(wo.Id);
        }
        for(ResourcePreference RP:[select Id,RelatedRecordId,ServiceResourceId from ResourcePreference where ServiceResourceId=:SRMap.keySet() and RelatedRecordId=:WOIds]){
           WOSRMap.put(RP.RelatedRecordId,RP.ServiceResourceId );
        }
        hrTradeScheduleSuppressProcessor.insertResourcePreference(WOlst,SRMap.values(),WOSRMap);
        
        
    }
    
    global void finish(Database.BatchableContext bc){ 
        
    }
    
}