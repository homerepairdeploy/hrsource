/********************************************************************************************** 
Name        : ClaimConnect_LightningFileUploadHandler
Description : Class used to retrieve and download photos from staging objects and from S3 Server
                                                            
VERSION        AUTHOR           DATE           DETAIL      DESCRIPTION   
1.0            Anketha,Vidhya   15-04-2020     Created     ClaimConnect APP
/***********************************************************************************************/
public class ClaimConnect_LightningFileUploadHandler {
    
    @AuraEnabled  
    public static List<ContentDocument> getFiles(string sObjectId){ 
        // TO avoid following exception 
        // System.QueryException: Implementation restriction: ContentDocumentLink requires
        // a filter by a single Id on ContentDocumentId or LinkedEntityId using the equals operator or 
        // multiple Id's using the IN operator.
        // We have to add sigle record id into set or list to make SOQL query call
        Set<Id> recordIds=new Set<Id>{sObjectId};
        Set<Id> documentIds = new Set<Id>(); 
        List<ContentDocumentLink> cdl=[SELECT id,LinkedEntityId,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN :recordIds];  
        for(ContentDocumentLink cdLink:cdl){  
            documentIds.add(cdLink.ContentDocumentId);  
        }      
        return [SELECT Id,Title,FileType,CreatedBy.Name,ContentSize FROM ContentDocument WHERE id IN: documentIds];  
    } 
    
    @AuraEnabled  
    public static void deleteFiles(string sdocumentId){ 
        delete [SELECT Id,Title,FileType from ContentDocument WHERE id=:sdocumentId];       
    }
    
      Public class fileWrapper{
        @AuraEnabled
        Public List<Id> SFfileIds;
        @AuraEnabled
        Public Boolean status;        
    }
  
    
    @AuraEnabled
    public static fileWrapper getAWSS3Files(string sObjectId)
     {    
         system.debug('getAWSS3Files' + sObjectId);          
         
         fileWrapper fwrapper = new fileWrapper();
       //  List<Id> SFfileIds = new List<Id>();         
         List<S3_FileStore__c> S3files = new List<S3_FileStore__c>(); 
         
        // get AWS files for Case 
        Set<Id> entityIds = new Set<Id>();
        entityIds.add(sObjectId);            
       system.debug('<<<entity'+entityIds);
        List<S3_FileStore__c> cfiles =[Select Id,S3ServerUrl__c,FileExtension__c,FileName__c,ContentSize__c,UploadedBy__r.Name from S3_FileStore__c where ObjectId__c IN :entityIds];                 
        
        S3files.addAll(cfiles);
        List <Room__c> roomList = [Select Id from Room__c where Claim__c=:sObjectId];
         system.debug('<<<sobe'+sObjectId);
         system.debug('<<<room'+roomList.size());
           for(Room__c room:roomList)
        {
            Set<Id> rentityIds = new Set<Id>();
            rentityIds.add(room.Id);            
            
            List <S3_FileStore__c> rfiles =[Select Id,S3ServerUrl__c,FileExtension__c,FileName__c,ContentSize__c,UploadedBy__r.Name from S3_FileStore__c where ObjectId__c IN :rentityIds];          
            S3files.addAll(rfiles); 
        }         
         List<ContentVersion> contentVer = new  List<ContentVersion>();
         for (S3_FileStore__c sfile:S3files)
         {
             system.debug('<<<sfile'+sfile.Id);
              FileData fdata = downloadfile(sfile.Id);
              
             // error downloading file, return status as false
              if (fdata.status == false)
              {
                  fwrapper.status = false;
                  return fwrapper;
              }
             
              String contentData= fdata.content;        
             
              ContentVersion conVer = new ContentVersion();
              conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
              conVer.PathOnClient = sfile.FileName__c; // The files name, extension is very important here which will help the file in preview.
              conVer.Title = sfile.FileName__c; // Display name of the files                     
              conVer.VersionData = EncodingUtil.Base64Decode(contentData);
              conVer.Description = 'Temporary Downloaded from AWS'; //this desc is very important here which will be used to delete temp documents created
              contentVer.add(conVer);
         }
         insert(contentVer);
         
         List<Id> SFIds = new List<Id>();         
         for (ContentVersion cv:contentVer)
         {
             Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
             SFIds.add(conDoc);
         }
         fwrapper.SFfileIds = sfIds;
         return fwrapper;
     }      
        
       @AuraEnabled
       public static FileData downloadfile(string fileId)
    {
        system.debug('<<<file'+fileid);
        List<AWS_Credential__mdt> lstAws=[Select Bucket_Name__c ,Host_Name__c,Root_Path__c  from AWS_Credential__mdt];
        String bucket=lstAws.get(0).Bucket_Name__c;
        String host=lstAws.get(0).Host_Name__c;
        String root=lstAws.get(0).Root_Path__c;
        String method='GET';
        ClaimConnect_AWSService service=new ClaimConnect_AWSService(bucket,method,host,root);
       	FileData data=service.GetDocumentUsingFileId(fileId);
        String contentData= data.content;
        return data; 
    }  
 }