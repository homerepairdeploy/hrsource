/************************************************************************************************************
Name: homeRepairTradeEmailCaptureWrapper
=============================================================================================================
Purpose: Apex Class to capture emails send to trades under activity history
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL        DESCRIPTION
1.0        xxx               xx             Created      HomeRepair
*************************************************************************************************************/
public class hrTradeEmailCaptureDataWrapper {
    @InvocableVariable(required=true)
    public String workorderId;
    @InvocableVariable(required=true)
    public String emailTemplate;
   
}