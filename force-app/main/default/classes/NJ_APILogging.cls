/**
 * Description: Utility Class for Logging. This class will be used for Logging the API calls in Salesforce
 * @CreatedDate : 06/11/2018
 * @CreatedBy : Nikhil Jaitly
 */

public inherited sharing class NJ_APILogging {    
    
    /**
     * Get current logging settings - if not present default to logging
     * Errors only.
     * 
     * @date    2019-07-02
     * @author  
     * @return  Logging_Settings__c - Logging custom setting
     */
    private static API_Logging_Settings__c loggingSettings {
        get {
            if (loggingSettings == null) {
                loggingSettings = API_Logging_Settings__c.getInstance();
                
                if (loggingSettings.Name == null) {
                    loggingSettings = new API_Logging_Settings__c(
                        Enable_Debug_Log_Messages__c = true
                    );
                }
            }
            return loggingSettings;
        }
        set;
    }
    
    /**
     * Log message to debug log from Detail wrapper
     * 
     * @date    2019-07-02
     * @author  
     * @param   LogDetail object including all the logging details
     * @return  void
     */
    public static void log(LogDetail detail) {        
        
        if (detail!=null && detail.methodName!=null && loggingSettings.Enable_Debug_Log_Messages__c) {
            Mobile_App_Logging__c log = new Mobile_App_Logging__c(
            
                API_VERB__c                = detail.apiVerb,
                Claim__c                   = detail.claim,
                Incoming_JSON__c           = detail.incomingJSON,
                API_Method_Name__c         = detail.methodName,
                Assessor__c                = detail.assessorId,
                Outgoing_JSON__c           = detail.outgoingJSON,
                Mobile_App_Version__c      = detail.mobileAppVersion,
                Request_Finish_Time__c     = System.now(),
                Request_Start_Time__c      = detail.requestStartTime,
                Response_Code__c           = detail.responseCode,
                Response_Error__c          = detail.responseError,
                ServiceAppointment__c      = detail.assessmentAppointment
				
            );                
            insert log;
        }
        
    }

    /**
     * Wrapper class to hold all the logging details. These details are finally saved
     * into a custom object.
     * @date    2019-07-02
     * @author  
     */
    public class LogDetail{
        public String apiVerb {get;set;}
        public String claim {get;set;}
        public String incomingJSON {get;set;}
        public String methodName {get;set;}
        public String outgoingJSON {get;set;}
        public String mobileAppVersion {get;set;}
        public Datetime requestStartTime {get;set;}
        public String responseCode {get;set;}
        public String assessorId {get;set;}
        public String responseError {get;set;}
        public Id assessmentAppointment {get;set;}
        public LogDetail(String methodName, RestRequest req){
            this.methodName = methodName;
            this.requestStartTime = System.now();

            if(req!=null){
                if(req.requestBody==null || req.requestBody.toString() == null || req.requestBody.toString().trim() == ''){
                    this.incomingJSON = JSON.serialize(req);
                }
                else{
                    String JSONContent = req.requestBody.toString();
                    try {
                        Map<String, Object> objMap = (Map<String, Object>) JSON.deserializeUntyped(JSONContent);
                        objMap.put('requestBody', JSONContent);
                        //String jsonString = JSON.serialize(objMap);
                        //this.incomingJSON = jsonString.replaceAll('\\"','"');
                        this.incomingJSON = JSON.serialize(objMap);
                    } catch (Exception ex) {
                        System.debug('\n\nError setting Incoming JSON');
                    }
                }
            }
        }
    }
}