public class hrARFeeInvoiceCreationService {
    @AuraEnabled
    public static string createInvoiceRecord(string recordId,string buttonValue) {
        
        Boolean isValidAuth=false;
        case childCase=[select Id,ParentId,Job_Type__c, submitted_date__c ,verified_date__c ,handback_date__c,Parent.Policy__r.State__c,Is_Event__c from case where Id=:recordId ];
        list<AR_Invoice__c> arlst=[select Id from AR_Invoice__c where Invoice_Type__c='Assessment Fee' and Status__c!='Rejected' and Authority__c=:childCase.Id];

        If (childCase.Job_Type__c == 'assessment' || childCase.Job_Type__c == 'Quote' || childCase.Job_Type__c == 'report' ) isValidAuth=true;

        system.debug('buttonValue' +buttonValue);
        switch on buttonValue {
            when 'proceed' {
                //update verified date on Authority
                If (isValidAuth && childCase.verified_date__c==null && childCase.submitted_date__c!=null) {
                    childCase.verified_date__c=system.now();
                    update childCase;
                    return 'success';
                }    
                
            }
            when 'submit' {
                
                If ((childCase.Job_Type__c == 'assessment' || childCase.Job_Type__c == 'Quote' || childCase.Job_Type__c == 'report') && arlst.size() == 0 )  createARFeeInvoice(childCase);
                system.debug('job type' +childCase.Job_Type__c);
                //update verified date on Authority
                If (isValidAuth && childCase.submitted_date__c==null){
                    childCase.submitted_date__c=system.now();
                    update childCase;
                    return 'success';
                }    
                           
            }
            when 'handback' {
                If (childCase.Job_Type__c == 'doAndCharge' &&  arlst.size() ==0)  createARFeeInvoice(childCase);
                //update verified date on Authority
                If ((isValidAuth && childCase.handback_date__c==null && childCase.submitted_date__c!=null) || (childCase.Job_Type__c == 'doAndCharge' && childCase.handback_date__c==null)) {
                    childCase.handback_date__c=system.now();
                    update childCase;
                    return 'success';
                }
                           
            }
        }
        
        return 'success';

        
    }
    
    public static void createARFeeInvoice(Case childCase){
        system.debug('childCase '+childCase);
        map<String,Integer> costCenterCodes=Taskservice.getAllCostCentres();
        map<String,Integer> tradeCodes=Taskservice.getAllTradeCodes();
               
        // Insert ARFee Invoice
        AR_Invoice__c theInvoice = new AR_Invoice__c();
        theInvoice.Authority__c = childCase.Id;
        theInvoice.Claim__c = childCase.ParentId;
        theInvoice.Status__c = 'Submitted';
        theInvoice.Invoice_Type__c = 'Assessment Fee';
        theInvoice.Oracle_Process_Status__c = 'Pending';
        theInvoice.ClaimHub_Upload_Status__c = 'Pending'; // ** JP Modified - Required to push new AR to ClaimsHub
        theInvoice.Generate_Invoice_Statement__c = true;
        theInvoice.Distribution_Set_Total_ex_GST__c = ('11.' + costCenterCodes.get(childCase.parent.Policy__r.State__c) + '.41100.' + tradeCodes.get(theInvoice.Invoice_Type__c) +'.'+ (childCase.Is_Event__c?'302':'301'));
        theInvoice.Distribution_Set_GST__c = ('11.' + '900' + '.23100.' +'000.' + '000');
        insert theInvoice;  
    }   
    
     
}