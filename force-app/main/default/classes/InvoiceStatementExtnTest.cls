@isTest
public class InvoiceStatementExtnTest {
    @isTest
    Static void invoiceStatementExtnTestMethod(){
        //Create the Account Record.
        Account acc = HomeRepairTestDataFactory.createAccounts('HomeRepair.Net.Au Pty Ltd')[0];        
        //Create the Contact Record.
        contact conVar = HomeRepairTestDataFactory.createTradeContact('test WO Contatct',acc.Id);   
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);                
        Case childCaseObj = HomeRepairTestDataFactory.createChildClaimAuthority(cs.Id);
		AR_Invoice__c ar = HomeRepairTestDataFactory.createARInvoice(childCaseObj.id,cs.id);
        AR_SOW__c arSOW = new AR_SOW__c();
        arSOW.AR_Invoice__c = ar.id;
        arSOW.Additional_Comments__c = 'yes';
        insert arSOW;
        InvoiceStatementExtn is = new InvoiceStatementExtn();
        is.invoice = ar;
        is.AddlCommts = 'No';
        is.acc = acc;
        PageReference pageRef = Page.ARCMFeesInvoiceStatement;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(ar.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(ar);
        InvoiceStatementExtn test = new InvoiceStatementExtn(sc);
        // is.AddlCommts;
    }
}