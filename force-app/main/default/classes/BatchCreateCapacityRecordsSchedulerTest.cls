/*==========================================================================================
Type:       Test class
Purpose:    Test cases for BatchCreateCapacityRecords and BatchCreateCapacityRecordsScheduler
============================================================================================*/
@isTest
private class BatchCreateCapacityRecordsSchedulerTest{
    @TestSetup
    static void TestdataCreateMethod() {
        
        Account acc = HomeRepairTestDataFactory.createAccounts('Test Trade Account')[0];
        contact con = HomeRepairTestDataFactory.createTradeContact('TestTrade Contatct',acc.Id);
        User user = HomeRepairTestDataFactory.createTradeUser(con.Id);
        ServiceResource sr = HomeRepairTestDataFactory.createNewServiceResource(user.Id);
        
    }
    static @isTest void homeRepPersonDetailAURAAllTest() {
        Account acc = [Select Id from Account limit 1];
        acc.Public_Liability_Expiry__c = Date.Today().addMonths(4);
        update acc;
        ServiceResource sr = [SELECT Id FROM ServiceResource LIMIT 1];
        sr.Default_Work_Items_Capacity__c=1000;
        sr.IsCapacityBased=true;
        sr.AccountId=acc.Id;
        update sr;
        //create a new date, which is the last date of the month
        Date endMonthDate = Date.newInstance(System.today().Year(), System.today().Month(), Date.daysInMonth(System.today().year(), System.today().month()));
        
        ServiceResourceCapacity newSRC = new ServiceResourceCapacity
                        (
                            StartDate            = System.today().toStartOfMonth(),
                            EndDate              = endMonthDate,
                            TimePeriod           = 'Month',
                            ServiceResourceId    = sr.Id,
                            CapacityInHours      = 1000,
                            CapacityInWorkItems  = (Integer) sr.Default_Work_Items_Capacity__c
                            
                        );
        insert newSRC;            
        
        //RUN THE Scheduler Class
        String CRON_EXP = '0 0 23 * * ?';
        BatchCreateCapacityRecordsScheduler scheObj = new BatchCreateCapacityRecordsScheduler();
        system.schedule('Test Scheduler Job', CRON_EXP, scheObj);
        
        //run the batch class
        Id z = Database.ExecuteBatch(new BatchCreateCapacityRecords(), 200);

    }
}