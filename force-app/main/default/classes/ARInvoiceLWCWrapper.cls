public class ARInvoiceLWCWrapper {

    
    @AuraEnabled
    public String recordId {get; set;}
    @AuraEnabled
    public String creditMemoDetails {get; set;}
    @AuraEnabled
    public Decimal labourAmtExGST {get; set;}
    @AuraEnabled
    public Decimal materialAmtExGST {get; set;}
    @AuraEnabled
    public Boolean partialAmount {get;set;}
        
    

}