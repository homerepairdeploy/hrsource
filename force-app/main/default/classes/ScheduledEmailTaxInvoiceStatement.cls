/**
 * Created by dilshanegodawela on 22/9/20.
 */

global with sharing class ScheduledEmailTaxInvoiceStatement implements Schedulable, Database.AllowsCallouts{
	global void execute(SchedulableContext sc) {

		BatchEmailTaxInvoiceStatement batch = new BatchEmailTaxInvoiceStatement();
		Database.executeBatch(batch, 10);
	}
}