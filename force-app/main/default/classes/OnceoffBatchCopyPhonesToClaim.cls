/***************************************************************** 
Purpose:  Once off copy contact phones to claim phone fields and daily updates to claim 
History                                                             
--------                                                            
VERSION        AUTHOR          DATE           DETAIL       Description 
1.0            Pardha       23/Dec/2019      Created      Home Repair Claim System 
*******************************************************************/
global class OnceoffBatchCopyPhonesToClaim implements Database.Batchable<sObject> {
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        String query='select id,contact.Phone,contact.HomePhone, contact.MobilePhone,Home_Phone__c,Customer_Mobile_Phone__c,Customer_Contact_Phone__c from case';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<case> scope) {
    
        list<case> updtcaselst=new list<case>();
        for(case c:scope){
             case updtcase=new case();
             updtcase.id=c.id;
             if (c.contact.HomePhone!=null && c.contact.HomePhone!=c.Home_Phone__c) updtcase.Home_Phone__c=c.contact.homephone;
             if (c.contact.MobilePhone!=null && c.contact.MobilePhone!=c.Customer_Mobile_Phone__c) updtcase.Customer_Mobile_Phone__c=c.contact.mobilephone;
             if (c.contact.Phone!=null && c.contact.Phone!=c.Customer_Contact_Phone__c ) updtcase.Customer_Contact_Phone__c=c.contact.phone;
             updtcaselst.add(updtcase);
            
        }
        update updtcaselst;
        
        
    }
    
    global void finish(Database.BatchableContext bc){ 
        
    }
    
}