/***************************************************************** 
Purpose:  Once off copy contact phones to claim phone fields and daily updates to claim 
History                                                             
--------                                                            
VERSION        AUTHOR          DATE           DETAIL       Description 
1.0            Pardha       17/01/2019     Created      Home Repair Claim System 
*******************************************************************/
global class ReleaseExcludeResourceFromWO implements Database.Batchable<sObject> {

     
    map<Id,ServiceResource> SRMap=new map<Id,ServiceResource>();
   
    public ReleaseExcludeResourceFromWO(map<Id,ServiceResource> SRMap){
         this.SRMap=SRMap;
    
    } 
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        set<Id> SRIds=SRMap.keySet();
        String query='select Id from ResourcePreference where ServiceResourceId in :SRIds';
		return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<ResourcePreference> RPlst) {
        list<ResourcePreference> DelRPlst=new list<ResourcePreference>(); 
        for(ResourcePreference RP:RPlst){
            DelRPlst.add(RP);  
        }
        
        If (!DelRPlst.isEmpty()) delete DelRPlst;
        
        
    }
    
    global void finish(Database.BatchableContext bc){ 
        
    }
    
}