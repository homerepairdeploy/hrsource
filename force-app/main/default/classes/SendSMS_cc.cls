public with sharing class SendSMS_cc {
    @auraEnabled
    public static string sendSMSToClaim(String phoneNumber, String smsBody, string ContactId, string caseId) {
        try {
            SMSUtility.ResponseWrapper res = SMSUtility.processSms(smsBody, phoneNumber);
            String response =  JSON.serialize(res); 
            //Mobile number should not be updated hence commented   
            /*if(ContactId != null && res.isSuccess) {                 
                Contact con = new Contact(Id = ContactId, MobilePhone = phoneNumber);
                update con;
                ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), caseId, ConnectApi.FeedElementType.FeedItem, 'Text Message Sent: '+smsBody);
            }*/
            if(CaseId != null && res.isSuccess) {                 
               ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), caseId, ConnectApi.FeedElementType.FeedItem, 'Text Message Sent: '+smsBody);
            }
            return response;
        }
        catch (Exception e) {
            
            system.debug('Exception Occured==>'+e.getMessage()+' :: StackTrace==>'+e.getStackTraceString());
            List<String> errorMessageList = new List<String>();
            //errorMessageList.add(e.getDmlMessage(0));            
            String errorMessage = e.getMessage();
            return JSON.serialize(new SMSUtility.ResponseWrapper (false, errorMessage, null ));            
        }
    } 
    @auraEnabled
    public static Case getClaimDetails(string caseId) {
        Case claim = [Select Id, ContactId, ContactMobile, Primary_Contact_Number__c, ContactPhone, SuppliedPhone, Contact_Phone__c FROM Case Where Id =: caseId];
        return claim;
    }
}