/* =====================================================================================
Type:       Test class
Purpose:    Test cases for RESTCongaComposerAPI
========================================================================================*/
@isTest
private class RESTCongaComposerAssessmentReportAPITest{
    @testSetup 
    static void setup(){
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        HomeRepairTestDataFactory.addContentVersionToParent(cs.id);
        HomeRepairTestDataFactory.generalSettings('Environment','Other');
        //Create Work Order Record
        workOrder workOrd = FSL_TestDataFactory.HRTradeWorkOrderWithCase(cs.Id);
        //Create Service Appointment Record
        ServiceAppointment serAppt = FSL_TestDataFactory.createNewServiceAppointment(workOrd.Id, 'Tentative', cs.Id);

    }   
    static testMethod void RESTCongaComposerAssessmentReportAPIGetTest() {
        Case cs = [SELECT ID FROM Case LIMIT 1];
        ServiceAppointment sa = [SELECT ID FROM ServiceAppointment LIMIT 1];
        RestRequest req = new RestRequest(); 
        req.params.put('claimId', cs.id);
        req.params.put('apptId', sa.id);
        req.params.put('isGenerateReport', 'true');
        req.params.put('isNCReport','true');
        RestResponse res = new RestResponse();          
        req.requestURI = '/homerepair/api/v1/congaassessmentreport';  
        req.httpMethod = 'Get';
        RestContext.request = req;
        RestContext.response = res;        
        Test.startTest();
        RESTCongaAttachmentResponseHandler results = RESTCongaComposerAssessmentReportAPI.GET();
        Test.stopTest();       
        
    }
    static testMethod void RESTCongaComposerAssessmentReportSystemtestGetTest() {
        HomeRepairTestDataFactory.generalSettings('Environment','systemtest');
        Case cs = [SELECT ID FROM Case LIMIT 1];
        ServiceAppointment sa = [SELECT ID FROM ServiceAppointment LIMIT 1];
        RestRequest req = new RestRequest(); 
        req.params.put('claimId', cs.id);
        req.params.put('apptId', sa.id);
        req.params.put('isGenerateReport', 'true');
        req.params.put('isNCReport','true');
        RestResponse res = new RestResponse();          
        req.requestURI = '/homerepair/api/v1/congaassessmentreport';  
        req.httpMethod = 'Get';
        RestContext.request = req;
        RestContext.response = res;        
        Test.startTest();
        RESTCongaAttachmentResponseHandler results = RESTCongaComposerAssessmentReportAPI.GET();
        Test.stopTest();       
        
    }
    static testMethod void RESTCongaComposerAssessmentReportAPIGetDevTestTest() {        
        HomeRepairTestDataFactory.generalSettings('Environment','hrdev');
        Case cs = [SELECT ID FROM Case LIMIT 1];
        ServiceAppointment sa = [SELECT ID FROM ServiceAppointment LIMIT 1];
        RestRequest req = new RestRequest(); 
        req.params.put('claimId', cs.id);
        req.params.put('apptId', sa.id);
        req.params.put('isGenerateReport', 'true');
        req.params.put('isNCReport','true');
        RestResponse res = new RestResponse();
        req.requestURI = '/homerepair/api/v1/congaassessmentreport';  
        req.httpMethod = 'Get';
        RestContext.request = req;
        RestContext.response = res;        
        Test.startTest();
        RESTCongaAttachmentResponseHandler results = RESTCongaComposerAssessmentReportAPI.GET();
        Test.stopTest();
    }
    static testMethod void RESTCongaComposerAssessmentReportAPIGetUATTest() {
        HomeRepairTestDataFactory.generalSettings('Environment','hruat');
        Case cs = [SELECT ID FROM Case LIMIT 1];
        ServiceAppointment sa = [SELECT ID FROM ServiceAppointment LIMIT 1];
        RestRequest req = new RestRequest(); 
        req.params.put('claimId', cs.id);
        req.params.put('apptId', sa.id);
        req.params.put('isGenerateReport', 'true');
        req.params.put('isNCReport','true');
        RestResponse res = new RestResponse();          
        req.requestURI = '/homerepair/api/v1/congaassessmentreport';  
        req.httpMethod = 'Get';
        RestContext.request = req;
        RestContext.response = res;        
        Test.startTest();
        RESTCongaAttachmentResponseHandler results = RESTCongaComposerAssessmentReportAPI.GET();
        Test.stopTest();
    }
    
}