@isTest
private class OnceoffGenerateARInvoicePDFTest{
    @testSetup
    public static void testSetup(){
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
		Home_Repairs_Trigger_Switch__c hrts = HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
         case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id);        
        AR_Invoice__c invoice = HomeRepairTestDataFactory.createARInvoiceCMFee(ca.id,cs.id);
    }
    static testMethod void ARCMFeesInvoiceStatementTest() {
        AR_Invoice__c arinvc=[select id,name from AR_Invoice__c limit 1];
        
        HRInvoiceTemp__c HRInvctemp=new HRInvoiceTemp__c();
        HRInvctemp.name=arinvc.name;
        insert HRInvctemp;
                
        Test.startTest(); 
                database.executeBatch(new OnceoffGenerateARInvoicePDF(),1);
               
        Test.stopTest();        
    }
}