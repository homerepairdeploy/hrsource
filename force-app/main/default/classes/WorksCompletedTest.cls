@IsTest
private class WorksCompletedTest {
    @testSetup public static void setup() {
        Id strRecordTypeAccId = Schema.SobjectType.Account.getRecordTypeInfosByName().get('Insurance Provider').getRecordTypeId();
        /* Create common test Accounts */
        List<Account> testAccounts = new List<Account>();
        for(Integer i=0;i<2;i++) {
            testAccounts.add(new Account(Name = 'TestAcct'+i));
        }
        insert testAccounts;  
        
        /* Create common test Insurance Accounts */
        List<Account> testAccountInsurance = new List<Account>();
        testAccountInsurance.add(new Account(Name = 'TestAcct',RecordTypeId=strRecordTypeAccId,pds__c='www.google.com'));
        insert testAccountInsurance;     
        
        /* Create common test Contacts */
        List<Contact> testContacts = new List<Contact>();
        for(Integer i=0;i<2;i++) {
            testContacts.add(new Contact(FirstName = 'TestAcct'+i, LastName = 'TestAcct'+i));
        }
        insert testContacts;
        
        /* Create policy record */
        List<Policy__c > policyList = new List<Policy__c >();
        Policy__c policyobj = new Policy__c();
        policyobj.State__c='NSW';
        policyList.add(policyobj);
        insert policyList;
        
        /* Create Common Case */
        List<Case> caseList = new List<Case>();
        case caseobj = new case();
        caseobj.ContactId = testContacts[0].id;
        caseobj.AccountId = testAccounts[0].id;
        caseobj.Status = 'Working';
        caseobj.Policy__c = policyList[0].id;
        caseobj.Excess__c = 300;
        
        case caseobject = new case();
        caseobject.ContactId = testContacts[0].id;
        caseobject.AccountId = testAccounts[0].id;
        caseobject.Status = 'Working';
        caseobject.Policy__c = policyList[0].id;
        caseobject.Excess__c = 300;
        caseList.add(caseobj);
        caseList.add(caseobject);
        
        insert caseList;
        system.debug('caseid'+caseList[0].id);
        
        // Create Insurance Case
        List<Case> caseListIns = new List<Case>();
        case caseobj1 = new case();
        caseobj1.ContactId = testContacts[0].id;
        caseobj1.AccountId = testAccountInsurance[0].id;
        caseobj1.Insurance_Provider__c=testAccountInsurance[0].id;
        caseobj1.Status = 'open';
        caseListIns.add(caseobj1);
        
        insert caseListIns;
        
        // Create Authority Case
        Id strRecordTypeCaseId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Authority').getRecordTypeId();  
        List<Case> Authority = new List<Case>();
        case caseobj2 = new case();
        caseobj2.ContactId = testContacts[0].id;
        caseobj2.AccountId = testAccountInsurance[0].id;
        caseobj2.parentid =caseList[0].id;
        caseobj2.RecordTypeId=strRecordTypeCaseId;
        caseobj2.Status = 'AcceptedinWork';
        caseobj2.Job_Type__c = 'doAndCharge';
        caseobj2.Action_Type__c ='authority';
        
        Authority.add(caseobj2);
        
        insert Authority;
        
       
         // Create Work Type
        List<worktype> tradeList=new List<worktype>();
        WorkType wt = new WorkType(name = 'Assessment',EstimatedDuration=1);
        WorkType wt1 = new WorkType(name = 'Plasterer',EstimatedDuration=1);
       // WorkType wt2 = new WorkType(name = 'MS - Temp Fencing',EstimatedDuration=1);
        
        tradeList.add(wt); tradeList.add(wt1); 
        Insert tradeList;
        
        // Create Work order
        
      	List<WorkOrder> workOrderList=new List<WorkOrder>();
        WorkOrder wo = new WorkOrder();
        wo.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Home Repair Trades').getRecordTypeId();                           
        wo.CaseId = caseList[0].id;
        wo.WorkTypeId = tradeList[0].Id ;
        wo.Authority__c = Authority[0].Id;
        wo.ContactId = testContacts[0].id;

        
        workOrderList.add(wo);
        insert workOrderList;
	   
        user usr=[select id from user where id=:userinfo.getuserid()]; 	
       	ServiceResource newSR = FSL_TestDataFactory.createNewServiceResource(usr.Id); 
       	
        List<ServiceAppointment> servApptList =new List<ServiceAppointment>();
        ServiceAppointment SA = new ServiceAppointment (status='New',
                                                        ParentRecordId= workOrderList[0].Id,
                                                        Work_Type_Name__c='Plasterer',Service_Resource__c=newSR.Id,
                                                       Claim__c = caseList[0].id);
    }
    @IsTest
    static void WorksCompleted_None(){

        PageReference pageRef = Page.WorksCompleted;
        Test.setCurrentPage(pageRef);

        Test.startTest();
        WorksCompleted controller = new WorksCompleted();
        PageReference result = controller.proceeSA();
        Test.stopTest();

        System.assertEquals(null, result, 'No page redirection');
        System.assertEquals(true, controller.isError, 'No SA Id');
        
    }
    
    @IsTest
    static void WorksCompleted(){
        
        case x = FSL_TestDataFactory.createClaim();
        workOrder y = FSL_TestDataFactory.HRTradeWorkOrderWithCase(x.Id);
        
        ServiceAppointment z = FSL_TestDataFactory.createNewServiceAppointment(y.Id, 'New');
		//ServiceAppointment z = [SELECT Id FROM ServiceAppointment WHERE Status NOT IN ('Cannot Complete', 'Completed', 'Cancelled') AND Work_Type_Name__c='Plasterer'];
        
        PageReference pageRef = Page.WorksCompleted;
        Test.setCurrentPage(pageRef);

        Test.startTest();
        ApexPages.currentPage().getParameters().put('id',z.Id);
        WorksCompleted controller = new WorksCompleted();
        PageReference result = controller.proceeSA();
        Test.stopTest();

        System.assertEquals(true, controller.isError, 'SA Update failed');  
        
    }
    
    @IsTest
    static void WorksCompletedOpen(){
        
        WorksCompleted.checkTest();

        
        
    }
}