/************************************************************************************************************
Name: WorkOrderLineItemHandler
=============================================================================================================
Purpose: Class for WorkOrderLineItem Trigger as Handler.
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL       DESCRIPTION
1.0        Vasu         10/10/2018        Created       Home Repair Claim System  
*************************************************************************************************************/
public without sharing class WorkOrderLineItemHandler implements ITrigger {
    private List<WorkOrderLineItem> sawoList = new List<WorkOrderLineItem>();
    private Set<String> workOrderSet= new Set<String>();
    public WorkOrderLineItemHandler() {}
    
    public void bulkBefore() {
        system.debug('Entering bulkBefore With Trigger.New'+trigger.new); 
        WorkOrderLineItem woNewRec;
        if(Trigger.IsInsert){
            for(Sobject woTemp : trigger.new) {
                woNewRec = (WorkOrderLineItem)woTemp;
                workOrderSet.add(woNewRec.WorkOrderId);
            }
        }
        if(Trigger.IsDelete){
            for(Sobject woTemp : trigger.old) {
                woNewRec = (WorkOrderLineItem)woTemp;
                workOrderSet.add(woNewRec.WorkOrderId);
            }
        }
        system.debug('Exiting bulkBefore With Trigger.New');
    }
    public void bulkAfter() {
        system.debug('Entering bulkAfter With Trigger.New');       
        if(Trigger.IsInsert||Trigger.IsUpdate){ 
            WorkOrderLineItem woNewRec;
            WorkOrderLineItem woOldRec;
            for(Sobject woTemp : trigger.new){
                woNewRec = (WorkOrderLineItem)woTemp;
                if (woNewRec.Site_Visit_Number__c != null & woNewRec.Work_Order_Work_Type__c != 'Internal/Assessors'
                    & woNewRec.Work_Order_Work_Type__c != 'Insurance' 
                    & woNewRec.Work_Order_Work_Type__c != 'Quality Assurance'
                    & woNewRec.Work_Order_Work_Type__c != 'Reimbursement'
                    & woNewRec.Work_Order_Work_Type__c != 'Cash Settlement'){
                    sawoList.add(woNewRec);
                }                
            }            
            if(Trigger.IsUpdate){
                Map<Id,WorkOrderLineItem> mapNewWoli = (Map<Id,WorkOrderLineItem>)Trigger.newMap;
                Map<Id,WorkOrderLineItem> mapOldWoli = (Map<Id,WorkOrderLineItem>)Trigger.oldMap;                
                for(WorkOrderLineItem woli : mapNewWoli.values()) {
                    if(woli.Quantity != mapOldWoli.get(woli.Id).Quantity || 
                        woli.Labour_Time__c != mapOldWoli.get(woli.Id).Labour_Time__c ||
                        woli.Material_Rate__c != mapOldWoli.get(woli.Id).Material_Rate__c ||
                        woli.Labour_Rate__c != mapOldWoli.get(woli.Id).Labour_Rate__c
                        ) {
                        workOrderSet.add(woli.WorkOrderId);
                    }
                }
            }
        }        
        system.debug('Exiting bulkAfter With Trigger.New');
    }
    public void beforeInsert(SObject so) {
    } 
    public void afterInsert(SObject so) {
    } 
    public void beforeUpdate(SObject oldSo, SObject so) {
        
    }
    public void beforeUnDelete(SObject so) {} 
    
    public void afterUpdate(SObject oldSo, SObject so) {} 
    public void beforeDelete(SObject so){} 
    public void afterDelete(SObject so) {} 
    public void afterUnDelete(SObject so) {} 
    public void andFinally() {
        system.debug('Entering and Finally from  WorkOrderLineItemHandler '+' woList: '+sawoList);
        if (!sawoList.isEmpty()){
            WorkOrderLineItemService.createServiceAppointment(sawoList);
        }
        if(workOrderSet.size() > 0 ) {
             WorkOrderLineItemService.isWorkOrderAmended(workOrderSet);
        }
        system.debug('Exiting andFinally from WorkOrderLineItemHandler');
    }               
}