/* =====================================================================================
Type:       Test class
Purpose:    Test cases for CreateRoom
========================================================================================*/
@isTest
public class CreateRoomTest {
    
    @testSetup 
    static void setup(){
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        //Migrated claim job to claim authority
        case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id);       
        List<WorkOrderLineItem> woliList=HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1);
        woliList.addAll(HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1));
        system.debug('wolist  '+woliList);
    }   
    
    static testMethod void CreateRoomTest() {
        //modified code by CRMIT
        //Logic modified by CRMIT
        Test.startTest();
        WorkOrderLineItem woli =[SELECT id,WorkOrderId FROM WorkOrderLineItem LIMIT 1];
           
        system.debug(woli.WorkOrderId);
        Room__c r = new Room__c();
        r.Name = 'Bed Room';
        r = CreateRoom.createNewRoom(r,woli.WorkOrderId);
        //Covering Negative scenario written by CRMIT
        r = CreateRoom.createNewRoom(r,woli.Id);
        Test.stopTest();
        
    }
}