/* =====================================================================================
Type:       Test class
Purpose:    Test cases for homeRepairCMFeeInvoiceCreationService
========================================================================================*/
@isTest(SeeAllData=true)
private class homeRepairCMFeeInvCreationSrvcTest{
    @isTest public static void testSetup(){
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        string accId;
        List<Account> acc=HomeRepairTestDataFactory.createRCTIAccounts('Testacc');
        Contact con=HomeRepairTestDataFactory.createContact('Testcon');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        system.debug('======parent case in home' +cs);

        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        system.debug('======wt in home' +wt);
        
        //Commented By CRMIT -19/02/2019  
        //Claim_Job__c cj=HomeRepairTestDataFactory.createClaimJob(cs.Id);
        //Added By CRMIT to create a child case for parent case
        Case childCaseObj = HomeRepairTestDataFactory.createChildClaimAuthority(cs.Id);
        system.debug('======childCaseObj in home' +childCaseObj);

        //Commented By CRMIT -19/02/2019  
        // WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCaseAR('Home Repair Trades',cs.id,wt.id,cj.id,acc[0].Id);
        //Added by CRMIT for create workorders for case and child case
        WorkOrder wo = HomeRepairTestDataFactory.createWorkOrdersWithCaseAR('Home Repair Trades',cs.id,wt.id,childCaseObj.id,acc[0].Id);
		system.debug('======wo in home' +wo);
        
        Map<Id,String> claimAuthorityMap = new Map<Id,String>();
        claimAuthorityMap.put(childCaseObj.Id, 'Submitted');
      	system.debug('======claimAuthorityMap in home' +claimAuthorityMap);
      
        //Commented By CRMIT
       /* List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem>();
        WorkOrderLineItem woli = new WorkOrderLineItem();
        woli.workOrderId = wo.Id;
        woli.description = 'abcd';
        woli.workTypeId = wt.Id;
        woliList.add(woli);
        woli = new WorkOrderLineItem();
        woli.workOrderId = wo.Id;
        woli.description = 'cdba';         
        woliList.add(woli);
        insert woliList; */
        
        //Added By CRMIT to create products and map product info to the Invoice Line Ltem (AR_SOW__c)
        Product2 proOb = new Product2();
        proOb.Name='ProductTest';
        proOb.ProductCode = 'CMFEES';
        proOb.IsActive = true;
        insert proOb;
        system.debug('======proOb in home' +proOb);

        
        test.startTest();
        hrCMFeeInvoiceCreationService.createInvoiceRecord(claimAuthorityMap);    
        test.stopTest();
    }    
}