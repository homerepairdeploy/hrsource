/* =====================================================================================
Type:       Test class
Purpose:    Test cases for hrARFeeInvoiceCreationService
========================================================================================*/
@isTest
private class hrARFeeInvoiceCreationServiceTest{
    @testSetup
    public static void testSetup(){
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        
    }
    static testMethod void hrARFeeInvoiceCreationServiceTestMethod() {
    
         Id hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
         case authcase=[select id from case where RecordType.name like '%auth%' limit 1];
        Test.startTest();
            hrARFeeInvoiceCreationService.createInvoiceRecord(authcase.id,'handback');
            hrARFeeInvoiceCreationService.createInvoiceRecord(authcase.id,'proceed');
            hrARFeeInvoiceCreationService.createInvoiceRecord(authcase.id,'submit');
        Test.stopTest();        
    }
}