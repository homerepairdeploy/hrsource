public class AccountHandler implements ITrigger {    
    public List<String> BSBSet          =  new List<String>();
    public List<ID> AccUpdtOrclFlds =  new List<ID>();
    
    public AccountHandler() {
        system.debug('account h');
    }
    
    public void bulkBefore() {  
        
        
    }
    public void bulkAfter() { 
        List<Account> newList = (List<Account>)Trigger.new;
        System.debug('Account Handler -  Entering bulk after with Trigger.New');
        if(Trigger.IsUpdate){
            System.debug('Entering isupdate with Trigger.New');
            Map<Id, Account> oldMap = (Map<Id, Account>) Trigger.oldMap;
            List<UpdatedBy_User_ID__mdt> userList = new List<UpdatedBy_User_ID__mdt>();
            
            for(Account acc : newList){ 
                if(acc.BSB__c != null && acc.BSB__c != oldMap.get(acc.Id).BSB__c){
                    BSBSet.add(acc.BSB__c);
                } 
                /*  String UserID;
userList = Database.query('SELECT User_ID__c FROM UpdatedBy_User_ID__mdt limit 1 ');

if(userList.size() > 0)
UserID = userList[0].User_ID__c;

if((acc.LastModifiedById != UserID  && acc.Type == 'Trade Company') && 
(oldMap.get(acc.ID).Exclude_From_Oracle_Integration__c == True &&  acc.Exclude_From_Oracle_Integration__c == True ) ||
(oldMap.get(acc.ID).Exclude_From_Oracle_Integration__c == False &&  acc.Exclude_From_Oracle_Integration__c == False )) */
                
                
                If((oldMap.get(acc.ID).Exclude_From_Oracle_Integration__c !=  acc.Exclude_From_Oracle_Integration__c  )                 ||
                   (oldMap.get(acc.ID).Oracle_Address_Name__c             !=  acc.Oracle_Address_Name__c  )                             ||
                   (oldMap.get(acc.ID).Oracle_Banking_Number__c           !=  acc.Oracle_Banking_Number__c  )                           ||
                   (oldMap.get(acc.ID).Oracle_Process_Status__c           !=  acc.Oracle_Process_Status__c  )                           ||
                   (oldMap.get(acc.ID).Oracle_Site_Assignment_Id__c       !=  acc.Oracle_Site_Assignment_Id__c  )                       ||
                   (oldMap.get(acc.ID).Oracle_Site_Id__c                  !=  acc.Oracle_Site_Id__c  )                                  ||
                   (oldMap.get(acc.ID).Oracle_Supplier_Interface_Loader_Job_Id__c !=  acc.Oracle_Supplier_Interface_Loader_Job_Id__c  )  ||
                   (oldMap.get(acc.ID).Oracle_Supplier_Job_Id__c          !=  acc.Oracle_Supplier_Job_Id__c  )                           ||
                   (oldMap.get(acc.ID).Oracle_Supplier_Name__c            !=  acc.Oracle_Supplier_Name__c  )                             ||
                   (oldMap.get(acc.ID).Oracle_Supplier_Number__c          !=  acc.Oracle_Supplier_Number__c  )                           ||
                   (oldMap.get(acc.ID).Oracle_Supplier_Record_Id__c       !=  acc.Oracle_Supplier_Record_Id__c  )                        ||
                   (oldMap.get(acc.ID).Oracle_Supplier_Site_Name__c       !=  acc.Oracle_Supplier_Site_Name__c  )                        ||
                   (oldMap.get(acc.ID).Oracle_Supplier_Upload_Step__c     !=  acc.Oracle_Supplier_Upload_Step__c  )                      ||
                   (oldMap.get(acc.ID).Oracle_Upload_Error_Message__c     !=  acc.Oracle_Upload_Error_Message__c  )                      ||
                   (oldMap.get(acc.ID).CountAccTradeTasks__c              !=  acc.CountAccTradeTasks__c))
                {
                    system.debug('no edit'  + acc); }
                else{ 
                    if(acc.Type == 'Trade Company'){
                        if((oldMap.get(acc.ID).name !=  acc.name  )                                                   ||
                           (oldMap.get(acc.ID).Registered_Trading_Name__c !=  acc.Registered_Trading_Name__c  )      ||
                           (oldMap.get(acc.ID).Preferred_Business_Name__c !=  acc.Preferred_Business_Name__c  )       ||
                           (oldMap.get(acc.ID).ABN__c !=  acc.ABN__c  )                                               ||
                           (oldMap.get(acc.ID).ABN_Status__c !=  acc.ABN_Status__c  )                                 ||
                           (oldMap.get(acc.ID).ACN__c !=  acc.ACN__c  )                                               ||
                           (oldMap.get(acc.ID).GST_Status__c !=  acc.GST_Status__c  )                                 ||
                           (oldMap.get(acc.ID).Bank_Account_Name__c !=  acc.Bank_Account_Name__c  )                   ||
                           (oldMap.get(acc.ID).BSB__c !=  acc.BSB__c  )                                               ||
                           (oldMap.get(acc.ID).Bank_Code__c !=  acc.Bank_Code__c  )                                   ||
                           (oldMap.get(acc.ID).Bank_Name__c !=  acc.Bank_Name__c  )                                   ||
                           (oldMap.get(acc.ID).Branch__c !=  acc.Branch__c  )                                         ||
                           (oldMap.get(acc.ID).Account_Number__c !=  acc.Account_Number__c  )                         ||
                           (oldMap.get(acc.ID).Payment_Terms__c !=  acc.Payment_Terms__c  )                           ||
                           (oldMap.get(acc.ID).Payment_Type__c !=  acc.Payment_Type__c  )                             ||
                           (oldMap.get(acc.ID).RCTI__c !=  acc.RCTI__c  )                                             ||
                           (oldMap.get(acc.ID).Remittance_Email_Address__c !=  acc.Remittance_Email_Address__c  ))
                          {
                               system.debug('About to Perform Oracle fields updates');
                               AccUpdtOrclFlds.add(acc.ID);
                        }
                    }
                }
                
            }
            
            If(AccUpdtOrclFlds.size() > 0)
            {                              
                system.debug('entering to update orcale fields');
                AccountService.resetSupplierIntgFlds(AccUpdtOrclFlds);
            }
        }
        
    }
    public void beforeInsert(SObject so) {
    
         Account acct=(Account)so;
        if (acct.phone!=null) acct.phone=CommonUtilityClass.formatNumber(acct.phone);
        if (acct.Accounts_Phone__c!=null) acct.Accounts_Phone__c=CommonUtilityClass.formatNumber(acct.Accounts_Phone__c);
        if (acct.Alternate_Phone__c!=null) acct.Alternate_Phone__c=CommonUtilityClass.formatNumber(acct.Alternate_Phone__c);
        if (acct.Job_Related_Phone__c!=null) acct.Job_Related_Phone__c=CommonUtilityClass.formatNumber(acct.Job_Related_Phone__c);
        if (acct.Job_Related_Alternate_Phone__c!=null) acct.Job_Related_Alternate_Phone__c=CommonUtilityClass.formatNumber(acct.Job_Related_Alternate_Phone__c);
        if (acct.Accounts_Alt_Phone__c!=null) acct.Accounts_Alt_Phone__c=CommonUtilityClass.formatNumber(acct.Accounts_Alt_Phone__c);
    } 
    public void afterInsert(SObject so) {
        
    } 
    public void beforeUpdate(SObject oldSo, SObject so) {
    
       Account acct = (Account)so;
       Account oldacct=(Account)oldSo;

        if (acct.phone!=null && acct.phone!=oldacct.phone) acct.phone=CommonUtilityClass.formatNumber(acct.phone);
        if (acct.Accounts_Phone__c!=null && acct.Accounts_Phone__c!=oldacct.Accounts_Phone__c) acct.Accounts_Phone__c=CommonUtilityClass.formatNumber(acct.Accounts_Phone__c);
        if (acct.Alternate_Phone__c!=null && acct.Alternate_Phone__c!=oldacct.Alternate_Phone__c) acct.Alternate_Phone__c=CommonUtilityClass.formatNumber(acct.Alternate_Phone__c);
        if (acct.Job_Related_Phone__c!=null && acct.Job_Related_Phone__c!=oldacct.Job_Related_Phone__c) acct.Job_Related_Phone__c=CommonUtilityClass.formatNumber(acct.Job_Related_Phone__c);
        if (acct.Job_Related_Alternate_Phone__c!=null && acct.Job_Related_Alternate_Phone__c!=oldacct.Job_Related_Alternate_Phone__c) acct.Job_Related_Alternate_Phone__c=CommonUtilityClass.formatNumber(acct.Job_Related_Alternate_Phone__c);
        if (acct.Accounts_Alt_Phone__c!=null && acct.Accounts_Alt_Phone__c!=oldacct.Accounts_Alt_Phone__c) acct.Accounts_Alt_Phone__c=CommonUtilityClass.formatNumber(acct.Accounts_Alt_Phone__c);
    }
    public void beforeUnDelete(SObject so) {} 
    
    public void afterUpdate(SObject oldSo, SObject so) {
        
    }  
    public void beforeDelete(SObject so){} 
    public void afterDelete(SObject so) {} 
    public void afterUnDelete(SObject so) {} 
    public void andFinally() {
        if (BSBSet.size() > 0){
            System.debug(BSBSet.size());
            AccountService.updateBSBReferenceValues(BSBSet);
        }
        
    }               
}