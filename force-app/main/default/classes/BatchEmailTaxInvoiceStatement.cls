/**
 * Created by dilshanegodawela on 22/9/20.
 */

global with sharing class BatchEmailTaxInvoiceStatement implements Database.Batchable<sObject> {

	private String query;

	global BatchEmailTaxInvoiceStatement() {
		this.query = 'Select Name, Account_Email__c, RCTI_PDF_Trade_Status__c, Work_Order__r.WorkOrderNumber FROM AP_Invoice__c WHERE '+
				+'Invoice_Type__c = \'Supplier\' AND Oracle_Process_Status__c = \'Processed\' AND RCTI_PDF_Trade_Status__c = \'Pending\' AND Account_Email__c != NULL AND RCTI_New__c = true';
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug([Select Name, Account_Email__c, RCTI_PDF_Trade_Status__c, Work_Order__r.WorkOrderNumber,Oracle_Process_Status__c, RCTI_New__c FROM AP_Invoice__c]);
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		System.debug((List<AP_Invoice__c>)scope);
		EmailRecipientCreatedTaxInvoiceStatement.EmailRecipientCreatedTaxInvoiceStatement((List<AP_Invoice__c>)scope);
	}
	global void finish(Database.BatchableContext BC) {

	}
}