/********************************************************************************************** 
Name        : ClaimConnect_Utilities
Description : Generic class to define picklist values in ClaimConnect_BuildingAssessment component
Purpose     : All the picklist values have been retrieved dynamically using Schema.describe and this
			  is used in ClaimConnect_BuildingAssessment to display picklist values
                                                            
VERSION        AUTHOR           		  DATE           DETAIL      DESCRIPTION   
1.0            Vidhya Sivaperuman         15-04-2020     Created     ClaimConnect APP
/***********************************************************************************************/
public with sharing class ClaimConnect_Utilities {
    
    @AuraEnabled  
    public static List<String>  getPicklistValues(String objectName,String field_apiname){ 
        
        List<String> optionlist = new List<String>(); 
        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectName);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_apiname).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            optionlist.add(a.getValue());//add the value  to our final list     
        }
        optionlist.sort();
        
        return optionlist;
    }
    
    @AuraEnabled
    public static List<String> getPickListmdt(String fieldname)
    {
        List<String> options = new List<String>();
        
        List<SF1App_PickList_Values__mdt> pick_list_values = [SELECT Field_Value__c FROM SF1App_PickList_Values__mdt where Field_Name__c=:fieldname];    
        for (SF1App_PickList_Values__mdt a : pick_list_values) {  
            options.add(a.Field_Value__c);     
        }
        return options;  
        
    }    
    
}