/***************************************************************** 
Purpose: Test class for BatchClaimJobIngestionStagingProcess 
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            Vasu          20/02/2018      Created      Home Repair Claim System  
*******************************************************************/
@istest 
public class BatchClaimJobIngestionStagingProcessTest { 
    @testSetup 
    static void setup(){
        HomeRepairTestDataFactory.createAccounts('TEST');        
        HomeRepairTestDataFactory.GeneralSettings('ITSupportEmail','ITSupportEmail'); 
        
        List<Claim_Job_Ingestion_Staging__c> cjobList=HomeRepairTestDataFactory.createClaimJobIngestionStaging('25533986','7177774','7177774','A020490615');     
       	Historical_Claim_Data__c hcd=HomeRepairTestDataFactory.createHistoricalClaimData('25533987');
       	//Historical_Claim_Data__c hcd=HomeRepairTestDataFactory.createHistoricalClaimData('25533986');
        HomeRepairTestDataFactory.createClaimJobWorkItemStaging('8370852','7177774',cjobList[0].id);
        
    }  
    
    static testmethod void testBatchWithContactAccountProcess() {  
        HomeRepairTestDataFactory.GeneralSettings('ClaimIngestionFilter','TRUE');
        Contact con=HomeRepairTestDataFactory.createContact('TEST');
        con.Policy_Number__c='A020490615';
        update con;         
        Account acc=[Select Id from Account where Name='TEST'];
        acc.Policy_Number__c='A020490615';
        update acc;

        //Added By CRMIT to get the list of created Claim_Job_Ingestion_Staging__c 
        Claim_Job_Ingestion_Staging__c cjob=[Select id,JobSubType__c,ClaimType__c, HomeAssist__c from Claim_Job_Ingestion_Staging__c where Brand__c='Test' LIMIT 1];  
        System.debug(' ======cjob===>>>>>>>' +cjob);
        cjob.JobSubType__c='doAndCharge';
        cjob.ClaimType__c='Building and Contents';
        update cjob;
        
        Test.startTest();   
        BatchClaimJobIngestionStagingProcess objClaim = new BatchClaimJobIngestionStagingProcess();
        Database.executeBatch(objClaim);          
        Test.stopTest();
        // objClaim.failureEmailAlert('Case','Test Class');
        System.AssertEquals([SELECT id FROM Account].size(),1);         
        //System.AssertEquals([SELECT RecordType.Name FROM Contact].RecordType.Name,'Policy Holder Contact');     
    }
    
    static testmethod void testBatchWithContactAccountProcess1() {   
        HomeRepairTestDataFactory.GeneralSettings('ClaimIngestionFilter','TRUE');
        Contact con=HomeRepairTestDataFactory.createContact('TEST');
        con.Policy_Number__c='A020490615';
        update con;         
        Account acc=[Select Id from Account where Name='TEST'];
        acc.Policy_Number__c='A020490615';
        update acc;    
        
        //Added By CRMIT to get the list of created Claim_Job_Ingestion_Staging__c 
        //Claim_Job_Ingestion_Staging__c cjob=[Select id,JobSubType__c,ClaimType__c, HomeAssist__c from Claim_Job_Ingestion_Staging__c where Brand__c='Test' LIMIT 1];  
        //System.debug(' ======cjob===>>>>>>>' +cjob);
        //cjob.JobSubType__c='doAndCharge';
        //cjob.ClaimType__c='Building and Contents';
        //update cjob;
        
        Test.startTest();   
        BatchClaimJobIngestionStagingProcess objClaim = new BatchClaimJobIngestionStagingProcess();
        Database.executeBatch(objClaim);          
        Test.stopTest();
        System.AssertEquals([SELECT id FROM Contact].size(),1);  
    }
    
    static testmethod void testBatchClaimJobIngestionStagingProcess() {
        //Added By CRMIT to cover the createHistoricalClaimData
        //Historical_Claim_Data__c hcd=HomeRepairTestDataFactory.createHistoricalClaimData('25533987');
        Historical_Claim_Data__c hcd=HomeRepairTestDataFactory.createHistoricalClaimData('25533986');
        HomeRepairTestDataFactory.GeneralSettings('ClaimIngestionFilter','TRUE');
        //Added By CRMIT to get the list of created Claim_Job_Ingestion_Staging__c 
        Claim_Job_Ingestion_Staging__c cjob=[Select id,JobType__c,JobSubType__c,ClaimType__c, HomeAssist__c from Claim_Job_Ingestion_Staging__c where Brand__c='Test' LIMIT 1];  
        System.debug(' ======cjob===>>>>>>>' +cjob);
        cjob.JobType__c='quote';
        cjob.JobSubType__c='assessment';
        cjob.ClaimType__c='Building';
        update cjob;
       
        Test.startTest();  
        BatchClaimJobIngestionStagingProcess objClaim = new BatchClaimJobIngestionStagingProcess();
        //objClaim.failureEmailAlert('Case','Requied Field Missing');
        Database.executeBatch(objClaim);          
        Test.stopTest();
        //Added By CRMIT to check the expected and actual value
        System.AssertEquals([SELECT id FROM Claim_Job_Ingestion_Staging__c].size(),1);         
    }

    static testmethod void testBatchClaimJobIngestionStagingProcessAuthorization() {
        //Added By CRMIT to cover the createHistoricalClaimData
        //Historical_Claim_Data__c hcd=HomeRepairTestDataFactory.createHistoricalClaimData('25533987');
        Historical_Claim_Data__c hcd=HomeRepairTestDataFactory.createHistoricalClaimData('25533986');
        hcd.Status__c='Open';
        
        update hcd;
            
        HomeRepairTestDataFactory.GeneralSettings('ClaimIngestionFilter','TRUE');
        //Added By CRMIT to get the list of created Claim_Job_Ingestion_Staging__c 
        Claim_Job_Ingestion_Staging__c cjob=[Select id,InternalReferenceId__c, JobNumber__c, JobType__c,Is_Event__c,Is_Manual__c,JobSubType__c,ClaimType__c, HomeAssist__c, Source_Channel__c, Source_Channel_Actual__c from Claim_Job_Ingestion_Staging__c where Brand__c='Test' LIMIT 1];  
        System.debug(' ======cjob===>>>>>>>' +cjob);
        //cjob.HomeAssist__c=true;
        cjob.SpecialInstructions__c='Testing Testing';
        cjob.JobType__c='quote';
        cjob.JobSubType__c='assessment';
        cjob.ClaimType__c='Building';
        cjob.Is_Manual__c=false;
        cjob.Is_Event__c=true;
        cjob.Source_Channel__c='Test1';
        cjob.Source_Channel_Actual__c='Test2';
        update cjob;
       
        // create work item
        HomeRepairTestDataFactory.createClaimJobWorkItemStaging(cjob.InternalReferenceId__c, cjob.JobNumber__c, cjob.Id);
        
        Test.startTest();  
        BatchClaimJobIngestionStagingProcess objClaim = new BatchClaimJobIngestionStagingProcess();
        
        //objClaim.failureEmailAlert('Case','Requied Field Missing');
        Database.executeBatch(objClaim); 
        
        cjob=[Select id,JobType__c,JobSubType__c,Process_Status__c,ClaimType__c, HomeAssist__c, Status__c from Claim_Job_Ingestion_Staging__c where Brand__c='Test' LIMIT 1];  
        System.debug(' ======cjob===>>>>>>>' +cjob);
        cjob.JobType__c='authority';
        cjob.JobSubType__c='assessment';
        cjob.ClaimType__c='Building';
        cjob.Status__c='Completed';
        cjob.Process_Status__c='Pending';
        update cjob;
        
        objClaim = new BatchClaimJobIngestionStagingProcess();
        //objClaim.failureEmailAlert('Case','Requied Field Missing');
        Database.executeBatch(objClaim);

        Test.stopTest();
        //Added By CRMIT to check the expected and actual value
        //System.AssertEquals([SELECT id FROM Case].size(),2);         
        System.AssertEquals([SELECT id FROM Claim_Job_Ingestion_Staging__c].size(),1);  
    }
    
    static testmethod void testBatchClaimJobIngestionStagingProcessGeneral() {
    	ClaimJobIngestionUtility.checkTest();
    }
}