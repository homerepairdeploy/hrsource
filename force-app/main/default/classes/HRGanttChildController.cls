/***
 * Class Name:      HRGanttChildController 
 * 
 * Author:          Stephen Moss
 * Created Date:    13/08/2018
 * Description:     Controller for the lightning component HRGanttChild
 * 
***/

public class HRGanttChildController {
  public static final String SA_STATUS_TENTATIVE = 'Tentative';
  public static final String SA_STATUS_CONFIRMED = 'Confirmed';
  public static final String SA_STATUS_AWAITING_CONFIRMATION = 'Awaiting Confirmation';
  public static final String SA_STATUS_AWAITING_CANCELLED = 'Canceled';
  public static final String SA_STATUS_AWAITING_NEW = 'New';
  public static final String WO_STATUS_ACCEPTED_WO = 'Accepted WO';
  public static final String WO_STATUS_NEW = 'New';
  public static final String WO_STATUS_WORKORDER_SCHEDULED = 'Work Order Scheduled';
  public class ServiceAppintmentWrap {
    @AuraEnabled
    public ServiceAppointment servApp;
    @AuraEnabled
    public Boolean isSelected;
    @AuraEnabled
    public String woNumber;
    @AuraEnabled
    public String srName;
    public ServiceAppintmentWrap(ServiceAppointment sa) {
        this.servApp = sa;
        isSelected = false;
        if(sa.Service_Resource__c != null) {
          this.srName = sa.Service_Resource__r.Account_Name_New__c;
        } else if(sa.Tier_2_Trade_Account__c != null){
          this.srName = sa.Tier_2_Trade_Account__r.Name;
        }
        else {
          this.srName = null;
        }
       System.debug('Service Company Name:'+srName); 
       
    }
  }
  /**
   * This method is used to get tentative service appointments
   * @param claimId Case Id
   * @return tentaAppointments : list of service appointment wrappers
   */
  @AuraEnabled
  public static List<ServiceAppintmentWrap> getTentativeAppointments(Id claimId) {
    Map<Id,ServiceAppintmentWrap> tentaAppointments = new Map<Id,ServiceAppintmentWrap>();
    List<ServiceAppointment> woAppointments = new List<ServiceAppointment>();
    Boolean addSA;
    for(WorkOrder wo : [SELECT Id, (SELECT Id, AppointmentNumber,Work_Order_Number__c, Status, ParentRecordId, Work_Type_Name__c,
                                           Service_Resource__c,Tier_2_Trade_Account__r.Name, Service_Resource__r.Account_Name_New__c
                                    FROM ServiceAppointments
                                    WHERE Status != :NJ_Constants.SA_STATUS_CONFIRMED
                                    AND Status != :NJ_Constants.SA_STATUS_AWAITINGCONFIRMATION)
                        FROM WorkOrder
                        WHERE CaseId = :claimId]) {
        addSA = true;
       if(wo.ServiceAppointments.size() > 0){
          for(ServiceAppointment sa : wo.ServiceAppointments) {
                if(sa.Status != SA_STATUS_TENTATIVE) {
                    addSA = false;
                    break;
                }
                woAppointments.add(sa);
            }
       }
        
                            
        // add SA only when all of them are tentative
        if(addSA) {
            for(ServiceAppointment sa : woAppointments) {
                tentaAppointments.put(sa.Id, new ServiceAppintmentWrap(sa));
            }
        }
    }      
    System.debug('Tentative Appointments:'+tentaAppointments.values());
    return tentaAppointments.values();
  }
  /**
   * This method is used to get confirmed service appointments
   * @param claimId Case Id
   * @return tentaAppointments : list of service appointment wrappers
   */
  @AuraEnabled
  public static List<ServiceAppintmentWrap> getConfirmedAppointments(Id claimId) {
    List<ServiceAppintmentWrap> tentaAppointments = new List<ServiceAppintmentWrap>();
    List<String> confirmStatus = new List<String> { SA_STATUS_CONFIRMED, SA_STATUS_AWAITING_CONFIRMATION, SA_STATUS_TENTATIVE};
    Boolean addSA = false;
    for(ServiceAppointment sa : [SELECT Id, AppointmentNumber,Work_Order_Number__c,Work_Type_Name__c, Service_Resource__c, Service_Resource__r.IsCapacityBased, Status,
                                        Tier_2_Trade_Account__c,Tier_2_Trade_Account__r.Name, Service_Resource__r.Account_Name_New__c
                                 FROM ServiceAppointment
                                 WHERE Status IN :confirmStatus 
                                 AND Claim__c = :claimId
                                 AND Work_Type_Name__c NOT IN :NJ_Constants.ASSREASSSET 
                                 AND (Service_Resource__c != Null
                                      OR Tier_2_Trade_Account__c != Null) ]) {
        addSA = false;
        if(sa.Status == SA_STATUS_TENTATIVE) {
          addSA = true;
        } else if (sa.Service_Resource__c != null && !sa.Service_Resource__r.IsCapacityBased
                  && sa.Status == SA_STATUS_CONFIRMED) {
          addSA = true;
        } else if( ((sa.Service_Resource__c != null && sa.Service_Resource__r.IsCapacityBased) || sa.Tier_2_Trade_Account__c != null)  
                   && (sa.Status == SA_STATUS_AWAITING_CONFIRMATION || sa.Status == SA_STATUS_CONFIRMED)) {
          addSA = true;
        }
        if(addSA) {
          tentaAppointments.add(new ServiceAppintmentWrap(sa));
        }
    }
    return tentaAppointments;
  }
  public class ConfirmAppointmentResponse {
    @AuraEnabled
    public String result;
    @AuraEnabled
    public Boolean hasError; 
    public ConfirmAppointmentResponse() {
      result = '';
      hasError = false;
    }
  }
  /**
   * This method is used to schedule tentative appointments
   * @param claimId Case Id
   * @return tentaAppointments : list of service appointment wrappers
   */
  @AuraEnabled
  public static ConfirmAppointmentResponse confirmAppointments(List<Id> appIds) {
    ConfirmAppointmentResponse car = new ConfirmAppointmentResponse();
    Set<Id> woIds = new Set<Id>();
    List<ServiceAppointment> woAppointments;
    Set<Id> appIdsSet = new Set<Id>(appIds);
    Map<Id,List<ServiceAppointment>> woServiceAppointments = new Map<Id, List<ServiceAppointment>>();
    Map<Id, ServiceAppointment> mapServiceAppointment = new Map<Id,ServiceAppointment>();
    for(ServiceAppointment sa : [SELECT Id, AppointmentNumber, ParentRecordId
                                 FROM ServiceAppointment
                                 WHERE Id IN :appIds
                                 AND Work_Type_Name__c NOT IN :NJ_Constants.ASSREASSSET
                                 ]) {
      woIds.add(sa.ParentRecordId);
      if(!woServiceAppointments.containsKey(sa.ParentRecordId)) {
        woServiceAppointments.put(sa.ParentRecordId, new List<ServiceAppointment>());
      }
      woAppointments = woServiceAppointments.get(sa.ParentRecordId);
      woAppointments.add(sa);
      woServiceAppointments.put(sa.ParentRecordId, woAppointments);
    }
    Map<Id,Set<Id>> mapWOAllSAIds = new Map<Id,Set<Id>>();
    Set<Id> saIds = new Set<Id>();
    Map<Id,List<ServiceAppointment>> workOrderAllAppointments = new Map<Id, List<ServiceAppointment>>();
    for(ServiceAppointment sa : [SELECT Id, AppointmentNumber, ParentRecordId, Service_Resource__c, Service_Resource__r.IsCapacityBased, Tier_2_Trade_Account__c, Status
                                 FROM ServiceAppointment
                                 WHERE ParentRecordId IN :woIds
                                 AND Status != :NJ_Constants.SA_STATUS_CONFIRMED
                                 AND Status != :NJ_Constants.SA_STATUS_AWAITINGCONFIRMATION]) {
      if(!workOrderAllAppointments.containsKey(sa.ParentRecordId)) {
          workOrderAllAppointments.put(sa.ParentRecordId, new List<ServiceAppointment>());
      }
      woAppointments = workOrderAllAppointments.get(sa.ParentRecordId);
      woAppointments.add(sa);
      workOrderAllAppointments.put(sa.ParentRecordId, woAppointments);

      if(!mapWOAllSAIds.containsKey(sa.ParentRecordId)) {
        mapWOAllSAIds.put(sa.ParentRecordId, new Set<Id>());
      }
      saIds = mapWOAllSAIds.get(sa.ParentRecordId);
      saIds.add(sa.Id);
      mapWOAllSAIds.put(sa.ParentRecordId, saIds);
      mapServiceAppointment.put(sa.Id, sa);
    }
    Map<Id,WorkOrder> mapWorkOrders = new Map<Id, WorkOrder>([SELECT Id, WorkType.Name, WorkOrderNumber
                                                              FROM WorkOrder
                                                              WHERE ID IN :woIds]);
    List<Id> errorWO = new List<Id>();
    List<Id> updateSA = new List<Id>();
    for(Id woId : mapWOAllSAIds.keySet()) {
      if(!appIdsSet.containsAll(mapWOAllSAIds.get(woId))) {
        car.hasError = true;
        errorWO.add(woId);
        workOrderAllAppointments.remove(woId);
      } else {
        updateSA.addAll(mapWOAllSAIds.get(woId));
      }
    }
    if(!errorWO.isEmpty()) {
      car.result += 'Please include all the Service Appointments to confirm : ';
      for(Id wo : errorWO) {
        car.result += mapWorkOrders.get(wo).WorkType.Name + '-' + mapWorkOrders.get(wo).WorkOrderNumber + ',';
      }
      car.result = car.result.substring(0, car.result.length() - 1);
      car.result += '.';
    }
    Map<Id,AssignedResource> appointAssignedResource = new Map<Id,AssignedResource>();
    List<ServiceAppointment> appointments = new List<ServiceAppointment>();
    for(AssignedResource ar : [SELECT Id, ServiceResource.IsCapacityBased, ServiceAppointmentId, ServiceAppointment.Tier_2_Trade_Account__c,
                                      ServiceAppointment.ParentRecordId
                               FROM AssignedResource
                               WHERE ServiceAppointment.Id IN :updateSA]) {
        appointAssignedResource.put(ar.ServiceAppointmentId, ar);
    }
    for(Id app : updateSA) {
      if(appointAssignedResource.containsKey(app) && appointAssignedResource.get(app).ServiceResource != null 
        && (!appointAssignedResource.get(app).ServiceResource.IsCapacityBased 
            || (mapWorkOrders.containsKey(appointAssignedResource.get(app).ServiceAppointment.ParentRecordId) &&
                (mapWorkOrders.get(appointAssignedResource.get(app).ServiceAppointment.ParentRecordId).WorkType.Name.startsWith(NJ_Constants.WORK_TYPE_STARTS_WITH_HOMEASSIST)
                 || mapWorkOrders.get(appointAssignedResource.get(app).ServiceAppointment.ParentRecordId).WorkType.Name.startsWith(NJ_Constants.WORK_TYPE_STARTS_WITH_MAKESAFE))
                  ))) {
          appointments.add(new ServiceAppointment(Id = app, Status = SA_STATUS_CONFIRMED,
                                                  Tier_2_Trade__c = null));

      } else {
          appointments.add(new ServiceAppointment(Id = app, Status = SA_STATUS_AWAITING_CONFIRMATION));
      }
    }
    System.debug('Nik appointments '+appointments);
    Savepoint sp = Database.setSavePoint();
    try {
      if(!appointments.isEmpty()) {
          update appointments;
      }           
    } catch(Exception ex) {
      Database.rollBack(sp);
      throw new AuraException(ex.getMessage());
    }
    return car;
  }
  /**
   * This method is used to Unschedule tentative appointments
   * @param claimId Case Id
   * @return tentaAppointments : list of service appointment wrappers
   */
  @AuraEnabled
  public static String unScheduleAppointments(List<Id> appIds, Boolean isSecondClick) {
    String result = '';
    Map<Id, ServiceAppointment> mapSA = new Map<Id,ServiceAppointment>([SELECT Id, Committed_Start_Date__c, SchedStartTime, ParentRecordId
                                                                        FROM ServiceAppointment
                                                                        WHERE Id IN :appIds 
                                                                        AND Work_Type_Name__c NOT IN :NJ_Constants.ASSREASSSET]);
    Set<Id> woIds = new Set<Id>();
    for(ServiceAppointment sa : mapSA.values()) {
      woIds.add(sa.ParentRecordId);
    }
    Map<Id,WorkOrder> mapWorkOrders = new Map<Id, WorkOrder>([SELECT Id, WorkType.Name, WorkOrderNumber
                                                              FROM WorkOrder
                                                              WHERE Id IN :woIds
                                                             ]);

    List<ServiceAppointment> appointments = new List<ServiceAppointment>();
    List<WorkOrder> erroWO = new List<WorkOrder>();
    Datetime tomorrow = Datetime.now()+1;
    Datetime today = Datetime.now();
    System.debug('Tomorrow:'+tomorrow);
    System.debug('Today:'+today);
    ServiceAppointment saTemp;
    for(Id sa : appIds) {
      saTemp = mapSA.get(sa);
      if((saTemp.Committed_Start_Date__c < tomorrow && saTemp.Committed_Start_Date__c  > today)  
          || (saTemp.SchedStartTime < tomorrow && saTemp.SchedStartTime  > today)) {
        erroWO.add(mapWorkOrders.get(saTemp.ParentRecordId));
      }  
      appointments.add(new ServiceAppointment(Id = sa, Status = SA_STATUS_AWAITING_NEW, Committed_End_Date__c = null,  
                                                  SchedStartTime = null, Service_Resource__c = null,
                                                  SchedEndTime = null,Committed_Start_Date__c = null,
                                                  Tier_2_Trade__c = null, Tier_2_Trade_Account__c = null,Time_Bound_by_Tradee__c=false, 
                                                  Is_Time_Bound__c = false));
      
    }
    Savepoint sp = Database.setSavePoint();
    try {
      if(!erroWO.isEmpty()) {
        result = 'This appointment is scheduled in the next 24 hours.Are you sure you want to unschedule : ';
        for(WorkOrder err : erroWO) {
          result +=  err.WorkOrderNumber + '-' + err.WorkType.Name + ' ,';
        }
        result = result.substring(0, result.length() - 1);
      }else{
        isSecondClick = true;
      }
      if(!appointments.isEmpty() && isSecondClick) {
          update appointments;
          result = '';
      }
    } catch(Exception ex) {
      Database.rollBack(sp);
      throw new AuraException(ex.getMessage());
    }
    return result;
  }
  /*** 
   * Method Name:     getClaimScheduledAppointments
   * Author:          Stephen Moss
   * Created Date:    13/08/2018
   * Description:     Get the Service Appointment details for the 
   *                  passed in Service Appointment Id where the SA
   *                  is at a stage of "Scheduled" or greater
   *                  in the lifecycle
   ***/
  @AuraEnabled
  public static List<srvTradeWrapper> getClaimScheduledAppointments(Id claimId) {
    List<WorkOrder> workOrderList = [SELECT Id, Status, WorkTypeId, WorkType.Name, CaseId, WorkOrderNumber
                                        FROM WorkOrder
                                        WHERE CaseId = :claimId
                                        ORDER BY WorkOrderNumber];
    if(!workOrderList.isEmpty()) {
        
      // build a set to use for getting Service Appointments
      Set<Id> woIdSet = new Set<Id>();
      for (WorkOrder w : workOrderList) {
          woIdSet.add(w.Id);
      }            
      List<ServiceAppointment> saList = [SELECT Id, Status, Subject, ParentRecordId, AppointmentNumber,
                                                WorkType.Name, SchedStartTime, SchedEndTime, Tier_2_Trade_Account__c,
                                                EarliestStartTime, DueDate,Committed_Start_Date__c,Committed_End_Date__c,  
                                                Service_Resource__r.Name, Tier_2_Trade_Account__r.Name, Service_Resource__r.IsCapacityBased,
                                                Service_Resource__r.RelatedRecord.CompanyName,
                                                Service_Resource__r.RelatedRecord.Contact.Account.Name,
                                                Service_Resource__r.RelatedRecord.Name,
                                                Service_Resource__r.ServiceCrew.Service_Crew_Company_Name__c
                                         FROM ServiceAppointment
                                         WHERE (ParentRecordId IN :woIdSet) AND (Status != 'New')
                                         ORDER BY SchedStartTime];
      String mapKey;
      // Build the return payload
      Map<String, List<srvApptWrapper>> companyNameSAMap = new Map<String, List<srvApptWrapper>>();
      List<srvTradeWrapper> returnSAList = new List<srvTradeWrapper>();
      if (!saList.isEmpty()) {
        for (ServiceAppointment s : saList) {
          // SA Details for Map
          srvApptWrapper saWrapper = new srvApptWrapper();
          saWrapper.srvApptId = s.Id;
          saWrapper.srvApptNumber = s.AppointmentNumber;
          saWrapper.srvApptStatus = s.Status;
          saWrapper.srvApptSubject = s.Subject;
          saWrapper.workTypeName = s.workType.Name;
          saWrapper.schedStart = s.SchedStartTime;
          saWrapper.schedFinish = s.SchedEndTime;
          saWrapper.earliestStart = s.EarliestStartTime;
          saWrapper.dueDate = s.DueDate;
          saWrapper.assignedToName = s.Service_Resource__r.Name;
          saWrapper.assignedToNameTier2 = s.Tier_2_Trade_Account__r.Name;
          saWrapper.committedStartDate = s.Committed_Start_Date__c;
          saWrapper.committedEndDate   = s.Committed_End_Date__c;
          saWrapper.capacityBased      = s.Service_Resource__r.IsCapacityBased;
          saWrapper.resourceName = s.Service_Resource__r.RelatedRecord.Name;
          if(s.Service_Resource__r.RelatedRecord.Contact.Account.Name != null) {
              mapKey = s.Service_Resource__r.RelatedRecord.Contact.Account.Name.toUpperCase(); 
          } else if(s.Tier_2_Trade_Account__r.Name != null) {
              mapKey = s.Tier_2_Trade_Account__r.Name.toUpperCase();
          } else if(s.Service_Resource__r.RelatedRecord.CompanyName != null) {
              mapKey = s.Service_Resource__r.RelatedRecord.CompanyName.toUpperCase();
          } else if(s.Service_Resource__r.ServiceCrew.Service_Crew_Company_Name__c != null) {   
              mapKey = s.Service_Resource__r.ServiceCrew.Service_Crew_Company_Name__c.toUpperCase();
          } 
          if(mapKey != null) {
            mapKey += (',' + s.workType.Name);
            mapKey += (',' + s.ParentRecordId);
            mapKey += ',' + (s.Tier_2_Trade_Account__c != null ? 'Tier 2' : s.Service_Resource__r.IsCapacityBased ? 'Capacity' : 'Calendar');
            System.debug('Nikhil mapKey '+mapKey);
            if(!companyNameSAMap.containsKey(mapKey)) {
                companyNameSAMap.put(mapKey, new List<srvApptWrapper>());
            }
            System.Debug(LoggingLevel.DEBUG, '***saWrapper = ' + saWrapper);
            // check if we already have a map entry for Work Type
            List<srvApptWrapper> sw = companyNameSAMap.get(mapKey);
            sw.add(saWrapper);
            companyNameSAMap.put(mapKey, sw);
          }
        }
    }
    // now build wrapper return payload
    Set<String> keyList = companyNameSAMap.keySet();
    for (String key : keyList) {
      srvTradeWrapper tempWrapper = new srvTradeWrapper();
      List<String> values = key.split(',');
      List<srvApptWrapper> tempList = companyNameSAMap.get(key);
      if(values.size() > 3) {
        tempWrapper.resourceType = values[3];
      }
      if(values.size() > 1) {
          tempWrapper.workTypeName = values[1];
      }
      if(values.size() > 0) {
          if(NJ_Constants.ASSREASSSET.contains(tempWrapper.workTypeName)) {
              if(!tempList.isEmpty()) {
                tempWrapper.accName = tempList[0].resourceName;
              }
          } else {
              tempWrapper.accName = values[0];
          }
      }
      tempWrapper.srvApptList = tempList;
      returnSAList.add(tempWrapper);
    } 
    return returnSAList;
    } else {
      // no Work Orders for Claim
      return null;    
    }
  }
  
  
  /*** 
   * Class Name:      srvTradeWrapper
   * Author:          Stephen Moss
   * Created Date:    09/08/2018
   * Description:     Wrapper Class for Service Appointment return list
   ***/
  private class srvTradeWrapper {
    // default no argument constructor
    private srvTradeWrapper() {
        srvApptList = new List<srvApptWrapper>();
    }
    
    // Wrapper Class Properties
    @AuraEnabled
    String accName { get; set; }
    @AuraEnabled 
    String workTypeName { get; set; }
    @AuraEnabled
    String resourceType { get; set; }
    @AuraEnabled
    List<srvApptWrapper> srvApptList {get; set;}           
  }

  
  
  
  /*** 
   * Class Name:      srvApptWrapper
   * Author:          Stephen Moss
   * Created Date:    09/08/2018
   * Description:     Wrapper Class for Service Appointment
   ***/
  private class srvApptWrapper {
    // default no argument constructor
    private srvApptWrapper() {
        
    }
    
    // Wrapper Class Properties
    @AuraEnabled
    Id srvApptId { get; set; }
    @AuraEnabled
    String srvApptNumber { get; set; }
    @AuraEnabled
    String srvApptStatus { get; set; }
    @AuraEnabled
    String srvApptSubject { get; set; }
    @AuraEnabled
    String workTypeName { get; set; }
    @AuraEnabled
    String assignedToName { get; set; }
    @AuraEnabled
    String assignedToNameTier2 { get; set; } // For Tier 2 Trade
    @AuraEnabled
    DateTime committedStartDate { get; set; } // For Tier 2 Trade
    @AuraEnabled
    DateTime committedEndDate { get; set; } // For Tier 2 Trade
    @AuraEnabled
    DateTime schedStart { get; set; }
    @AuraEnabled
    DateTime schedFinish { get; set; }
    @AuraEnabled
    DateTime earliestStart { get; set; }
    @AuraEnabled
    DateTime dueDate { get; set; }
    @AuraEnabled
    Boolean capacityBased { get; set; }
    @AuraEnabled
    String resourceName { get; set; }
  }
}