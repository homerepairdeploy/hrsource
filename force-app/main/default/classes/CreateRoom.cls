public with sharing class CreateRoom {
    @AuraEnabled
    public static Room__c createNewRoom(Room__c room, String worecordId){
        try{
          List<WorkOrder> woList = [SELECT id, caseId
                                      FROM WorkOrder 
                                      WHERE Id =:worecordId];
            string claimId = woList[0].caseId;
            room.Claim__c = claimId;
            if(room.Name != '')
                insert room;
				return room;            
        }
        catch (System.Exception se) {
        }
        finally {}  
        return null;
    }      
}