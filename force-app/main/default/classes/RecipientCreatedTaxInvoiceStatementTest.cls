@IsTest
private class RecipientCreatedTaxInvoiceStatementTest {
    @IsTest
    static void RCTIPDF(){

        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Test');   
        //create a Claim Authority(Added by CRMIT)
        case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id);
        AP_Invoice__c invoice = HomeRepairTestDataFactory.createAPInvoiceWithWorkOrder(wo.id);
        
        Test.startTest();
        RecipientCreatedTaxInvoiceStatement.RecipientCreatedTaxInvoiceStatement(new List<String>{invoice.Id});
        Test.stopTest();

        List<Attachment> at = [SELECT Id FROM Attachment];

        system.assertEquals(1, at.size(), 'RCTI PDF is Generated');
        
    }
}