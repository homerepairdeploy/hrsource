@isTest
public class OnceOffUpdateVendorTest {
    @testSetup 
    static void setup(){
        Home_Repairs_Trigger_Switch__c hrts = HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        List<String> ids=new List<String>();
		 //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        Claim_Job__c cj=HomeRepairTestDataFactory.createClaimJob(cs.Id);
        
        
    }  
    public static testmethod void testBatchOnceOffUpdateVendorNotes() { 
            case c=[select id,cjid__c from case limit 1];
            claim_job__c cj=[select id from claim_job__c limit 1];
            c.CJId__c=cj.id;
            update c;
            Vendor_Note__c  t=new Vendor_Note__c();
            t.Claim_Job__c =cj.id;
			t.Claim__c=c.id;
            insert t;
            Test.startTest(); 
               
                database.executeBatch(new OnceOffUpdateVendorNote(),1);
               
            Test.stopTest();
        
    }
    
}