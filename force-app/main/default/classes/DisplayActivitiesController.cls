public class DisplayActivitiesController {
    @AuraEnabled
    Public static List<TaskWrapper> getActivities(Id recordId){
        List<ActionWrapper> awList = DisplayActivitiesController.getActions();
        List<TaskWrapper> twList = new List<TaskWrapper>();
        System.debug(recordId);
        if(recordId != null)
        {
            List<Task> taskList = [select id,subject,Description,ActivityDate,status,createddate,WhatId,Owner.Name from task where WhatId =: recordId ORDER BY status,createddate DESC];
            Map<String,List<Task>> taskMap = new Map<String,List<Task>>();
            List<Task> pendingTasks = new List<Task>();
            if(taskList.size()>0){
                for(Task t : taskList){
                    if(t.Status != 'Completed' && t.Status != 'Closed'){
                        pendingTasks.add(t);
                        taskMap.put('Upcoming and Overdue',pendingTasks);
                    }
                    Integer month;
                    Integer year;
                    String monthName;
                    String convertedDate;
                    if(t.ActivityDate != null)
                    {
                        month = t.ActivityDate.Month();
                        year = t.ActivityDate.year();
                        monthName =  convertMonthTextToNumber(month);
                        convertedDate = monthName+'-'+year;
                    }else{
                        month = t.CreatedDate.Month();
                        year = t.CreatedDate.year();
                        monthName =  convertMonthTextToNumber(month);
                        convertedDate = monthName+'-'+year;
                    }
                    if(t.Status == 'Completed')
                    {
                        if(taskMap.get(convertedDate) !=null){
                            List<Task> tempTaskList = new List<Task>();
                            tempTaskList.add(t);
                            for(Task tempTask : taskMap.get(convertedDate)){
                                tempTaskList.add(tempTask);
                            }
                            taskMap.put(convertedDate,tempTaskList);
                        }else{
                            List<Task> tempTaskList = new List<Task>();
                            tempTaskList.add(t);
                            taskMap.put(convertedDate,tempTaskList);
                        }
                    }
                    
                }
                if(taskMap != null)
                {
                    for(string s : taskMap.keyset()){
                        TaskWrapper tw = new TaskWrapper();
                        tw.taskDate = s;
                        tw.taskList = taskMap.get(s);
                        tw.taskCount = taskMap.get(s).size();
                        tw.awList = awList;
                        twList.add(tw);
                    }
                }
            }
        }
        return twList;
    }
    Public class ActionWrapper{
        @AuraEnabled
        Public String actionName{get;set;}
        @AuraEnabled
        Public String actionType{get;set;}
        
    }
    Public static List<ActionWrapper> getActions(){
        List<ActionWrapper> awList = new List<ActionWrapper>();
        String pageLayout = 'Task-Task Layout';
        List<Metadata.Metadata> layouts = 
            Metadata.Operations.retrieve(Metadata.MetadataType.Layout, 
                                         new List<String> {pageLayout});
        Metadata.Layout layoutMd = (Metadata.Layout) layouts.get(0);
        if(layoutMd.platformActionList.platformActionListItems != null)
        {
        for(Metadata.PlatformActionListItem pl : layoutMd.platformActionList.platformActionListItems){
            ActionWrapper aw = new ActionWrapper();
            if(pl.actionName.contains('.'))
                aw.actionName = pl.actionName.substringAfter('.');
            else
                aw.actionName = pl.actionName;
            aw.actionType = String.valueOf(pl.actionType)+':'+aw.actionName;
            awList.add(aw);
        }
        }
        System.debug(awList);
        return awList;
    }
    Public class TaskWrapper{
        @AuraEnabled
        Public String taskDate{get;set;}
        @AuraEnabled
        Public List<Task> taskList{get;set;}
        @AuraEnabled
        Public Integer taskCount{get;set;}
        @AuraEnabled
        Public List<ActionWrapper> awList{get;set;}
        
    }
    @AuraEnabled
    Public static String updateTask(Task tsk,String Status){
        String returnMessage = '';
        try{
            tsk.Status = Status;
            update tsk;
            returnMessage = 'Task Updated Successfully';
        }catch(Exception e){
            returnMessage = e.getMessage();
        }
        return returnMessage;
    }
    @AuraEnabled
    Public Static List<String> getTaskPickLListValues(){
        List<String> picklistValues = new List<String>();
        String objectName = 'Task';
        String fieldName ='Status';
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            picklistValues.add(pickListVal.getValue());
            System.debug(pickListVal.getLabel() +' '+pickListVal.getValue());
        }  
        return picklistValues;
    }  
    Public static String convertMonthTextToNumber(Integer monthNumber){
        if(monthNumber == 1){
            return 'January';   
        }else if(monthNumber == 2){
            return 'February';   
        }else if(monthNumber == 3){
            return 'March';   
        }else if(monthNumber == 4){
            return 'April';   
        }else if(monthNumber == 5){
            return 'May';       
        }else if(monthNumber == 6){            
            return 'June';   
        }else if(monthNumber == 7){
            return 'July';   
        }else if(monthNumber == 8){
            return 'August';   
        }else if(monthNumber == 9){
            return 'September';   
        }else if(monthNumber == 10){
            return  'October';  
        }else if(monthNumber == 11){
            return 'November';  
        }else{
            return 'December';
        }
    } 
}