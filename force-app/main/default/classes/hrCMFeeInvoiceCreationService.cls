/************************************************************************************************************
Name: hrCMFeeInvoiceCreationService
=============================================================================================================
Purpose: Class having re-usable logic for CM Fees invoice creation.
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL        DESCRIPTION
1.0        xx                xx                 xx       HomeRepair - Finance Module
*************************************************************************************************************/

public with sharing class hrCMFeeInvoiceCreationService {
    
    /***************************************************************** 
Purpose: Method to create CM Fees Invoice records from ClaimJob.                                                       
Parameters: Map of ClaimJob ID & Status.
Returns: none
Throws [Exceptions]: None                                                          
History                                                             
--------                                                            
VERSION    AUTHOR           DATE             DETAIL        DESCRIPTION
1.0        xx                xx                 xx       HomeRepair - Finance Module  
*******************************************************************/
    public static void createInvoiceRecord(Map<ID,String> claimAuthorityRecs) {
        system.debug('ClaimJob KeySet' + claimAuthorityRecs.keySet()); 
        system.debug('111case records====' + claimAuthorityRecs); 
        
        
        try {
            //Added By CRMIT instead of Claim_job repalced with Case 
            //And removed the Name filed from the query
            Id hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
            List<Case> caseChildList = [Select ID,RecordTypeId,ParentId,Claim_Number__c,Claim_Type__c,Status, Job_Type__c,State__c, Parent.Policy__r.State__c
                                        FROM Case  
                                        WHERE RecordTypeId =:hrAuthorityRecId AND Id =: claimAuthorityRecs.keySet()
                                       ];
            system.debug('111case record infooo=======' + caseChildList); 
            system.debug('111case record infooo=======' + caseChildList.size()); 
            List<case> claimList = new List<case>();
            Map<String,AR_Invoice__c> invMap = new Map<String,AR_Invoice__c>();
            Map<String,AR_SOW__c> invLIMap = new Map<String,AR_SOW__c>();                  
            Map<String,AR_Invoice__c> cmFeeInvMap = new Map<String,AR_Invoice__c>();                  
            Map<string, String> codeMap = new Map<string, String>();    
            Map<string, String> codeMapGST = new Map<string, String>();
            List<AR_Distribution_Code__mdt> distributionSet = [SELECT Id, Distribution_Set_GST__c, Distribution_Set_Total__c, Invoice_Type__c, 
                                                               Job_Type__c,State__c FROM AR_Distribution_Code__mdt WHERE Invoice_Type__c ='CM Fee'];
            system.debug('111distributionSet=======' +distributionSet); 
            
            for(AR_Distribution_Code__mdt code : distributionSet) {
                String key = code.Job_Type__c+''+code.State__c;            
                codeMap.put(code.State__c, code.Distribution_Set_Total__c);
                codeMapGST.put(code.State__c, code.Distribution_Set_GST__c);
            }
            system.debug('111codeMap=======>'+codeMap);
            system.debug('111codeMapGST=======>'+codeMapGST);
            
            //Added by CRMIT(Replaced the Claim_Job with Case object)
            for(Case cj : caseChildList) {                                       
                AR_Invoice__c InvToCreate = new AR_Invoice__c();
                //Added BY Crmit( InvToCreate.Authority__c = cj.Id;InvToCreate.Claim__c=cj.ParentId;)
                InvToCreate.Authority__c = cj.Id;
                InvToCreate.Claim__c=cj.ParentId;
                InvToCreate.Status__c = 'Submitted';
                InvToCreate.Invoice_Type__c = 'CM Fees';
                InvToCreate.Oracle_Process_Status__c = 'Pending';
                InvToCreate.Generate_Invoice_Statement__c = true;    
                String key = cj.Job_Type__c+''+cj.State__c; 
                system.debug('key==>'+cj.State__c);
                //Updated By CRMIT
                InvToCreate.Distribution_Set_Total_ex_GST__c = codeMap.get(cj.Parent.Policy__r.State__c);
                InvToCreate.Distribution_Set_GST__c  = codeMapGST.get(cj.Parent.Policy__r.State__c);
                system.debug('InvToCreate.Distribution_Set_Total_ex_GST__c==>'+InvToCreate.Distribution_Set_Total_ex_GST__c);              
                cmFeeInvMap.put(cj.Id,InvToCreate);
                system.debug('111invoice records map here=======>'+cmFeeInvMap);
                //Added by CRMIT
                //Passing parent id of child case for update purpose(Id=cj.Claim__c replaced by Id = cj.parentId)
                claimList.add(new case(Id = cj.parentId, CM_Fees_Created__c = true));
                system.debug('111claimList parent updated list here=======>'+claimList);
                
                
            }
            if (cmFeeInvMap.size() > 0){
                insert cmFeeInvMap.Values();                      
            }
            
            //Get CMFee Product Details,Create Invoice Line Item & Associate to Invoice
            List<Product2> cmFeesLineItem = [SELECT Full_Product_Name__c,IsActive,Name,ProductCode 
                                             FROM Product2 where ProductCode = 'CMFEES' and IsActive = true
                                            ];                      
            
            for(Product2 cmfli : cmFeesLineItem) {
                AR_SOW__c InvLiItemToCreate = new AR_SOW__c();
                InvLiItemToCreate.Scope_Of_Work__c = cmfli.Full_Product_Name__c;                     
                
                //Get newly created CMFee InvoiceId and associated to Line Item
                for(String cmFeeInvId : cmFeeInvMap.keySet()){
                    InvLiItemToCreate.AR_Invoice__c = cmFeeInvMap.get(cmFeeInvId).Id;
                    system.debug('createdInvoice ' + InvLiItemToCreate.AR_Invoice__c);
                }             
                invLIMap.put(cmfli.Id,InvLiItemToCreate);
                system.debug('111createdInvoice line item  ' + invLIMap);
                
                
            }
            if (invLIMap.size() > 0){
                insert invLIMap.Values();
            }
            //Added by CRMIT (claimList.size() > 0)
            if (claimList.size() > 0){
                update claimList; 
            }
            //Update CMFeeInvCheckFlag in WO to restrict creation of more than one supplier invoice            
                        /*     List<WorkOrder> wotoUpdate = [select id, CMFeeInvCheck__c from WorkOrder where Id =: workorderRecords.keySet()];
            if(wotoUpdate != null)
            { 
            for (WorkOrder woCmf : wotoUpdate)
            {
            woCmf.CMFeeInvCheck__c = true;
            update wotoUpdate;                            
            }
            }*/
            
            
        }// try ends
        catch (System.QueryException se) {
        }
        finally {}
    }//createInvoiceRecord ends
}//hrCMFeeInvoiceCreationService ends