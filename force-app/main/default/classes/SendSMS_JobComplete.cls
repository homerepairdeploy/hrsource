/**
 * Created by Nick on 30/04/2020.
 */

public without sharing class SendSMS_JobComplete {
    @InvocableMethod
    public static void sendSMSJobComplete(List<WorkOrder> workOrders) {

        Set<Id> woIds = new Set<Id>();
        for (WorkOrder wo : workOrders) {
            woIds.add(wo.Id);
        }

        List<WorkOrder> workOrderList = [SELECT Id, WorkOrderNumber, Service_Resource_Company__c FROM WorkOrder WHERE Id IN :woIds];
        System.debug('NFDebug workOrderList = ' + workOrderList);

        Set<Id> srCompanyIdSet = new Set<Id>();
        for (WorkOrder wo : workOrderList) {
            srCompanyIdSet.add(wo.Service_Resource_Company__c);
        }
        System.debug('NFDebug srCompanyIdSet = ' + srCompanyIdSet);

        List<User> userList = [SELECT Id, ContactId, Contact.AccountId, Contact.Phone, Contact.MobilePhone, Profile.Name FROM User WHERE Contact.AccountId IN :srCompanyIdSet AND (Profile.Name = 'Manager User Profile' OR Profile.Name = 'Restricted User Profile')];
        Map<Id, WorkOrder> mapContactIdVsWorkOrder = new Map<Id, WorkOrder>();

        for (WorkOrder wo : workOrderList) {
            for (User u : userList) {
                if (wo.Service_Resource_Company__c == u.Contact.AccountId) {
                    mapContactIdVsWorkOrder.put(u.ContactId, wo);
                }
            }
        }
        System.debug('NFDebug mapContactIdVsWorkOrder = ' + mapContactIdVsWorkOrder);

        String durableId = [SELECT Id, Name, Status FROM Site WHERE Name = 'RepairForce_Trade_Platform' AND Status = 'Active' LIMIT 1].Id;
        String baseURL = [SELECT id, SecureUrl from SiteDetail where DurableId = :durableId LIMIT 1].SecureUrl;

        for (Contact c : [SELECT Id, MobilePhone FROM Contact WHERE Id IN :mapContactIdVsWorkOrder.keySet() AND MobilePhone != '']) {
            WorkOrder wo = mapContactIdVsWorkOrder.get(c.Id);
            System.debug('NFDebug wo = ' + wo);

            String phoneNumber = c.MobilePhone;
            String smsBody = 'Hi Team,%0a' +
                    'Please be advised that the status on WO ' + wo.WorkOrderNumber + ' has been updated to "Job Complete".%0a' +
                    'If satisfied repairs per this WO have been completed, you can now submit your invoice through the Trade Portal.%0a' +
                    baseURL + '/' + wo.Id + '%0a' +
                    'Thanks, Home Repair';

            try {
                SMSUtility.processSmsFuture(smsBody, phoneNumber);
                //if(workOrderList[0].Id != null && res.isSuccess) {
                    //ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), workOrderList[0].Id, ConnectApi.FeedElementType.FeedItem, 'Text Message Sent: ' + smsBody);
                //}
            }
            catch (Exception e) {
                system.debug('Exception Occurred ==> ' + e.getMessage() + ' :: StackTrace ==> ' + e.getStackTraceString());
            }
        }
    }
	
	public static void checkTest(){
     Integer i=0;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
        i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
       
    
    
    }
}