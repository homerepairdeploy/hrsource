@isTest
public class HomeRepairUtilTest {
    public static final Id hrAuthorityRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Authority').getRecordTypeId();
    public static final Id hrClaimRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Claim').getRecordTypeId();
    
    @TestSetup
    static void TestdataCreateMethod() {
        //Create the Account Record.
        Account acc = HomeRepairTestDataFactory.createAccounts('Test WorkOrder Account')[0];        
        //Create the Contact Record.
        contact conVar = HomeRepairTestDataFactory.createTradeContact('test WO Contatct',acc.Id);        
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);                
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        Case childCaseObj = HomeRepairTestDataFactory.createChildClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrdersWithCase('Home Repair Trades',cs.id,wt.id,childCaseObj.id);
        List<WorkOrderLineItem> woliList=HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1);
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
        Room__c r = HomeRepairTestDataFactory.createRoom(cs.Id,woliList);
        Attachment attach=new Attachment();   	
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=conVar.id;
        insert attach;
    }
    @isTest
    static void createServiceAppointmentTest(){
        test.startTest();
        Attachment at = [select id,parentId,body,Name,Description,ContentType from Attachment];
        List<Attachment> attachments = new List<Attachment>();
        attachments.add(at);
        Set<String> parentId = new Set<String>();
        parentId.add(at.parentId);
        Case c = [select id from case where RecordTypeId =: hrClaimRecId];
        Case authority = [select id from case where RecordTypeId =: hrAuthorityRecId LIMIT 1];
        ContentVersion cvList = [SELECT Id, Title, ContentDocumentId FROM ContentVersion LIMIT 1];
        Room__c r = [Select id,Claim__c from Room__c];
        
        Set<String> roomId = new Set<String>();
        roomId.add(r.Id);
        Set<String> contentDocumentIds = new Set<String>();
        contentDocumentIds.add(cvList.ContentDocumentId);
        WorkOrder wo = [Select id,CaseId,WorkTypeId,status,Authority__c from WorkOrder];
        Set<String> objectId = new Set<String>();
        objectId.add(wo.id);
        List<HomeRepairUtil.contentWrapper> cwList  = new List<HomeRepairUtil.contentWrapper>();
        HomeRepairUtil.contentWrapper cw = new HomeRepairUtil.contentWrapper(at.Name,String.valueOf(at.Body),String.valueOf(cvList.ContentDocumentId));
        cwList.add(cw);
        HomeRepairUtil.createServiceAppointment(wo.id);
        HomeRepairUtil.createTaskTradePortal(wo.id,'Invoice',wo.CaseId);
        HomeRepairUtil.contentDocumentList(objectId);
        HomeRepairUtil.updateContentFile('Invoice',true,wo.Id,true,cvList.ContentDocumentId,c.Id,authority.Id);
        HomeRepairUtil.doUploadContentFromAttachment(attachments,parentId);
        HomeRepairUtil.doUploadContent(at.ParentId,wo.id,cwList);
        
        HomeRepairUtil.deleteContentVersion(contentDocumentIds);
        HomeRepairUtil.deleteRepairItems(roomId,'Room__c');
        HomeRepairUtil.deleteRepairItems(objectId,'WorkOrder');
        HomeRepairUtil.getDependentOptions('case','Building_Restoration__c','Building_Restoration_Type__c');
        test.stopTest();
        //negative scenario
        // HomeRepairUtil.deleteRepairItems(objectId,'WorkOrders');
        
    }
}