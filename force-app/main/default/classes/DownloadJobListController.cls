/*********************************************************************************************************
* Purpose: Controller for Lightning Component DownloadJobList.cmp. 
*          Helps download the Open Work Order for Trades in an xls file.
* Test Class: DownloadJobListControllerTest
* Returns: String
*                                                           
* History                                                             
* --------                                                            
* VERSION        AUTHOR                DATE           DETAIL       Description 
* 1.0            Harpreet Dhillon      20/07/2019     Created     
**********************************************************************************************************/


public class DownloadJobListController {
    
    //The init method of DownloadJobList component to get the related Work Order and Work Order Lines data 
    //in an HTML format
    @AuraEnabled
    public static string getRelatedData(String accId)
    {
    
        //A check for whether there is data to be populated or not
        boolean hasData = false;
        
        //Set of all Open WorkOrder Status values
        Set<String> openWOStatuses    = new Set<String>{'Work Order Assigned','Accepted WO','Customer Contacted','Work Commenced'};
        
        //String which will return all the data in an HTML table format, if there is data
        String htmlBody               = '';
        
        //The workOrder header in the table. Has to be added again in case the last record had a list of WorkOrderLineItems
        String woHeader               = '<tr style="background-color:#dddddd"><th>Serial No.</th><th>Work Order Number</th><th>Claim</th><th>Claim Job Type</th>'
                                      +'<th>Work Type Name</th><th>Status</th><th>CreatedDate</th><th>Claim Contact</th>'
                                      +'<th>Risk Address</th><th>Up To Amount</th></tr>';
        
        //The list of all the WorkOrder records and their associated WorkOrderLineItems
        List<WorkOrder> workOrderList = [Select Id, Account.Name, WorkOrderNumber, Claim__c, Service_Resource_Company__c,
                                         Claim_Job_Type__c, Work_Type_Name__c, Status, CreatedDate, Claim_Contact__c, 
                                         Risk_Address1__c, Up_To_Amount__c,Service_Resource_Company__r.Name,
                                                 (Select Site_Visit_Number__c, Sort_Order__c, Service_Appointment__c, 
                                                 Service_Appointment__r.AppointmentNumber, Product_Description__c, Room__c, 
                                                 Room__r.Name, Product_Code__c, Quantity, Labour_Time__c 
                                                 FROM WorkOrderLineItems)
                                         FROM WorkOrder WHERE Service_Resource_Company__c=:accId AND Status in: openWOStatuses 
                                         ORDER BY WorkOrderNumber];
        
        //progress only if there are Work Order records to process
        if(workOrderList.size()!=0)
        {
            hasData = true;
            Integer sNo = 0;
            //A check for whether the Last Work Order had a WOLI related to it or not
            boolean hadWoli = false;
            
            //create html table..
            htmlBody = '<table border="1" bordercolor="aaaaaa" style="border-collapse: collapse"><caption><h3>Job List Data: ' 
                     + workOrderList[0].Service_Resource_Company__r.Name + '</h3></caption>' + woHeader;

            //iterate over the WorkOrderlist and output columns/data into table rows...
            for(WorkOrder wo : workOrderList)
            {
                //get the values of the required fields in variables, and check them for not null values
                String workOrderNumber           = wo.WorkOrderNumber;
                String claim                     = wo.Claim__c==null? '' : wo.Claim__c;
                String claimJobType              = wo.Claim_Job_Type__c==null? '' : wo.Claim_Job_Type__c;
                String workTypeName              = wo.Work_Type_Name__c==null? '' : wo.Work_Type_Name__c;
                String status                    = wo.Status;
                String createdDate               = String.valueOf(wo.CreatedDate.day()) + '-'
                                                   + String.valueOf(wo.CreatedDate.month()) + '-' + String.valueOf(wo.CreatedDate.year());
                String claimContact              = wo.Claim_Contact__c==null? ''  : wo.Claim_Contact__c;
                String riskAddress1              = wo.Risk_Address1__c==null? ''  : wo.Risk_Address1__c;
                String upToAmount                = wo.Up_To_Amount__c==null? ''     : '$ '+ String.valueOf(wo.Up_To_Amount__c);

                //If the last WorkOrder had WorkOrderLineItems, add another row for WorkOrder Header in the table
                if(hadWoli)
                {
                    hadWoli=false;
                    htmlBody +=woHeader;
                }
                //increment the serial number
                sNo++;
                //Populate the values of the reuired WorkOrder fields in a new table row
                htmlBody += '<tr><td>' + sNo + '</td><td style="vnd.ms-excel.numberformat:00000000">' + workOrderNumber + '</td><td>' + claim + '</td><td>' + claimJobType + '</td><td>' + workTypeName 
                         + '</td><td>' + status + '</td><td>' + createdDate + '</td><td>' + claimContact + '</td><td>' + riskAddress1 + '</td><td>' 
                         + upToAmount + '</td></tr>';
            
            }
            
            
            //close the HTML table...
            htmlBody += '</table>';
            
            //Add the footer to the table
            htmlBody+='<table><caption><div style="border:0px; font-size:80%;">COPYRIGHT 2019 HOMEREPAIR. ABN 51 603 320 314</div></caption>'
                    +'<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></table>';

        }
        
        //return the html table if there were WorkOrder records, or a blank value, if it didn't..
        return hasData ? htmlBody : '';
        
    }//end of getRelatedData method
}