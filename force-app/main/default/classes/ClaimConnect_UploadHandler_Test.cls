/********************************************************************************************** 
Name        : ClaimConnect_UploadHandler_Test
Description : Test Class for ClaimConnect_LightningFileUploadHandler
                                                            
VERSION        AUTHOR           DATE           DETAIL      DESCRIPTION   
1.0            Prasanth         25-05-2020     Created     ClaimConnect APP
/***********************************************************************************************/
@isTest
public class ClaimConnect_UploadHandler_Test {
    
    @testSetup static void setup() {
        
        //Create Document Parent Record
        Account acc = new Account(Name='Test Account');
        Insert acc;
        
        //Create Contact Record
        List<Contact> testContacts = new List<Contact>();
        testContacts.add(new Contact(FirstName = 'Test Account', LastName = 'Test Account'));
        insert testContacts;
        
        
        // Create Common Case 
        List<Case> caseList = new List<Case>();
        case caseobj = new case();
        caseobj.ContactId = testContacts[0].id;
        caseobj.AccountId = acc.Id;
        caseobj.Status = 'Working';
        caseList.add(caseobj);
        
        insert caseList;
        
        // Create Room Record
        
        List<Room__c> roomList = new List<Room__c>();
        Room__c SAroom = new Room__c();
        SAroom.Name = 'Room1';
        SAroom.Claim__c = caseList[0].id;
        roomList.add(SAroom);
        
        Insert roomList;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.png';
        cv.VersionData = Blob.valueOf('Test Content');
        //cv.FileExtension='jpg';
        cv.IsMajorVersion = true;
        Insert cv;
        
        //Get Content Documents
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
        
        //Create ContentDocumentLink 
        List<ContentDocumentLink> cdLinkList = new List<ContentDocumentLink>();
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = acc.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        cdLinkList.add(cdl);
        
        ContentDocumentLink cd2 = New ContentDocumentLink();
        cd2.LinkedEntityId = caseList[0].Id;
        cd2.ContentDocumentId = conDocId;
        cd2.shareType = 'V';
        cdLinkList.add(cd2);
        
        ContentDocumentLink cd3 = New ContentDocumentLink();
        cd3.LinkedEntityId = roomList[0].Id;
        cd3.ContentDocumentId = conDocId;
        cd3.shareType = 'V';
        cdLinkList.add(cd3);
        
        Insert cdLinkList;
        
        List<AWS_Credential__mdt> lstAws=[Select Bucket_Name__c ,Host_Name__c,Root_Path__c  from AWS_Credential__mdt];
        string filepath= 'https://' + lstAws.get(0).Bucket_Name__c + '.' + lstAws.get(0).Host_Name__c + '/' +  lstAws.get(0).Root_Path__c +'/Case/'+String.valueOf(caseList[0].Id).substring(0, 15)+'%20-%20'+userinfo.getFirstName()+'/'+'TestDocument.png';

         List<S3_FileStore__c> fileList = new List<S3_FileStore__c>();
          S3_FileStore__c fileStore1=new S3_FileStore__c();
           fileStore1.S3ServerUrl__c=filepath;
           fileStore1.FileName__c='TestDocument.png';
           fileStore1.FileExtension__c='jpg';
           fileStore1.ObjectId__c=roomList[0].Id;
           fileList.add(fileStore1);
        
           insert fileList;
        
    }
    
    @isTest static void getFiles(){
        account acc =[SELECT Id FROM Account WHERE Name = 'Test Account'];
        Test.startTest();
        List<ContentDocument> cdList = ClaimConnect_LightningFileUploadHandler.getFiles(acc.Id);
        system.assertEquals(cdList.size(),1);
        Test.stopTest();    
    }
    
    @isTest static void deleteFiles(){
        ContentVersion acc =[SELECT ContentDocumentId FROM ContentVersion Limit 1];
        Test.startTest();
        ClaimConnect_LightningFileUploadHandler.deleteFiles(acc.ContentDocumentId);
        Test.stopTest();   
    }
    
      @isTest static void getAWSS3Files(){
        account acc =[SELECT Id FROM Account WHERE Name = 'Test Account'];
       List<Case> objCase =[SELECT ID from Case Limit 1];
        Test.startTest();
       Test.setMock(HttpCalloutMock.class, new ClaimConnect_FileMock());  
       ClaimConnect_LightningFileUploadHandler.getAWSS3Files(objCase[0].id);
        Test.stopTest();   
    }
    
     @isTest static void downloadfile(){
        ContentVersion acc =[SELECT ContentDocumentId FROM ContentVersion Limit 1];
        Test.startTest();
         system.debug('<<<reco'+acc.ContentDocumentId);
        ClaimConnect_LightningFileUploadHandler.downloadfile(acc.ContentDocumentId);
        Test.stopTest();   
    }
}