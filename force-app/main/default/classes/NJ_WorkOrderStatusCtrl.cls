/**
 * Controller class for NJ_WorkOrderStatus
 * @CreatedDate : 19/07/2019
 * @Author : Nikhil Jaitly
 */
public with sharing class NJ_WorkOrderStatusCtrl {
  public class WorkOrderInfo {
    @AuraEnabled
    public WorkOrder wo;
    @AuraEnabled
    public List<NJ_SelectOptionLightning> status;
    @AuraEnabled
    public Boolean isValidStatus;
    public WorkOrderInfo() {
      status = new List<NJ_SelectOptionLightning>();
      isValidStatus = false;
    }
  }
  /**
  * init method of NJ_WorkOrderStatus component
  * @CreatedDate : 19/07/2019
  * @Author : Nikhil Jaitly
  */
  @AuraEnabled
  public static WorkOrderInfo initWorkOrderInfo(Id woId){
    WorkOrderInfo woi = new WorkOrderInfo();
    Map<String,String> settings = NJ_Utilities.getSettings();
    for(WorkOrder wor : [SELECT Id, Status, Rejection_Reason__c, Customer_Contacted_Date__c
                         FROM WorkOrder
                         WHERE Id = :woId]) {
      woi.wo = wor;
    }
    for(String str : settings.get('WO_STATUS_COMMUNITY').split(',')) {
      if(woi.wo.Status == str) {
        woi.isValidStatus = true;
      }
      woi.status.add(new NJ_SelectOptionLightning(str, str));
    }
    return woi;
  }
  /**
  * This method is used to save work order
  * @CreatedDate : 19/07/2019
  * @Author : Nikhil Jaitly
  */
  @AuraEnabled
  public static void saveWorkOrder(WorkOrder wo){
    try {
      update wo;
    } catch(Exception ex) {
      throw new AuraException(ex.getMessage());
    }
  }
}