/************************************************************************************************************
Name: hrClaimPhoneUpdateOnContactUpdate
=============================================================================================================
Purpose: Update Phone Fields for all claims associated with contact for each phone update
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL        DESCRIPTION
1.0        Pardha           18/12/2019       Created        HomeRepair 
*************************************************************************************************************/

public class hrClaimPhoneUpdateOnContactUpdate {
    
    @InvocableMethod
    public static void collectCTCforClaimPhoneUpdates(List<Id> CTCIds) {
    
        set<Id> contactIdSet=new set<Id>();
        contactIdSet.addAll(CTCIds);
        If (!system.isFuture() && !system.isBatch()) processUpdateClaimPhonesOnCTCUpdate(contactIdSet);
        
    }

    @future
    public static void processUpdateClaimPhonesOnCTCUpdate(set<Id> contactIdSet){
    
        list<case> updateclaimlst=new list<case>();
        for(Case claim:[select Id,contactId,Contact.Phone,contact.HomePhone, contact.MobilePhone,Home_Phone__c,Customer_Mobile_Phone__c,Customer_Contact_Phone__c from case where contactId=:contactIdSet]){
             
                case c=new case();
                c.Id=claim.Id;
                c.Home_Phone__c=claim.contact.HomePhone;
                c.Customer_Mobile_Phone__c=claim.contact.mobilephone;
                c.Customer_Contact_Phone__c=claim.contact.phone;
                updateclaimlst.add(c);
           
        }
        
        If (updateclaimlst.size() > 0)update updateclaimlst;
            
    }
    
        


    
    
}