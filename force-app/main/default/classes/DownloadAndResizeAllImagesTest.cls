@isTest
public class DownloadAndResizeAllImagesTest {
/*    @testSetup 
    public static void setup() {
        Id strRecordTypeAccId = Schema.SobjectType.Account.getRecordTypeInfosByName().get('Insurance Provider').getRecordTypeId();
        /* Create common test Accounts 
        List<Account> testAccounts = new List<Account>();
        for(Integer i=0;i<2;i++) {
            testAccounts.add(new Account(Name = 'TestAcct'+i));
        }
        insert testAccounts;  
        
        /* Create common test Insurance Accounts 
        List<Account> testAccountInsurance = new List<Account>();
        testAccountInsurance.add(new Account(Name = 'TestAcct',RecordTypeId=strRecordTypeAccId,pds__c='www.google.com'));
        insert testAccountInsurance;     
        
        /* Create common test Contacts 
        List<Contact> testContacts = new List<Contact>();
        for(Integer i=0;i<2;i++) {
            testContacts.add(new Contact(FirstName = 'TestAcct'+i, LastName = 'TestAcct'+i));
        }
        insert testContacts;
        
        /* Create policy record 
        List<Policy__c > policyList = new List<Policy__c >();
        Policy__c policyobj = new Policy__c();
        policyobj.State__c='NSW';
        policyList.add(policyobj);
        insert policyList;
        
        /* Create Common Case 
        List<Case> caseList = new List<Case>();
        case caseobj = new case();
        caseobj.ContactId = testContacts[0].id;
        caseobj.AccountId = testAccounts[0].id;
        caseobj.Status = 'Working';
        caseobj.Policy__c = policyList[0].id;
        caseobj.Excess__c = 300;
        
        case caseobject = new case();
        caseobject.ContactId = testContacts[0].id;
        caseobject.AccountId = testAccounts[0].id;
        caseobject.Status = 'Working';
        caseobject.Policy__c = policyList[0].id;
        caseobject.Excess__c = 300;
        caseList.add(caseobj);
        caseList.add(caseobject);
        
        insert caseList;
        system.debug('caseid'+caseList[0].id);
        
        // Create Insurance Case
        List<Case> caseListIns = new List<Case>();
        case caseobj1 = new case();
        caseobj1.ContactId = testContacts[0].id;
        caseobj1.AccountId = testAccountInsurance[0].id;
        caseobj1.Insurance_Provider__c=testAccountInsurance[0].id;
        caseobj1.Status = 'open';
        caseListIns.add(caseobj1);
        
        insert caseListIns;
        
        // Create Authority Case
        Id strRecordTypeCaseId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Authority').getRecordTypeId();  
        List<Case> Authority = new List<Case>();
        case caseobj2 = new case();
        caseobj2.ContactId = testContacts[0].id;
        caseobj2.AccountId = testAccountInsurance[0].id;
        caseobj2.parentid =caseList[0].id;
        caseobj2.RecordTypeId=strRecordTypeCaseId;
        caseobj2.Status = 'AcceptedinWork';
        caseobj2.Job_Type__c = 'doAndCharge';
        caseobj2.Action_Type__c ='authority';
        
        Authority.add(caseobj2);
        
        insert Authority;
        
        
        // Create Work Type
        List<worktype> tradeList=new List<worktype>();
        WorkType wt = new WorkType(name = 'Assessment',EstimatedDuration=1);
        WorkType wt1 = new WorkType(name = 'Plasterer',EstimatedDuration=1);
        // WorkType wt2 = new WorkType(name = 'MS - Temp Fencing',EstimatedDuration=1);
        
        tradeList.add(wt); tradeList.add(wt1); 
        Insert tradeList;
        
        // Create Work order
        
        List<WorkOrder> workOrderList=new List<WorkOrder>();
        WorkOrder wo = new WorkOrder();
        wo.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Home Repair Trades').getRecordTypeId();                           
        wo.CaseId = caseList[0].id;
        wo.WorkTypeId = tradeList[0].Id ;
        wo.Authority__c = Authority[0].Id;
        wo.ContactId = testContacts[0].id;
        workOrderList.add(wo);
        insert workOrderList;
        
        List<ServiceAppointment> servApptList =new List<ServiceAppointment>();
        ServiceAppointment SA = new ServiceAppointment (status='New',
                                                        ParentRecordId= workOrderList[0].Id,
                                                        Work_Type_Name__c='Assessment',
                                                        Claim__c = caseList[0].id);
        servApptList.add(SA);
        insert servApptList; 
        
        // Create Ba Record
        
        List<Staging_Assessor_ba__c> baList = new List<Staging_Assessor_ba__c>();
        
        Staging_Assessor_ba__c StagingBA = new Staging_Assessor_ba__c();
        
        StagingBA.Case__c = caseList[0].Id;
        StagingBA.Claim_Proceeding__c = 'Non fit';
        StagingBA.Building_Height__c = 'Single';
        StagingBA.Building_Restoration__c = true;
        StagingBA.Building_Restoration_Type_Mould__c = true;
        StagingBA.Insurable__c = true;
        StagingBA.Cash_Settlement__c = false;
        
        baList.add(StagingBA);
        
        Staging_Assessor_ba__c StagingBA1 = new Staging_Assessor_ba__c();
        StagingBA1.Case__c = caseList[0].Id;
        StagingBA1.Claim_Proceeding__c = ' ';
        StagingBA1.Building_Height__c = 'Single';    
        baList.add(StagingBA1);
        
        Staging_Assessor_ba__c StagingBA2 = new Staging_Assessor_ba__c();
        StagingBA2.Case__c = caseList[0].Id;
        StagingBA2.Claim_Proceeding__c = 'Yes';
        StagingBA2.Building_Height__c = 'Single';
        StagingBA2.Building_Restoration__c = true;
        StagingBA2.Building_Restoration_Type_Drying__c = true;
        StagingBA2.Report_Type__c = 'Leak';
        StagingBA2.Insurable__c = true;
        StagingBA2.Cash_Settlement__c = false;
        baList.add(StagingBA2);
        
        Staging_Assessor_ba__c StagingBA3 = new Staging_Assessor_ba__c();
        StagingBA3.Case__c = caseList[0].Id;
        StagingBA3.Claim_Proceeding__c = 'Non fit';
        StagingBA3.Building_Height__c = 'Single';
        StagingBA3.Building_Restoration__c = true;
        StagingBA3.Building_Restoration_Type_Drying__c = true;
        StagingBA3.Insurable__c = true;
        StagingBA3.Cash_Settlement__c = false;
        baList.add(StagingBA3);
        
        Staging_Assessor_ba__c StagingBA4 = new Staging_Assessor_ba__c();
        StagingBA4.Case__c = caseList[1].Id;
        StagingBA4.Claim_Proceeding__c = 'Non fit';
        StagingBA4.Building_Height__c = 'Single';
        StagingBA4.Building_Restoration__c = true;
        StagingBA4.Building_Restoration_Type_Drying__c = true;
        StagingBA4.Insurable__c = true;
        StagingBA4.Cash_Settlement__c = false;
        baList.add(StagingBA4);
        
        Insert baList;
        system.debug('<<<ba'+baList);
        
        // Create Room Record
        
        List<Staging_Assessor_sow_rooms__c> roomList = new List<Staging_Assessor_sow_rooms__c>();
        Staging_Assessor_sow_rooms__c SAroom = new Staging_Assessor_sow_rooms__c();
        SAroom.Name = 'Sun Room';
        SAroom.Staging_Assessor_BA__c = baList[0].id;
        roomList.add(SAroom);
        
        Insert roomList;
        
        // Create Suncorp Room Record
        
        List<Room__c> rmList = new List<Room__c>();
        Room__c room = new Room__c();
        room.Name = 'Sun Room';
        room.Length__c=10;
        room.Height__c=20;
        room.Width__c=10;
        room.Claim__c=caseList[0].id;
        rmList.add(room);
        
        Insert rmList;
        
        Staging_Assessor_sow_rooms__c SAroom1 = new Staging_Assessor_sow_rooms__c();
        SAroom1.Name = 'Sun Room';
        SAroom1.Staging_Assessor_BA__c = baList[3].id;
        SAroom1.UniqueRoomId__c = rmList[0].Id;
        
        Insert SAroom1;
        
        
        // Create RoomItem Record
        
        List<Staging_Assessor_sow_roomitems__c> roomItemList = new List<Staging_Assessor_sow_roomitems__c>();
        Staging_Assessor_sow_roomitems__c roomItem = new Staging_Assessor_sow_roomitems__c();
        roomItem.Work_Type__c = 'Plasterer';
        roomItem.Level_1_Description__c = 'Cornice';
        roomItem.Level_2_Description__c = 'Cove 75mm';
        roomItem.Level_3_Description__c = '7.1 to 10 LM';
        roomItem.Level_4_Description__c = 'Strip out & Replace';
        roomItem.Level_5_Description__c = '-';
        roomItem.Site_Visit_Number__c =1;
        roomItem.Staging_Assessor_SOW_rooms__c = roomList[0].id;
        roomItem.UOM__c ='EA';
        roomItem.Labour_Time__c =60;
        roomItem.Quantity__c=1;
        roomItem.Labour_Price__c=100;
        roomItem.Material_Amount__c=300;
        roomItem.Cash_Settled__c = true;  
        roomItemList.add(roomItem);
        
        Staging_Assessor_sow_roomitems__c roomItem1 = new Staging_Assessor_sow_roomitems__c();
        roomItem1.Work_Type__c = 'Plasterer';
        roomItem1.Level_1_Description__c = 'Cornice';
        roomItem1.Level_2_Description__c = 'Cove 75mm';
        roomItem1.Level_3_Description__c = '7.1 to 10 LM';
        roomItem1.Level_4_Description__c = 'Strip out & Replace';
        roomItem1.Level_5_Description__c = '-';
        roomItem1.Site_Visit_Number__c =1;
        roomItem1.Staging_Assessor_SOW_rooms__c = roomList[0].id;
        roomItem1.UOM__c ='EA';
        roomItem1.Labour_Time__c =60;
        roomItem1.Quantity__c=1;
        roomItem1.Labour_Price__c=100;
        roomItem1.Material_Amount__c=300;
        roomItem1.Cash_Settled__c = false;  
        roomItemList.add(roomItem1);
        
        Insert roomItemList;
        
        // Create Work Code Group Records
        
        List<Work_Code_Group__c> wcGroupList1 = new List<Work_Code_Group__c>();
        Work_Code_Group__c wcGroup1 = new Work_Code_Group__c();
        wcGroup1.Level__c  = 'Level 1';
        wcGroup1.Description__c  ='Cornice';
        wcGroupList1.add(wcGroup1);
        Insert wcGroupList1;
        
        List<Work_Code_Group__c> wcGroupList2 = new List<Work_Code_Group__c>();
        Work_Code_Group__c wcGroup2 = new Work_Code_Group__c();
        wcGroup2.Level__c  = 'Level 2';
        wcGroup2.Description__c  ='Cove 75mm';
        wcGroupList2.add(wcGroup2);
        Insert wcGroupList2;
        
        List<Work_Code_Group__c> wcGroupList3 = new List<Work_Code_Group__c>();
        Work_Code_Group__c wcGroup3 = new Work_Code_Group__c();
        wcGroup3.Level__c  = 'Level 3';
        wcGroup3.Description__c  ='7.1 to 10 LM';
        wcGroupList3.add(wcGroup3);
        Insert wcGroupList3;
        
        List<Work_Code_Group__c> wcGroupList4 = new List<Work_Code_Group__c>();
        Work_Code_Group__c wcGroup4 = new Work_Code_Group__c();
        wcGroup4.Level__c  = 'Level 4';
        wcGroup4.Description__c  ='Strip out & Replace';
        wcGroupList4.add(wcGroup4);
        Insert wcGroupList4;
        
        
        List<Work_Code_Group__c> wcGroupList5 = new List<Work_Code_Group__c>();
        Work_Code_Group__c wcGroup5 = new Work_Code_Group__c();
        wcGroup5.Level__c  = 'Level 5';
        wcGroup5.Description__c  ='-';
        wcGroupList5.add(wcGroup5);
        Insert wcGroupList5;       
        
        // Create Product Record
        
        List<Product2> prodList = new List<Product2>();
        Product2 pb2 = new Product2();
        pb2.Name='Cornice Cove 75mm 7.1 to 10 LM Strip out & Replace -';
        pb2.Level_1__c  =wcGroupList1[0].id;
        pb2.Level_2__c  =wcGroupList2[0].id;
        pb2.Level_3__c  =wcGroupList3[0].id;
        pb2.Level_4__c  =wcGroupList4[0].id;
        pb2.Level_5__c  =wcGroupList5[0].id;
        pb2.Work_Type__c=tradeList[0].id;
        pb2.UOM__c='EA';
        pb2.IsActive = true;
        prodList.add(pb2);
        
        Insert prodList;
        
        system.debug('<<checl'+prodList);
        
        Id pricebookId = Test.getStandardPricebookId();
        
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = prodList[0].Id,
            UnitPrice = 10000, 
            IsActive = true);
        insert standardPrice;
        
        
        List<Pricebook2> pbList1 = new List<Pricebook2>();
        
        Pricebook2 objPb1 = new Pricebook2();
        
        objPb1.Labour_TIME_mins__c  = 90;
        objPb1.State__c  = 'NSW';
        objPb1.Name  = 'Standard Price Book';
        objPb1.IsActive=true;
        
        pbList1.add(objPb1);
        Insert pbList1;
        
        // Create pricebookentry Record
        
        List<pricebookentry> pbeList1 = new List<pricebookentry>();
        
        pricebookentry objPbe1 = new pricebookentry();
        
        objPbe1.UnitPrice =100;
        objPbe1.Material_Price__c =100;
        objPbe1.Labour_Price__c =100;
        objPbe1.IsActive=true;
        objPbe1.Pricebook2Id  = pbList1[0].Id;
        objPbe1.Product2Id  = prodList[0].id;
        
        pbeList1.add(objPbe1);
        Insert pbeList1;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.png';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
        
        //Get Content Documents
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
        
        //Create ContentDocumentLink 
        List<ContentDocumentLink> cdLinkList = new List<ContentDocumentLink>();
        
        ContentDocumentLink cd1 = New ContentDocumentLink();
        cd1.LinkedEntityId = baList[0].id;
        cd1.ContentDocumentId = conDocId;
        cd1.shareType = 'V';
        cdLinkList.add(cd1);
        
        Insert cdLinkList;
        
        AssessmentReports__c asrc=new AssessmentReports__c();
        asrc.Case__c=caseList[0].id;
        asrc.Service_Appointment__c=servApptList[0].id;
        asrc.Staging_Assessor_BA__c=baList[0].id;
        asrc.ReportGenerated__c=false;
        asrc.PhotosUploaded__c=false;
        insert asrc;
        
        
        List<AWS_Credential__mdt> lstAws=[Select Bucket_Name__c ,Host_Name__c,Root_Path__c  from AWS_Credential__mdt];
        string filepath= 'https://' + lstAws.get(0).Bucket_Name__c + '.' + lstAws.get(0).Host_Name__c + '/' +  lstAws.get(0).Root_Path__c +'/Case/'+String.valueOf(caseList[0].Id).substring(0, 15)+'%20-%20'+userinfo.getFirstName()+'/'+'TestDocument.png';
        string filepath1 = 'https://' + 'TestBucket' + '.' + lstAws.get(0).Host_Name__c + '/' +  ' ' +'/Case/'+' '+userinfo.getFirstName()+'/'+'TestDocument.png';
        
        
        List<S3_FileStore__c> FileStoreList = new List<S3_FileStore__c>();
        S3_FileStore__c fStore=new S3_FileStore__c();
        fStore.ContentSize__c = cv.ContentSize;                
        fStore.Description__c = cv.Description;
        fStore.FileExtension__c = cv.fileextension;
        fstore.FileName__c = 'TestImage1.png';
        fStore.ObjectId__c=caseList[0].id;  
        fStore.S3ServerUrl__c= filepath;
        fStore.Requires_Resizing__c=true;
        fStore.Uploaded_date__c = Date.today();
        Insert fStore;    
        
        S3_FileStore__c fStore1=new S3_FileStore__c();
        fStore1.ContentSize__c = cv.ContentSize;                
        fStore1.Description__c = cv.Description;
        fStore1.FileExtension__c = cv.fileextension;
        fstore1.FileName__c = 'TestImage2.png';
        fStore1.ObjectId__c=caseList[1].id;  
        fStore1.S3ServerUrl__c= filepath1;
        fStore1.Requires_Resizing__c=true;
        fStore1.Uploaded_date__c = Date.today();
        Insert fStore1;
        
        //Create Document for reports
        ContentVersion cvn = new ContentVersion();
        cvn.Title = 'TestAssessmentReport'+date.today();
        cvn.PathOnClient = 'TestDocument.pdf';
        cvn.VersionData = Blob.valueOf('Test Content');
        cvn.IsMajorVersion = true;
        Insert cvn;
        
        //Create Conga Templates for report generation
        List<APXTConga4__Conga_Template__c> TemplatesList = new List<APXTConga4__Conga_Template__c>();
        APXTConga4__Conga_Template__c ct = new APXTConga4__Conga_Template__c(APXTConga4__Name__c = 'Priced Assessment Report');
        APXTConga4__Conga_Template__c ct1 = new APXTConga4__Conga_Template__c(APXTConga4__Name__c = 'Unpriced Building Assessment NC');
        APXTConga4__Conga_Template__c ct2 = new APXTConga4__Conga_Template__c(APXTConga4__Name__c = 'Partial Cash Settlement');
        APXTConga4__Conga_Template__c ct3 = new APXTConga4__Conga_Template__c(APXTConga4__Name__c = 'Full Cash Settlement');
        TemplatesList.add(ct); TemplatesList.add(ct1); TemplatesList.add(ct2); TemplatesList.add(ct3);
        insert TemplatesList;
        
        List<APXTConga4__Conga_Solution__c> CongaSolutions = new List<APXTConga4__Conga_Solution__c>();
        APXTConga4__Conga_Solution__c sol = new APXTConga4__Conga_Solution__c();
        sol.APXTConga4__Composer_Parameters__c = '';
        sol.Name = 'Assessment Report';
        
        APXTConga4__Conga_Solution__c sol1 = new APXTConga4__Conga_Solution__c();
        sol1.APXTConga4__Composer_Parameters__c = '';
        sol1.Name = 'Assessment Report No Cost';
        
        CongaSolutions.add(sol);  
        CongaSolutions.add(sol1);
        insert CongaSolutions;
        
        
        String baseUrl = URL.getOrgDomainUrl().toExternalForm();
        //Create records in GeneralSettings custom settings
        GeneralSettings__c genSetting = new GeneralSettings__c();
        if(baseUrl.contains('hrmob')){
            genSetting.Value__c = 'hrmob';}
        else if(baseUrl.contains('hrapptest')){
            genSetting.Value__c = 'hrapptest';}
        else if(baseUrl.contains('hruat')){
            genSetting.Value__c = 'hruat';}
        else if(baseUrl.equals('homerepair')){
            genSetting.Value__c = 'prod';}
        else 
            genSetting.Value__c = ' ';
        genSetting.Key__c = 'Environment';
        genSetting.Name = 'Environment';
        insert genSetting;
    }   
    
    @isTest
    public static void DownloadResizePhotoBaseCall(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DownloadResizeBaseCall_FileMock());
        //DownloadResizePhotoRedirect();
        Test.stopTest();
    }
    
    @isTest
    public static void DownloadResizePhotoRedirect(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DownloadResizeRedirect_FileMock());
        
        Test.stopTest();
    }*/
    
    @IsTest
    Public static void RunTest(){
        DownloadAndResizeAllImages.checkTest();
    }
}