/* =====================================================================================
Type:       Test class
Purpose:    Test cases for CaseTrigger 
========================================================================================*/
@isTest
private class CaseTriggerTest{
    @testSetup 
    static void setup(){
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //insert custom setting GeneralSettings__c
        Home_Repairs_Trigger_Switch__c hrts = HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        //Create the Case Record.
        Case objectOfCase1 = HomeRepairTestDataFactory.createCaseWithPolicyDeclineReasonDetails(po.id,con.Id,'Accidental breakage of glassees','Other');
        Case objectOfCase2 = HomeRepairTestDataFactory.createCaseWithPolicyDeclineReasonDetails(po.id,con.Id,'Accidental breakage of glassees','Other;Gas');
        system.debug('cId1  '   +objectOfCase1.id);
        system.debug(objectOfCase2.id);
        //create the authority claim record
        case au1=HomeRepairTestDataFactory.createClaimAuthority(objectOfCase1.Id);      
        au1.Status = 'New';
        
        case au2=HomeRepairTestDataFactory.createClaimAuthority(objectOfCase2.Id);
        au2.Status = 'New';
        au2.Client_Status__c = 'New';
        
        system.debug(au1);
        //create the Account record
        List <Account> acc = HomeRepairTestDataFactory.createAccounts('AAMI');
        //create the Product2 record
        product2 pro = HomeRepairTestDataFactory.createProductForWorkcode('test [Potential Risks Animal]','OC0255',10.00,'test Price Book');
        //create the Assessment_Report_Item__c record
        Assessment_Report_Item__c ari = HomeRepairTestDataFactory.createAssessmentReportItems(objectOfCase1.id,pro.id);
        // get the ClaimDeclineClause__mdt custom metadata record
        ClaimDeclineClause__mdt ClaimDeclineClause = [SELECT Brand__c, Claim_Decline_Clause__c, Decline_Clause__c, Decline_Reason_Details__c, Id 
                                                      FROM ClaimDeclineClause__mdt
                                                      where MasterLabel='AAMI Accidental breakage of glass'];
        // get the AssessmentReportItemsForMobile__mdt custom metadata record
        List<AssessmentReportItemsForMobile__mdt> AssessmentReportItemsForMobile =  [SELECT ItemDescription__c, Item_Code__c, Type__c, Label, Id 
                                                                                 FROM AssessmentReportItemsForMobile__mdt ];
    }
    
    @IsTest
    static void testUpdateCase() {         
        List<Case> listOfCase = [Select id,(select id from cases) from case where Potential_Risks_To_Trades__c ='Other'];
        system.debug(listOfCase[0].cases);
        List <Account> listOfAccount = [Select id from Account];
        checkRecursive.claimUpdateRun=true;
        listOfCase[0].Insurance_Provider__c = listOfAccount[0].id;
        listOfCase[0].Decline_Reason_Details__c = 'Accidental breakage of glass';
        listOfCase[0].Potential_Risks_To_Trades__c = 'Animal';
        listofCase[0].Job_Type__c = 'doAndCharge';
        listofCase[0].Status = 'Pending';
        listofCase[0].Client_Status__c = 'Pending';
       update listOfCase;
   }
     @IsTest
    static void testDeleteCase() {
        List<Case> listOfCase = [Select id from case where Appoint_Repair_Link_Assessor__c = Null];
        checkRecursive.claimUpdateRun=true;
        listOfCase[1].status = 'Closed';
        listOfCase[1].Appoint_Repair_Link_Assessor__c ='No';
        delete listOfCase;
    }

    @IsTest
    static void testInsertCase() {         
        Id CaseRecId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('HR_Claim').getRecordTypeId();
        case Parent = [SELECT Id FROM Case WHERE RecordTypeId = :CaseRecId LIMIT 1];
        
        checkRecursive.claimUpdateRun=true;
        case au2=HomeRepairTestDataFactory.createClaimAuthority(Parent.Id);
        au2.Status = 'New';
        au2.Client_Status__c = 'New';
        
        au2.Decline_Reason_Details__c = 'Accidental breakage of glass';
       au2.Potential_Risks_To_Trades__c = 'Animal';
        au2.Job_Type__c = 'doAndCharge';
        
       insert au2;
   }
}