/***************************************************************** 
Purpose: Batch job to create future/monthly Capacity records for a 
Service Resource record based on Public Liability Expiry on Account

Test class: BatchCreateCapacityRecordsSchedulerTest
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            Harpreet       2/08/2019      Created        
*******************************************************************/

global class BatchCreateCapacityRecords implements Database.Batchable<sObject>, Database.Stateful 
{

    /************************START METHOD*************************/
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
      String query = 'Select Id, Default_Work_Items_Capacity__c, '
                   +'Account.Public_Liability_Expiry__c, (Select Id, StartDate '
                   +'from ServiceResourceCapacities order by StartDate) '
                   +'from ServiceResource where IsActive=true and IsCapacityBased=true and '
                   +'Account.Public_Liability_Expiry__c > Today and Default_Work_Items_Capacity__c!=null';
      
      System.debug('query '+query); 
      return Database.getQueryLocator(query);
    }
    
    
    /****************************EXECUTE METHOD**********************************/
    global void execute(Database.BatchableContext BC, List<ServiceResource> scope) 
    {
        //the list of ServiceResourceCapacity records to create
        List<ServiceResourceCapacity>  resourceCapacitiesToCreate =  new List<ServiceResourceCapacity>();
        
        //iterate over all ServiceResource returned in query
        for (ServiceResource sResource : scope) 
        {
            //save the StartDates of existing ServiceResourceCapacity records in a set
            Set<Date> startDateSet = new Set<Date>();
            for(ServiceResourceCapacity src: sResource.ServiceResourceCapacities)
            {
                startDateSet.add(src.StartDate);
            }
            
            //calculate the months in between today and the Account's Public Liability expiry
            Integer monthsBetween = Date.Today().monthsBetween(sResource.Account.Public_Liability_Expiry__c);
            
            //create a new StartDate which is first date of this month 
            Date newStartDate     = System.today().toStartOfMonth();
            
            //iterate over the number of months between today and Account's Public Liability expiry
            for(integer i = 0;i <= monthsBetween; i++)
            {
                //add increment date by i months
                newStartDate = newStartDate.addMonths(1);

                //check that the set does not contain the newStartdate
                if(!startDateSet.contains(newStartDate))
                {
                    //create a new date, which is the last date of the month
                    Date endMonthDate = Date.newInstance(newStartDate.Year(), newStartDate.Month(), Date.daysInMonth(newStartDate.year(), newStartDate.month()));

                    //check if the end date of the month hasn't passed the account's public liability expiry
                    if(endMonthDate <= sResource.Account.Public_Liability_Expiry__c)
                    {          
                    
                        //create the ServiceResourceCapacity record
                        ServiceResourceCapacity newSRC = new ServiceResourceCapacity
                        (
                            StartDate            = newStartDate,
                            EndDate              = endMonthDate,
                            TimePeriod           = 'Month',
                            ServiceResourceId    = sResource.Id,
                            CapacityInHours      = 1000,
                            CapacityInWorkItems  = (Integer) sresource.Default_Work_Items_Capacity__c
                            
                        );
                        
                        //add the ServiceResourceCapacity record in the list
                        resourceCapacitiesToCreate.add(newSRC);
                    }
                }
            }
        }//end of service resource loop
        
        //insert the ServiceResourceCapacity records if the list isn't empty
        if(!resourceCapacitiesToCreate.IsEmpty())
        {
            insert resourceCapacitiesToCreate;
        }
    }
    
    /*****************FINISH METHOD******************/
    global void finish(Database.BatchableContext BC) {  
        
    }
    
}