/************************************************************************************************************
Name: ServiceAppointmentHandler
=============================================================================================================
Purpose: Class for ServiceAppointment Trigger as Handler.
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL       DESCRIPTION
1.0        Vasu         4/07/2018        Created       Home Repair Claim System  
*************************************************************************************************************/
public without sharing class ServiceAppointmentHandler implements ITrigger {
    private Map<Id, User> serviceAppointmentUserMap = new Map<Id, User>();
    private List<ServiceAppointment> ServiceAppointmentList = new List<ServiceAppointment>();
 //   private List<TaskWrapper> tskwprList  = new List<TaskWrapper>();
    Private ServiceAppointment saRec;
    public ServiceAppointmentHandler() {}
    public static Set<String> saStatus = new Set<String> { NJ_Constants.SA_STATUS_CONFIRMED, NJ_Constants.SA_STATUS_AWAITINGCONFIRMATION,
        NJ_Constants.SA_STATUS_COMPLETED,
        NJ_Constants.SA_STATUS_CANNOTCOMPLETE,
        NJ_Constants.SA_STATUS_NEW, NJ_Constants.SA_STATUS_CANCELLED
        };
            public static Set<String> woStatus = new Set<String> { NJ_Constants.WO_STATUS_ASSIGNED, NJ_Constants.WO_STATUS_SCHEDULED,
                NJ_Constants.WO_STATUS_ACCEPTED, NJ_Constants.WO_STATUS_REJECTED,
                NJ_Constants.WO_STATUS_WORKCOMMENCED, NJ_Constants.WO_STATUS_WORKCOMPLETE,
                NJ_Constants.WO_STATUS_CLOSED, NJ_Constants.SA_STATUS_NEW, NJ_Constants.WO_STATUS_CANCELLED};
                    public void bulkBefore() {
                        system.debug('Entering SA bulkBefore With Trigger.New');
                        if(Trigger.IsUpdate||Trigger.IsInsert){
                            for(Sobject saTemp : trigger.new){ 
                                saRec = (ServiceAppointment)saTemp; 
                                if(saRec.RecordTypeId == null) {
                                    saRec.RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByName().get('Service Appointment General').getRecordTypeId();
                                }
                                ServiceAppointmentList.add(saRec);
                            }
                        }
                        system.debug('Exiting SA bulkBefore With Trigger.New');
                    }
    public void bulkAfter() {
        system.debug('Entering SA bulkAfter With Trigger.New');
        if(Trigger.IsInsert||Trigger.IsUpdate){  
            
            Set <Id> setServiceResourceId = new Set<Id>();
            Set <Id> setTier2AccountId = new Set<Id>();
            
            ServiceAppointmentList = new List<ServiceAppointment>();
            for(Sobject saTemp : trigger.new){ 
                saRec = (ServiceAppointment)saTemp;
                ServiceAppointmentList.add(saRec);
                If (saRec.Service_Resource__c != null){
                    system.debug('Service Resource Id: '+saRec.Service_Resource__c);
                    setServiceResourceId.add(saRec.Service_Resource__c);
                }else{
                    If (saRec.Tier_2_Trade_Account__c != null){
                        system.debug('Tier 2 Account: '+saRec.Service_Resource__c);
                        setTier2AccountId.add(saRec.Tier_2_Trade_Account__c);
                    }
                }
                
                /*taskWrapper tskObj = new  taskWrapper();
                if(Trigger.IsUpdate) {
                    Map<Id,SObject> oldAppointments1 = Trigger.oldMap;
                    ServiceAppointment oldSA  = (ServiceAppointment)oldAppointments1.get(saRec.ID) ;
                    
                    if(saRec.lastModifiedByProfileName__c == 'Trade Platfrom Community Plus User' &&  saRec.Is_Time_Bound__c ==  False  && 
                       (oldSA.Committed_Start_Date__c != saRec.Committed_Start_Date__c  || oldSA.Committed_End_Date__c != saRec.Committed_End_Date__c))
                    {
                        tskObj.ActivityDate = system.today().addDays(1);
                        tskObj.subject  = saRec.Task_Subject_1__c;
                        tskObj.Priority = 'Important';
                        tskObj.Status = 'Not Started';
                        tskObj.whoIDs =  saRec.Claim__c;  
                        tskObj.assignID = System.Label.HR_Claims;
                        tskObj.TaskType = 'Trade Enquiry';
                        tskwprList.add(tskObj);
                        
                    }
                }
                
                If(saRec.Status == 'Cannot Complete') {
                    tskObj.ActivityDate = system.today().addDays(3);
                    tskObj.subject  = saRec.Task_Subject_2__c;
                    tskObj.Priority = 'Critical';
                    tskObj.Status = 'Not Started';
                    tskObj.whoIDs =  saRec.Claim__c;  
                    tskObj.assignID = System.Label.HR_Claims;
                    tskObj.TaskType = 'Trade Enquiry';
                    tskwprList.add(tskObj);
                    
                }*/
            }
            
            //if(tskwprList.size() > 0 )
               // HomeRepairUtil.createTask(tskwprList);
            //system.debug('tskwprList' + tskwprList);
                
            
            List<Account> accountList=[Select Id,(Select Id from Contacts) from Account where Id IN : setTier2AccountId];
            
            System.debug('SOQL Queries until Line ServiceAPPOINTMENTHANDLER:'+Limits.getQueries()); 
            
            Set <Id> setAccountContactIds = new Set<Id>();
            Map<string,User> accountUserMap = new Map<string,User>();
            for(Account acc : accountList){
                if(acc.Contacts.size() > 0){
                    setAccountContactIds.add(acc.Contacts[0].Id);                    
                }
            }
            
            System.debug('SOQL Queries till this point:'+Limits.getQueries()); 
            List<User> userList=[SELECT Id, ContactId,contact.account.Job_Email_Address__c,AccountId,Email FROM User where contactId IN : setAccountContactIds];
            System.debug('SOQL Queries after User query:'+Limits.getQueries());  
            for(User ur : userList){
                accountUserMap.put(ur.Contact.accountId,ur);
            }
            
            Map<Id,User> mapServiceResourceUser = new Map<Id,User>();
            Map<Id,ServiceResource> mapServiceResource = new Map<Id,ServiceResource >();
            Set<String> serviceCrewIds=new Set<String>();
            Map<String,String> serviceCrewMemberMap=new Map<String,String>();
            for(ServiceResource sr : [Select RelatedRecord.Id,RelatedRecord.AccountId,RelatedRecord.UserRole.Id,RelatedRecord.UserRole.PortalType,ResourceType,ServiceCrewId from ServiceResource where id 
                                      in :setServiceResourceId] ){
                                          if(sr.ServiceCrewId != null){
                                              serviceCrewIds.add(sr.ServiceCrewId);
                                          }
                                      }
            System.debug('SOQL Queries after for Loop:'+Limits.getQueries());  
            
            List<ServiceCrew> serviceCrewList=[select id,(select Id,ServiceResourceId from ServiceCrewMembers where ServiceResourceId != null) from ServiceCrew where id IN : serviceCrewIds];
            
            System.debug('SOQL Queries after query on ServiceCrew:'+Limits.getQueries());                
            
            for(ServiceCrew sc : serviceCrewList){
                if(sc.ServiceCrewMembers.size() > 0){
                    setServiceResourceId.add(sc.ServiceCrewMembers[0].ServiceResourceId);
                    serviceCrewMemberMap.put(sc.Id,sc.ServiceCrewMembers[0].ServiceResourceId);
                }
            }
            for(ServiceResource sr : [Select RelatedRecord.Id,RelatedRecord.AccountId,RelatedRecord.contact.account.Job_Email_Address__c,RelatedRecord.Email,RelatedRecord.UserRole.Id,RelatedRecord.UserRole.PortalType,ResourceType,
                                      ServiceCrewId from ServiceResource where id 
                                      in :setServiceResourceId] ){
                                          mapServiceResourceUser.put(sr.id,sr.RelatedRecord);
                                          mapServiceResource.put(sr.id,sr);
                                      }
            
            system.debug('mapServiceResourceUser :'+mapServiceResourceUser);
            
            for(Sobject saTemp : trigger.new){ 
                saRec = (ServiceAppointment)saTemp;
                If (saRec.Service_Resource__c != null && (saRec.Status ==  NJ_Constants.SA_STATUS_CONFIRMED || saRec.Status ==  NJ_Constants.SA_STATUS_AWAITINGCONFIRMATION)){
                    If (mapServiceResourceUser.containskey(saRec.Service_Resource__c)){
                        if(mapServiceResource.get(saRec.Service_Resource__c).ResourceType=='C'){
                            if(serviceCrewMemberMap.containskey(mapServiceResource.get(saRec.Service_Resource__c).ServiceCrewId)){
                                user u = mapServiceResourceUser.get(serviceCrewMemberMap.get(mapServiceResource.get(saRec.Service_Resource__c).ServiceCrewId));
                                serviceAppointmentUserMap.put(saRec.ParentRecordId,u);                                
                            }
                        }else{
                            user u = mapServiceResourceUser.get(saRec.Service_Resource__c);
                            serviceAppointmentUserMap.put(saRec.ParentRecordId,u);                            
                        }
                    }
                }
                If(saRec.Service_Resource__c == null && saRec.Tier_2_Trade_Account__c != null && (saRec.Status ==  NJ_Constants.SA_STATUS_CONFIRMED || saRec.Status ==  NJ_Constants.SA_STATUS_AWAITINGCONFIRMATION)){
                    if(accountUserMap.containskey(saRec.Tier_2_Trade_Account__c)){
                        user u = accountUserMap.get(saRec.Tier_2_Trade_Account__c);
                        serviceAppointmentUserMap.put(saRec.ParentRecordId,u);                        
                    }
                }
            }
            if(Trigger.isInsert) {
                ServiceAppointmentService.UpdateClaimStatus(ServiceAppointmentList);
            }
            
        }
        
        if(Trigger.isUpdate) {
            Map<Id,SObject> oldAppointments = Trigger.oldMap;
            Map<Id,SObject> newAppointments = Trigger.newMap;
            List<SObject> newAppointment = Trigger.new;
            
            ServiceAppointment oldApp, newApp;
            Set<Id> workOrders = new Set<Id>();
            for(Id sa : newAppointments.keySet()) {
                oldApp = (ServiceAppointment)oldAppointments.get(sa);
                newApp = (ServiceAppointment)newAppointments.get(sa);
                System.debug('oldApp '+oldApp);
                System.debug('newApp '+newApp);
                System.debug('saStatus.contains(newApp.Status) '+saStatus.contains(newApp.Status));
                //Updates workorder status
                if(oldApp.Status != newApp.Status && saStatus.contains(newApp.Status)
                   && !NJ_Constants.ASSESSTYPES.contains(newApp.Work_Type_Name__c)) {
                       workOrders.add(newApp.ParentRecordId);
                   }
            }
            System.debug('workOrders '+workOrders);
            if(!workOrders.isEmpty()) {
                ServiceAppointmentHelper.updateWorkOrder(workOrders);
            }
            // ServiceAppointmentHelper.updateWOStatusToWorkCommencedForFirstSA((List<ServiceAppointment>)newAppointment, (Map<Id,ServiceAppointment>)oldAppointments);
            //ServiceAppointmentHelper.updateSAStatusWhenSAScheduled((List<ServiceAppointment>)newAppointment, (Map<Id,ServiceAppointment>)oldAppointments);
        }   
        system.debug('Exiting bulkAfter With Trigger.New');
    }
    public void beforeInsert(SObject so) {
    } 
    public void afterInsert(SObject so) {
    } 
    public void beforeUpdate(SObject oldSo, SObject so) {
        
    }
    public void beforeUnDelete(SObject so) {} 
    
    public void afterUpdate(SObject oldSo, SObject so) {} 
    public void beforeDelete(SObject so){} 
    public void afterDelete(SObject so) {} 
    public void afterUnDelete(SObject so) {} 
    public void andFinally() {
        system.debug('Entering andFinally from ServiceAppointmentHandler');
        if(Trigger.IsBefore) {
            //assign fields to service appointment
            ServiceAppointmentService.assignFieldsServiceAppointment(ServiceAppointmentList);
        }else{
            if (!serviceAppointmentUserMap.isEmpty()){
                ServiceAppointmentService.updateWorkOrderTradeUserDetails(serviceAppointmentUserMap);
            }
            ServiceAppointmentService.upsertAllServiceAppointments(ServiceAppointmentList,Trigger.oldMap,Trigger.isUpdate);
            
            
            
            
        } 
        system.debug('Exiting andFinally from ServiceAppointmentHandler');
    }               
}