public class ContactHandler implements ITrigger {    
        
    public ContactHandler() {
        system.debug('contact h');
    }
    
    public void bulkBefore() {  
        
        
    }
    public void bulkAfter() { 
       
        
    }
    public void beforeInsert(SObject so) {
        Contact ctc = (Contact)so;
        if (ctc.phone!=null ) ctc.phone=CommonUtilityClass.formatNumber(ctc.phone);
        if (ctc.homephone!=null) ctc.homephone=CommonUtilityClass.formatNumber(ctc.homephone);
        if (ctc.mobilephone!=null)  ctc.mobilephone=CommonUtilityClass.formatNumber(ctc.mobilephone);
    } 
    public void afterInsert(SObject so) {
        
    } 
    public void beforeUpdate(SObject oldSo, SObject so) {
    
        Contact ctc = (Contact)so;
        Contact oldctc=(Contact)oldSo;
        
        if (ctc.phone!=null && ctc.phone!=oldctc.phone) ctc.phone=CommonUtilityClass.formatNumber(ctc.phone);
        if (ctc.homephone!=null && ctc.homephone!=oldctc.homephone) ctc.homephone=CommonUtilityClass.formatNumber(ctc.homephone);
        if (ctc.mobilephone!=null && ctc.mobilephone!=oldctc.mobilephone) ctc.mobilephone=CommonUtilityClass.formatNumber(ctc.mobilephone);
         
    
    
    }
    public void beforeUnDelete(SObject so) {} 
    
    public void afterUpdate(SObject oldSo, SObject so) {
        
    }  
    public void beforeDelete(SObject so){} 
    public void afterDelete(SObject so) {} 
    public void afterUnDelete(SObject so) {} 
    public void andFinally() {
        
        
    }               
}