//wrapper class to store content information of Task
public class taskWrapper {
    public String Subject {get; set;}
    public Date ActivityDate {get; set;}
    public String Priority {get; set;}
    public String Status {get; set;}
    public String whoIDs {get; set;}
    public String whatID {get; set;}
    public String assignID {get; set;} 
    public String TaskType {get; set;} 
    public string claimJob {get; set;}
    public string Description {get; set;}
    public String workOrder {get; set;}
}