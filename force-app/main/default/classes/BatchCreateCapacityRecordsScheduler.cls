global class BatchCreateCapacityRecordsScheduler implements Schedulable {
  global void execute(SchedulableContext sc) {
  BatchCreateCapacityRecords b = new BatchCreateCapacityRecords();  
  database.executebatch(b,200);
  } 
}