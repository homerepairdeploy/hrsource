/***************************************************************** 
Purpose: Generating Assessment Report                                                       
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            HomeRepair    23/06/2020      Created      Home Repair Claim System  
*******************************************************************/
global class GenerateAssessReport implements Database.Batchable<sObject>, Database.allowsCallOuts {
    
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query='select id,Status__c,Case__c,Service_Appointment__c,ErrorMsg__c,ReportGenerated__c from AssessmentReports__c where ReportGenerated__c=false';
        Datetime currTimeMinusFive = Datetime.now().addMinutes(-5);         
        if( !Test.isRunningTest() ) query += ' AND CreatedDate <= :currTimeMinusFive ';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<AssessmentReports__c > scope) {
    
       list<AssessmentReports__c> updtlstreport=new list<AssessmentReports__c>();
       for(AssessmentReports__c asr:scope){
          RESTCongaAttachmentResponseHandler data=ClaimConnect_SummaryController.generateReports(asr.Case__c, asr.Service_Appointment__c);
          if (data.status=='Error'){
             AssessmentReports__c asrone=new AssessmentReports__c();
             asrone.ErrorMsg__c=data.Errorcode+data.Message;
             asrone.id=asr.id;
             asrone.Status__c='Error';
             updtlstreport.add(asrone);
          }
          else if (data.status=='OK'){
          
             AssessmentReports__c asrone=new AssessmentReports__c();
             asrone.id=asr.id;
             asrone.ReportGenerated__c =true;
             asrone.Status__c='Successful';
             updtlstreport.add(asrone);
             string cashSettleDef;
             for(Staging_Assessor_ba__c stgassr:[select id,Cash_Settlement_Reason__c, Case__c from Staging_Assessor_ba__c where Case__c=:asr.Case__c]){
                 if(string.isNotBlank(stgassr.Cash_Settlement_Reason__c)) cashSettleDef=stgassr.Cash_Settlement_Reason__c;
             }
             if(cashSettleDef == 'Partial' || cashSettleDef == 'Full cash details'){
             
               RESTCongaAttachmentResponseHandler response=ClaimConnect_SummaryController.generateCashSettlementReports(asr.Case__c,asr.Service_Appointment__c,cashSettleDef);
             }  
          
          }
        
       }
       update updtlstreport;
       
        
    }
    
    global void finish(Database.BatchableContext bc){ 
        
    }
    
    public static void checkTest(){
     Integer i=0;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    
    }
    
    
           
}