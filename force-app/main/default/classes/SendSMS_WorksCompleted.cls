/**
 * Created by Nick on 30/04/2020.
 */

public without sharing class SendSMS_WorksCompleted {
    @InvocableMethod
    public static void sendSMSWorksCompleted(List<ServiceAppointment> serviceAppointments) {

        Set<Id> saIds = new Set<Id>();
        for (ServiceAppointment sa : serviceAppointments) {
            saIds.add(sa.Id);
        }

        List<ServiceAppointment> serviceAppointmentList = [SELECT Id, Work_Order__r.WorkOrderNumber, Work_Order__r.Service_Resource_Company__c,
                                                                Work_Order__r.Street, Work_Order__r.City, Work_Order__r.State, Work_Order__r.PostalCode,
                                                                Work_Order__r.Trade_RCTI__c, Work_Order__r.Work_Order_Authority__c
                                                                FROM ServiceAppointment WHERE Id IN :saIds];

        System.debug('NFDebug serviceAppointmentList = ' + serviceAppointmentList);

        List<ServiceAppointment> serviceAppointmentListToProcess = new List<ServiceAppointment>();
        for (ServiceAppointment sa: serviceAppointmentList) {
            if (sa.Work_Order__r.Trade_RCTI__c == true) {
                if (sa.Work_Order__r.Work_Order_Authority__c == 'Priced Work Order' || sa.Work_Order__r.Work_Order_Authority__c == 'Quote Approved' || sa.Work_Order__r.Work_Order_Authority__c == 'Rectification') {
                    serviceAppointmentListToProcess.add(sa);
                }
            }
        }
        System.debug('NFDebug serviceAppointmentListToProcess = ' + serviceAppointmentListToProcess);

        Set<Id> srCompanyIdSet = new Set<Id>();
        for (ServiceAppointment so : serviceAppointmentListToProcess) {
            srCompanyIdSet.add(so.Work_Order__r.Service_Resource_Company__c);
        }
        System.debug('NFDebug srCompanyIdSet = ' + srCompanyIdSet);

        List<User> userList = [SELECT Id, ContactId, Contact.AccountId, Contact.Phone, Contact.MobilePhone, Profile.Name FROM User WHERE Contact.AccountId IN :srCompanyIdSet AND (Profile.Name = 'Manager User Profile' OR Profile.Name = 'Restricted User Profile')];
        Map<Id, ServiceAppointment> mapContactIdVsServiceAppointment = new Map<Id, ServiceAppointment>();

        for (ServiceAppointment so : serviceAppointmentListToProcess) {
            for (User u : userList) {
                if (so.Work_Order__r.Service_Resource_Company__c == u.Contact.AccountId) {
                    mapContactIdVsServiceAppointment.put(u.ContactId, so);
                }
            }
        }
        System.debug('NFDebug mapContactIdVsServiceAppointment = ' + mapContactIdVsServiceAppointment);

        String durableId = [SELECT Id, Name, Status FROM Site WHERE Name = 'RepairForce_Trade_Platform' AND Status = 'Active' LIMIT 1].Id;
        String baseURL = [SELECT id, SecureUrl from SiteDetail where DurableId = :durableId LIMIT 1].SecureUrl;

        for (Contact c : [SELECT Id, MobilePhone FROM Contact WHERE Id IN :mapContactIdVsServiceAppointment.keySet() AND MobilePhone != '']) {
            ServiceAppointment so = mapContactIdVsServiceAppointment.get(c.Id);
            System.debug('NFDebug so = ' + so);

            String phoneNumber = c.MobilePhone;
            String smsBody = 'Hi Team,%0a' +
                    'You are due to attend ' + so.Work_Order__r.Street + ' ' + so.Work_Order__r.City + ' ' + so.Work_Order__r.Postalcode + ' ' + so.Work_Order__r.State + ' in relation to WO' + so.Work_Order__r.WorkOrderNumber + ' tomorrow.%0a' +
                    'When you have completed works, please select the following: ' + baseURL + '/WorksCompleted?id=' + so.Id + '%0a' +
                    'This will complete your Service Appointment and the RCTI process will trigger.%0a' +
                    'If you have any queries, please access the Trade Portal.%0a' + 
                    'Regards,%0a' + 
                    'HomeRepair';
            try {
                SMSUtility.processSmsFuture(smsBody, phoneNumber);
                //if(serviceAppointmentList[0].Id != null && res.isSuccess) {
                //    ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), serviceAppointmentList[0].Id, ConnectApi.FeedElementType.FeedItem, 'Text Message Sent: ' + smsBody);
                //}
            }
            catch (Exception e) {
                system.debug('Exception Occurred ==> ' + e.getMessage() + ' :: StackTrace ==> ' + e.getStackTraceString());
            }
        }
    }
	
	public static void checkTest(){
     Integer i=0;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
        i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
          i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;

       
    
    
    }
}