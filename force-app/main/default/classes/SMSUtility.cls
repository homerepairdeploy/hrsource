public with sharing class SMSUtility {

    @future (callout = true)
    public static void processSmsFuture(String smsBody, string phoneNumber){
        try {
            //GeneralSettings__c gs = GeneralSettings__c.getValues('ClaimIngestionFilter').Value__c ;

            HttpRequest req = new HttpRequest();
            String accountSid = GeneralSettings__c.getValues('Account SID').Value__c;
            String fromNumber = GeneralSettings__c.getValues('Twilio From Number').Value__c;
            String authToken = GeneralSettings__c.getValues('Account Token').Value__c;
            String TwilioURL = GeneralSettings__c.getValues('Twilio SMS URL').Value__c;
            //req.setEndpoint(TwilioURL+accountSid+'/SMS/Messages.json');
            req.setEndpoint(TwilioURL+accountSid+'/Messages.json');
            //req.setEndpoint('callout:Twilio/2010-04-01/Accounts/'+accountSid+'/Messages.json');
            //req.setEndpoint('https://api.twilio.com/2010-04-01/Accounts/');
            req.setMethod('POST');

            String VERSION  = '3.2.0';
            req.setHeader('X-Twilio-Client', 'salesforce-' + VERSION);
            req.setHeader('User-Agent', 'twilio-salesforce/' + VERSION);
            req.setHeader('Accept', 'application/json');
            req.setHeader('Accept-Charset', 'utf-8');
            Blob headerValue = Blob.valueOf(accountSid + ':' + authToken);
            String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);
            //req.setHeader('Authorization','Basic '+EncodingUtil.base64Encode(Blob.valueOf('AC82cb02546cdcf6eb5d8855779e6052ef:0bc058e302ab0171852cea01f2fb7aea')));

            req.setBody('To='+EncodingUtil.urlEncode(phoneNumber,'UTF-8')+'&From='+EncodingUtil.urlEncode(fromNumber,'UTF-8')+'&Body='+smsBody);

            Http http = new Http();
            HTTPResponse res = http.send(req);
            System.debug('ResponseBody==>'+res.getBody());
            Integer statusCode = res.getStatusCode();
            System.debug('res.getStatus()==>'+res.getStatus());
            if(res.getStatus() == 'CREATED')
                //return new ResponseWrapper (true, 'SMS sent Succcessfully!', statusCode );
            ResponseWrapper errorRes = (ResponseWrapper )json.deserialize(res.getBody(),ResponseWrapper.class);
            //errorRes.isSuccess = false;
            //return errorRes;
        }
        catch(Exception e) {
            system.debug('An exception Occured==>'+e.getMessage()+' StackTrace==>'+e.getStackTraceString());
            //return new ResponseWrapper (false, e.getMessage(), null);
        }
    }

    public static ResponseWrapper processSms(String smsBody, string phoneNumber){
       try { 
           //GeneralSettings__c gs = GeneralSettings__c.getValues('ClaimIngestionFilter').Value__c ;
           
           HttpRequest req = new HttpRequest();
           String accountSid = GeneralSettings__c.getValues('Account SID').Value__c;
           String fromNumber = GeneralSettings__c.getValues('Twilio From Number').Value__c;
           String authToken = GeneralSettings__c.getValues('Account Token').Value__c;
           String TwilioURL = GeneralSettings__c.getValues('Twilio SMS URL').Value__c;
           //req.setEndpoint(TwilioURL+accountSid+'/SMS/Messages.json');
           req.setEndpoint(TwilioURL+accountSid+'/Messages.json');
           //req.setEndpoint('callout:Twilio/2010-04-01/Accounts/'+accountSid+'/Messages.json');
           //req.setEndpoint('https://api.twilio.com/2010-04-01/Accounts/');
           req.setMethod('POST');
    
           String VERSION  = '3.2.0';
           req.setHeader('X-Twilio-Client', 'salesforce-' + VERSION);
           req.setHeader('User-Agent', 'twilio-salesforce/' + VERSION);
           req.setHeader('Accept', 'application/json');
           req.setHeader('Accept-Charset', 'utf-8');
           Blob headerValue = Blob.valueOf(accountSid + ':' + authToken);
           String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
           req.setHeader('Authorization', authorizationHeader);
           //req.setHeader('Authorization','Basic '+EncodingUtil.base64Encode(Blob.valueOf('AC82cb02546cdcf6eb5d8855779e6052ef:0bc058e302ab0171852cea01f2fb7aea')));
    
           req.setBody('To='+EncodingUtil.urlEncode(phoneNumber,'UTF-8')+'&From='+EncodingUtil.urlEncode(fromNumber,'UTF-8')+'&Body='+smsBody);
    
           Http http = new Http();
           HTTPResponse res = http.send(req);
           System.debug('ResponseBody==>'+res.getBody());
           Integer statusCode = res.getStatusCode();
           System.debug('res.getStatus()==>'+res.getStatus());
           if(res.getStatus() == 'CREATED')
               return new ResponseWrapper (true, 'SMS sent Succcessfully!', statusCode );
           ResponseWrapper errorRes = (ResponseWrapper )json.deserialize(res.getBody(),ResponseWrapper.class);   
           errorRes.isSuccess = false;
           return errorRes;
       }
       catch(Exception e) {
           system.debug('An exception Occured==>'+e.getMessage()+' StackTrace==>'+e.getStackTraceString());
           return new ResponseWrapper (false, e.getMessage(), null);
       }
   }
   public class ResponseWrapper {
       public Boolean isSuccess = false;
       public String message;
       public Integer responseCode;
       public String code;       
       public String moreInfo;

       public String status;   
       public ResponseWrapper(Boolean isSuccess, String message, Integer responseCode) {
           this.isSuccess = isSuccess;
           this.message = message;
           this.responseCode = responseCode;
       }
   }
}