/***
 * Class Name:    ServiceAppointmentManagerController  
 *
 * Author:      Stephen Moss
_s * Created Date:  07/08/2018
 * Description:   Controller for the lightning component ServiceAppointmentManager
 *
 *
***/

public class NJ_ServiceAppointmentManagerController {
  /***
   * Method Name:   getSADetails
   * Author:      Stephen Moss
   * Created Date:  07/08/2018
   * Description:   Get the details for the passed in Service Appointment Id
   ***/
  @AuraEnabled
  public static srvAppointmentWrapper getSADetails(Id srvAppointmentId) { 
    List<ServiceAppointment> apptList = [SELECT Id, AppointmentNumber, Status, WorkTypeId, WorkType.Name, Subject, EarliestStartTime,
                                         DueDate,  SchedStartTime, SchedEndTime, Service_Resource__c, Service_Resource__r.Name, ParentRecordId,
                                         Claim__c, Committed_Start_Date__c, Committed_End_Date__c, Tier_2_Trade__r.Name, ServiceTerritory.Name,
                                         Tier_2_Trade_Account__r.Name , Is_Time_Bound__c, Service_Resource__r.IsCapacityBased, Duration,
                                         FSL__IsMultiDay__c, Time_Bound_by_Tradee__c, Team__c, Team_Size__c
                                         FROM ServiceAppointment
                                         WHERE Id = :srvAppointmentId
                                             LIMIT 1];
    if (!apptList.isEmpty()) {
      List<WorkOrder> woList = [SELECT Id, Special_Instructions__c, Description, Special_Instructions_New__c
                                FROM WorkOrder
                                WHERE Id = :apptList[0].ParentRecordId];

      Boolean disableFindCandidate = false;
      Boolean disableTier2Candidate = false;
      for (ServiceAppointment sa : [SELECT Id, ParentRecordId, Service_Resource__c, Tier_2_Trade_Account__c FROM ServiceAppointment
                                    WHERE ParentRecordId = :apptList[0].ParentRecordId AND Id != :srvAppointmentId]) {
        if (sa.Service_Resource__c != null) {
          disableTier2Candidate = true;
        }
        if (sa.Tier_2_Trade_Account__c != null) {
          disableFindCandidate = true;
        }
      }

      // create and return Wrapper object
      srvAppointmentWrapper rtnAppointment = new srvAppointmentWrapper(apptList[0], woList, disableFindCandidate, disableTier2Candidate);
      return rtnAppointment;
    } else {
      return null;
    }

  }

  /***
   * Method Name:   updateSADetails
   * Author:      Stephen Moss
   * Created Date:  25/08/2018
   * Description:   Update the details for the passed in Service Appointment
   ***/
  @AuraEnabled
  public static srvUpdateResponseWrapper updateSADetails(Id srvAppointmentId,
      String srvSubject,
      DateTime srvEarlyStart,
      DateTime srvDueDate) {

    List<ServiceAppointment> apptList = [SELECT Id, AppointmentNumber, Status, WorkTypeId, WorkType.Name, Subject, EarliestStartTime,
                                         DueDate, Claim__c, Is_Time_Bound__c, FSL__IsMultiDay__c,  Tier_2_Trade_Account__r.Name, Duration,  SchedStartTime, SchedEndTime, Service_Resource__c, Service_Resource__r.Name, ParentRecordId
                                         FROM ServiceAppointment
                                         WHERE Id = :srvAppointmentId
                                             LIMIT 1];
    if (!apptList.isEmpty()) {
      try {
        // update the appointment
        apptList[0].Subject = srvSubject;
        apptList[0].EarliestStartTime = srvEarlyStart;
        apptList[0].DueDate = srvDueDate;
        update apptList[0];
        srvUpdateResponseWrapper rspSuccess = new srvUpdateResponseWrapper();
        rspSuccess.updateStatus = 'SUCCESS';
        rspSuccess.updateMessage = 'Appointment Successfully Updated';
        List<WorkOrder> woList = [SELECT Id, Special_Instructions__c, Description, Special_Instructions_New__c
                                  FROM WorkOrder
                                  WHERE Id = :apptList[0].ParentRecordId];
        srvAppointmentWrapper rtnAppointment = new srvAppointmentWrapper(apptList[0], woList, false, false);
        rspSuccess.updatedAppointment = rtnAppointment;
        return rspSuccess;
      } catch (Exception e) {
        srvUpdateResponseWrapper rspException = new srvUpdateResponseWrapper();
        rspException.updateStatus = 'ERROR';
        rspException.updateMessage = e.getMessage() + e.getStackTraceString();
        System.debug('Exception Message:' + e.getMessage() + e.getStackTraceString());
        return rspException;
      }
    } else {
      srvUpdateResponseWrapper rspEmptyList = new srvUpdateResponseWrapper();
      rspEmptyList.updateStatus = 'ERROR';
      rspEmptyList.updateMessage = 'Service Appointment ' + srvAppointmentId + ' NOT FOUND';
      return rspEmptyList;
    }
  }
  /***
   * Method Name:     updateWorkOrder
   * Author:          Nikhil
   * Created Date:    27/10/2018
   * Description:     This method is used to update work orders
   ***/
  @AuraEnabled
  public static String updateWorkOrder(String woId, String claimId, String specIns, String descr, Boolean applyAll, String saId, Boolean isTimeBound,
                                       DateTime commStartTime, DateTime commEndTime, DateTime scheStartTime, DateTime scheEndTime, Decimal dura) {
    System.debug(' woId ' + woId + ' claimId ' + claimId + ' specIns ' + specIns + ' descr ' + descr + ' applyAll ' + applyAll);
    System.debug('commStartTime ' + commStartTime + ' commEndTime ' + commEndTime + ' scheStartTime ' + scheStartTime + ' scheEndTime ' + scheEndTime);
    String isError = '';
    List<WorkOrder> woUpdate = new List<WorkOrder>();
    Date scheStartDate = scheStartTime.date(), scheEndDate = scheEndTime.date(), commStartDate = commStartTime.date(), commEndDate = commEndTime.date();
    if (commStartDate != scheStartDate || commEndDate != scheEndDate) {
      isError = 'Committed Start Date and End Dates should be in range of Scheduled Start and End dates.';
      return isError;
    }

    if (applyAll) {
      for (WorkOrder wo : [SELECT Id, Special_Instructions__c, Description, Special_Instructions_New__c
                           FROM WorkOrder
                           WHERE Claim__c = :claimId]) {
        wo.Special_Instructions_New__c = specIns;
        wo.Description = descr;
        woUpdate.add(wo);
      }
    } else {
      woUpdate.add(new WorkOrder(Id = woId, Special_Instructions_New__c = specIns, Description = descr));
    }

    try {
      update woUpdate;
      if (dura != null && dura > 0) {
        commEndTime = commStartTime + (dura / 24.0);
      }
      if (isTimeBound) {
        ServiceAppointment saUpdate = new ServiceAppointment(Id = saId, Is_Time_Bound__c = isTimeBound, Committed_Start_Date__c = commStartTime,
            Committed_End_Date__c = commEndTime);
        update saUpdate;
      }
      isError = '';
    } catch (Exception ex) {
      System.debug(ex.getMessage());
      isError = ex.getMessage();
      throw new AuraException(ex.getMessage());
    }
    return isError;
  }
  /***
   * Method Name:   getSACandidates
   * Author:      Stephen Moss
   * Created Date:  07/08/2018
   * Description:   Get the candidates for the passed in Service Appointment Id
   ***/
  @AuraEnabled
  public static List<srvCandidateWrapper> getSACandidates(Id srvAppointmentId, Id workOrderId, String workType) {
    // FSL.GradeSlotsService class
    // The GetGradedMatrix method returns a matrix of resource id's and graded time slots

    Id serviceAppointmentId = srvAppointmentId;
    ServiceAppointment sa = [select id, EarliestStartTime, DueDate, Team__c, Team_Size__c from ServiceAppointment where Id = :serviceAppointmentId];
    // Shouldn't hardcode Scheduling Policy...
    Id schedulingPolicyId = [select id from FSL__Scheduling_Policy__c where Name = 'Soft Boundaries' limit 1].Id;

    // Generate the graded time slots for the service appointment
    FSL.GradeSlotsService mySlotService = new FSL.GradeSlotsService(schedulingPolicyId, serviceAppointmentId);
    System.debug(' mySlotService ' + mySlotService);

    // Store the matrix of service resource id's and graded time slots
    FSL.AdvancedGapMatrix myResultMatrix = mySlotService.GetGradedMatrix(true);
    System.debug(' myResultMatrix ' + myResultMatrix);

    Map<Id, FSL.ResourceScheduleData> mySRGradedTimeSlotMap = myResultMatrix.ResourceIDToScheduleData;
    System.debug(' mySRGradedTimeSlotMap ' + mySRGradedTimeSlotMap);
    System.debug(' mySRGradedTimeSlotMap size ' + mySRGradedTimeSlotMap.size());

    for (id thisresourceid : mySRGradedTimeSlotMap.keySet()) {
      for (FSL.SchedulingOption thisso : mySRGradedTimeSlotMap.get(thisresourceid).SchedulingOptions ) {
        system.debug('***** Resource id' + thisresourceid);
        system.debug('***** Start - ' + thisso.Interval.Start);
        system.debug('***** Finish - ' + thisso.Interval.Finish);
        system.debug('****** Grade - ' + thisso.Grade);
      }
    }
    String assignedResourceAccountName;
    for (AssignedResource ar : [   SELECT Id, ServiceResourceId, ServiceResource.RelatedRecord.Contact.Account.Name, ServiceResource.RelatedRecord.Contact.AccountId,
                                   ServiceResource.RelatedRecord.CompanyName, ServiceResource.ResourceType, ServiceCrew.Service_Crew_Company_Name__c
                                   FROM AssignedResource
                                   WHERE ServiceAppointment.ParentRecordId = :workOrderId
                                       AND ServiceAppointment.WorkType.Name = :workType]) {
      if (assignedResourceAccountName == null) {
        If (ar.ServiceResource.ResourceType == 'T') {
          assignedResourceAccountName = ar.ServiceResource.RelatedRecord.Contact.AccountId != null ? ar.ServiceResource.RelatedRecord.Contact.Account.Name
                                        : ar.ServiceResource.RelatedRecord.CompanyName;
        } else {
          assignedResourceAccountName = ar.ServiceCrew.Service_Crew_Company_Name__c;
        }
      }
    }




    // Get Service Resource details
    Set<Id> timeSlots = mySRGradedTimeSlotMap.keySet();
    String srQuery = ' SELECT Id, Name, IsCapacityBased,Capacity_Exceeded__c ,Capacity_Used__c,ResourceType,ServiceCrewId,ServiceCrew.CrewSize,ServiceCrew.Service_Crew_Company_Name__c';
    srQuery += ' FROM ServiceResource WHERE IsActive = True ';

    if (assignedResourceAccountName != null) {
        
        if (sa.Team__c) {
          srQuery += ' AND ((ServiceCrew.Service_Crew_Company_Name__c = :assignedResourceAccountName) OR (Account_Name__c = :assignedResourceAccountName AND RelatedRecord.IsActive = True)) ';
        } else {
          srQuery += ' AND Account_Name__c = :assignedResourceAccountName AND RelatedRecord.IsActive = True ';
        }
      }
    else {
      srQuery += ' AND Id IN :timeSlots ';
    }

    if (sa.Team__c) {
      srQuery += 'AND ((ResourceType=\'C\' AND ServiceCrew.CrewSize=' + sa.Team_Size__c + ') OR (ResourceType=\'T\' AND IsCapacityBased = true AND Capacity_Team__c = true AND RelatedRecord.IsActive = True)) ';

    } else {
      srQuery += 'AND ResourceType=\'T\' AND Capacity_Team__c = False AND RelatedRecord.IsActive = True';
    }
    System.debug('srQuery ' + srQuery );
    System.debug('srQuery1'+srQuery.Substring(0,250));
    System.debug('srQuery2'+srQuery.Substring(250,srQuery.Length()));
    System.debug(' assignedResourceAccountName ' + assignedResourceAccountName);
    List<ServiceResource> resourceList = Database.query(srQuery);
    System.debug('Resource List size '+resourceList.size());
    // Now Build the Return Dataset
    List<srvCandidateWrapper> rtnCandidates = new List<srvCandidateWrapper>();
    for (ServiceResource s : resourceList) {
      srvCandidateWrapper sc = new srvCandidateWrapper();
      sc.serviceResourceId = s.Id;
      sc.serviceResourceName = s.Name;
      sc.isCapacityBased = s.IsCapacityBased;
      sc.percentCapacityUsed = s.Capacity_Used__c;
      sc.serviceResourceType = s.ResourceType;
      sc.serviceResourceCrewId = s.ServiceCrewId;
      sc.serviceResourceTeamSize = s.ServiceCrew.CrewSize;
      if (mySRGradedTimeSlotMap.containsKey(s.Id)) {
        for (FSL.SchedulingOption thisso : mySRGradedTimeSlotMap.get(s.Id).SchedulingOptions ) {
          srvCandidateSlotWrapper apptSlot = new srvCandidateSlotWrapper();
          apptSlot.slotStart = thisso.Interval.Start;
          apptSlot.slotFinish = thisso.Interval.Finish;
          apptSlot.slotGrade = String.valueOf(thisso.Grade);
          sc.appointmentSlots.add(apptSlot);
          System.debug('apptSlot'+apptSlot);
        }
      }
      rtnCandidates.add(sc);
    }
    return rtnCandidates;
  }
  public class TempCandidateMetadata {
    @AuraEnabled
    public String workType;
    @AuraEnabled
    public String servTerritory;
    @AuraEnabled
    public List<NJ_SelectOptionLightning> areas;
  }
  /**
   * This method is used to get metadata for tier2 candidates
   * @param  srvAppointmentId - Id of the service appointment
   * @return                  Metadata of the tier2 candidate
   */
  @AuraEnabled
  public static TempCandidateMetadata initTempCandidates(Id srvAppointmentId) {
    TempCandidateMetadata tcm = new TempCandidateMetadata();
    Id woId;
    for (ServiceAppointment sa : [SELECT Id, WorkType.Name, ServiceTerritory.Name,                                     ParentRecordId
                                  FROM ServiceAppointment
                                  WHERE Id = :srvAppointmentId]) {
      tcm.workType = sa.WorkType.Name;
      tcm.servTerritory = sa.ServiceTerritory.Name;
      woId = sa.ParentRecordId;
    }
    if(woId != null && String.isBlank(tcm.servTerritory)) { 
      for(WorkOrder wo : [SELECT Id, ServiceTerritoryId, ServiceTerritory.Name
                          FROM WorkOrder
                          WHERE Id = :woId]) {
        tcm.servTerritory = wo.ServiceTerritory.Name;                  
      }
    }
    tcm.areas = new List<NJ_SelectOptionLightning> { new NJ_SelectOptionLightning('', 'All')};
    tcm.areas.addAll(NJ_Utilities.getPicklistValues(Account.sObjectType, 'Service_Areas__c'));
    return tcm;
  }
  /**
   * This method is used to search tier2 candidates
   * @param  tradeType - trade type selected by the user
   * @param  servArea - service areas selected by the user
   * @param  nme - name of the account
   * @return List<Account> found using search
   * @Author : Nikhil
   */
  @AuraEnabled
  public static List<Account> findTempCandidates(String workOrderId, String tradeType, String servArea, String nme) {
    System.debug(' workOrderId ' + workOrderId + 'Nik tradeType ' + tradeType + ' servArea ' + servArea + ' nme' + nme);
    ServiceAppointment saDetails;
    List<Account> foundAccounts = new List<Account>();
    for (ServiceAppointment sa : [SELECT Id, Tier_2_Trade_Account__c, Tier_2_Trade_Account__r.Name, Tier_2_Trade_Account__r.Service_Areas__c
                                  FROM ServiceAppointment
                                  WHERE ParentRecordId = :workOrderId
                                      AND Tier_2_Trade_Account__c != null
                                      LIMIT 1]) {
      saDetails = sa;
    }
    if (saDetails != null) {
      foundAccounts.add(new Account(Id = saDetails.Tier_2_Trade_Account__c, Name = saDetails.Tier_2_Trade_Account__r.Name,
                                    Service_Areas__c = saDetails.Tier_2_Trade_Account__r.Service_Areas__c));
    } else {
      Set<Id> ttAcc = new Set<Id>();
      Set<Id> servAreaAcc = new Set<Id>();
      Set<Id> nameAcc = new Set<Id>();
      Set<Id> nameAccApproved = new Set<Id>();  // For Approved Trade Compliances only. Added by Nikhil on 02/11/2018
      Set<Id> finalAcc = new Set<Id>();
      if (String.isNotBlank(tradeType)) {
        String ttStr = '%' + tradeType + '%';
        for (Trade_Type__c tt : [SELECT Id, Account__c
                                 FROM Trade_Type__c
                                 WHERE Work_Type__r.Name LIKE :tradeType
                                 AND Status__c = 'Approved' ]) {
          ttAcc.add(tt.Account__c);
        }
      }
      System.debug('ttAcc '+ttAcc); 
      if (String.isNotBlank(servArea)) {
        for (Account ac : [SELECT Id
                           FROM Account
                           WHERE Service_Areas__c includes (:servArea)]) {  
          servAreaAcc.add(ac.Id);
        }
      }
      System.debug('servAreaAcc '+servAreaAcc);

        if (String.isNotBlank(nme)) {
        String nameStr = '%' + nme + '%';
        for (Account acc : [SELECT Id
                            FROM Account
                            WHERE Name LIKE :nameStr
                           ]) {
          nameAcc.add(acc.Id);
        }
      }
      if (String.isNotBlank(tradeType) && String.isNotBlank(servArea)) {
        finalAcc.addAll(ttAcc);
        finalAcc.retainAll(servAreaAcc);
      } else if (String.isNotBlank(tradeType)) {
        finalAcc.addAll(ttAcc);
      } else if (String.isNotBlank(servArea)) {
        finalAcc.addAll(servAreaAcc);
      }
      if (!nameAcc.isEmpty()) {
        finalAcc.addAll(nameAcc);
      }
      for (Account acc : [SELECT Id, Name, Service_Areas__c,suppress_from_schedule__c,Suppressed_from_Scheduling__c,
                          (SELECT Id FROM Trade_Types__r WHERE Status__c = 'Approved' LIMIT 1)
                          FROM Account
                          WHERE Id IN :finalAcc
                          AND Supplier_Status__c = 'Active'
                              AND Resource_Type__c = 'Tier_2'  ]) {
        if (!acc.Trade_Types__r.isEmpty() && !acc.suppress_from_schedule__c && !acc.Suppressed_from_Scheduling__c) {
          foundAccounts.add(acc);
        }
      }
    }

    System.debug('FoundAccounts' + foundAccounts);
    return foundAccounts;

  }
  /**
  * This method is used to schedule temp candidate appointment
  * @param
  * @return List<Account> found using search
  * @Author : Nikhil
  */
  @AuraEnabled
  public static srvScheduleResponseWrapper scheduleTempCandidate(Id servAppId, Id accId, Datetime scheStartTime, Datetime scheEndTime, Decimal duration, String workOrderId) {
    System.debug('Nikhil servAppId ' + servAppId + ' accId ' + accId + ' commStartTime ' + scheStartTime + ' commEndTime' + scheEndTime + ' workOrderId ' + workOrderId);
    List<Id> contactIds = new List<Id>();
    Id tempAssignedAcc;
    if (duration != null) {
      scheEndTime = scheStartTime + (duration / 24.0);
    }
    for (Contact ct : [SELECT Id
                       FROM Contact
                       WHERE AccountId = :accId]) {
      contactIds.add(ct.Id);
    }
    System.debug('contactIds ' + contactIds);
    Set<Id> userIds = new Set<Id>();
    Set<Id> userIdsFSL = new Set<Id>();

    for (User usr : [SELECT Id
                     FROM User
                     WHERE ContactId IN :contactIds]) {
      userIds.add(usr.Id);
    }
    for (PermissionSetAssignment psa : [SELECT AssigneeId
                                        FROM PermissionSetAssignment
                                        WHERE PermissionSet.Name LIKE '%FSL%'
                                        AND AssigneeId IN :contactIds
                                       ]) {
      userIdsFSL.add(psa.AssigneeId);
    }
    userIds.removeAll(userIdsFSL);
    if (!userIds.isEmpty()) {
      Id accoId  = (new List<Id>(userIds)).get(0);
      List<User> usAcc = [Select AccountId from User WHERE ID = :accoId];
      Id acc = usAcc[0].AccountId;
      tempAssignedAcc = acc;
    }
    System.debug('tempAssignedUser ' + tempAssignedAcc + ' servAppId ' + servAppId);
    if (tempAssignedAcc != null && servAppId != null) {
      Savepoint sp = Database.setSavePoint();
      try {

        WorkOrder wordOrd;
        for(WorkOrder wo : [SELECT Id, Work_Type_Name__c
                            FROM WorkOrder
                            WHERE Id = :workOrderId
                            LIMIT 1]) {
          wordOrd = wo;
        }
        ServiceAppointment sa = new ServiceAppointment(Id = servAppId, Tier_2_Trade_Account__c  = tempAssignedAcc, SchedStartTime = scheStartTime, SchedEndTime =  scheEndTime);
        if(wordOrd.Work_Type_Name__c.startsWith(NJ_Constants.WORK_TYPE_STARTS_WITH_MAKESAFE) ||
           wordOrd.Work_Type_Name__c.startsWith(NJ_Constants.WORK_TYPE_STARTS_WITH_HOMEASSIST)) {
          sa.Status = NJ_Constants.SA_STATUS_CONFIRMED;
        } else {
          sa.Status = NJ_Constants.SA_STATUS_TENTATIVE;
        }
        sa.Status_Change_to_Tentative__c = Datetime.now();
        System.debug('Nikhil sa '+sa);
        update sa;
        srvScheduleResponseWrapper rspSuccess = new srvScheduleResponseWrapper();
        rspSuccess.schedulingStatus = 'SUCCESS';
        rspSuccess.schedulingMessage = 'Appointment Successfully Scheduled';
        return rspSuccess;
      } catch (Exception ex) {
        Database.rollback(sp);
        String mesg = ex.getMessage();
        if (mesg.contains('Cannot change status from Awaiting Confirmation to Tentative')) {
          mesg = 'Cannot change status from Awaiting Confirmation to Tentative';
        }
        throw new AuraHandledException(mesg);
      }
    } else {
      throw new AuraHandledException('No user found!!!');
    }
  }


  /***
   * Method Name:   scheduleAppointmentForCandidate
   * Author:      Stephen Moss
   * Created Date:  09/08/2018
   * Description:   Schedule an Appointment for a specified candidate
   ***/
  @AuraEnabled
  public static srvScheduleResponseWrapper scheduleAppointmentForCandidate(Id srvAppointmentId,
      Id srvResourceId,
      Long apptStartTime,
      String srvResourceType,
      Id srvResourceCrewId, 
      integer srvResourceTeamSize) {
    DateTime srvApptStartTimeGMT = DateTime.newInstance(apptStartTime);
    // find the Service Appointment
    List<ServiceAppointment> srvList = [SELECT Id, Duration, DurationType, Status, SchedStartTime, SchedEndTime,
                                               Service_Resource__c, Committed_Start_Date__c, Committed_End_Date__c,
                                               ParentRecordId, Work_Type_Name__c
                                        FROM ServiceAppointment
                                        WHERE Id = :srvAppointmentId];
          
    ServiceAppointment s = new ServiceAppointment();
    system.debug('Homerepair servicelist'+srvlist);      
    if (!srvList.isEmpty()) {
      Savepoint sp = Database.setSavePoint();
      try {
        // update Status, Scheduled Start and Scheduled Finish on Service Appointment
        s = srvList[0];
        system.debug('appointmentrecord' + s);
        system.debug('worktypename' + s.Work_Type_Name__c);
        if(s.Work_Type_Name__c.startsWith(NJ_Constants.WORK_TYPE_STARTS_WITH_MAKESAFE) 
            || s.Work_Type_Name__c.startsWith(NJ_Constants.WORK_TYPE_STARTS_WITH_HOMEASSIST)){
              s.Status = NJ_Constants.SA_STATUS_CONFIRMED;
        } else {
              s.Status = NJ_Constants.SA_STATUS_TENTATIVE;
        } 
        //Updating the Service Resource
        s.Service_resource__c = srvResourceId;
        s.Status_Change_to_Tentative__c = Datetime.now();
        s.SchedStartTime = srvApptStartTimeGMT;
        Integer numMinutes = 0;
        if (s.DurationType == 'Hours') {
          numMinutes = Integer.valueOf((s.Duration * 60).round());
          s.SchedEndTime = srvApptStartTimeGMT.addMinutes(numMinutes);
        } else if (s.FSL__IsMultiDay__c != true) {
          // convert Minutes to seconds
          //numMinutes = Integer.valueOf((s.Duration).round());
          s.SchedEndTime = srvApptStartTimeGMT + (s.Duration / 24.0);
        } else {
          // convert Minutes to seconds
          numMinutes = Integer.valueOf((s.Duration).round());
          s.SchedEndTime = srvApptStartTimeGMT.addMinutes(numMinutes);
        }

        //Check whether the Service Resource is Capacity or Calandar. If its calendar, populate
        //Committed times with Scheduled Times.

        List<Serviceresource> sr = [SELECT Id, IsCapacityBased
                                    FROM Serviceresource
                                    WHERE ID = :srvResourceId];

        If(!sr[0].IsCapacityBased) {
          s.Committed_Start_Date__c = s.SchedStartTime;
          s.Committed_End_Date__c   = s.SchedEndTime;

        }
        System.debug('Calendar resource:' + sr[0]);
        System.debug('Nikhil Service Appointment '+s);
        update s;
        system.debug('after updaet of  SA in scheduleAppointmentForCandidate');


        // check if there is an existing Resource Assignment Record, if not create it
        List<AssignedResource> assignedResourceList = [SELECT Id, ServiceResourceId, ServiceAppointmentId, ServiceCrewId
            FROM AssignedResource
            WHERE ServiceAppointmentId = :srvAppointmentId
                                         LIMIT 1];

        if (!assignedResourceList.isEmpty()) {
          // update existing
          System.debug('Update Existing Assigned Resource - Service Resource' + srvResourceId);
          AssignedResource a = assignedResourceList[0];
          if (srvResourceType == 'T') {
            a.ServiceResourceId = srvResourceId;
          } else {
            a.ServiceResourceId = srvResourceId;
            a.ServiceCrewId = srvResourceCrewId;
          }
          update a;
        } else {
          // create new
          System.debug('Create New Assigned Resource' + srvResourceId);
          AssignedResource newAR = new AssignedResource();
          newAR.ServiceAppointmentId = srvAppointmentId;
          if (srvResourceType == 'T') {
            newAR.ServiceResourceId = srvResourceId;
          } else {
            newAR.ServiceResourceId = srvResourceId;
            newAR.ServiceCrewId = srvResourceCrewId;
          }
          insert newAR;
        }
        if (srvResourceId != null) {
          System.debug('Service Resource Updated:' + srvResourceId) ;
          s.Service_Resource__c = srvResourceId;
          update s;
        }
        // call a future method to update Service Appointment Capacity fields
       // updateServiceResourceCapacity(srvAppointmentId, srvResourceId);

        srvScheduleResponseWrapper rspSuccess = new srvScheduleResponseWrapper();
        rspSuccess.schedulingStatus = 'SUCCESS';
        rspSuccess.schedulingMessage = 'Appointment Successfully Scheduled';
        return rspSuccess;
      } catch (Exception e) {
        Database.rollback(sp);
        srvScheduleResponseWrapper rspException = new srvScheduleResponseWrapper();
        rspException.schedulingStatus = 'ERROR';
        rspException.schedulingMessage = e.getMessage();
        system.debug('Exception >>>' + e.getMessage() + ' ' + e.getStackTraceString());
        return rspException;
      }

    } else {
      srvScheduleResponseWrapper rspError = new srvScheduleResponseWrapper();
      rspError.schedulingStatus = 'ERROR';
      rspError.schedulingMessage = 'Appointment ' + srvAppointmentId + ' could not be found in the system.';
      return rspError;
    }
  }
  /***
   * Class Name:    srvAppointmentWrapper
   * Author:      Stephen Moss
   * Created Date:  07/08/2018
   * Description:   Wrapper Class for Service Appointment JSON to be returned to
   *          Lightning Component
   ***/
  public class srvAppointmentWrapper {

    // default no argument constructor
    // if you want to map fields manually
    public srvAppointmentWrapper() {

    }
    public srvAppointmentWrapper(ServiceAppointment srvAppt, List<WorkOrder> woList, Boolean disableFindCandidate, Boolean disableTier2Candidate) {
      this(srvAppt);
      if (!woList.isEmpty()) {
        this.wo = woList[0];
      } else {
        this.wo = new WorkOrder();
      }
      this.disableFindCandidate = disableFindCandidate;
      this.disableTier2Candidate = disableTier2Candidate;
    }
    // constructor to map a Service Appointment to the wrapper
    // automatically upon creation
    public srvAppointmentWrapper(ServiceAppointment srvAppt) {

      // default time and date if a field is empty

      // DateTime defaultDT = DateTime.newInstanceGmt(2000, 01, 01);

      DateTime defaultDT = DateTime.now();


      this.Id = srvAppt.Id;
      this.appointmentNumber = srvAppt.AppointmentNumber;
      this.status = srvAppt.Status;
      this.workTypeId = srvAppt.WorkTypeId;
      this.workTypeName = srvAppt.WorkType.Name;
      this.workOrderId = srvAppt.ParentRecordId;
      this.claimId = srvAppt.Claim__c;
      this.isTimeBound = srvAppt.Is_Time_Bound__c;
      this.duration = srvAppt.Duration;
      if (srvAppt.Subject != null) {
        this.subject = srvAppt.Subject;
      } else {
        this.subject = '';
      }
      this.earliestStart = srvAppt.EarliestStartTime;
      this.dueDate = srvAppt.DueDate;

      if (srvAppt.SchedStartTime != null) {
        this.scheduledStartTime = srvAppt.SchedStartTime;
      } else {
        this.scheduledStartTime = defaultDT;
      }

      if (srvAppt.SchedStartTime != null && srvAppt.Duration != null && srvAppt.FSL__IsMultiDay__c != true) {
        this.scheduledFinishTime = srvAppt.SchedStartTime + (srvAppt.Duration / 24.0);
      } else {
        this.scheduledFinishTime = srvAppt.SchedEndTime;
      }
      if (srvAppt.SchedStartTime != null && (srvAppt.Service_Resource__r.IsCapacityBased || srvAppt.Tier_2_Trade_Account__c != null) ) {
        if (srvAppt.Is_Time_Bound__c || srvAppt.Time_Bound_by_Tradee__c || srvAppt.FSL__IsMultiDay__c) {
          this.committedStartTime = srvAppt.Committed_Start_Date__c;
          this.committedFinishTime = srvAppt.Committed_End_Date__c;
          this.disableCommitted = true;
        } else if (srvAppt.Duration != null) {
          this.committedStartTimeDefault = srvAppt.SchedStartTime;
          this.committedFinishTimeDefault = srvAppt.SchedStartTime + (srvAppt.Duration / 24.0);
          this.disableCommitted = false;
        }
      }
      if (srvAppt.Service_Resource__c != null) {
        this.serviceResourceId = srvAppt.Service_Resource__c;
      } else {
        this.serviceResourceId = '[Not Assigned]';
      }

      if (srvAppt.Service_Resource__r.Name != null) {
        this.isCapacityBased = srvAppt.Service_Resource__r.IsCapacityBased;
        this.serviceResourceName = srvAppt.Service_Resource__r.Name;
      } else {
        this.isCapacityBased = true;
        this.serviceResourceNameTier2 = srvAppt.Tier_2_Trade_Account__r.Name;
      }

    }

    // Wrapper Class Properties
    @AuraEnabled
    WorkOrder wo { get; set; }
    @AuraEnabled
    String Id { get; set; }
    @AuraEnabled
    String appointmentNumber { get; set; }
    @AuraEnabled
    public String workOrderId { get; set; }
    @AuraEnabled
    String claimId { get; set; }
    @AuraEnabled
    String status { get; set; }
    @AuraEnabled
    String workTypeId { get; set; }
    @AuraEnabled
    String workTypeName { get; set; }
    @AuraEnabled
    String subject { get; set; }
    @AuraEnabled
    DateTime earliestStart { get; set; }
    @AuraEnabled
    DateTime dueDate { get; set; }
    @AuraEnabled
    DateTime scheduledStartTime { get; set; }
    @AuraEnabled
    DateTime scheduledFinishTime { get; set; }
    @AuraEnabled
    DateTime committedStartTime { get; set; }
    @AuraEnabled
    DateTime committedFinishTime { get; set; }
    @AuraEnabled
    DateTime committedStartTimeDefault { get; set; }
    @AuraEnabled
    DateTime committedFinishTimeDefault { get; set; }
    @AuraEnabled
    Boolean disableCommitted { get; set; }
    @AuraEnabled
    String serviceResourceId { get; set; }
    @AuraEnabled
    String serviceResourceName { get; set; }
    @AuraEnabled
    Boolean isTimeBound { get; set; }
    @AuraEnabled
    String serviceResourceNameTier2 { get; set; } // For Tier 2 Trades
    @AuraEnabled
    Boolean isCapacityBased { get; set; }
    @AuraEnabled
    Decimal duration { get; set; }
    @AuraEnabled
    Boolean disableFindCandidate { get; set; }
    @AuraEnabled
    Boolean disableTier2Candidate { get; set; }
  }

  /***
   * Class Name:    srvCandidateWrapper
   * Author:      Stephen Moss
   * Created Date:  07/08/2018
   * Description:   Wrapper Class for Service Appointment Candidate JSON to be returned to
   *          Lightning Component
   ***/
  public class srvCandidateWrapper {

    // default no argument constructor
    public srvCandidateWrapper() {
      appointmentSlots = new List<srvCandidateSlotWrapper>();
    }

    // Wrapper Class Properties
    @AuraEnabled @TestVisible
    Id serviceResourceId { get; set; }
    @AuraEnabled @TestVisible
    String serviceResourceName { get; set; }
    @AuraEnabled @TestVisible
    String serviceResourceType { get; set; }
    @AuraEnabled @TestVisible
    Boolean isCapacityBased { get; set; }
    @AuraEnabled @TestVisible
    Decimal percentCapacityUsed { get; set; }
    @AuraEnabled @TestVisible
    Id serviceResourceCrewId { get; set; }
    @AuraEnabled @TestVisible
    integer serviceResourceTeamSize { get; set; }
    @AuraEnabled @TestVisible
    List<srvCandidateSlotWrapper> appointmentSlots { get; set; }

  }

  /***
   * Class Name:    srvCandidateSlotWrapper
   * Author:      Stephen Moss
   * Created Date:  07/08/2018
   * Description:   Wrapper Class for Service Appointment Candidate Slot
   ***/
  public class srvCandidateSlotWrapper {
    // default no argument constructor
    public srvCandidateSlotWrapper() {

    }
    // Wrapper Class Properties
    @AuraEnabled @Testvisible
    DateTime slotStart { get; set; }
    @AuraEnabled @Testvisible
    DateTime slotFinish { get; set; }
    @AuraEnabled @Testvisible
    String slotGrade { get; set; }

  }


  /***
   * Class Name:    srvScheduleResponseWrapper
   * Author:      Stephen Moss
   * Created Date:  09/08/2018
   * Description:   Wrapper Class for response from Service
   *          Appointment Scheduling Request
   ***/
  public class srvScheduleResponseWrapper {
    // default no argument constructor
    public srvScheduleResponseWrapper() {

    }

    // Wrapper Class Properties
    @AuraEnabled
    String schedulingStatus { get; set; }
    @AuraEnabled
    String schedulingMessage { get; set; }
  }

  /***
   * Class Name:    srvUpdateResponseWrapper
   * Author:      Stephen Moss
   * Created Date:  27/08/2018
   * Description:   Wrapper Class for response from Service
   *          Appointment Update Request
   ***/
  public class srvUpdateResponseWrapper {
    // default no argument constructor
    public srvUpdateResponseWrapper() {
      updatedAppointment = new srvAppointmentWrapper();
    }

    // Wrapper Class Properties
    @AuraEnabled
    public String updateStatus { get; set; }
    @AuraEnabled
    public String updateMessage { get; set; }
    @AuraEnabled
    public srvAppointmentWrapper updatedAppointment { get; set; }
  }

  /**
   * This method update Service Resource Capacity
   * @param srvAppointmentId - Id of Service Appointment
   
  @future(callout = false)
  public static void updateServiceResourceCapacity(Id srvAppointmentId, Id srvResourceId) {
    
    List<Serviceresource> sr = [SELECT Id, IsCapacityBased
                                FROM Serviceresource
                                WHERE ID = :srvResourceId];

    List<ServiceAppointment> lstSA = new List<ServiceAppointment>([SELECT Id, Team__c, Team_Size__c, Duration, SchedStartTime
                                                                   FROM ServiceAppointment
                                                                   WHERE Id =  :srvAppointmentId]);
    Date tempDate = lstSA[0].SchedStartTime.Date();
    Date firstOfMonth = tempDate.toStartOfMonth();

    System.debug('SA ScheduleStartDate >> ' + lstSA[0].SchedStartTime.Date() + ' <<< First Date of Month >>' + firstOfMonth);


    if (lstSA[0].Team__c && sr[0].IsCapacityBased) {
      List<ServiceResourceCapacity> lstServResCapacity = new List<ServiceResourceCapacity>();

      for (ServiceResourceCapacity src : [SELECT Id, CapacityInWorkItems, CapacityInHours, ServiceResourceId, StartDate, EndDate, TimePeriod, FSL__MinutesUsed__c, FSL__Work_Items_Allocated__c
                                          FROM ServiceResourceCapacity
                                          WHERE ServiceResourceId = :sr[0].Id]) {
        // Check when Time Period is 'Day' and Start Date is same as Schedule Start Date
        if (src.TimePeriod == 'Day' && src.StartDate == lstSA[0].SchedStartTime.date()) {
          if (src.CapacityInHours != null) {
            if(src.FSL__MinutesUsed__c != null){
              src.FSL__MinutesUsed__c += lstSA[0].Duration * lstSA[0].Team_Size__c * 60;
            }else{
              src.FSL__MinutesUsed__c = lstSA[0].Duration * lstSA[0].Team_Size__c * 60;
            }
          } 
          if (src.CapacityInWorkItems != null) {
            if(src.FSL__Work_Items_Allocated__c != null){
              src.FSL__Work_Items_Allocated__c += lstSA[0].Team_Size__c - 1;
            }else{
              src.FSL__Work_Items_Allocated__c = lstSA[0].Team_Size__c - 1;
            }
          }
          lstServResCapacity.add(src);
        } else if (src.TimePeriod == 'Week' && src.StartDate <= lstSA[0].SchedStartTime.date() && src.EndDate >= lstSA[0].SchedStartTime.date()) {
          // Check when Time Period is 'Week' and Schedule Start Date is between Start Date and End Date
          if (src.CapacityInHours != null) {
            if(src.FSL__MinutesUsed__c != null){
              src.FSL__MinutesUsed__c += lstSA[0].Duration * lstSA[0].Team_Size__c * 60;
            }else{
              src.FSL__MinutesUsed__c = lstSA[0].Duration * lstSA[0].Team_Size__c * 60;
            }
          } 
          if(src.CapacityInWorkItems != null){
            if(src.FSL__Work_Items_Allocated__c != null){
              src.FSL__Work_Items_Allocated__c += lstSA[0].Team_Size__c - 1;
            }else{
              src.FSL__Work_Items_Allocated__c = lstSA[0].Team_Size__c - 1;
            }
          }
          lstServResCapacity.add(src);
        } else if (src.TimePeriod == 'Month' && src.StartDate == firstOfMonth) {
          // Check when Time Period is 'Day' and Start Date is the first of the month of Schecule Start Date
          if (src.CapacityInHours != null) {
            if(src.FSL__MinutesUsed__c != null){
              src.FSL__MinutesUsed__c += lstSA[0].Duration * lstSA[0].Team_Size__c * 60;
            }else{
              src.FSL__MinutesUsed__c = lstSA[0].Duration * lstSA[0].Team_Size__c * 60;
            }
          } 
          if(src.CapacityInWorkItems != null){
            if(src.FSL__Work_Items_Allocated__c != null){
              src.FSL__Work_Items_Allocated__c += lstSA[0].Team_Size__c - 1;
            }else{
              src.FSL__Work_Items_Allocated__c = lstSA[0].Team_Size__c - 1;
            }
          }
          lstServResCapacity.add(src);
        }
      }
      System.debug('ServiceRC:' + lstServResCapacity);

      if (!lstServResCapacity.isEmpty()) {
        update lstServResCapacity;
      }
    }
  }*/

}