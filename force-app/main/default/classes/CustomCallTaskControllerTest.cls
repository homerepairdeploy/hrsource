@isTest
public class CustomCallTaskControllerTest {

    @testSetup 
    static void setup(){
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact firstcon=HomeRepairTestDataFactory.createContact('Testcontactone');
        Case csone=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,firstcon.Id);
        
         
    }  

     
    public static testMethod void testCustomCallTask() {
        case caserec=[select id from case limit 1];
        contact ctc=[select id from contact limit 1];
        Task t=new Task(subject='Inboundcall',whoid=ctc.id,whatid=caserec.id,Status='Completed');
        insert t;
        
        Test.startTest();
             ApexPages.StandardController sc = new ApexPages.StandardController(t);
             CustomCallTaskController cctc=new CustomCallTaskController(sc);
             cctc.doSave();
             
        
        Test.stopTest();        
    }
}