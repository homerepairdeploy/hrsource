/**
* @Author : Nikhil Jaitly
* @Created Date : 15/12/2018
* Helper class of work order handler
*/
public without sharing class WorkOrderHelper {
    /**
* This class is used to update Service Appointment Status based on work order status
* @Author : Nikhil Jaitly
* @Created Date : 15/12/2018
* @param woOrders List of work orders
*/
    public static void updateServiceAppointments(Map<Id,WorkOrder> woOrders) {
        System.debug('Nik WorkOrderHelper '+woOrders);
        Map<Id, List<ServiceAppointment>> mapWOSAs = new Map<Id, List<ServiceAppointment>>();
        List<ServiceAppointment> saTemp; 
        for(ServiceAppointment sa : [SELECT Id, ParentRecordId, Status, Service_Resource__r.IsCapacityBased, Tier_2_Trade_Account__c
                                     FROM ServiceAppointment
                                     WHERE ParentRecordId IN :woOrders.keySet()
                                    ]) {
                                        if(!mapWOSAs.containsKey(sa.ParentRecordId)) {
                                            mapWOSAs.put(sa.ParentRecordId, new List<ServiceAppointment>());
                                        }
                                        saTemp = mapWOSAs.get(sa.ParentRecordId);
                                        saTemp.add(sa);
                                        mapWOSAs.put(sa.ParentRecordId, saTemp);
                                    }
        WorkOrder wo;
        Map<Id,ServiceAppointment> saUpdate = new Map<Id,ServiceAppointment>();
        for(Id woId : mapWOSAs.keySet()) {
            wo = woOrders.get(woId);
            if(wo.Status == NJ_Constants.WO_STATUS_CANCELLED) {
                for(ServiceAppointment sa : mapWOSAs.get(wo.Id)) {
                    sa.Status = NJ_Constants.SA_STATUS_CANCELLED;
                    saUpdate.put(sa.Id,sa);
                }
            } else if(wo.Status == NJ_Constants.WO_STATUS_ACCEPTED) {
                System.debug('Nik mapWOSAs.get(wo.Id) '+mapWOSAs.get(wo.Id));
                for(ServiceAppointment sa : mapWOSAs.get(wo.Id)) {
                    if(sa.Tier_2_Trade_Account__c != null || sa.Service_Resource__r.IsCapacityBased) {
                        sa.Status = NJ_Constants.SA_STATUS_CONFIRMED;
                        saUpdate.put(sa.Id,sa);
                    }
                }
            } else if(wo.Status == NJ_Constants.WO_STATUS_REJECTED) {
                for(ServiceAppointment sa : mapWOSAs.get(wo.Id)) {
                    saUpdate.put(sa.Id,new ServiceAppointment(Id = sa.Id, Committed_End_Date__c = null,  
                                                              SchedStartTime = null, Service_Resource__c = null,
                                                              SchedEndTime = null,Committed_Start_Date__c = null,
                                                              Tier_2_Trade_Account__c = null,Time_bound_by_tradee__c = false, 
                                                              Is_Time_Bound__c = false,
                                                              Status = NJ_Constants.SA_STATUS_NEW));
                    
                }
            }
        }
        System.debug('Nikhil saUpdate '+saUpdate);
        if(!saUpdate.isEmpty()) {
            update saUpdate.values();
        }
    }  
    public static void addQualityAssuranceTextTask (List<WorkOrder> workOrderList){
        List<Task> newTaskList = new List<Task>();
        List<User> userList=[Select Id,Name,Email 
                             FROM User
                             WHERE Name = : NJ_CONSTANTS.HR_CLAIM 
                             Limit 1];
        for(WorkOrder wo : workOrderList) {
            Task t = new Task();
            t.priority = NJ_CONSTANTS.TASK_PRIORITY_IMPORTANT;
            t.status = NJ_CONSTANTS.TASK_STATUS_NOT_STARTED;
            t.subject = NJ_CONSTANTS.QUALITY_ASSURANCE_TASK;
            t.Description = wo.Quality_Assurance_Text__c;
            t.IsReminderSet = true;                            
            t.OwnerId = userList[0].id;
            t.ActivityDate = System.Today() + 1;
            t.ReminderDateTime = System.now()+1;
            t.WhatId =  Wo.CaseId;
            t.Work_Order__c = wo.Id;
            newTaskList.add(t);            
        }
        system.debug('newTaskList '+newTaskList);
          if (newTaskList.Size() > 0){
                insert newTaskList; 
            }
    }
    
    /**
     * This method is used to create Task when a WO is rejected (Status = Rejected Wo)
     * @param woOrders map of work orders
     * @Author : Nikhil
     * @CreatedDate : 30 May 2019
     */
    public static void createTaskWhenWORejected(Map<Id,WorkOrder> woOrders) {

        List<Task> lstTaskToInsert = new List<Task>();
        for(WorkOrder wo : woOrders.values()){
            if(wo.Status == NJ_Constants.WO_STATUS_REJECTED){
                Task t = new Task();
                t.ActivityDate = system.today();
                t.Description = 'A work order has been rejected by the trade.';
                t.Due_Date_Time__c = system.now() + 0.125;
                t.OwnerId = Label.HR_Claims;
                t.Priority = 'Normal';
                t.Status = 'Not Started';
                t.Subject = 'Work order rejected';
                t.WhatId = wo.CaseId;
                t.Work_Order__c = wo.Id;
                lstTaskToInsert.add(t);
            }
        }
        system.debug('Task creation on WO rejection >>>'+lstTaskToInsert.size());
        if(!lstTaskToInsert.isEmpty()){
            insert lstTaskToInsert; 
        }
    }
}