/********************************************************************************************** 
Name        : FileData
Description : Class used in ClaimConnect_AWSService and ClaimConnect_LightningFileUploadHandler
Purpose 	: This class is used to read file data stored in AWS S3 bucket
                                                            
VERSION        AUTHOR           	DATE           DETAIL      DESCRIPTION   
1.0            Vidhya Sivaperuman   20-05-2020     Created     ClaimConnect APP
/***********************************************************************************************/
public class FileData
{
    @AuraEnabled
    public string Content{get;set;}
    @AuraEnabled
    public blob ContentBlob{get;set;}
    @AuraEnabled
    public string ContentType{get;set;}
    @AuraEnabled
    public boolean status{get;set;}
}