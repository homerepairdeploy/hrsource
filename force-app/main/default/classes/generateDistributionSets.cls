//class to generate distribution sets on accounts sent in
public class generateDistributionSets {
    
    
    //generate GST Distribution Set
    public static void generateGSTDistribuitonSet(Map<ID,String> invoiceRecord){
        
        try{
            List<AP_Invoice__c> apInvoice = [SELECT id, Work_Order__c, Trade_Account__c,Job_Type__c,IsEvent__c,SourceChannel__c,EventNonEventCode__c
                                             FROM AP_Invoice__c
                                             WHERE Id =:invoiceRecord.keySet()
                                             LIMIT 1];
            system.debug(LoggingLevel.DEBUG, '***BMapInvoice: ' + apInvoice.size());
            //determine if work order is eligible for taxation
            
            //booleans representing conditions to be checked
            Boolean accountGST = false;
            /*Boolean woliGST = false;
            
            
            //place and woli's with a gst value above zero into a list
            List<WorkOrderLineItem> woli = [SELECT id, GST__c
                                            FROM WorkOrderLineItem
                                            WHERE workOrderId = :apInvoice[0].Work_Order__c
                                            AND (GST_Labour__c != 0.00 OR GST_Material__c != 0.00)];
            
            system.debug(LoggingLevel.DEBUG, '***BMwoli: ' + woli.size());
            //determine if any woli's have an above zero gst value, set bool
            if (woli.size() > 0){
                woliGST = true;
            }
            //woliGST is true if there is gst value in woli's*/
            if(true){
                
                 
                //query for work order related to invoice
                List<WorkOrder> workOrder = [SELECT id, job_Type__c, Claim_Job_Type__c , Claim__c, Work_Type_Name__c
                                             FROM WorkOrder
                                             WHERE Id =:apInvoice[0].Work_Order__c
                                             LIMIT 1];
                system.debug(LoggingLevel.DEBUG, '***BMworkOrder: ' + workOrder);
                //query for claim related to workOrder
                if(workOrder.size() > 0){
                    List<Case> Claim = [SELECT id, State1__c
                                        FROM Case
                                        WHERE CaseNumber = :workOrder[0].Claim__c];
                    
                    Decimal CostCentreMDT=0; 
                    Decimal TradeMDT=0; 
                    Boolean isTradeCodesExists=false;                  
                    //get Cost Centre Part of DS
                    //query for the CC_Code__c code that matched the job type on the related work order

                    
                    If (apInvoice[0].SourceChannel__c == 'SCRO'){
                        List<CostCenterCodes__mdt> SCROCostCentre=[SELECT id, CCCode__c
                                                                   FROM CostCenterCodes__mdt
                                                                   WHERE Channel__c=:apInvoice[0].SourceChannel__c and State__c=:Claim[0].State1__c
                                                                   LIMIT 1];
                        If ( SCROCostCentre.size() > 0){                                           
                            CostCentreMDT = SCROCostCentre[0].CCCode__c;
                            List<TradeCodes__mdt> SCROTrade = [SELECT Channel__c, Job_Type__c,TradeCode__c
                                                               FROM TradeCodes__mdt
                                                               WHERE Channel__c = :apInvoice[0].SourceChannel__c and Job_Type__c=:apInvoice[0].Job_Type__c];
                            TradeMDT = SCROTrade[0].TradeCode__c;
                            isTradeCodesExists=true;
                        }
                    }                                               
                    Else{                                               
                        List<Cost_Centre__mdt> CostCentre = [SELECT id, CC_Code__c, Description__c
                                                            FROM Cost_Centre__mdt
                                                            WHERE Description__C = :workOrder[0]. Claim_Job_Type__c
                                                            AND State__c = :Claim[0].State1__c
                                                            LIMIT 1];
                            If (CostCentre.size() > 0) {                                   
                                CostCentreMDT = CostCentre[0].CC_Code__c;
                                List<Trade__mdt> Trade = [SELECT CC_Code__c, description__c
                                                          FROM Trade__mdt
                                                          WHERE description__c = :workOrder[0].Work_Type_Name__c]; 
                                TradeMDT = Trade[0].CC_Code__c;
                                isTradeCodesExists=true;
                            }    
                    }                                        
                    //system.debug('CostCentre==> ' + CostCentre[0].CC_Code__c);
                    
                           
                    If (isTradeCodesExists) {                         
                                           
                        Double x = double.valueOf(CostCentreMDT);
                        integer CC = x.intValue();
                        Double z = double.valueOf(TradeMDT);
                        integer TradeCode = z.intValue();
                        
                        //create labour distribution set with found values
                        Map<String, Decimal> DistributionSetLabour = new Map<String, Decimal>();
                        DistributionSetLabour.put('CostCentre' , CC);
                        DistributionSetLabour.put('Trade' , TradeCode);
                        String gstDisSet;
                        String MaterialDisSet;
                        String LabourDisSet;  
                        //set cost center as 000 for GST, set GST paid code to 23100
                       
                        If (workOrder[0].Work_Type_Name__c =='Insurance'){
                            MaterialDisSet = ('11.' + CC + '.51600.' + TradeCode +'.'+ apInvoice[0].EventNonEventCode__c);
                            LabourDisSet = ('11.' + CC + '.51600.' + TradeCode + '.'+apInvoice[0].EventNonEventCode__c);
                        }
                        else if (workOrder[0].Work_Type_Name__c =='Reimbursement'){
                        
                            MaterialDisSet = ('11.' + CC + '.51900.' + TradeCode +'.'+ apInvoice[0].EventNonEventCode__c);
                            LabourDisSet = ('11.' + CC + '.51900.' + TradeCode + '.'+apInvoice[0].EventNonEventCode__c);
                        }
                        else{
                        
                            
                            MaterialDisSet = ('11.' + CC + '.51400.' + TradeCode +'.'+ apInvoice[0].EventNonEventCode__c);
                            LabourDisSet = ('11.' + CC + '.51300.' + TradeCode + '.'+ apInvoice[0].EventNonEventCode__c);
                        
                        }
                        gstDisSet = ('11.' + '000' + '.23100.' + '000.' +'000');
                        
                        system.debug('MaterialDisSet==> ' + MaterialDisSet); 
                        system.debug('gstDisSet==> ' + gstDisSet);                          
                        //assign created value to map and update apInvoice
                        List<AP_Invoice__c> apInvtoUpdate = [SELECT id, Status__c 
                                                                FROM AP_Invoice__c 
                                                                WHERE Id =:invoiceRecord.keySet()];
                                                            
                        if(apInvtoUpdate != null)
                            for (AP_Invoice__c apinv : apInvtoUpdate) {
                                apinv.GST_Distribution_Set__c = gstDisSet; 
                                apinv.Material_Distribution_Set__c = MaterialDisSet;
                                apinv.Labour_Distribution_Set__c = LabourDisSet;
                            }
                        update apInvtoUpdate;
                    }    
                    
                }
            }
            
            //}
        } 
        catch(exception e){
            system.debug('==>Exception'+e.getMessage()+' stackTrace==>'+e.getStackTraceString());
            return;
        }
    }
}