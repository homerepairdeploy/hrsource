/********************************************************************************************** 
Name        : ClaimConnect_AssessmentController_Test
Description : Test Class for ClaimConnect_AssessmentController
                                                            
VERSION        AUTHOR           DATE           DETAIL      DESCRIPTION   
1.0            Prasanth         25-05-2020     Created     ClaimConnect APP
/***********************************************************************************************/
@isTest
public class ClaimConnect_AssessmentController_Test {
    
    @testSetup static void setup() {
        Id strRecordTypeAccId = Schema.SobjectType.Account.getRecordTypeInfosByName().get('Insurance Provider').getRecordTypeId();
        /* Create common test Accounts */
        List<Account> testAccounts = new List<Account>();
        for(Integer i=0;i<2;i++) {
            testAccounts.add(new Account(Name = 'TestAcct'+i));
        }
        insert testAccounts;  
        
        /* Create common test Insurance Accounts */
        List<Account> testAccountInsurance = new List<Account>();
        testAccountInsurance.add(new Account(Name = 'TestAcct',RecordTypeId=strRecordTypeAccId,pds__c='www.google.com'));
        insert testAccountInsurance;     
        
        /* Create common test Contacts */
        List<Contact> testContacts = new List<Contact>();
        for(Integer i=0;i<2;i++) {
            testContacts.add(new Contact(FirstName = 'TestAcct'+i, LastName = 'TestAcct'+i));
        }
        insert testContacts;
        
        /* Create policy record */
        List<Policy__c > policyList = new List<Policy__c >();
        Policy__c policyobj = new Policy__c();
        policyobj.State__c='NSW';
        policyList.add(policyobj);
        insert policyList;
        
        /* Create Common Case */
        List<Case> caseList = new List<Case>();
        case caseobj = new case();
        caseobj.ContactId = testContacts[0].id;
        caseobj.AccountId = testAccounts[0].id;
        caseobj.Status = 'Working';
        caseobj.Policy__c = policyList[0].id;
        caseobj.Assessment_Report_Created__c=false;
        caseList.add(caseobj);
        
        insert caseList;
        system.debug('caseid'+caseList[0].id);
        
        // Create Insurance Case
        List<Case> caseListIns = new List<Case>();
        case caseobj1 = new case();
        caseobj1.ContactId = testContacts[0].id;
        caseobj1.AccountId = testAccountInsurance[0].id;
        caseobj1.Insurance_Provider__c=testAccountInsurance[0].id;
        caseobj1.Status = 'open';
        caseListIns.add(caseobj1);
        
        insert caseListIns;
        
        // Create Authority Case
        Id strRecordTypeCaseId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Authority').getRecordTypeId();  
        List<Case> Authority = new List<Case>();
        case caseobj2 = new case();
        //caseobj2.ContactId = testContacts[0].id;
        //caseobj2.AccountId = testAccountInsurance[0].id;
        caseobj2.parentid =caseList[0].id;
        caseobj2.RecordTypeId=strRecordTypeCaseId;
        caseobj2.Status = 'AcceptedinWork';
        caseobj2.Job_Type__c = 'doAndCharge';
        caseobj2.Action_Type__c ='authority';
        
        Authority.add(caseobj2);
        
        insert Authority;
        
        // Create Work Type
        List<worktype> tradeList=new List<worktype>();
        tradeList.add(new worktype(name = 'Assessment',EstimatedDuration=1));
        Insert tradeList;
        
        // Create Work order
        
      List<WorkOrder> workOrderList=new List<WorkOrder>();
      		  WorkOrder wo = new WorkOrder();
              wo.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Home Repair Assessors').getRecordTypeId();                           
              wo.CaseId = caseList[0].id;
              wo.WorkTypeId = tradeList[0].Id ;
              wo.Authority__c = Authority[0].Id;
              wo.ContactId = testContacts[0].id;
        	  wo.Work_Order_Authority__c = 'Do and Charge';
              workOrderList.add(wo);
        insert workOrderList;
        
      /* Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
     	User usr = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason.liveston@asdf.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US'
                           );
        
        Insert usr;*/
        
        
       Date d = date.today(); 
        
        OperatingHours bhs = new OperatingHours();
        bhs.Name = 'Base Calendar (Syd/Vic)';
        bhs.Name = 'Australia/Sydney';
        insert bhs;
        
        ServiceTerritory territory = new ServiceTerritory();
        territory.Name = 'National';
        territory.OperatingHoursId = bhs.Id;
        territory.IsActive = true;
        insert territory;
        
         Id UserId = UserInfo.getUserId();
        
        User us=[Select id from User where Id=:UserId Limit 1];
        ServiceResource Sr = new ServiceResource();
            Sr.RelatedRecordId=UserId;
        	Sr.Name = 'CurrentUserResource';
        	Sr.IsActive = true;
            Insert Sr;
        
        ServiceTerritoryMember stm = new ServiceTerritoryMember();
        stm.OperatingHoursId = bhs.Id;
        stm.ServiceResourceId = Sr.Id;
        stm.ServiceTerritoryId = territory.Id;
        stm.EffectiveStartDate = d-60;
        stm.EffectiveEndDate = d+60;
        //stm.TerritoryType = Primary;
        insert stm; 
        
        system.debug('WorkOrder---'+workOrderList[0]);
       List<ServiceAppointment> servApptList =new List<ServiceAppointment>();
        ServiceAppointment SA = new ServiceAppointment (status='Confirmed',
                                                        ParentRecordId= workOrderList[0].Id,
                                                        Work_Type_Name__c='Assessment',
                                                       Claim__c = caseList[0].id,Service_Resource__c = Sr.Id);
                                                       // SchedStartTime = d-2,SchedEndTime = d+10,
                                                       // ServiceTerritoryId = territory.Id);
                                       
        servApptList.add(SA);
        insert servApptList; 
        
        /* system.debug('servApptList'+servApptList + 'SA wo---' +servApptList[0].ParentRecordId);  
        
        
        List<AssignedResource> AssignedResource = new List<AssignedResource>();
        AssignedResource ar = new AssignedResource();
        ar.ServiceAppointmentId = SA.Id;
        ar.ServiceResourceId = Sr.Id;
        insert ar;*/
        
        
        
        // Create Ba Record
        
        List<Staging_Assessor_ba__c> baList = new List<Staging_Assessor_ba__c>();
        
        Staging_Assessor_ba__c StagingBA = new Staging_Assessor_ba__c();
        
        StagingBA.Case__c = caseList[0].Id;
        StagingBA.Building_Height__c = 'Single';
        StagingBA.Insurable__c = true;
        StagingBA.Cash_Settlement__c = false;
        
        baList.add(StagingBA);
        Insert baList;
        system.debug('<<<ba'+baList);
        
        // Create Room Record
        
        List<Staging_Assessor_sow_rooms__c> roomList = new List<Staging_Assessor_sow_rooms__c>();
        Staging_Assessor_sow_rooms__c SAroom = new Staging_Assessor_sow_rooms__c();
        SAroom.Name = 'Room1';
        SAroom.Staging_Assessor_BA__c = baList[0].id;
        roomList.add(SAroom);
        
        Insert roomList;
        
        
                //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
        
        //Get Content Documents
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
        
        //Create ContentDocumentLink
        List<ContentDocumentLink> cdLinkList = new List<ContentDocumentLink>();        
        ContentDocumentLink cd2 = New ContentDocumentLink();
        cd2.LinkedEntityId = baList[0].Id;
        cd2.ContentDocumentId = conDocId;
        cd2.shareType = 'V';
        cdLinkList.add(cd2);
        
        Insert cdLinkList;
        
        // Create Suncorp Room Record
        
        List<Room__c> rmList = new List<Room__c>();
        Room__c room = new Room__c();
        room.Name = 'Sun Room';
        room.Length__c=10;
        room.Height__c=20;
        room.Width__c=10;
        room.Claim__c=caseList[0].id;
        rmList.add(room);
        
        Room__c room1 = new Room__c();
        room1.Name = 'Sun Room1';
        room1.Claim__c=caseList[0].id;
        rmList.add(room1);
        
        Insert rmList;
        
        
        // Create RoomItem Record
        
        List<Staging_Assessor_sow_roomitems__c> roomItemList = new List<Staging_Assessor_sow_roomitems__c>();
        Staging_Assessor_sow_roomitems__c roomItem = new Staging_Assessor_sow_roomitems__c();
        roomItem.Site_Visit_Number__c =1;
        roomItem.Staging_Assessor_SOW_rooms__c = roomList[0].id;
        roomItem.UOM__c ='EA';
        roomItem.Labour_Time__c =60;
        roomItem.Quantity__c=1;
        roomItem.Labour_Price__c=100;
        roomItem.Material_Amount__c=300;
        roomItem.Cash_Settled__c = true;  
        roomItemList.add(roomItem);
        
        Insert roomItemList;
        
        // Create Work Code Group Records
        
        List<Work_Code_Group__c> wcGroupList1 = new List<Work_Code_Group__c>();
        Work_Code_Group__c wcGroup1 = new Work_Code_Group__c();
        wcGroup1.Level__c  = 'Level 1';
        wcGroup1.Description__c  ='Cornice';
        wcGroupList1.add(wcGroup1);
        Insert wcGroupList1;
        
        List<Work_Code_Group__c> wcGroupList2 = new List<Work_Code_Group__c>();
        Work_Code_Group__c wcGroup2 = new Work_Code_Group__c();
        wcGroup2.Level__c  = 'Level 2';
        wcGroup2.Description__c  ='Cove 75mm';
        wcGroupList2.add(wcGroup2);
        Insert wcGroupList2;
        
        List<Work_Code_Group__c> wcGroupList3 = new List<Work_Code_Group__c>();
        Work_Code_Group__c wcGroup3 = new Work_Code_Group__c();
        wcGroup3.Level__c  = 'Level 3';
        wcGroup3.Description__c  ='7.1 to 10 LM';
        wcGroupList3.add(wcGroup3);
        Insert wcGroupList3;
        
        List<Work_Code_Group__c> wcGroupList4 = new List<Work_Code_Group__c>();
        Work_Code_Group__c wcGroup4 = new Work_Code_Group__c();
        wcGroup4.Level__c  = 'Level 4';
        wcGroup4.Description__c  ='Strip out & Replace';
        wcGroupList4.add(wcGroup4);
        Insert wcGroupList4;
        
        
        List<Work_Code_Group__c> wcGroupList5 = new List<Work_Code_Group__c>();
        Work_Code_Group__c wcGroup5 = new Work_Code_Group__c();
        wcGroup5.Level__c  = 'Level 5';
        wcGroup5.Description__c  ='-';
        wcGroupList5.add(wcGroup5);
        Insert wcGroupList5;
        
        
        // Create Product Record
        
        List<Product2> prodList = new List<Product2>();
        Product2 pb2 = new Product2();
        pb2.name='Cornice Cove 75mm 7.1 to 10 LM Strip out & Replace -';
        pb2.Level_1__c  =wcGroupList1[0].id;
        pb2.Level_2__c  =wcGroupList2[0].id;
        pb2.Level_3__c  =wcGroupList3[0].id;
        pb2.Level_4__c  =wcGroupList4[0].id;
        pb2.Level_5__c  =wcGroupList5[0].id;
        pb2.Work_Type__c=tradeList[0].id;
        pb2.UOM__c='EA';
        pb2.IsActive = true;
        
        
        prodList.add(pb2);
        Insert prodList;
        
        system.debug('<<checl'+prodList);
        
        Id pricebookId = Test.getStandardPricebookId();
        
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = prodList[0].Id,
            UnitPrice = 10000, 
            IsActive = true);
        insert standardPrice;
        
        
        List<Pricebook2> pbList1 = new List<Pricebook2>();
        
        Pricebook2 objPb1 = new Pricebook2();
        
        objPb1.Labour_TIME_mins__c  = 90;
        objPb1.State__c  = 'NSW';
        objPb1.Name  = 'Standard Price Book';
        objPb1.IsActive=true;
        
        pbList1.add(objPb1);
        Insert pbList1;
        
        // Create pricebookentry Record
        
        List<pricebookentry> pbeList1 = new List<pricebookentry>();
        
        pricebookentry objPbe1 = new pricebookentry();
        
        objPbe1.UnitPrice =100;
        objPbe1.Material_Price__c =100;
        objPbe1.Labour_Price__c =100;
        objPbe1.IsActive=true;
        objPbe1.Pricebook2Id  = pbList1[0].id;
        objPbe1.Product2Id  = prodList[0].id;
        
        pbeList1.add(objPbe1);
        Insert pbeList1;
        
    }    
    @isTest static void getCustomerName(){   
        
        List<Case> objCase =[SELECT ID from Case Limit 1];
        
        Test.startTest();
        String Name = ClaimConnect_AssessmentController.getCustomerName(objCase.get(0).Id);
        system.assertNotEquals(Name, null);
        Test.stopTest();
    }    
    
    @isTest static void getPdsDetails(){   
        
        List<Case> objCase =[SELECT ID from Case where Insurance_Provider__c !=null Limit 1 ];
        
        Test.startTest();
        String Pds = ClaimConnect_AssessmentController.getPdsDetails(objCase.get(0).Id);
        system.assertNotEquals(Pds, null);
        Test.stopTest();
    }  
    
    @isTest static void fetchWorkTypeNames(){   
        
        Test.startTest();
        List<WorkType> tradeList = ClaimConnect_AssessmentController.fetchWorkTypeNames();
        system.debug('<<<'+tradeList.size());
        //system.assertEquals(tradeList.size(), null);
        Test.stopTest();
    }     
    
    @isTest static void fetchWorkCodeGroupCatagory(){  
        
        List<Product2> prodList =[SELECT work_type__c from Product2 Limit 1];
        
        Test.startTest();
        List<String> category = ClaimConnect_AssessmentController.fetchWorkCodeGroupCatagory(prodList.get(0).work_type__c);
        system.debug('<<<'+category);
        //system.assertEquals(tradeList.size(), null);
        Test.stopTest();
    }     
    
    @isTest static void fetchWorkCodeGroupItemDescription(){  
        
        List<Product2> prodList = [SELECT work_type__c,Level_1_Description__c  from Product2 Limit 1];
        
        Test.startTest();
        List<String> itemDesc = ClaimConnect_AssessmentController.fetchWorkCodeGroupItemDescription(prodList.get(0).work_type__c,prodList.get(0).Level_1_Description__c);
        system.debug('<<<'+itemDesc);
        //system.assertEquals(tradeList.size(), null);
        Test.stopTest();
    }  
    
    @isTest static void fetchWorkCodeGroupDimensions(){  
       
        List<Product2> prodList = [SELECT work_type__c,Level_1_Description__c,Level_2_Description__c  from Product2 Limit 1];
        
        Test.startTest();
        List<String> grpDimension = ClaimConnect_AssessmentController.fetchWorkCodeGroupDimensions(prodList.get(0).work_type__c,prodList.get(0).Level_1_Description__c,prodList.get(0).Level_2_Description__c);
        system.debug('<<<'+grpDimension);
        //system.assertEquals(tradeList.size(), null);
        Test.stopTest();
    }  
    
    @isTest static void fetchWorkCodeGroupRepairMethod(){  
       
        List<Product2> prodList = [SELECT work_type__c,Level_1_Description__c,Level_2_Description__c,Level_3_Description__c  from Product2 Limit 1];
        
        Test.startTest();
        List<String> repairMethod = ClaimConnect_AssessmentController.fetchWorkCodeGroupRepairMethod(prodList.get(0).work_type__c,prodList.get(0).Level_1_Description__c,prodList.get(0).Level_2_Description__c,prodList.get(0).Level_3_Description__c);
        system.debug('<<<'+repairMethod);
        //system.assertEquals(tradeList.size(), null);
        Test.stopTest();
    }  
    
    @isTest static void fetchWorkCodeGroupAdditionalNotes(){  
        
        List<Product2> prodList = [SELECT work_type__c,Level_1_Description__c,Level_2_Description__c,Level_3_Description__c,Level_4_Description__c  from Product2 Limit 1];
        
        Test.startTest();
        List<String> addNotes = ClaimConnect_AssessmentController.fetchWorkCodeGroupAdditionalNotes(prodList.get(0).work_type__c,prodList.get(0).Level_1_Description__c,prodList.get(0).Level_2_Description__c,prodList.get(0).Level_3_Description__c,prodList.get(0).Level_4_Description__c);
        system.debug('<<<'+addNotes);
        //system.assertEquals(tradeList.size(), null);
        Test.stopTest();
    }  
    
    @isTest static void fetchProduct(){  
        
        List<Staging_Assessor_ba__c> objBa =[SELECT ID from Staging_Assessor_ba__c Limit 1];
        List<Product2> prodList = [SELECT work_type__c,Level_1_Description__c,Level_2_Description__c,Level_3_Description__c,Level_4_Description__c,Level_5_Description__c  from Product2 Limit 1];
        system.debug('prod'+prodList);
        Test.startTest();
        ClaimConnect_AssessmentController.ProductWrapper pw = ClaimConnect_AssessmentController.fetchProduct(prodList.get(0).work_type__c,prodList.get(0).Level_1_Description__c,prodList.get(0).Level_2_Description__c,prodList.get(0).Level_3_Description__c,
                                                                                                             prodList.get(0).Level_4_Description__c,prodList.get(0).Level_5_Description__c,objBa.get(0).Id);
        system.debug('<<<'+pw);
        //system.assertEquals(tradeList.size(), null);
        Test.stopTest();
    }  
    
    @isTest static void getBARecord(){ 
        
        List<Staging_Assessor_ba__c> objBa =[SELECT ID from Staging_Assessor_ba__c Limit 1];
        Test.startTest();
        Staging_Assessor_ba__c ba = ClaimConnect_AssessmentController.getBARecord(objBa.get(0).Id);
        system.debug('<<<'+ba);
        system.assertEquals(ba.Insurable__c, true);
        Test.stopTest();
    }   

    
    @isTest static void getClaimRecord(){ 
        List<Case> objCase =[SELECT ID from Case Limit 1];
        // caseWrapper caseDetail = new caseWrapper();
        Test.startTest();
        ClaimConnect_AssessmentController.caseWrapper cw = ClaimConnect_AssessmentController.getClaimRecord(objCase.get(0).Id);
        system.debug('<<<'+cw);
        // system.assertEquals(ba, false);
        Test.stopTest();
    }  
    
    @isTest static void insertRooms(){ 
        List<Staging_Assessor_ba__c> objBa =[SELECT ID from Staging_Assessor_ba__c Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.insertRooms(objBa.get(0).Id);
        Test.stopTest();
    }   
    
    @isTest static void deleteRooms(){ 
        List<Staging_Assessor_sow_rooms__c> objRoom =[SELECT ID from Staging_Assessor_sow_rooms__c Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.deleteRooms(objRoom.get(0).Id);
        Test.stopTest();
    }   
    
    @isTest static void autoSaveRoomRecords(){ 
        List<Staging_Assessor_sow_rooms__c> objRoom =[SELECT ID from Staging_Assessor_sow_rooms__c Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.autoSaveRoomRecords(objRoom);
        Test.stopTest();
    }   
    
    @isTest static void insertRoomItems(){ 
        List<Staging_Assessor_sow_rooms__c> objRoom =[SELECT ID from Staging_Assessor_sow_rooms__c Limit 1];
        List<Product2> prodList = [SELECT Id from Product2 Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.insertRoomItems(prodList.get(0).Id,objRoom.get(0).Id,60,2,100,100);
        Test.stopTest();
    }   
    
    @isTest static void duplicateRoomWithTradeItems(){ 
        List<Staging_Assessor_sow_rooms__c> objRoom =[SELECT ID from Staging_Assessor_sow_rooms__c Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.duplicateRoomWithTradeItems(objRoom.get(0).Id);
        Test.stopTest();
    }   
    
    @isTest static void deleteTradeItems(){ 
        List<Staging_Assessor_sow_roomitems__c> objRoomItem =[SELECT ID from Staging_Assessor_sow_roomitems__c Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.deleteTradeItems(objRoomItem.get(0).Id);
        Test.stopTest();
    }   
    
    @isTest static void getRoomRecords(){ 
        List<Staging_Assessor_ba__c> objBa =[SELECT ID from Staging_Assessor_ba__c Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.getRoomRecords(objBa.get(0).Id);
        Test.stopTest();
    }   
    
    @isTest static void getRoomItemRecords(){ 
        List<Staging_Assessor_sow_rooms__c> objRoom =[SELECT ID from Staging_Assessor_sow_rooms__c Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.getRoomItemRecords(objRoom.get(0).Id);
        Test.stopTest();
    }   
    
  /*  @isTest static void checkSiteNumber(){ 
        List<Staging_Assessor_sow_roomitems__c> objRoomItem =[SELECT ID from Staging_Assessor_sow_roomitems__c Limit 1];
        Test.startTest();
        Decimal siteNumber=ClaimConnect_AssessmentController.checkSiteNumber(objRoomItem.get(0).Id);
        Test.stopTest();
    }  */ 
    
    @isTest static void getUploadedPhotos(){ 
        List<Staging_Assessor_ba__c> objBa =[SELECT ID from Staging_Assessor_ba__c Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.getUploadedPhotos(objBa.get(0).Id);
        Test.stopTest();
    }   
    
    @isTest static void deleteTempContentDocs(){ 
        Test.startTest();
        ClaimConnect_AssessmentController.deleteTempContentDocs();
        Test.stopTest();
    } 
    
    @isTest static void populateRoomDetails(){ 
        List<Case> objCase =[SELECT ID from Case Limit 1];
        List<Staging_Assessor_ba__c> objBa =[SELECT ID from Staging_Assessor_ba__c Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.populateRoomDetails(objCase.get(0).Id,objBa.get(0).Id);
        Test.stopTest();
    }   
    
    @isTest static void getStagingBADataSrch(){ 
        Test.startTest();
        ClaimConnect_AssessmentController.getStagingBADataSrch('523456');
        Test.stopTest();
    } 
    
    @isTest static void getSubAssessmentsSrch(){ 
       /* ServiceAppointment SA = [Select id,ParentRecordId,AppointmentNumber from ServiceAppointment WHERE Service_Resource__c IN 
                                 (select Id from ServiceResource where RelatedRecordId= :userinfo.getuserid())Limit 1];
        WorkOrder WO = [Select  Case.Id,Case.CaseNumber,Case.Claim_Number__c, Case.Description,Case.Full_Risk_Address__c,Authority__r.Assessment_Completed__c  From WorkOrder
                                      	 where Id = :SA.ParentRecordId];*/
        
        Test.startTest();
        ClaimConnect_AssessmentController.getSubAssessmentsSrch('523456');
        Test.stopTest();
    }   
    
    @isTest static void getStagingBAData(){ 
        Test.startTest();
        ClaimConnect_AssessmentController.getStagingBAData(1,1);
        Test.stopTest();
    }   
    
    @isTest static void getSubAssessments(){ 
        ServiceAppointment SA = [Select id,AppointmentNumber from ServiceAppointment Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.getSubAssessments(1,1);
        Test.stopTest();
    }  
    
    @isTest static void getServAppointments(){ 
        ServiceAppointment SA = [Select id,AppointmentNumber from ServiceAppointment Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.getServAppointments(1,1);
        Test.stopTest();
    }  
    
    @isTest static void getServAppointmentsSrch(){ 
       ServiceAppointment SA = [Select id,AppointmentNumber from ServiceAppointment Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.getServAppointmentsSrch(SA.AppointmentNumber);
        Test.stopTest();
    } 
    
    @isTest static void getcreateBARecord(){ 
        List<Case> objCase =[SELECT ID from Case Limit 1];
        List<ServiceAppointment> servApptList =[SELECT ID from ServiceAppointment Limit 1];
		Test.startTest();
        ClaimConnect_AssessmentController.createBARecord(objCase[0].id,servApptList[0].ID);
        Test.stopTest();
        List<Staging_Assessor_ba__c> stagList=[SELECT Id from Staging_Assessor_ba__c Limit 1];
        system.debug('<<<ba2'+stagList);
        ClaimConnect_AssessmentController.updateBARecord(stagList[0]);
        ClaimConnect_AssessmentController.getPrevAssmnt(objCase[0].id);
    } 
    
        @isTest static void saveTradeItems(){ 
       List<Staging_Assessor_sow_roomitems__c> roomItemList =[SELECT ID from Staging_Assessor_sow_roomitems__c Limit 1];
        Test.startTest();
        ClaimConnect_AssessmentController.saveTradeItems(roomItemList[0]);
        Test.stopTest();
    } 
    
}