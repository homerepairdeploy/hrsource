/***************************************************************** 
Purpose: Test Class for AccountTrigger 
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            -          22/03/2019      Created      Home Repair Claim System  
*******************************************************************/
@istest
public class AccountTriggerTest {
    
    @TestSetup
    static void TestdataCreateMethod() {
        //Create the Account Record.
        Account acc = HomeRepairTestDataFactory.createAccounts('Test WorkOrder Account')[0];  
        BSB_Reference__c  bsb = new BSB_Reference__c(Branch_Number__c = '121', Bank_Branch_Name__c = 'hello1', Bank_Code__c = '3232', Bank_Name__c = 'ewewe');
        insert bsb;
        //Create the Contact Record.
        contact conVar = HomeRepairTestDataFactory.createTradeContact('test WO Contatct',acc.Id);        
        
        Home_Repairs_Trigger_Switch__c hrts = HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        BypassWOValidation__c  byps = HomeRepairTestDataFactory.createBypassWOValidation(false);
        //Create the Policy Record.
                
    }

    private static testMethod void testUpdateWorkOrder() {
        Account accl=[Select id,RecordType.name from Account limit 1];
        accl.Accounts_Alt_Phone__c = '8723923';
        accl.bsb__C = '121';
        accl.name = 'hello';
        accl.Type  = 'Trade Company';
        accl.RecordType.Name='Trade Account';
        System.debug('in test ' + accl);
        Test.startTest();
        update accl;
        
        Test.stopTest();
    }
    
    private static testMethod void testUpdateWorkOrder2() {
        Account accl=[Select id from Account limit 1];
        accl.Accounts_Alt_Phone__c = '8723923';
        accl.name = 'hello';
        accl.Type  = 'Trade Company';
        accl.Oracle_Site_Id__c = 'dsds';
        System.debug('in test ' + accl);
        Test.startTest();
        update accl;
        
        Test.stopTest();
    }
    

}