/************************************************************************************************************
Name: hrTradeScheduleSuppressProcessor
=============================================================================================================
Purpose: Supress Trades from Assigning Appointments 
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL        DESCRIPTION
1.0        xx               xxx              Created        HomeRepair 
*************************************************************************************************************/

public class hrTradeScheduleSuppressProcessor {
    /***************************************************************** 
    Purpose: Invocable Method called from the Process builder which controls                                                      
    Parameters: List of Account Ids
    Returns: none
    Throws [Exceptions]: None                                                          
    History                                                             
    --------                                                            
   VERSION    AUTHOR            DATE             DETAIL            DESCRIPTION
    1.0        xx               xxx              Created           HomeRepair  
    ****************************************************************************************/
    @InvocableMethod
    public static void collectTradeAccountIds(List<Id> AcctIds) {
    
        set<Id> suppressTradeIds=new set<Id>();
        set<Id> releaseTradeIds=new set<Id>();
        for(Account acc:[select Id,Suppressed_from_Scheduling__c,suppress_from_schedule__c from Account where Id=:AcctIds]){
             system.debug('suppress' +acc.suppress_from_schedule__c);
             If (acc.suppress_from_schedule__c || acc.Suppressed_from_Scheduling__c) suppressTradeIds.add(acc.Id); else releaseTradeIds.add(acc.Id);
        }
        If (!suppressTradeIds.isEmpty())processSuppressTradesFromScheduling(suppressTradeIds);
        If (!releaseTradeIds.isEmpty()) processUnSupressTradesFromScheduling(releaseTradeIds);
        
        
    }

     
    public static void processSuppressTradesFromScheduling(set<Id> suppressTradeIds){
    
        map<Id,ServiceResource> SRMap=new map<Id,ServiceResource>([select Id from ServiceResource where (Contact__r.AccountId =:suppressTradeIds or ServiceCrew.Service_Crew_Company__c=:suppressTradeIds) and isactive=true]);
        If (SRMap.keySet().size() > 0) Database.executeBatch(new AddExcludeResourceToWO(SRMap),200);
         
                   
    }
    
    
    public static void processUnSupressTradesFromScheduling(set<Id> releaseTradeIds){
    
    
		map<Id,ServiceResource> SRMap=new map<Id,ServiceResource>([select Id from ServiceResource where Contact__r.AccountId =:releaseTradeIds or ServiceCrew.Service_Crew_Company__c=:releaseTradeIds]);
		If (SRMap.keySet().size() > 0) Database.executeBatch(new ReleaseExcludeResourceFromWO(SRMap),200);        
    }
    
    public static void insertResourcePreference(list<WorkOrder> WOlst,list<ServiceResource> SRlst,map<Id,Id> WOSRMap){
    
      
       list<ResourcePreference> InsRPlst=new list<ResourcePreference>();
        for(WorkOrder wk:WOlst){
            for(ServiceResource SR:SRlst){
                    If (!WOSRMap.containsKey(WK.Id) && WOSRMap.get(WK.Id) != SR.Id){
                        ResourcePreference RP=new ResourcePreference();
                        RP.RelatedRecordId=wk.Id;
                        RP.ServiceResourceId=SR.Id;
                        RP.PreferenceType='Excluded';
                        InsRPlst.add(RP);
                    }    
                   
            }   
        
        }
        If (!InsRPlst.isEmpty()) insert InsRPlst;
    
    
    }

        


    
    
}