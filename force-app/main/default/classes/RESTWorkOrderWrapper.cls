global class RESTWorkOrderWrapper{
    public String workOrderId;
    public string status;
    public string assignedResource;
    public String workTypeId;
    public String workTypeName;
    public Decimal sortOrder;
    public Decimal totalLabourAmount;
    public Decimal totalMaterialAmount;
    public Boolean isPreview;
    public string claimJobID;
    public string claimJobType;
    public String workOrderNumber;
    public List<RESTWorkOrderLineItemWrapper> workOrderLineItems;
    public RESTWorkOrderWrapper(WorkOrder wo,List<RESTWorkOrderLineItemWrapper> workOrderLineItemList){
        this.workOrderId  = wo.Id;
        this.status = wo.status;
        this.assignedResource=wo.Assigned_Resource__c;
        this.workTypeId=wo.WorkTypeId;
        this.workTypeName=wo.WorkType.Name;
        this.sortOrder = wo.Sort_Order__c;
        this.totalLabourAmount=wo.Total_Labour_Amount_New__c;
        this.totalMaterialAmount=wo.Total_Material_Amount_New__c;
        this.workOrderLineItems=workOrderLineItemList; 
        this.isPreview = wo.isPreview__c;
        this.claimJobID = wo.Authority__r.Id;
        this.workOrderNumber = wo.WorkOrderNumber;
        this.claimJobType = wo.Authority__r.Job_Type__c;
    }
    
}