/***************************************************************** 
Purpose: AccountService for AccountTrigger                                                      
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            Vasu          05/06/2019      Created      Home Repair Claim System  
*******************************************************************/
public with sharing class AccountService {
    /***************************************************************** 
Purpose: Get values from BSB Reference and update account with respective field values
Parameters:Set<String>
Returns: none
Throws [Exceptions]: DML                                                          
*******************************************************************/  
    public static  Boolean updateDone = False;
    public static void updateBSBReferenceValues(List<String> BSBSet){
        
        List <Account> acctList = [Select id,BSB__c,Branch__c,Bank_Name__c,Bank_Code__c  from Account where Type = 'Trade Company' and RecordType.Name='Trade Account' and BSB__c IN : BSBSet];
        if(acctList.size() > 0){
            List<BSB_Reference__c> bsbList=new List<BSB_Reference__c>();
            Map<string,BSB_Reference__c> bsbReferenceMap=new Map<string,BSB_Reference__c>();
            
            for(BSB_Reference__c bsb : [Select id,Bank_Branch_Name__c,Bank_Code__c,Bank_Name__c,Branch_Number__c from BSB_Reference__c where Branch_Number__c IN : BSBSet]){
                bsbReferenceMap.put(bsb.Branch_Number__c,bsb);
            }
            for(Account acct : acctList) {                                
                if(bsbReferenceMap.containskey(acct.BSB__c)){
                    acct.Branch__c= bsbReferenceMap.get(acct.BSB__c).Bank_Branch_Name__c; 
                    acct.Bank_Name__c= bsbReferenceMap.get(acct.BSB__c).Bank_Name__c;
                    acct.Bank_Code__c = bsbReferenceMap.get(acct.BSB__c).Bank_Code__c;                        
                }else{
                    
                    Account ff = (Account)Trigger.newMap.get(acct.id); 
                    ff.addError('BSB Not Found');
                }                                   
            }
        }
        if(acctList.size() > 0){
            Update acctList;
        }
        
    }
    
    public static void resetSupplierIntgFlds(List<ID> accnt){                 
        
        if(!updateDone) {
            system.debug('In the mehtod to update Oracle fields :' + updateDone);
            List<Account> acList  = new List<Account>([Select ID, Exclude_From_Oracle_Integration__c, Oracle_Supplier_Upload_Step__c, Oracle_Process_Status__c FROM Account WHERE Type = 'Trade Company' AND ID in :accnt]);
            List<Account> acList1 =  new List<Account> ();
            For(Account acc : acList) { 
                Account accObj = new Account();
                accObj.Oracle_Supplier_Upload_Step__c =  ' ';
                accObj.Oracle_Process_Status__c  = 'Pending';
                accObj.Exclude_From_Oracle_Integration__c	= True;
                accObj.ID = acc.ID;
                acList1.add(accObj);
            }
            
            if(acList1.size() > 0){ 
                updateDone = true;
                system.debug('Orcale fields updated');
                Update acList1;
                
            }
        }
        
    }
}