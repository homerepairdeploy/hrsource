/***************************************************************** 
Purpose:  Once off format contact phone fields. 
History                                                             
--------                                                            
VERSION        AUTHOR          DATE           DETAIL       Description 
1.0            Pardha       23/Dec/2019      Created      Home Repair Claim System 
*******************************************************************/
global class OnceoffBatchFormatCTCPhone implements Database.Batchable<sObject> {
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        String query='select id,Phone,HomePhone,MobilePhone from contact';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<contact> scope) {
    
        list<contact> updtctclst=new list<contact>();
        for(contact ctc:scope){
              
              contact ct=new contact();
			  ct.id=ctc.id;
    	      if (ctc.phone!=null) ct.phone=CommonUtilityClass.formatNumber(ctc.phone);
		      if (ctc.homephone!=null) ct.homephone=CommonUtilityClass.formatNumber(ctc.homephone);
			  if (ctc.mobilephone!=null) ct.mobilephone=CommonUtilityClass.formatNumber(ctc.mobilephone);
			  updtctclst.add(ct);
            
        }
        update updtctclst;
        
        
    }
    
    global void finish(Database.BatchableContext bc){ 
        
    }
    
}