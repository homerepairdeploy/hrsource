/***************************************************************** 
Purpose: Test Class for AccountTrigger 
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            -          22/03/2019      Created      Home Repair Claim System  
*******************************************************************/
@istest
public class VariationStatusUpdateInvoceTest {
    @TestSetup
    static void TestdataCreateMethod() {
        Account acc = HomeRepairTestDataFactory.createAccounts('Test WorkOrder Account')[0];        
        //Create the Contact Record.
        contact conVar = HomeRepairTestDataFactory.createTradeContact('test WO Contatct',acc.Id);        
        
        Home_Repairs_Trigger_Switch__c hrts = HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        BypassWOValidation__c  byps = HomeRepairTestDataFactory.createBypassWOValidation(false);
        //Create the Policy Record.
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        //Create the Contact Record.
        Contact con=HomeRepairTestDataFactory.createContact('ClaimTest');
        //Create the Case Record.
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);                
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');        
        case ca=HomeRepairTestDataFactory.createClaimAuthority (cs.Id);        
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id); 
        wo.Status  = 'Accepted WO';
        update wo;
        List<WorkOrderLineItem> woliList=HomeRepairTestDataFactory.createWorkOrderLineItem('Home Repair',cs.id,wt.Id,wo.Id,1);
        
        
    }   
  
    private static testMethod void testUpdateWorkOrder() {
                list<WorkOrder> wolist = [Select id,CaseId,WorkTypeId,Authority__c from WorkOrder limit 1];
                Variation__c vv = new Variation__c(work_order__c = wolist[0].ID, Variation_Status__c = 'Requested');
                 insert vv;

    Test.startTest();
        VariationStatusUpdateInvoce.updateVariationStatus(woList);
        Test.stopTest();
    }

    }