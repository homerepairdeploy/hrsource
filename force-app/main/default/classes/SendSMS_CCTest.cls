@isTest
public class SendSMS_CCTest {
    public static testMethod void sendSMSTest() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorTwilio ());
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        HomeRepairTestDataFactory.generalSettings('Account SID', '22222222222222333dfdf');
        HomeRepairTestDataFactory.generalSettings('Twilio From Number', '04xxxxx');
        HomeRepairTestDataFactory.generalSettings('Account Token', '1111111111');
        HomeRepairTestDataFactory.generalSettings('Twilio SMS URL', 'https://api.twilio.com/2010-04-01/Accounts/');
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        
        Test.startTest();
            case Claim=SendSMS_cc.getClaimDetails(cs.id);
            String response = SendSMS_cc.sendSMSToClaim('04xxxxxx', 'Hi There', con.Id, cs.Id);
            system.assertNotEquals(Claim.Id, null);
        Test.stopTest();
    }
    public static testMethod void sendSMSNegativeTest() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorTwilio ());        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorTwilio ());
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        HomeRepairTestDataFactory.generalSettings('Account SID', '22222222222222333dfdf');
        HomeRepairTestDataFactory.generalSettings('Twilio From Number', '04xxxxx');
        HomeRepairTestDataFactory.generalSettings('Account Token', '1111111111');
        HomeRepairTestDataFactory.generalSettings('Twilio SMS URL', 'https://api.twilio.com/2010-04-01/Accounts/');
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);      
        Test.startTest();
            String response = SendSMS_cc.sendSMSToClaim('04xxxxx', 'Hi There', '12345678', cs.Id);
           
            system.assertNotEquals(response , null);
        Test.stopTest();
    }
    
    public static testMethod void sendSMSPositiveTest() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorTwilio ());
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        HomeRepairTestDataFactory.generalSettings('Account SID', '22222222222222333dfdf');
        HomeRepairTestDataFactory.generalSettings('Twilio From Number', '04xxxxx');
        HomeRepairTestDataFactory.generalSettings('Account Token', '1111111111');
        HomeRepairTestDataFactory.generalSettings('Twilio SMS URL', 'https://api.twilio.com/2010-04-01/Accounts/');
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        
        Test.startTest();
            
           
            SMSUtility.processSmsFuture('Hi There','04xxxxxxx' );
            
        Test.stopTest();
    }
}