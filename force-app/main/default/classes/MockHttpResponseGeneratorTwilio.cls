@isTest
global class MockHttpResponseGeneratorTwilio implements HttpCalloutMock{
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"sid": "SMfae9b0ab909846249ccf2c3b28b37ebe", "date_created": "Wed, 03 Apr 2019 22:46:57 +0000", "date_updated": "Wed, 03 Apr 2019 22:46:57 +0000", "date_sent": null, "account_sid": "AC82cb02546cdcf6eb5d8855779e6052ef", "to": "+61421275936", "from": "+61488852667", "body": "ggff", "status": "CREATED"}');
        res.setStatusCode(200);
        res.setStatus('CREATED');
        return res;
    }
}