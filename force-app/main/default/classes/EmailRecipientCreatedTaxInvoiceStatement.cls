Global class EmailRecipientCreatedTaxInvoiceStatement {

    @InvocableMethod
    public static void EmailRecipientCreatedTaxInvoiceStatement(List<AP_Invoice__c> invoices) {

        Map<Id,Attachment> attachmentByInvoice = new Map<Id,Attachment>();
        List<AP_Invoice__c> updateList = new List<AP_Invoice__c>();
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'accounts@homerepair.com.au'];
        List<EmailTemplate> templates = [SELECT Id, Subject, Body, DeveloperName FROM EmailTemplate WHERE DeveloperName = 'RCTI_PDF_Invoice_Email'];

        for(Attachment attachment : [SELECT Name, Body,ParentId FROM Attachment WHERE ParentId IN :invoices AND Name LIKE 'RCTI%']) {
            if(!attachmentByInvoice.containsKey(attachment.ParentId)) {
                attachmentByInvoice.put(attachment.ParentId, attachment);
            }
        }
        for(AP_Invoice__c invoice : invoices) {

            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

            // Set the recipient address
            message.setToAddresses(new String[]{
                    invoice.Account_Email__c
                    //'dilshan.egodawela@cludo.com.au'
                });

            if (owea.size() > 0) {
                message.setOrgWideEmailAddressId(owea.get(0).Id);
                }

            // Attach files to the message
            List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName('RCTI-' + invoice.Name + '-' + System.today().format() + '.pdf');

            if (!Test.isRunningTest() && attachmentByInvoice.containsKey(invoice.Id)) {
                efa.setBody(attachmentByInvoice.get(invoice.Id).Body);
            } else {
                efa.setBody(Blob.valueOf('Test Data'));
            }

            attachments.add(efa);
            System.debug('efa = ' + efa);
            message.setFileAttachments(attachments);

            // Set the message template

            if (!templates.isEmpty()) {
                message.setTemplateId(templates[0].Id);
                if (!Test.isRunningTest()) {
                    String subject = templates[0].Subject.replaceAll('AP_Invoice_Name', invoice.Name);
                    message.setSubject(subject.replaceAll('AP_Invoice_Work_Order', invoice.Work_Order__r.WorkOrderNumber));
                    String body = templates[0].Body.replaceAll('AP_Invoice_Name', invoice.Name);
                    message.setPlainTextBody(body.replaceAll('AP_Invoice_Work_Order', invoice.Work_Order__r.WorkOrderNumber));
                } else {
                    message.setSubject('test');
                    message.setPlainTextBody('test');
                }
                message.setWhatId(invoice.Id);
            }

            // Send the message
            try {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                        message
                });
                invoice.RCTI_PDF_Trade_Status__c = 'Sent';
                updateList.add(invoice);
            } catch (Exception e) {
                invoice.RCTI_PDF_Trade_Status__c = 'Error';
                updateList.add(invoice);
                System.debug('Error :'+e.getMessage());
            }
        }
        if(!updateList.isEmpty()) {
            update updateList;
        }
    }
}