public class OnceOffUpdateWOValue implements Database.Batchable<sObject> {
    
    public static final String WO_STATUS_COMPLETED = 'closed';
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query='select id,Total_Variations_Amounts_Ex_Rejected__c,Variation_Total__c,Up_To_Amount__c from workorder where status!=:WO_STATUS_COMPLETED';
        return Database.getQueryLocator(query);
    }
    
    
    public void execute(Database.BatchableContext BC, List<workorder> scope) {
        list<workorder> lstwo=new list<workorder>(); 
        for(workorder wo:scope){
            workorder wosingle=new workorder();
            wosingle.id=wo.id;
            If (wo.Up_To_Amount__c==null) wo.Up_To_Amount__c=0;
            If (wo.Variation_Total__c==null) wo.Variation_Total__c=0;
            
            wosingle.Total_Variations_Amounts_Ex_Rejected__c=wo.Up_To_Amount__c+wo.Variation_Total__c;
            lstwo.add(wosingle);
        }
       
        update lstwo;
       
      
    }
    
    public void finish(Database.BatchableContext bc){ 
        
    }
    
    
           
}