/************************************************************************************************************
Name: WorkOrderHandler
=============================================================================================================
Purpose: Class for WorkOrder Trigger as Handler.
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL       DESCRIPTION
1.0        Vasu         4/07/2018        Created       Home Repair Claim System  
*************************************************************************************************************/
public without sharing class WorkOrderHandler implements ITrigger {
    private List<WorkOrder> woList = new List<WorkOrder>();
    private Map<Id,String> workOrderDetMap=new Map<Id,String>();
    public WorkOrderHandler() {}
    
    public void bulkBefore() {
        if(Trigger.IsInsert){
            List<WorkOrder> woList = (List<WorkOrder>)Trigger.new;
            
            Set<Id> parentWOIds=new Set<Id>();
            Map<String,String> workTypeMap=new Map<String,String>();
            for(WorkOrder wo : woList) {
                system.debug('WO Authority' + wo.Work_Order_Authority__c); 
                if(wo.Rectification__c && wo.ParentWorkOrderId != null){
                    parentWOIds.add(wo.ParentWorkOrderId);
                }
                
                         
               
                /*
                Commenting this code because a Process builder is handling this scenario now
                else{
                    if(wo.Work_Type_Name__c.startsWith(NJ_Constants.WORK_TYPE_STARTS_WITH_HOMEASSIST)) {
                        wo.Up_To_Amount__c = 250;
                    } else if(wo.Work_Type_Name__c.startsWith(NJ_Constants.WORK_TYPE_STARTS_WITH_MAKESAFE)) {
                        wo.Up_To_Amount__c = 3000;
                    } 
                }*/
            }
            
            if(parentWOIds.size() > 0){
                for(WorkOrder wo : [Select Id,WorkTypeId from WorkOrder where Id IN :  parentWOIds]){
                    workTypeMap.put(wo.Id,wo.WorkTypeId);
                }
                system.debug('woList :' + woList);
                for(WorkOrder wo : woList) {
                    
                    if (wo.Rectification__c && wo.ParentWorkOrderId != null && workTypeMap.containsKey(wo.ParentWorkOrderId)){
                        system.debug('workTypeId: '+workTypeMap.get(wo.ParentWorkOrderId));
                        wo.WorkTypeId = workTypeMap.get(wo.ParentWorkOrderId);
                    }
                }
            }
        }
    }
    public void bulkAfter() {
        system.debug('Entering bulkAfter With Trigger.New'); 
        
        Map<Id, RecordType> tradeWorkOrderRTMap = new MAP<Id, RecordType>([SELECT Id FROM RecordType WHERE DeveloperName = 'Home_Repair_Trades' OR DeveloperName = 'Home_Repair_Trades_Locked']);
        if(Trigger.isUpdate) {
            if (checkRecursive.runOnceWO()){
                Map<Id,WorkOrder> mapOldWO = (Map<Id,WorkOrder>)Trigger.oldMap;
                Map<Id,WorkOrder> mapNewWO = (Map<Id,WorkOrder>)Trigger.newMap;
                List<WorkOrder> woList = (List<WorkOrder>)Trigger.new;
                Map<Id,WorkOrder> mapUpdateWO = new Map<Id,WorkOrder>();
                List<WorkOrder> workOrderQAList=new List<WorkOrder>();  
                Set<Id> dAndCTypeJob = new set<Id>(); 
                Set<String> woStatus = new Set<String> { NJ_Constants.WO_STATUS_CANCELLED, 
                    NJ_Constants.WO_STATUS_ACCEPTED,
                    NJ_Constants.WO_STATUS_REJECTED };
                //Assessor worktype collection to exclude completion call creation
                    Set<String> assessWorkTypes = new Set<String> { NJ_Constants.WORK_TYPE_ASSESSMENT, 
                    NJ_Constants.WORK_TYPE_REASSESSMENT,
                    NJ_Constants.WORK_TYPE_QUALITY_ASSURANCE,
                    NJ_Constants.WORK_TYPE_COORD_ASSESSMENT,
                    NJ_Constants.WORK_TYPE_INTERNALASSESSORS};
                        //List<Id> parentOrderId = new List<Id>();
                        for(WorkOrder wo : mapNewWO.values()) {
                            system.debug('Variation_Approval_Status__c' + wo.Variation_Approval_Status__c);
                           if((wo.Variation_Approval_Status__c == 'Approved' || wo.Variation_Approval_Status__c == 'Rejected') &&
                               wo.Active_Variation_Exists__c != mapOldWO.get(wo.Id).Active_Variation_Exists__c)
                            {  system.debug('h1');
                                VariationStatusUpdateInvoce.updateVariationStatus(mapNewWO.values());
                                system.debug('h2'); 
                            }
                            if(wo.Status != mapOldWO.get(wo.Id).Status && woStatus.contains(wo.Status)
                               && (wo.Status == NJ_Constants.WO_STATUS_CANCELLED || !NJ_Constants.ASSREASSSET.contains(wo.Work_Type_Name__c))) {
                                   mapUpdateWO.put(wo.Id, wo);
                               }
                            //add quality assurance text as task
                            if(wo.Quality_Assurance_Text__c != null && wo.Quality_Assurance_Text__c != mapOldWO.get(wo.Id).Quality_Assurance_Text__c) {
                                workOrderQAList.add(wo);
                            }                
                            /*if(wo.Invoice_Type__c == NJ_CONSTANTS.LINE_ITEM_CREDIT_MEMO && wo.Status == NJ_CONSTANTS.WO_STATUS_JOBCOMPLETE && !String.isEmpty(wo.ParentWorkOrderId))
parentOrderId.add(wo.ParentWorkOrderId);*/
                            
                            System.debug('-----------------> Inside ----->'+wo.Claim_Job_Type__c);
                            System.debug('----------------->WO Status-->'+wo.Status);
                            
                            //List D&C and ContentJob
                            //&& tradeWorkOrderRTMap.containsKey(wo.RecordTypeId)
                            if(//(wo.Claim_Job_Type__c == NJ_CONSTANTS.JOB_TYPE_DOANDCHARGE || wo.Claim_Job_Type__c == NJ_CONSTANTS.JOB_TYPE_CONTENT) &&
                                (wo.Status == NJ_CONSTANTS.WO_STATUS_CLOSED || wo.Status == NJ_CONSTANTS.WO_STATUS_JOBCOMPLETE || wo.Status == NJ_CONSTANTS.WO_STATUS_CANCELLED ) ) {
                                    System.debug('-----------------> Inside ----->'+wo.Claim_Job_Type__c);
                                    System.debug('-----------------> Inside ----->'+ wo.Work_Type_Name__c);
                                    //Skip completion call creation if the work order is of type Assessment
                                    //If(wo.Work_Type_Name__c != NJ_Constants.JOB_TYPE_ASSESMENT ){
                                     If(!assessWorkTypes.contains(wo.Work_Type_Name__c)){
                                         // Changed for Authority changes 
                                       // dAndCTypeJob.add(wo.Claim_Job__c);
                                        dAndCTypeJob.add(wo.Authority__c);
                                    }
                                }
                        }
                if(!mapUpdateWO.isEmpty()) {
                    WorkOrderHelper.updateServiceAppointments(mapUpdateWO);
                    WorkOrderHelper.createTaskWhenWORejected(mapUpdateWO);
                }
                if(!workOrderQAList.isEmpty()) {
                    WorkOrderHelper.addQualityAssuranceTextTask(workOrderQAList);
                }
                /*if(!parentOrderId.isEmpty()) {
                Map<Id, AggregateResult> parentOrderMap = new Map<Id, AggregateResult>();
                AggregateResult[] aggResults = ([SELECT WorkOrderId, SUM(Labour_Amount_New__c) labour, 
                SUM(Material_Amount_New__c) material
                FROM WorkOrderLineItem WHERE WorkOrderId IN: parentOrderId GROUP BY WorkOrderId]);
                for(AggregateResult result : aggResults) {
                parentOrderMap.put((Id)result.get('WorkOrderId'), result);
                }
                AggregateResult[] groupedResults = [SELECT SUM(Labour_Amount_New__c) labourTotal,                                                            
                SUM(Material_Amount_New__c) totalMaterial, WorkOrder.ParentWorkOrderId ParentWorkOrderId
                FROM WorkOrderLineItem 
                WHERE WorkOrder.ParentWorkOrderId IN : parentOrderId AND WorkOrder.Invoice_Type__c =: NJ_CONSTANTS.LINE_ITEM_CREDIT_MEMO 
                GROUP BY WorkOrder.ParentWorkOrderId ];
                set<Id> inValidCreditParent = new set<Id>();
                for(AggregateResult result : groupedResults) {
                Id parentId = (Id)result.get('ParentWorkOrderId');
                AggregateResult parentOrder = parentOrderMap.get(parentId);
                decimal labourTotal = (decimal)result.get('labourTotal');
                // decimal totalAmountGST = (decimal)result.get('totalAmountGST');
                decimal totalMaterial = (decimal)result.get('totalMaterial');
                system.debug('result==>'+result);
                system.debug('parentOrder==>'+parentOrder);
                if(labourTotal > (decimal) parentOrder.get('labour') || totalMaterial > (decimal)parentOrder.get('material')) {
                system.debug('parentOrder.Id==>'+parentId);
                inValidCreditParent.add(parentId);       
                }
                }
                if(!inValidCreditParent.isEmpty()) {
                for(WorkOrder wo : mapNewWO.values()) {
                if(inValidCreditParent.contains(wo.ParentWorkOrderId)) {
                wo.addError('Credit memo amount cannot be greater than work order total');
                }
                }
                }
                }*/
                system.debug('dAndCTypeJob==>'+dAndCTypeJob);
                if(!dAndCTypeJob.isEmpty()) {                    
                    TaskService.createCompletionCallTask(dAndCTypeJob);
                }
            }
        }
        system.debug('Exiting bulkAfter With Trigger.New');
    }
    public void beforeInsert(SObject so) {
    
        
         
    } 
    public void afterInsert(SObject so) {
    
        WorkOrder wo = (WorkOrder)so; 
        WorkOrder woauth=[select Id,Work_Order_Authority_Details__c,Work_Order_Authority__c from workorder where Id=:wo.Id];    
        //Update WorkOrder Authority Details based on WO Authority Type selection 
        If  (!String.isBlank(woauth.Work_Order_Authority__c)){
            workOrderDetMap.put(woauth.Id,woauth.Work_Order_Authority__c);
            woauth.Work_Order_Authority_Details__c=hrTradeWOAuthCaptureProcessor.processWOAuthCaptureCls(workOrderDetMap);
            update woauth;
             
        }  
        
    } 
    public void beforeUpdate(SObject oldSo, SObject so) {
        Boolean descriptionCheck = true;
        WorkOrder wo = (WorkOrder)so;
        WorkOrder oldwo=(WorkOrder)oldSo;
       //update Upto amount with Total ExGST Amount

		If (wo.Assessor_Submitted_WO__c) {         
            // Modified JP 05-10-2020 : HRD-789 - Investigation into 'Up to Amount' field possibly automatically updating
            If (wo.Up_To_Amount__c == null) wo.Up_To_Amount__c=wo.Total_Amount__c;
			If (wo.Up_To_Amount__c >=10000) wo.Quote_Uploaded__c=true;
	    }	
    
    //RCTI fix to update accepdated WO date
    If (wo.status =='Accepted WO'&& oldwo.status!=wo.status){
           wo.Accepted_WO_Date__c=Date.today();
        
        }                      
    
    if(wo.Status == NJ_Constants.WO_STATUS_REJECTED) {
            wo.Service_Resource_Company__c = null;
            wo.Service_Resource__c = null;
        }
       If  (!String.isBlank(wo.Work_Order_Authority__c) && wo.Work_Order_Authority__c!=oldwo.Work_Order_Authority__c){
            workOrderDetMap.put(wo.Id,wo.Work_Order_Authority__c);
            wo.Work_Order_Authority_Details__c=hrTradeWOAuthCaptureProcessor.processWOAuthCaptureCls(workOrderDetMap);
        }
        
        If (!String.isBlank(wo.Service_Resource__c) && (wo.Service_Resource__c!=oldwo.Service_Resource__c) ){   
            wo.Send_Account_Email__c=wo.ServiceResourceEmailAddress__c;
        }    
        else If (!String.isBlank(wo.Service_Resource_Company__c) && (wo.Service_Resource_Company__c!=oldwo.Service_Resource_Company__c)) {
            wo.Send_Account_Email__c=wo.ServiceResourceCompanyEmailAddress__c;
        }    
        else If (!String.isBlank(wo.Resource_Email__c) && (wo.Resource_Email__c!=oldwo.Resource_Email__c)){ 
            wo.Send_Account_Email__c=wo.Resource_Email__c;  
        }
        if(wo.Work_Order_Authority__c == 'Quote Approved' && wo.Service_Resource__c != null && wo.Service_Resource__c != oldwo.Service_Resource__c){
            System.debug('code is reaching here *** ');
            for(WorkOrderLineItem line : [SELECT id,description FROM WorkOrderLineItem WHERE WorkOrderId = :wo.Id]){
                if(line.Description != '' && line.Description != null){
                    descriptionCheck = false;
                }
            }
            System.debug('** descriptionCheck '+descriptionCheck);
            if(descriptionCheck){
              wo.addError('Please fill in description field of work order line items');    
            }
        }
       
                
    }
    public void beforeUnDelete(SObject so) {} 
    
    public void afterUpdate(SObject oldSo, SObject so) {
        
    } 
    public void beforeDelete(SObject so){} 
    public void afterDelete(SObject so) {} 
    public void afterUnDelete(SObject so) {} 
    public void andFinally() {}
   	public static void checkTest(){
     Integer i=0;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
         }
}