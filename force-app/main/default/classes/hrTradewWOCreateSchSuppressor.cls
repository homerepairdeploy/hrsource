/************************************************************************************************************
Name: hrTradewWOCreateSchSuppressor
=============================================================================================================
Purpose: Supress Trades from Assigning Appointments 
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL        DESCRIPTION
1.0        xx               xxx              Created        HomeRepair 
*************************************************************************************************************/

public class hrTradewWOCreateSchSuppressor {
    
    @InvocableMethod
    public static void collectWOForScheduleSuppress(List<Id> WKIds) {
    
        set<Id> acctIds=new set<Id>();
        set<Id> wkIdset=new set<Id>();
        wkIdset.addAll(WKIds);
        for(Account acc:[select Id,Suppressed_from_Scheduling__c,suppress_from_schedule__c from Account where type='Trade Company']){
             If (acc.Suppressed_from_Scheduling__c ||acc.suppress_from_schedule__c )
                 acctIds.add(acc.Id);
        }
        
        ProcessWOSupressTradesScheduling pwsts=new ProcessWOSupressTradesScheduling(acctIds,wkIdset);
        System.enqueueJob(pwsts);
        //processWOSuppressTradesFromScheduling(acctIds,wkIdset);
        
    }

    
    
    
        


    
    
}