/**
 * Class for NJ_ClaimAppointments component
 * @Author : Nikhil Jaitly
 */
public class NJ_ClaimAppointmentsController {
	static Map<Id,WorkOrder> woMap = new Map<Id,WorkOrder>();
  /*** 
   * Method Name:		getSADetails
   * Author:			Stephen Moss
   * Created Date:	07/08/2018
   * Description:		Get the details for the passed in Service Appointment Id
   ***/
  @AuraEnabled
	public static String getClaimServiceData(Id claimId) {
    //Fetch all the Claim related Work order
    woMap = new Map<Id,WorkOrder>();
    for(WorkOrder trade : [SELECT Id, Status, WorkTypeId, WorkType.Name, CaseId, WorkOrderNumber,Case.CaseNumber,NJ_WO_Cash_Settled__c
                           FROM WorkOrder
                           WHERE CaseId = :claimId
                           AND NJ_WO_Cash_Settled__c  = false AND Work_Type_Name__c != 'Internal/Assessors' AND Work_Type_Name__c != 'Insurance' 
                           AND Work_Type_Name__c != 'Quality Assurance' AND Work_Type_Name__c != 'Coordinator Assessment' AND (Up_to_Amount__c <= 3000.00 OR
                           Approved__c = True)
                           ORDER BY WorkOrderNumber
                           LIMIT 200]){
      woMap.put(trade.Id,trade);
    }
    if(!woMap.isEmpty()) {
      Set<Id> appointmentWorkOrderSet = new Set<Id>();
      // Query all related Service Appointments
	      List<ClaimWorkWrapper> returnWOList = new List<ClaimWorkWrapper>();
      for(ServiceAppointment sa : [SELECT Id, Status,ParentRecordId, AppointmentNumber, Sort_Order__c,SchedStartTime, SchedEndTime,DurationInMinutes,
                                         	EarliestStartTime, DueDate,Work_Type_Name__c, Service_Resource__r.Name, Claim__c,Team__c,Team_Size__c,
                                          Claim__r.Claim_Number__c,Claim__r.CaseNumber, Duration,
                                       	 (SELECT Product2.Name, Product_Description__c
                                          FROM Work_Order_Line_Items__r 
                                          WHERE Cash_Settled__c != True)
                                   FROM ServiceAppointment
                                   WHERE ParentRecordId IN :woMap.keySet() 
                                   ORDER BY Sort_Order__c ASC NULLS LAST
                                   LIMIT 500]){                              
          
         	returnWOList.add(new ClaimWorkWrapper(sa,woMap.get(sa.ParentRecordId)));	
          appointmentWorkOrderSet.add(sa.ParentRecordId);
      }
      
      for(String workOrderId : woMap.keySet()){
        if(!appointmentWorkOrderSet.contains(workOrderId))
        	returnWOList.add(new ClaimWorkWrapper(Null,woMap.get(workOrderId)));
      }                        
      return JSON.serialize(returnWOList);
    } 
    else {
      // no Work Orders for Claim
      return null;    
    }
	}
    
  /*** 
   * Class Name:		ClaimWorkWrapper
   * Author:			Devendra Dhaka
   * Created Date:	09/08/2018
   * Description:		Wrapper Class for Work Order JSON to be returned to
   * 					Lightning Component
   ***/
  public class ClaimWorkWrapper {
      // Wrapper Class Properties
    String workOrderId;
    String appointmentId;
    String workOrderNumber;
    String workTypeName;
    String appointmentNumber;
    String appointmentDuration;
    String appointmentStatus;
    String claimNumber;
    Boolean team;
    double teamSize;
    List<WorkScopeWrapper> workScopeList;
    public ClaimWorkWrapper(ServiceAppointment sa, WorkOrder trade){
      if(sa != null && sa.Claim__c != null) {
      	this.claimNumber = sa.Claim__r.CaseNumber;
      }
      this.appointmentDuration = '0 Hours';
      this.appointmentNumber = 'Appointment not created';
      if(sa!=Null){
      	this.appointmentNumber = sa.AppointmentNumber;
          this.appointmentDuration = String.valueOf(sa.Duration.setScale(2)) + ' Hours ';
          this.appointmentStatus = sa.Status;
          this.appointmentId = sa.Id;	
          this.team=sa.Team__c;
          this.teamSize=sa.Team_Size__c;
          if(sa.Work_Order_Line_Items__r!=Null){
              this.workScopeList = new List<WorkScopeWrapper>();
              for(WorkOrderLineItem work : sa.Work_Order_Line_Items__r){
                  this.workScopeList.add(new WorkScopeWrapper(work));
              }    
          }
      }
  	  this.workOrderId = trade.Id;
      this.workOrderNumber = trade.WorkOrderNumber;
      this.workTypeName = trade.workType.Name;
    }
  }
  /*** 
   * Class Name:		WorkScopeWrapper
   * Author:			  Devendra Dhaka
   * Created Date:	09/08/2018
   * Description:		Wrapper Class for WOLIs
   ***/
  public class WorkScopeWrapper {
    String productName;
    String prodDescription;
    Public WorkScopeWrapper(WorkOrderLineItem workItem){
        this.productName = workItem.Product2.Name;
        this.prodDescription = workItem.Product_Description__c;
    }          
  }
}