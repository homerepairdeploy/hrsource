/***************************************************************** 
Purpose:  Once off format contact phone fields. 
History                                                             
--------                                                            
VERSION        AUTHOR          DATE           DETAIL       Description 
1.0            Pardha       23/Dec/2019      Created      Home Repair Claim System 
*******************************************************************/
global class OnceoffBatchInitialFormatCTCPhone implements Database.Batchable<sObject> {
    
    global boolean charExists=false;
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        String query='select id,Phone,HomePhone,MobilePhone from contact';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<contact> scope) {
    
        list<contact> updtctclst=new list<contact>();
        for(contact ctc:scope){
              charExists=false;
              contact ct=new contact();
              ct.id=ctc.id;
              
              If (ctc.phone!=null && isCharExists(ctc.phone)) { charExists=true; ct.phone=deleteChar(ctc.phone);}
              If (ctc.HomePhone!=null && isCharExists(ctc.HomePhone)) {charExists=true; ct.HomePhone=deleteChar(ctc.HomePhone);}
              If (ctc.MobilePhone!=null && isCharExists(ctc.MobilePhone)) {charExists=true; ct.MobilePhone=deleteChar(ctc.MobilePhone);}
              If (charExists) updtctclst.add(ct);
            
        }
        update updtctclst;
        
        
    }
    
    public static boolean isCharExists(string phone){
  
     if (phone.isNumeric()) return false; else return true;
   }

    public static string deleteChar(string phone){
       system.debug('Before Phone' + phone);
       string phoneText = phone.replaceAll('[^\\+|\\d]','');
       system.debug('aAfter Phone' + phoneText); 
       return phoneText;
        


    }
    
    global void finish(Database.BatchableContext bc){ 
        
    }
    
}