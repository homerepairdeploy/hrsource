/***************************************************************** 
Purpose: Submit Assessment in asychronous mode                                                      
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            HomeRepair    23/06/2020      Created      Home Repair Claim System  
*******************************************************************/
public class AsyncSubmitAssessment implements Queueable{

    public Id claimId;
    public String servApptId;
    public Id BARecordId;
    public String datejson;
   
    public AsyncSubmitAssessment(Id ClaimId, String ServApptId, Id BARecordId, String datejson)
    {
        this.claimId = claimId;
        this.servApptId = servApptId;
        this.BARecordId = BARecordId;
        this.datejson = datejson;
    }
    public void execute(QueueableContext context) {
    
          ClaimConnect_SummaryController.submitAssessmentAsyn(ClaimId, ServApptId, BARecordId, datejson);
          
    }
    
    
        
}