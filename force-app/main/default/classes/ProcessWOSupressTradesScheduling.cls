/***************************************************************** 
Purpose: create WOResource Preference in asychronous mode                                                      
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            HomeRepair    23/06/2020      Created      Home Repair Claim System  
*******************************************************************/
public class ProcessWOSupressTradesScheduling implements Queueable{

    private set<Id> suppressTradeIds=new set<Id>();
    private set<Id> wkIdset=new set<Id>();
   
    public ProcessWOSupressTradesScheduling(set<Id> suppressTradeIds,set<Id> wkIdset)
    {
        this.suppressTradeIds = suppressTradeIds;
        this.wkIdset = wkIdset;
        
    }
    public void execute(QueueableContext context) {
    
        map<Id,ServiceResource> SRMap=new map<Id,ServiceResource>([select Id from ServiceResource where Contact__r.AccountId =:suppressTradeIds and IsActive=true]);
        list<ServiceResource> SRlst=new list<ServiceResource>([select Id from ServiceResource where Contact__r.AccountId =:suppressTradeIds and IsActive=true]);
        map<Id,workorder> womap=new map<Id,workorder>([select Id from WorkOrder where status not in ('closed','Job complete','cancelled') and Id=:wkIdset]);
        list<workorder> wolst=new list<workorder>([select Id from WorkOrder where status not in ('closed','Job complete','cancelled') and Id=:wkIdset]);
        set<Id> ExcludeRP=new set<Id>();
        map<Id,Id> ExcludeRPMap=new map<Id,Id>();
        list<ResourcePreference> RPlst=[select Id,RelatedRecordId,ServiceResourceId from ResourcePreference where RelatedRecordId=:womap.keySet() and PreferenceType='Excluded'];
        for (ResourcePreference RP:RPlst){
             ExcludeRPMap.put(RP.ServiceResourceId,RP.RelatedRecordId);
        }
        hrTradeScheduleSuppressProcessor.insertResourcePreference(wolst,SRlst,ExcludeRPMap);
          
    }
    
    
        
}