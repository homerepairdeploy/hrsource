/*********************************************************************************************************** 
Purpose: Mark the Photo complete is completed after all the photos are uploaded to AWS and delete drafts                                                       
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            HomeRepair    17/07/2020      Created      Home Repair Claim System  
***********************************************************************************************************/
public class MarkPhotoCompleteAndDeleteDraft implements Database.Batchable<sObject> {
    
     
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query='select id,Case__c,Staging_Assessor_BA__c,PhotosUploaded__c from AssessmentReports__c where PhotosUploaded__c=false';
        Datetime currTimeMinusFive = Datetime.now().addMinutes(-5);         
        if( !Test.isRunningTest() ) query += ' AND CreatedDate <= :currTimeMinusFive ';
    return Database.getQueryLocator(query);
    }
    
    
    public void execute(Database.BatchableContext BC, List<AssessmentReports__c > scope) {
       Set<Id> SRBAIdset=new Set<Id>();
       Set<Id> BAIdset=new Set<Id>();
       Set<Id> contentIdSet=new Set<Id>();
       list<AssessmentReports__c> updtAsrlst=new list<AssessmentReports__c>();
       for(AssessmentReports__c asr:scope)
       {   
           // JP Modified 03-09 : Changes to not include/remove items where the Staging BA no longer exists
      If(asr.Staging_Assessor_BA__c != null)
             {
               BAIdset.add(asr.Staging_Assessor_BA__c);
               SRBAIdset.add(asr.Staging_Assessor_BA__c);
             }
           
           for(Staging_Assessor_sow_rooms__c StagingRooms : [Select id,Name,UniqueRoomId__c,Staging_Assessor_BA__r.Case__c from Staging_Assessor_sow_rooms__c where Staging_Assessor_BA__c=:BAIdSet]){
                SRBAIdset.add(StagingRooms.id);
                
           }

           If (!SRBAIdset.isEmpty()){
            for(ContentDocumentLink link:[SELECT ContentDocumentId,LinkedEntityId FROM ContentDocumentLink where LinkedEntityId IN :SRBAIdset]){
                contentIdSet.add(link.ContentDocumentId);
            } 
            list<ContentVersion> lstcontentversion=[SELECT VersionData,Title,ContentDocumentId,ContentSize,Description,CreatedById,FileExtension FROM ContentVersion WHERE ContentDocumentId = :contentIdSet and islatest = true and AWSUploaded__c=false];
            If (lstcontentversion.size() ==0){
                AssessmentReports__c asrc=new AssessmentReports__c();
                asrc.id=asr.id;
                asrc.PhotosUploaded__c=true;
                //markascompleted : Shilpy 
                AssessmentReports__c assessreprt = [select Id,MarkAsCompleted__c,ReportGenerated__c from AssessmentReports__c where id =:asrc.id Limit 1] ;
                if(assessreprt.ReportGenerated__c == true){
                    asrc.MarkAsCompleted__c = true;
                }
                updtAsrlst.add(asrc);
            }
          } 
         
       }
       //delete draft assessments
       If (updtAsrlst.size() > 0) {
           update updtAsrlst;
           list<Staging_Assessor_ba__c> lstStagingABA = [Select Id from Staging_Assessor_ba__c where Id = :BAIdset ];
           If (lstStagingABA.size() > 0) delete lstStagingABA; 
       }

        
    }
    
    public void finish(Database.BatchableContext bc){ 
        
    }       


   public static void checkTest(){
     Integer i=0;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
         i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    }    
}