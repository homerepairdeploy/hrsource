@isTest
public class OnceOffUpdateAuthItemTest {
    @testSetup 
    static void setup(){
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
        Claim_Job__c cj=HomeRepairTestDataFactory.createClaimJob(cs.Id);   
        HomeRepairTestDataFactory.createCJWorkItem(cj);
    }  
    public static testmethod void testBatchOnceoffUpdateAuthitem() { 
            case c=[select id,cjid__c from case limit 1];
            claim_job__c cj=[select id from claim_job__c limit 1];
            c.CJId__c=cj.id;
            update c;
        
            Test.startTest(); 
               
                database.executeBatch(new OnceOffUpdateAuthItem(),1);
               
            Test.stopTest();
        
    }
    
}