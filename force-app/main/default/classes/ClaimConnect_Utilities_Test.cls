/********************************************************************************************** 
Name        : ClaimConnect_Utilities_Test
Description : Test Class for ClaimConnect_Utilities
                                                            
VERSION        AUTHOR           DATE           DETAIL      DESCRIPTION   
1.0            Prasanth         25-05-2020     Created     ClaimConnect APP
/***********************************************************************************************/
@isTest
public class ClaimConnect_Utilities_Test {
    
    @isTest static void getPicklistValues(){ 
        Test.startTest();
        List<String> options = ClaimConnect_Utilities.getPicklistValues('case','Building_height__c');
        system.debug('<<<'+options.size());
        system.assert(options.size()>0);
        Test.stopTest();
    } 
    
    @isTest static void getPickListmdt(){ 
        Test.startTest();
        List<String> options= ClaimConnect_Utilities.getPickListmdt('Claim Proceeding');
        system.debug('<<<'+options.size());
        system.assert(options.size()>0);
        Test.stopTest();
    }    
    
}