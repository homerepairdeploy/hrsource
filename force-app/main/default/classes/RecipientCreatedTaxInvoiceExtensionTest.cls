@IsTest
private class RecipientCreatedTaxInvoiceExtensionTest {
    @IsTest
    static void loadAPInvoice(){

        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Test');   
        //create a Claim Authority(Added by CRMIT)
        case ca=HomeRepairTestDataFactory.createClaimAuthority(cs.Id);
        WorkOrder wo=HomeRepairTestDataFactory.createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,ca.id);
        AP_Invoice__c invoice = HomeRepairTestDataFactory.createAPInvoiceWithWorkOrder(wo.id);

        PageReference pageRef = Page.RecipientCreatedTaxInvoiceStatement;
        Test.setCurrentPage(pageRef);
        
        Test.startTest();

        ApexPages.currentPage().getParameters().put('id',invoice.id);
		ApexPages.StandardController stdAP = new ApexPages.StandardController(invoice);
	    RecipientCreatedTaxInvoiceExtension custAP  = new RecipientCreatedTaxInvoiceExtension(stdAP);

        Test.stopTest();
        
        List<ContentVersion> cv = [SELECT Id FROM ContentVersion];

        //system.assertEquals(null, custAP.woliList, 'NO work order line item is attached');
        
    }
}