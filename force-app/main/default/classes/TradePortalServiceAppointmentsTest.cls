/* =====================================================================================
Type:       Test class
Purpose:    Test cases for TradePortalServiceAppointments
========================================================================================*/
@isTest
private class TradePortalServiceAppointmentsTest{
    @TestSetup
    public static void setup() {
        Account acc = new Account(Name = 'Oil Company', Job_Email_Address__c = 'abc@gmail.com');
        insert acc;
        
        Contact con = new Contact(LastName = 'TestName', AccountId = acc.id);
        insert con;
        
        FSL_TestDataFactory.createFSLUser(con.Id);
    }
    @isTest 
    static void testTradePortalServiceAppointments(){
        
        
        User u = [SELECT Id, FirstName, LastName, contactId
                  FROM User 
                  WHERE LastName = 'Cunningham'];
       
        
        test.startTest();
       
        u = [SELECT Id, FirstName, LastName, contactId, contact.account.Job_Email_Address__c, Email, AccountId
                  FROM User 
                  WHERE LastName = 'Cunningham'];
        
        ServiceResource testResource = FSL_TestDataFactory.createNewServiceResource(u.Id);
        
        //Operating Hours
        OperatingHours testOPHours = FSL_TestDataFactory.createOperatingHours('FSL_Base');
        //system.debug('FSL_Base: ' + testOPHours);
        
        //setup service territory
        ServiceTerritory testTerritory = FSL_TestDataFactory.createServiceTerritory('FSL_Base_Territory', testOPHours.Id, '303 Collins St', 'Melbourne',
                                                                                    'VIC', '3000','Australia');
        
        //setup service territory memeber
        ServiceTerritoryMember newSTM = FSL_TestDataFactory.createServiceTerritoryMember(testResource.Id, testTerritory.Id, testOPHours.Id);
        //System.debug('Service Territory Memeber: ' + newSTM);
        
        //create Work Type        
        WorkType WT = FSL_TestDataFactory.createWorkType('Repair Items', 'ELECTRICIAN');
        
        
        case ConfirmationTestCase = FSL_TestDataFactory.createClaim();
        
        
        workOrder wO = FSL_TestDataFactory.createWorkOrderWithCase('Home Repair Trades', ConfirmationTestCase.Id, WT.Id, testTerritory.Id,'300 Bourke Street', 'Melbourne', 'VIC', '3000', 'Australia');
        
        ServiceAppointment testServApp = FSL_TestDataFactory.createNewServiceAppointment(wO.Id, 'New');
        ServiceAppointment testServApp2 = FSL_TestDataFactory.createNewServiceAppointment(wO.Id, 'New');
        
        DateTime testTime = DateTime.now()+1;
        
        Long testLongTime = testTime.getTime();
        
        //call API
        system.debug('********status transistions: ' + FSL.GlobalAPIS.GetAllStatusTransitions());
        
        FSL.GlobalAPIS.addStatusTransition('New', 'Tentative');
        FSL.GlobalAPIS.addStatusTransition('New', 'Confirmed');
        FSL.GlobalAPIS.addStatusTransition('Tentative', 'Confirmed');
        FSL.GlobalAPIS.addStatusTransition('New', 'Awaiting Confirmation');
        //schedule appointments
        NJ_ServiceAppointmentManagerController.srvScheduleResponseWrapper testScheduleAppt = NJ_ServiceAppointmentManagerController.scheduleAppointmentForCandidate(testServApp.Id, testResource.Id, testLongTime,'T',null,0);
        NJ_ServiceAppointmentManagerController.srvScheduleResponseWrapper testScheduleAppt2 = NJ_ServiceAppointmentManagerController.scheduleAppointmentForCandidate(testServApp2.Id, testResource.Id, testLongTime,'T',null,0);
        
        system.debug('testScheduleAppt: ' + testScheduleAppt);
        system.debug('testScheduleAppt2: ' + testScheduleAppt2);
        
        TradePortalServiceAppointments.getServiceAppointments(wo.Id);
        TradePortalServiceAppointments.getAllServiceAppointments(wo.Id); 
        Map<Id, User> userMap = new Map<Id, User>();
        userMap.put(wo.Id, u);
        ServiceAppointmentService.updateWorkOrderTradeUserDetails(userMap);
         test.stopTest();
    }   
}