/***************************************************************** 
Purpose: To manage Repair Items for Room                                                       
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            Vasu          22/03/2018      Created      Home Repair Claim System 
1.1            Vasu          12/03/2019      Modified     HRNEW-336 - Reassessment changes
*******************************************************************/
@RestResource(urlMapping='/homerepair/api/v1/roomrepairitems/*')
global with sharing class RESTRoomRepairItemsAPI{   
    /***************************************************************** 
Purpose: Get method to retrieve repair items for Room
Parameters:None
Returns: List of Room repair items 
Throws [Exceptions]: Rest exception                                                          
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL   Description 
1.0            Vasu          22/03/2018     Created   Home Repair Claim system 
*******************************************************************/
    
    @HttpGet
    global static RESTRepairItemsResponseHandler GET() {        
        // Initialise request context to get request parameters
        // and response handler to return results
        RestRequest request = RestContext.request;
        RESTRepairItemsResponseHandler response = new RESTRepairItemsResponseHandler();

        NJ_APILogging.LogDetail logDetails = new NJ_APILogging.LogDetail('roomrepairitems - GET', request);
        logDetails.claim = request.params.get('claimId');
        logDetails.apiVerb = request.params.get('apiVerb');
        logDetails.mobileAppVersion = request.params.get('appVersion');
        logDetails.assessorId = request.params.get('assessorId');        
        logDetails.assessmentAppointment = request.params.get('apptId');
        
        System.debug('NJ_LogDetails:'+logDetails);
        
        // validate the request parameters for get method
        try {
            validateWSGetInput(request,'GET');
        } catch (RESTCustomException re) {
            logDetails.responseError = re.getMessage();
            logDetails.responseCode = 'INVALID_INPUT';
            NJ_APILogging.log(logDetails);
            return responseWithError('Error','INVALID_INPUT',re.getMessage());
        }
        
        //get claim Id 
        String claimId = request.params.get('claimId');
        System.Debug(LoggingLevel.DEBUG, '*** param claimId = ' + claimId);
        //get appointment Id
        String appointmentId = request.params.get('apptId');        
        System.Debug('*** param assessment  appointmentId = ' +  appointmentId);
        System.Debug('***Request Context = ' + request);
        
        List<Room__c> roomList=new List<Room__c>();
        Map<string,Room__c> roomMap=new Map<string,Room__c>();
        
        for(Room__c rm : [select id,Claim__c from Room__c where Claim__c=:claimId]){
            roomMap.put(rm.Claim__c,rm);
        }
        
        List<WorkOrder> workOrderClaimList=new List<WorkOrder>();
        
        try {
            workOrderClaimList = [select id,CaseId,WorkTypeId,WorkType.Name,Status,Sort_Order__c,Authority__r.Id,TotalPrice,Total_Labour_Amount_New__c,Total_Material_Amount_New__c,IsPreview__c,Assigned_Resource__c,WorkOrderNumber,
                                  Authority__r.Job_Type__c,
                                  (select id,WorkTypeId,Room__r.CannotDeleteRoom__c,WorkType.Name,Sort_Order__c,Site_Visit_Number__c,Room__c,Room__r.Name,PricebookEntryId,PricebookEntry.Name,Additional_Labour_Amount_AUD__c,Additional_Materials_Amount_AUD__c,
                                   Cash_Settled__c,IsPreview__c,Contents_Type__c,UOM__c,Quantity,Description,Labour_Rate__c,Material_Rate__c,Labour_Time__c,Product2.Team__c,Product2.Team_Size__c, 
                                   Assessment_Service_Appointment__c,Price_Book_Entry__c,Display_Order__c, WorkOrderId, LineItemNumber from WorkOrderLineItems order by LineItemNumber, WorkType.Name, Room__r.Name) from WorkOrder 
                                   where CaseId=:claimId AND (NOT WorkOrder.WorkType.Name LIKE 'MS - %') AND (NOT WorkOrder.WorkType.Name LIKE 'HA - %')
                                   AND (NOT WorkOrder.WorkType.Name LIKE 'CO - %') order by WorkOrderNumber ASC];
        } catch (System.QueryException se) {
            logDetails.responseError = se.getMessage();
            logDetails.responseCode = 'QUERY_ERROR';
            NJ_APILogging.log(logDetails);
            return responseWithError('Error','QUERY_ERROR',se.getMessage());
        }
        
        Map<string,RESTWorkOrderWrapper> workOrderWrapperMap=new Map<string,RESTWorkOrderWrapper>();
        Map<string,WorkOrder> workOrderMap=new Map<string,WorkOrder>();        
        Map<string,List<RESTWorkOrderLineItemWrapper>> workOrderlineWrapperMap=new Map<string,List<RESTWorkOrderLineItemWrapper>>();        
        Set<string> roomIds=new Set<string>();
        
        system.debug('workOrderClaimList :'+workOrderClaimList);

        for(WorkOrder wo : workOrderClaimList){            
            if(wo.WorkOrderLineItems.size() > 0 && wo.Authority__r.Job_Type__c == 'doAndCharge' ){
                List<RESTWorkOrderLineItemWrapper> workOrderLineItemList=new List<RESTWorkOrderLineItemWrapper>();  
                for(WorkOrderLineItem woil : wo.WorkOrderLineItems){
                    String key = woil.Room__r.Id + '' + wo.Id;
                    system.debug('\n\nWo:'+ Key);
                    if(workOrderlineWrapperMap.containsKey(key)) {
                        if(woil.Room__r.Id != null){
                            roomIds.add(woil.Room__r.Id);
                            system.debug('workOrderlineWrapperMapIn :'+workOrderlineWrapperMap.get(key));
                            List<RESTWorkOrderLineItemWrapper> workOrderLineItemListTemp= workOrderlineWrapperMap.get(key);
                            workOrderLineItemListTemp.add(new RESTWorkOrderLineItemWrapper(woil));
                            workOrderlineWrapperMap.put(key, workOrderLineItemListTemp);
                        }                        
                    }else{
                        workOrderlineWrapperMap.put(key, new List<RESTWorkOrderLineItemWrapper>{new RESTWorkOrderLineItemWrapper(woil)});
                    }                    
                }
                workOrderMap.put(wo.id,wo);
            }
        }

        /*
        for(WorkOrder wo : workOrderClaimList){            
            if(wo.WorkOrderLineItems.size() > 0){
                List<RESTWorkOrderLineItemWrapper> workOrderLineItemList=new List<RESTWorkOrderLineItemWrapper>();  
                for(WorkOrderLineItem woil : wo.WorkOrderLineItems){
                    String key = woil.Room__r.Id + woil.WorkType.Name;
                    system.debug('Wo:'+woil.Room__r.Id+'--'+woil.WorkType.Name+'----'+woil.Room__r.Id);
                    if(workOrderlineWrapperMap.containsKey(key)) {
                        if(woil.Room__r.Id != null){
                            roomIds.add(woil.Room__r.Id);
                            system.debug('workOrderlineWrapperMapIn :'+workOrderlineWrapperMap.get(key));
                            List<RESTWorkOrderLineItemWrapper> workOrderLineItemListTemp= workOrderlineWrapperMap.get(key);
                            workOrderLineItemListTemp.add(new RESTWorkOrderLineItemWrapper(woil));
                            workOrderlineWrapperMap.put(key, workOrderLineItemListTemp);
                        }                        
                    }else{
                        workOrderlineWrapperMap.put(key, new List<RESTWorkOrderLineItemWrapper>{new RESTWorkOrderLineItemWrapper(woil)});
                    }
                    workOrderMap.put(wo.id,wo);
                }
            }
        }
        */
        
        system.debug('workOrderWrapperMap :'+workOrderWrapperMap);
        system.debug('workOrderlineWrapperMap :'+ workOrderlineWrapperMap);
        
        List<RESTClaimWrapper> claimWrapperList=new List<RESTClaimWrapper>();
        RESTClaimWrapper cw;   
        List<RESTRoomRepairItemListWrapper> roomRepairItemListWrapper =new List<RESTRoomRepairItemListWrapper>();     
        Case csRoom = new Case();
        
        for(Case cs : [select id,ContactId,CaseNumber,Description,Total_Price__c,Estimated_Cost_of_Cash_Settlement__c,Assessment_Report_Created__c from Case where id=:claimId]){
            csRoom=cs;
        }
        
        Set<string> roomWithWo=new Set<string>();
        for(Room__c rm :[select id,Name,Claim__c,Floor__c,height__c,length__c,width__c,wall__c,perimeter__c,Display_Order__c, CannotDeleteRoom__c,(select id,WorkType.Name,WorkOrder.Id from Work_Order_Line_Items__r) from Room__c where Claim__c=:claimId]){
            List<RESTWorkOrderWrapper> workOrderList=new List<RESTWorkOrderWrapper>(); 
            if(rm.Work_Order_Line_Items__r.size() > 0){                                   
                for(WorkOrderLineItem woil : rm.Work_Order_Line_Items__r){
                    if(workOrderMap.containsKey(woil.WorkOrder.Id)){
                        WorkOrder wo=workOrderMap.get(woil.WorkOrder.Id);
                        
                        String key = rm.Id + '' + woil.WorkOrder.Id;
                        if(workOrderlineWrapperMap.containsKey(key)) {
                            if(!roomWithWo.Contains(key)){
                                List<RESTWorkOrderLineItemWrapper> workOrderLineItemList=new List<RESTWorkOrderLineItemWrapper>();
                                workOrderLineItemList.addAll(workOrderlineWrapperMap.get(key));

                                RESTWorkOrderWrapper workOrderWrapper=new RESTWorkOrderWrapper(wo,workOrderLineItemList);                                
                                workOrderList.add(workOrderWrapper);
                                roomWithWo.add(key);
                            }
                        }
                    }                                      
                } 
                roomRepairItemListWrapper.add(new RESTRoomRepairItemListWrapper(rm,workOrderList));
                
            }else{                
                roomRepairItemListWrapper.add(new RESTRoomRepairItemListWrapper(rm,workOrderList));
            }
        }
        cw=new RESTClaimWrapper(csRoom ,roomRepairItemListWrapper); 
        system.debug('roomRepairItemListWrapper Json ' + JSON.serialize(claimWrapperList));
        RestContext.response.addHeader('Content-Type', 'application/json');
        response.Status = 'OK';
        response.Message = 'Success';   
        response.Data = cw;

        logDetails.outgoingJSON = JSON.serialize(response);        
        NJ_APILogging.log(logDetails);


        return response;
    }
    
    @HttpPut
    global static RESTRepairItemsResponseHandler PUT() {
        
        // Initialise request context to get request parameters
        // and response handler to return results
        RestRequest request = RestContext.request; //the RestRequest for the Apex REST method
        RESTRepairItemsResponseHandler response = new RESTRepairItemsResponseHandler();
        
        NJ_APILogging.LogDetail logDetails = new NJ_APILogging.LogDetail('roomrepairitems - PUT', request);
        
        logDetails.apiVerb = request.params.get('apiVerb');
        logDetails.mobileAppVersion = request.params.get('appVersion');
        logDetails.assessorId = request.params.get('assessorId');
        logDetails.assessmentAppointment = request.params.get('apptId');

        System.debug('NJ_LogDetails:'+logDetails);
        // validate the request parameters for get method
        
        try {
            validateWSGetInput(request,'PUT');
        } catch (RESTCustomException re) {
            logDetails.responseError = re.getMessage();
            logDetails.responseCode = 'INVALID_INPUT';
            NJ_APILogging.log(logDetails);

            return responseWithError('Error','INVALID_INPUT',re.getMessage());
        }
        
        System.Debug('Room Repair Item Body: ' + request.requestBody.toString());
        
        RESTClaimWrapper claimWrapperItem= (RESTClaimWrapper) JSON.deserialize(request.requestBody.toString(),RESTClaimWrapper.class); 
        
        List<Room__c> roomList=new List<Room__c>();
        Set<String> roomIds=new Set<String>();
        Set<String> woliIds=new Set<String>();
        List<WorkOrder> workOrderList=new List<WorkOrder>();
        List<WorkOrderLineItem> workOrderLineItemList=new List<WorkOrderLineItem>();
        Map<string,Room__c> roomMap=new Map<string,Room__c>();
        Map<string,WorkOrder> workOrderMap=new Map<string,WorkOrder>();
        Map<string,string> claimAuthorityMap=new Map<string,string>();
        Map<string,WorkOrder> workTypeMap=new Map<string,WorkOrder>();
        List<ContentDocumentLink> contentLinkToBeCreated = new List<ContentDocumentLink>();
        system.debug('claimWrapperItem '+claimWrapperItem);
        //get claim Id 
        String claimId = request.params.get('claimId');
        logDetails.claim = claimId;
        system.debug('claim Id Param: ' + claimId);
        
        // make sure we have a valid service appointment id
        Id appointmentId = request.params.get('apptId');
        logDetails.assessmentAppointment = appointmentId;
        system.debug('appointment Id Param: ' + appointmentId);
                
        if (appointmentId == null || appointmentId.getSObjectType().getDescribe().getName() != 'ServiceAppointment') {
            logDetails.responseError = 'You must supply a Service Appointment Id';
            logDetails.responseCode = 'INVALID_INPUT';
            NJ_APILogging.log(logDetails);
            throw new RESTCustomException('You must supply a Service Appointment Id');
        }
        ServiceAppointment serveApp;
        //Find the parent Id and parent type of SA.
        try {
            serveApp = [SELECT ID,AppointmentNumber,ParentRecordType, ParentRecordId FROM ServiceAppointment
                        WHERE ID = : appointmentId LIMIT 1];
        }Catch (StringException se) {
            system.debug('appointmentId: '+appointmentId);
            logDetails.responseError = 'No Service Appointment record found';            
            NJ_APILogging.log(logDetails);

            throw new RESTCustomException('No Service Appointment record found');
        }  
        
        Savepoint sp = Database.setSavepoint();
        //Claim Job map which will be used later to assoicate work order line item with work order
        WorkOrder parentWO = [Select Id,CaseId,Authority__c From WorkOrder where Id = : serveApp.ParentRecordId Limit 1];
        claimAuthorityMap.put(parentWO.CaseId,parentWO.Authority__c);      
        Case claim = new Case();
        claim.id = claimWrapperItem.claimId;
        claim.Assessment_Report_Created__c = claimWrapperItem.assessmentReportCreated;
        if (claimWrapperItem.assessmentReportCreated){
           claim.Assessment_Downloaded__c = false;
           Case claimdet=[select id,Insurable__c,Job_Readiness__c,Claim_Proceeding__c from case where id=:claimWrapperItem.claimId limit 1];
           If (claimdet.Insurable__c =='Yes' && claimdet.Job_Readiness__c == 'Yes') claim.Claim_Proceeding__c ='Yes'; 
           
        }
        claim.Estimated_Cost_of_Cash_Settlement__c = claimWrapperItem.estimatedCostofCashSettlement;
        try{
            Upsert Claim;
        }Catch(DMLException de){
            Database.rollback(sp);
            logDetails.responseError = de.getMessage();
            logDetails.responseCode = 'DML_ERROR';
            NJ_APILogging.log(logDetails);

            return responseWithError('Error','DML_ERROR',de.getMessage());
        }
        
        //Delete rooms
        for(RESTRoomRepairItemListWrapper rwil : claimWrapperItem.rooms){
            if(rwil.isDeleted){
                roomIds.add(rwil.roomId);
            }
        }
        if(roomIds.size()>0){
            if(HomeRepairUtil.deleteRepairItems(roomIds,'Room__c') != 'success'){
                return responseWithError('Error','DML_ERROR',HomeRepairUtil.deleteRepairItems(roomIds,'Room__c'));
            } 
        }
        
        //Read work order after room delete
        for(WorkOrder wo : [select Id,WorkType.Name, Status from WorkOrder where CaseId =:claimId order by WorkOrderNumber Asc]){
            if(!workTypeMap.containsKey(wo.workType.Name)){
                workTypeMap.put(wo.WorkType.Name,wo);
            }
        }
        
        for(RESTRoomRepairItemListWrapper rwil : claimWrapperItem.rooms){
            if(!rwil.isDeleted){
                Room__c rm=new Room__c();            
                rm.Id=rwil.roomId; 
                rm.Name=rwil.name;                     
                rm.Height__c=rwil.height;
                rm.Length__c=rwil.length;
                rm.Width__c=rwil.width;
                rm.Display_Order__c = rwil.displayOrder;
                if(rwil.roomId == null){
                    rm.Claim__c=rwil.claimId;
                }
                for(RESTWorkOrderWrapper wow : rwil.workOrders){
                    if(!workOrderMap.containsKey(wow.workTypeName)){
                        WorkOrder wo =new WorkOrder();
                        if(workTypeMap.containsKey(wow.workTypeName)&& workTypeMap.get(wow.workTypeName).Status!=NJ_Constants.WO_STATUS_JOBCOMPLETE){
                            wo.Id = workTypeMap.get(wow.workTypeName).Id;
                        }
                        wo.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Home Repair Trades').getRecordTypeId();                           
                        wo.WorkTypeId=wow.workTypeId;
                        wo.Sort_Order__c = wow.sortOrder;
                        wo.isPreview__c = wow.isPreview;
                        wo.CaseId=rwil.claimId;
                        wo.ContactId = claimWrapperItem.contactId;
                        if(claimAuthorityMap.containsKey(rwil.claimId)){
                            wo.Authority__c=claimAuthorityMap.get(rwil.claimId);
                        }
                        workOrderList.add(wo);
                        workOrderMap.put(wow.workTypeName,wo);
                    }                    
                }            
                roomMap.put(rm.Name,rm);
                roomList.add(rm);
            }
        }
        if(roomList.size() > 0){            
            try{
                upsert roomList;
                system.debug('roomList '+roomList);
            }Catch(DMLException de){
                Database.rollback(sp);
                system.debug('Room Error '+de.getMessage());
                logDetails.responseError = de.getMessage();
                logDetails.responseCode = 'DML_ERROR';
                NJ_APILogging.log(logDetails);
                return responseWithError('Error','DML_ERROR',de.getMessage());
            }
        }
        
        if(workOrderList.size() > 0){                                
            try{
                upsert workOrderList;
            }Catch(DMLException de){
                Database.rollback(sp);
                logDetails.responseError = de.getMessage();
                logDetails.responseCode = 'DML_ERROR';
                NJ_APILogging.log(logDetails);
                return responseWithError('Error','DML_ERROR',de.getMessage());
            }
        }
        system.debug('claimWrapperItem.rooms'+claimWrapperItem.rooms);
        set<string> productNameSet=new set<string>();
        for(RESTRoomRepairItemListWrapper rwil : claimWrapperItem.rooms){
            for(RESTWorkOrderWrapper wow : rwil.workOrders){
                for(RESTWorkOrderLineItemWrapper woliw : wow.workOrderLineItems ){ 
                    if(woliw.workOrderLineItemId == null ){                            
                        productNameSet.add(woliw.productName);
                    }
                }
            }
        }
        
        Map<string,String> pricebookEntryMap = new Map<string,string>();
        
        for(PricebookEntry pbe: [Select Product2Id,Product2.Name,UnitPrice From PricebookEntry Where Product2.Name IN : productNameSet and PriceBook2.isStandard=true]){
            pricebookEntryMap.put(pbe.Product2.Name,pbe.Id);
        }
        
        for(RESTRoomRepairItemListWrapper rwil : claimWrapperItem.rooms){
            for(RESTWorkOrderWrapper wow : rwil.workOrders){
                for(RESTWorkOrderLineItemWrapper woliw : wow.workOrderLineItems ){                    
                    if(woliw.isDeleted){
                        woliIds.add(woliw.workOrderLineItemId);
                    }else{
                        WorkOrderLineItem woli=new WorkOrderLineItem();
                        woli.Contents_Type__c = woliw.contentsType;
                        woli.Quantity=woliw.quantity;  
                        woli.Description=woliw.description;
                        woli.Work_Item_Description__c = woliw.workTypeName+woliw.productName;
                        woli.Cash_Settled__c = woliw.cashSettled;
                        woli.Site_Visit_Number__c=woliw.siteVisitNumber;
                        woli.Labour_Rate__c=woliw.labourRate;
                        woli.Additional_Labour_Amount_AUD__c = woliw.additionalLabourAmount;
                        woli.Material_Rate__c=woliw.materialRate;
                        woli.Additional_Materials_Amount_AUD__c=woliw.additionalMaterialsAmount;
                        woli.Labour_Time__c=woliw.labourTime;
                        woli.Sort_Order__c = woliw.sortOrder;
                        woli.isPreview__c = woliw.isPreview;
                        woli.Price_Book_Entry__c = woliw.productId;
                        woli.Assessment_Service_Appointment__c = woliw.assessmentAppointmentId;
                        woli.Display_Order__c = woliw.displayOrder;
                        
                        if (woliw.cashSettled == true){
                            woli.RecordTypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('Cash Settlement').getRecordTypeId();               
                        }else{
                            woli.RecordTypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('Repair Items').getRecordTypeId();
                        }
                        
                        if(woliw.workOrderLineItemId == null ){
                            woli.WorkOrderId = workOrderMap.get(woliw.workTypeName).Id;
                            if(pricebookEntryMap.containsKey(woliw.productName)){
                                woli.PricebookEntryId=pricebookEntryMap.get(woliw.productName);
                            }
                        }else{
                            woli.Id=woliw.workOrderLineItemId;
                        }
                        
                        if(woliw.workTypeId == null){
                            if(workOrderMap.containsKey(woliw.workTypeName)){
                                woli.WorkTypeId=workOrderMap.get(woliw.workTypeName).WorkTypeId;                                
                            }                                           
                        }else{
                            woli.workTypeId=woliw.workTypeId;
                        } 
                        
                        if(woliw.roomId == null){
                            if(roomMap.containsKey(woliw.roomName)){
                                woli.Room__c=roomMap.get(woliw.roomName).Id;
                            }
                        }else{
                            woli.Room__c=woliw.roomId;
                        }
                        workOrderLineItemList.add(woli); 
                    }                  
                }
            }
            if(woliIds.size()>0){
                if(HomeRepairUtil.deleteRepairItems(woliIds,'WorkOrderLineItem') != 'success'){
                    Database.rollback(sp);
                    logDetails.responseError = 'Error at HomeRepairUtil.deleteRepairItems';                    
                    NJ_APILogging.log(logDetails);
                    return responseWithError('Error','DML_ERROR',HomeRepairUtil.deleteRepairItems(woliIds,'WorkOrderLineItem'));
                }                
            }
            
        }
        
        system.debug('workOrderLineItemList :' + workOrderLineItemList);
        if(workOrderLineItemList.size() > 0){
            try{
                upsert workOrderLineItemList;
                system.debug('workOrderLineItemList :' + workOrderLineItemList);
            }Catch(DMLException de){
                Database.rollback(sp);
                
                logDetails.responseError = de.getMessage();
                logDetails.responseCode = 'DML_ERROR';
                NJ_APILogging.log(logDetails);

                return responseWithError('Error','DML_ERROR',de.getMessage());
            }
        }
        //system.debug('contentLinkToBeCreated :' + contentLinkToBeCreated);
        //insert content links
        if(!contentLinkToBeCreated.isEmpty()){
            try{
                insert contentLinkToBeCreated;
                system.debug('contentLinkToBeCreated :' + contentLinkToBeCreated);
            }Catch(DMLException de){
                 Database.rollback(sp);
                logDetails.responseError = de.getMessage();
                logDetails.responseCode = 'DML_ERROR';
                NJ_APILogging.log(logDetails);

                return responseWithError('Error','DML_ERROR',de.getMessage());
            }
        }
        if(response.Status != 'Error'){
            response.Status = 'OK';
            response.Message = 'Success';
        }

        logDetails.outgoingJSON = JSON.serialize(response);
        NJ_APILogging.log(logDetails);
        return response;
    }
    private static RESTRepairItemsResponseHandler responseWithError(String eStatus,String eErrorCode,String eMessage) {
        RESTRepairItemsResponseHandler response = new RESTRepairItemsResponseHandler();
        response.Status = eStatus;
        response.ErrorCode = eErrorCode;
        response.Message = eMessage; 
        return response;        
    }
    /*******************************************************
* validate the input parameters for the GET Request
********************************************************/ 
    private static void validateWSGetInput(RestRequest request, String method) {
        //validate GET method parameters
        if (method == 'GET'){
            // make sure we have a valid calims id
            String claimId = request.params.get('claimId');
            if (claimId == null) {
                throw new RESTCustomException('You must supply a claimId');
            }     
        }
        //validate POST method parameters
        if (method == 'PUT'){
            // make sure we have a valid calims id
            String roomDetails= request.requestBody.toString();
            //make sure that we have a valid Room JSON
            if (roomDetails == null) {
                throw new RESTCustomException('You must supply a Valid JSON');
            }
        }
    }
}