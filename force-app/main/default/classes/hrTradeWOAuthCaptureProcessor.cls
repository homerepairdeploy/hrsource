/**
 * Constants Class 
 * @CreatedDate : 15/11/2019
 * @CreatedBy : Pardha
 * @Description: Helper Class to populate the Work Authority Details depending on Work Authority type
 * Updated By :Prakash for the New value 'General Works' in Picklist
 */
public class hrTradeWOAuthCaptureProcessor {
    
        
     public static string processWOAuthCaptureCls(Map<Id,String> workOrderDetMap){
    
        
        map<string,string> mapTemplateNames=new map<string,string>{NJ_Constants.HOMEASSIST=>NJ_Constants.HOMEASSISTTEMP,
            													  NJ_Constants.LIABILITY=>NJ_Constants.LIABILITYTEMP,
            													  NJ_Constants.MAKESAFE=>NJ_Constants.MAKESAFETEMP,
                                                                  NJ_Constants.PRICEDWO=>NJ_Constants.PRICEDWOTEMP,
            													  NJ_Constants.RECTIFICATION=>NJ_Constants.RECTIFICATIONTEMP,
            												      NJ_Constants.QUOTEAPPR=>NJ_Constants.QUOTEAPPRTEMP,
            													  NJ_Constants.REPQUOT=>NJ_Constants.REPQUOTTEMP,
            													  NJ_Constants.DOCHG=>NJ_Constants.DOCHGTEMP,
                                                                  NJ_Constants.GWORKS=>NJ_Constants.REPQUOTTEMP};
                                                                 
        list<workorder> lstworkorder=[select Id,Service_Resource__r.Name,Claim_Contact__c,Contact_Phone__c,WorkOrderNumber,Work_Type_Name__c,Work_Order_Authority__c,Risk_Address__c,contact.name,contact.homephone,
                                     Claim_Contact_Mobile__c,Claim_Job__r.Name,Claim__c,Description,Special_Instructions_New__c,ThreadID__c,Relationship_to_insured__c from WorkOrder
                                     where Id=:workOrderDetMap.keyset()];                                                         
        string resultstr;  
        Map<Id,String> worktypeDetMap=new map<Id,String>(); 
        for(Workorder wk:lstworkorder){
            
            switch on workOrderDetMap.get(wk.Id){
                
            
               when 'Make Safe'{
                   switch on (wk.Work_Type_Name__c){
                   
                      when 'MS - Asbestos'{
                          
                         mapTemplateNames.put(NJ_Constants.MSASBESTOS,NJ_Constants.MSASBESTOSAUTHTEMP);
                         worktypeDetMap.put(wk.Id,NJ_Constants.MSASBESTOS);
                         mapTemplateNames.remove(NJ_Constants.MAKESAFE);
                      }
                      when 'MS - Tree Lopper'{
                            
                          mapTemplateNames.put(NJ_Constants.MSTREELOOPER,NJ_Constants.MSTREELOOPERAUTHTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.MSTREELOOPER);
                          mapTemplateNames.remove(NJ_Constants.MAKESAFE);
                      }
                      when 'MS - Roofer – Metal'{
                          mapTemplateNames.put(NJ_Constants.MSROOFERMETAL,NJ_Constants.MSROOFERAUTHTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.MSROOFERMETAL);
                          mapTemplateNames.remove(NJ_Constants.MAKESAFE);
                      } 
                      when 'MS - Roofer – Tiles'{
                         mapTemplateNames.put(NJ_Constants.MSROOFERTILE,NJ_Constants.MSROOFERAUTHTEMP);
                         worktypeDetMap.put(wk.Id,NJ_Constants.MSROOFERTILE);
                         mapTemplateNames.remove(NJ_Constants.MAKESAFE);
                      }
                      when 'MS - Restorer'{
                         mapTemplateNames.put(NJ_Constants.MSRESTORER,NJ_Constants.MSRESTORERAUTHTEMP);
                         worktypeDetMap.put(wk.Id,NJ_Constants.MSRESTORER);
                         mapTemplateNames.remove(NJ_Constants.MAKESAFE);
                      }
                      when 'MS - Temp Fencing'{
                          mapTemplateNames.put(NJ_Constants.MSTEMPFENCING,NJ_Constants.MSTEMPFENCINGAUTHTEMP);
                         worktypeDetMap.put(wk.Id,NJ_Constants.MSTEMPFENCING);
                         mapTemplateNames.remove(NJ_Constants.MAKESAFE);
                      }
                      when else{
                          mapTemplateNames.put(NJ_Constants.MAKESAFE,NJ_Constants.MAKESAFETEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.MAKESAFE);
                           
                      } 
                                 
                   }
                   
               }
               when 'Report and Quote'{
                   switch on (wk.Work_Type_Name__c){
                   
                      when 'Leak Detection'{
                          mapTemplateNames.put(NJ_Constants.RQLEAKDETECTION,NJ_Constants.RQLEAKAUTHTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.RQLEAKDETECTION);
                          mapTemplateNames.remove(NJ_Constants.REPQUOT);
                      }
                      when 'Roofer - Metal'{
                          mapTemplateNames.put(NJ_Constants.RQMETAL,NJ_Constants.RQROOFERAUTHTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.RQMETAL);
                          mapTemplateNames.remove(NJ_Constants.REPQUOT);
                      }
                      when 'Roofer - Polycarbonate'{
                          mapTemplateNames.put(NJ_Constants.RQPOLYCARB,NJ_Constants.RQROOFERAUTHTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.RQPOLYCARB);
                          mapTemplateNames.remove(NJ_Constants.REPQUOT);
                      }
                      when 'Roofer-Tiles'{
                          mapTemplateNames.put(NJ_Constants.RQTILES,NJ_Constants.RQROOFERAUTHTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.RQTILES);
                          mapTemplateNames.remove(NJ_Constants.REPQUOT);
                      }
                      when 'Engineer'{
                          mapTemplateNames.put(NJ_Constants.RQENGINEER,NJ_Constants.RQENGINEERAUTHTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.RQENGINEER);
                          mapTemplateNames.remove(NJ_Constants.REPQUOT);
                      } 
                      when else {
                          mapTemplateNames.put(NJ_Constants.REPQUOT,NJ_Constants.REPQUOTTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.REPQUOT);
                      }  
                      
                                 
                   }
                   
               }
                
                
                  when 'General Works'{
                   switch on (wk.Work_Type_Name__c){
                   
                      when 'Leak Detection'{
                          mapTemplateNames.put(NJ_Constants.RQLEAKDETECTION,NJ_Constants.RQLEAKAUTHTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.RQLEAKDETECTION);
                          mapTemplateNames.remove(NJ_Constants.REPQUOT);
                      }
                      when 'Roofer - Metal'{
                          mapTemplateNames.put(NJ_Constants.RQMETAL,NJ_Constants.RQROOFERAUTHTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.RQMETAL);
                          mapTemplateNames.remove(NJ_Constants.REPQUOT);
                      }
                      when 'Roofer - Polycarbonate'{
                          mapTemplateNames.put(NJ_Constants.RQPOLYCARB,NJ_Constants.RQROOFERAUTHTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.RQPOLYCARB);
                          mapTemplateNames.remove(NJ_Constants.REPQUOT);
                      }
                      when 'Roofer-Tiles'{
                          mapTemplateNames.put(NJ_Constants.RQTILES,NJ_Constants.RQROOFERAUTHTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.RQTILES);
                          mapTemplateNames.remove(NJ_Constants.REPQUOT);
                      }
                      when 'Engineer'{
                          mapTemplateNames.put(NJ_Constants.RQENGINEER,NJ_Constants.RQENGINEERAUTHTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.RQENGINEER);
                          mapTemplateNames.remove(NJ_Constants.REPQUOT);
                      } 
                      when else {
                          mapTemplateNames.put(NJ_Constants.REPQUOT,NJ_Constants.REPQUOTTEMP);
                          worktypeDetMap.put(wk.Id,NJ_Constants.REPQUOT);
                      }  
                      
                                 
                   }
                   
               }    
                  
                 
            } 
            resultstr='';
            If (workOrderDetMap.get(wk.Id) == NJ_Constants.MAKESAFE || workOrderDetMap.get(wk.Id) ==NJ_Constants.REPQUOT){
             
                emailTemplate emtemp1=[Select Name, Id, Body, HtmlValue,  IsActive, Subject from EmailTemplate where DeveloperName=:mapTemplateNames.get(worktypeDetMap.get(wk.Id))];
                resultstr=emtemp1.HtmlValue;   
            }    
            else{
                emailTemplate emtemp=[Select Name, Id, Body, HtmlValue,  IsActive, Subject from EmailTemplate where DeveloperName=:mapTemplateNames.get(workOrderDetMap.get(wk.Id))];  
                resultstr=emtemp.HtmlValue;       
            }    
          
                                 
                                     
                                     
        }                            
        
        
        return resultstr;
        
    }
            
    
     public static void checkTest(){
     Integer i=0;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
	  i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
	} 

    
    
}