/**
 * This class is helper class for variation trigger
 * @CreatedDate : 01/02/2019
 * @Author : Nikhil
 */
public without sharing class VariationHelper {
    private static final String VARIATION_APPROVAL_PROCESS = 'Variation_Processesv7';
    private static final String VARIATION_PRODUCT_NAME = 'Variation';
    //public enum WorkOrderOperation { LOCK, UNLOCK }
    //public enum ServiceAppointmentOperation { LOCK, UNLOCK }
    public enum LockUnlockOperation { LOCK, UNLOCK }

    /**
     * This method is used to submit variation for approval and create a Variation Submitted Task
     * @param variations list of variations
     * @Author : Nikhil
     * @CreatedDate : 02/02/2019
     */
    public static void submitVariationForApproval(List<Variation__c> variations){	
      Set<Id> setWOIds = new Set<Id>();
      Map<Id,workOrder> mapWOToCaseId = new Map<Id,WorkOrder>();
      Map<Id,Variation__c> VarMap = new Map<Id,Variation__c>();  
      List<Variation__c> lstVariation = new List<Variation__c>();
      List<workOrder> woList       = new List<workOrder>();
      List<Task> lstTaskToInsert = new List<Task>();

     // Id currUserId = UserInfo.getUserId();
      for(Variation__c var : variations) {
          if(var.Total_Variation_Amount__c > 0) {
              varMap.put(var.Work_Order__c, var );
              setWOIds.add(var.Work_Order__c);
              lstVariation.add(var);              // Added as part of HRD-104 – Work Order Invoice Fields
          }
      }
        /*
              Approval.ProcessSubmitRequest varReq = new Approval.ProcessSubmitRequest();
              varReq.setObjectId(var.Id);
              varReq.setSubmitterId(currUserId);
              varReq.setProcessDefinitionNameOrId(VARIATION_APPROVAL_PROCESS);
              varReq.setSkipEntryCriteria(false);
              // Send variation request for approval
              Approval.ProcessResult result = Approval.process(varReq);
              setWOIds.add(var.Work_Order__c);
              lstVariation.add(var);
              System.debug('Approval Submission Result:'+result);
          }
      } */
 
      for(WorkOrder wo : [SELECT Id, Up_To_Amount__c, Total_Amount__c, WorkOrderNumber,CaseId,
                            (SELECT Additional_Labour_Amount__c, Additional_Materials_Amount__c,Variation_Status__c
                             FROM Variations__r WHERE Work_Order__c IN :setWOIds AND Variation_Status__c <> 'Rejected')
                          FROM WorkOrder 
                          WHERE Id IN :setWOIds]){
         
          workOrder wo1 = new workOrder();
          system.debug('in Variation Handler');
          decimal varLab = varMap.get(wo.id).Additional_Labour_Amount__c == null ? 0.0 : varMap.get(wo.id).Additional_Labour_Amount__c ;      
          decimal varMat = varMap.get(wo.id).Additional_Materials_Amount__c == null ? 0.0 : varMap.get(wo.id).Additional_Materials_Amount__c ;          
//          wo1.Total_WO_Incl_Variation_ex_GST__c = varLab + varMat + wo.Up_To_Amount__c ;
          
          // wo1.Variation_Total__c  = varLab + varMat ;    commented as part of HRD-104 – Work Order Invoice Fields 
          wo1.ID = wo.ID;         
          wo1.Active_Variation_Exists__c = true;
                              
          //wo1.Total_Variations_Amounts_Ex_Rejected__c = 0.0; 
          wo1.Variation_Total__c  = 0.0;
          wo1.Variation_Approval_Status__c  = 'Requested';
                              
         for(Variation__c vr : wo.Variations__r)
               {
                   if(vr.Additional_Labour_Amount__c == null)
                       vr.Additional_Labour_Amount__c = 0.0;
                   
                   if(vr.Additional_Materials_Amount__c == null)
                       vr.Additional_Materials_Amount__c = 0.0;
                  
                   if (vr.Variation_Status__c == 'Approved')
                   {
                    //   wo1.Total_Variations_Amounts_Ex_Rejected__c = wo1.Total_Variations_Amounts_Ex_Rejected__c + vr.Additional_Labour_Amount__c + vr.Additional_Materials_Amount__c;
                       wo1.Variation_Total__c = wo1.Variation_Total__c  + vr.Additional_Labour_Amount__c + vr.Additional_Materials_Amount__c;
                   }
               }
                 // wo1.Total_Variations_Amounts_Ex_Rejected__c = wo1.Total_Variations_Amounts_Ex_Rejected__c + wo.Up_To_Amount__c ;
                 wo1.Total_Variations_Amounts_Ex_Rejected__c =wo1.Variation_Total__c  + wo.Up_To_Amount__c ;
                              
                              
           woList.add(wo1);
                              
                              
          mapWOToCaseId.put(wo.Id, wo);
      }
        
        if(woList.size() > 0 )
              Update woList;
        

      for(Variation__c var : lstVariation){
        Task t = new Task();
        t.ActivityDate = System.today().addDays(1);
        t.Description =  mapWOToCaseId.get(var.Work_Order__c).WorkOrderNumber;
        t.Work_Order__c = var.Work_Order__c;
        t.OwnerId = System.Label.HR_Claims;
        t.Priority = 'Critical';
        t.Status = 'Not Started';
        t.Subject = 'Variation Approval submitted for Work Order'; 
        t.Task_Type_Reporting__c = 'Variation Submission';
        t.WhatId = mapWOToCaseId.get(var.Work_Order__c).CaseId;
        lstTaskToInsert.add(t);
      }

      if(!lstTaskToInsert.isEmpty()){
        insert lstTaskToInsert;
      }

    }

    /**
     * This method is used to create task when a variation is approved or rejected from approval process.
     * @param variations list of variations
     * @Author : Nikhil
     * @CreatedDate : 02/02/2019
     // CK commented
     public static void createTaskForApprovedRejectVariation(List<Variation__c> newList, Map<Id,Variation__c> oldMap){
        Set<Id> setWOIds = new Set<Id>();
        Map<Id,WorkOrder> mapWOToCaseId = new Map<Id,WorkOrder>();
        List<Variation__c> lstVariation = new List<Variation__c>();
        List<Task> lstTaskToInsert = new List<Task>();

        for(Variation__c var : newList){
          if((var.Approved__c != oldMap.get(var.Id).Approved__c && var.Approved__c == true) || (var.Rejected__c != oldMap.get(var.Id).Rejected__c && var.Rejected__c == true)){
            setWOIds.add(var.Work_Order__c);
            lstVariation.add(var);
          }
        }

        for(WorkOrder wo : [SELECT Id, WorkOrderNumber, CaseId FROM WorkOrder WHERE Id IN :setWOIds]){
          mapWOToCaseId.put(wo.Id, wo);
        }

        for(Variation__c var : lstVariation){
          Task t = new Task();
          t.ActivityDate = System.today().addDays(1);
          if(var.Approved__c == true){
            t.Description = 'Variation Approved for Work Order ' + mapWOToCaseId.get(var.Work_Order__c).WorkOrderNumber;
            t.Subject = 'Variation Approval Task for Work Order ' + mapWOToCaseId.get(var.Work_Order__c).WorkOrderNumber;
             t.Task_Type_Reporting__c = 'Variation Approval';
          }
          if(var.Rejected__c == true){
            t.Description = 'Variation is rejected for Work Order ' + mapWOToCaseId.get(var.Work_Order__c).WorkOrderNumber +'. See the variation record for more information.';
            t.Subject = 'Variation Rejection Task for Work Order ' + mapWOToCaseId.get(var.Work_Order__c).WorkOrderNumber;
            t.Task_Type_Reporting__c = 'Variation Rejected';  
          }
          t.Work_Order__c = var.Work_Order__c;
          t.OwnerId = System.Label.HR_Claims;
          t.Priority = 'Critical';
          t.Status = 'Not Started';
          t.WhatId = mapWOToCaseId.get(var.Work_Order__c).CaseId;
          lstTaskToInsert.add(t);
        }

        if(!lstTaskToInsert.isEmpty()){
          insert lstTaskToInsert;
        }
     }  */


    /**
     * This method is used to lock/unlock work orders and its child service appointment when variation is inserted. When the variation is 
     * created from service appointment we are locking/unlocking SA with its parent WO and all sibling SA's.
     * @param variations list of variations
     * @Author : Nikhil
     * @CreatedDate : 02/02/2019
     // CK commented 
    public static void lockUnlocaWOAndSA(List<Variation__c> variations, LockUnlockOperation oper) {
        List<WorkOrder> woUpdates = new List<WorkOrder>();
        Set<Id> setWorkOrderIds = new Set<Id>();
        List<ServiceAppointment> lstServiceAppToUpdate = new List<ServiceAppointment>();

        Id rtLocked = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(NJ_Constants.WORKORDER_LOCKED_RT_DEVELOPERNAME).getRecordTypeId();
        Id rtLockedForSA = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get(NJ_Constants.SERVICEAPPOINTMENT_LOCKED_RT_DEVELOPERNAME).getRecordTypeId();
        Id rtUnlockedForSA = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get(NJ_Constants.SERVICE_APPOINTMENT_RT_GENERAL).getRecordTypeId();
        
        WorkOrder woTemp;
        for(Variation__c var : variations) {
            setWorkOrderIds.add(var.Work_Order__c);
            woTemp = new WorkOrder(Id = var.Work_Order__c);
            woTemp.RecordtypeId = oper == LockUnlockOperation.LOCK ? rtLocked : var.WorkOrderRecordTypeID__c;
            woUpdates.add(woTemp);
        }

        for(ServiceAppointment sa : [SELECT Id, RecordtypeId, ParentRecordId 
                                     FROM   ServiceAppointment 
                                     WHERE  ParentRecordId IN : setWorkOrderIds]){
            sa.RecordtypeId =  oper == LockUnlockOperation.LOCK ? rtLockedForSA : rtUnlockedForSA;
            lstServiceAppToUpdate.add(sa);
        }

        try{
            if(!woUpdates.isEmpty()) {
                update woUpdates;
            }
            if(!lstServiceAppToUpdate.isEmpty()){
                update lstServiceAppToUpdate;
            }
        }catch(Exception ex) {
            variations[0].addError(ex.getMessage());
        }
        
    }  */
    
    /**
     * This method is used to create a work order line item when variation is approved
     * @param variations list of variations
     * @Author : Nikhil
     * @CreatedDate : 02/02/2019
     */
    public static void createWorkOrderLineItem(List<Variation__c> variations) {
        List<WorkOrderLineItem> woliItems = new List<WorkOrderLineItem>();
        Map<Id, WorkOrderLineItem> mapVarIdWorkOrderLineItem = new Map<Id, WorkOrderLineItem>();
        WorkOrderLineItem tempWOLI;
        Id prodId;
        Id woliVarRtId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByDeveloperName().get(NJ_Constants.WORKORDERLINEITEM_RT_VARIATION).getRecordTypeId();
        Id pbeId;
        for(Product2 prod : [SELECT Id
                                                 FROM Product2
                                                 WHERE Name = :VARIATION_PRODUCT_NAME
                                                 LIMIT 1]) {
            prodId = prod.Id;
        }
        for(PriceBookEntry pbe : [SELECT Id
                                                          FROM PriceBookEntry
                                                          WHERE Product2Id = :prodId
                                                          AND IsActive = true
                                                          AND Pricebook2.Name = :NJ_Constants.STANDARD_PRICEBOOK_NAME
                                                          LIMIT 1]) {
            pbeId = pbe.Id;
        }
        for(Variation__c var : variations) {
            tempWOLI = new WorkOrderLineItem(WorkOrderId = var.Work_Order__c, Additional_Labour_Amount_AUD__c = var.Additional_Labour_Amount__c,
                                                                             Additional_Materials_Amount_AUD__c = var.Additional_Materials_Amount__c, ProductWOLI__c = prodId,
                                                                             RecordTypeId = woliVarRtId, PricebookEntryId = pbeId, 
                                                                             Variation_Amount__c = (var.Additional_Labour_Amount__c != null ? var.Additional_Labour_Amount__c : 0 )
                                                                              + (var.Additional_Materials_Amount__c != null ? var.Additional_Materials_Amount__c : 0),
                                                                             Variation__c = var.Id);
            woliItems.add(tempWOLI);
            mapVarIdWorkOrderLineItem.put(var.Id, tempWOLI);
        }
        if(!woliItems.isEmpty()) {
            try {
                System.debug('Nik '+woliItems);
                insert woliItems;
                for(Variation__c var : variations) {
                    System.debug('mapVarIdWorkOrderLineItem.get(var.Id).Id; '+mapVarIdWorkOrderLineItem.get(var.Id).Id);
                    var.Work_Order_Line_Item__c = mapVarIdWorkOrderLineItem.get(var.Id).Id;
                }
            }catch(Exception ex) {
                variations[0].addError(ex.getMessage());
            }
        } 
    } 
    /**
     * This method is used to create a service appointment when variation is approved and variation reason is additional scope
     * @param variations list of variations
     * @Author : Nikhil
     * @CreatedDate : 02/02/2019
     */
    public static void createServiceAppointment(List<Variation__c> variations) {
        List<ServiceAppointment> servAppointments = new List<ServiceAppointment>();
        Map<Id, ServiceAppointment> mapVarIdSA = new Map<Id, ServiceAppointment>();
        ServiceAppointment tempSA;
        Id saRTId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get(NJ_Constants.SERVICE_APPOINTMENT_RT_GENERAL).getRecordTypeId();
        for(Variation__c var : variations) {
            tempSA = new ServiceAppointment(DueDate = Datetime.now().addDays(14), EarliestStartTime = Datetime.now(),
                                                                            FSL__InternalSLRGeolocation__latitude__s = 35, FSL__InternalSLRGeolocation__longitude__s = 35,
                                                                            Duration = var.Additional_Labour_Time_Hours__c, ParentRecordId = var.Work_Order__c,
                                                                            RecordtypeId = saRTId, Status = NJ_Constants.VARIATION_STATUS_NEW);
            servAppointments.add(tempSA);
            mapVarIdSA.put(var.Id, tempSA);
        }
        if(!servAppointments.isEmpty()) {
            try {
                insert servAppointments;
                for(Variation__c var : variations) {
                    var.Service_Appointment__c = mapVarIdSA.get(var.Id).Id;
                }
            }catch(Exception ex) {
                variations[0].addError(ex.getMessage());
            }
        } 
    }
    
    
    // Added as part of HRD-104 – Work Order Invoice Fields 
    public static void updateVariationAmount(List<Variation__c> variations){	
        Set<Id> setWOIds = new Set<Id>();         
        Map<Id,Variation__c> VarMap = new Map<Id,Variation__c>();          
        List<workOrder> woList       = new List<workOrder>();       
        
        for(Variation__c var : variations) {
            if(var.Total_Variation_Amount__c > 0) {
                varMap.put(var.Work_Order__c, var );
                setWOIds.add(var.Work_Order__c);
            }
        }
        
        
        for(WorkOrder wo : [SELECT Id, Up_To_Amount__c, Total_Amount__c, WorkOrderNumber,CaseId,
                            (SELECT Additional_Labour_Amount__c, Additional_Materials_Amount__c,Variation_Status__c
                             FROM Variations__r WHERE Work_Order__c IN :setWOIds AND Variation_Status__c <> 'Rejected')
                            FROM WorkOrder 
                            WHERE Id IN :setWOIds]){
                                
                                workOrder wo1 = new workOrder();
                                system.debug('in Variation Handler');                        
                                wo1.ID = wo.ID;                                        
                                wo1.Variation_Total__c  = 0.0;
                                //wo1.Variation_Approval_Status__c  = 'Requested';
                                
                                for(Variation__c vr : wo.Variations__r)
                                {
                                    if(vr.Additional_Labour_Amount__c == null)
                                        vr.Additional_Labour_Amount__c = 0.0;
                                    
                                    if(vr.Additional_Materials_Amount__c == null)
                                        vr.Additional_Materials_Amount__c = 0.0;
                                    
                                    if (vr.Variation_Status__c == 'Approved')
                                    {                                 
                                        wo1.Variation_Total__c = wo1.Variation_Total__c  + vr.Additional_Labour_Amount__c + vr.Additional_Materials_Amount__c;
                                    }
                                }                                
                                wo1.Total_Variations_Amounts_Ex_Rejected__c =wo1.Variation_Total__c  + wo.Up_To_Amount__c ;
                                woList.add(wo1);                                                                
                            }
        
        if(woList.size() > 0 )
            Update woList;
    }
}