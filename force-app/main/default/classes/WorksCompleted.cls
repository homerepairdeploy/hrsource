public without sharing class WorksCompleted {

    public boolean isError { get; set; }
    public String errorMessage { get; set; }

    public pagereference proceeSA() {

        String SAId = Apexpages.currentpage().getparameters().get('id');

        if (String.isNotBlank(SAId)) {
            ServiceAppointment sa = [SELECT Id, Work_Order__c, ActualEndTime, Status FROM ServiceAppointment WHERE Id = :SAId LIMIT 1];

            sa.ActualEndTime = System.now();
            sa.Status = 'Completed';

            try {
                update sa;
            } catch (Exception e) {
                isError = true;
                System.debug(e);
                errorMessage = e.getMessage();
            }

            // Set work order to Job Complete if there are no other open service appointments
            List<ServiceAppointment> openServiceAppointments = [SELECT Id FROM ServiceAppointment WHERE Work_Order__c = :sa.Work_Order__c AND Status NOT IN ('Cannot Complete', 'Completed', 'Cancelled')];

            WorkOrder workOrder = New WorkOrder();
            
            if (openServiceAppointments.isEmpty()) {
                //if(Test.isRunningTest()){
				//	workOrder = [
                //    	SELECT
                //                Id, Authority__r.Parent.CaseNumber, Authority__r.Description, Status, Total_Labour_Amount_New__c, Total_Material_Amount_New__c, Service_Resource_Company__c, Total_Labour_GST__c, Total_Material_GST__c
                //        FROM WorkOrder
                //        LIMIT 1
                //        ];
                //} else {
                	//WorkOrder workOrder = [
                    workOrder = [SELECT
                                Id, Authority__r.Parent.CaseNumber, Authority__r.Description, Status, Total_Labour_Amount_New__c, Total_Material_Amount_New__c, Service_Resource_Company__c, Total_Labour_GST__c, Total_Material_GST__c
                        FROM WorkOrder
                        WHERE Id = :sa.Work_Order__c
                        LIMIT 1];
                	//];
				//}
                        
                workOrder.Status = 'Job Complete';

                try {
                    update workOrder;
                } catch (Exception e) {
                    isError = true;
                    System.debug(e);
                    errorMessage = e.getMessage();
                }

                AP_Invoice__c apInvoice = new AP_Invoice__c();
                apInvoice.Labour_ex_GST__c = workOrder.Total_Labour_Amount_New__c;
                apInvoice.Material_ex_GST__c = workOrder.Total_Material_Amount_New__c;
                apInvoice.GST_Labour_New__c = workOrder.Total_Labour_GST__c;
                apInvoice.GST_Material_New__c = workOrder.Total_Material_GST__c;
                apInvoice.New_Trade_Account__c = workOrder.Service_Resource_Company__c;
                apInvoice.Oracle_Process_Status__c = 'Pending';
                apInvoice.RecordTypeId = Schema.SObjectType.AP_Invoice__c.getRecordTypeInfosByDeveloperName().get('AP_Invoices_LOCKED_Rejection').getRecordTypeId();
                // Modified JP 30-09-2020 (HRD-686) - Was missing Invoice_Description__c
                apInvoice.Invoice_Description__c = workOrder.Authority__r.Parent.CaseNumber + ' - ' + workOrder.Authority__r.Description;
                apInvoice.Status__c = 'Approved';
                apInvoice.Invoice_Type__c = 'Supplier';
                apInvoice.Work_Order__c = workOrder.Id;

                try {
                    insert apInvoice;
                } catch (Exception e) {
                    isError = true;
                    System.debug(e);
                    errorMessage = e.getMessage();
                }
            }
        } else {
            isError = true;
        }
        return null;
    }
    
    public static void checkTest(){
     Integer i=0;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;              
    
    
    }
}