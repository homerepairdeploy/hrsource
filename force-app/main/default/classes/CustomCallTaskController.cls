public class CustomCallTaskController {

    public ApexPages.standardController stdController = null;
   public task taskrec {get; set;}
    public string contactname {get;set;}
    public string subject {get;set;}
    public string caseId {get;set;}
    public string description {get;set;}
    public string callresult {get;set;}
     

    public CustomCallTaskController(ApexPages.StandardController controller) {
        
        
       taskrec = (Task)controller.getRecord();
       Task taskrecsingle=[select subject,who.name,what.name,Call_Result__c,Description from task where Id=:taskrec.Id];
       If (taskrecsingle.who.name!=null) contactname=taskrecsingle.who.name;
       subject=taskrecsingle.subject;
       if (taskrecsingle.what.name!=null) caseId=taskrecsingle.what.name;
       
        


    } 
    
    public PageReference doSave(){
         
       update taskrec;
       PageReference pg = new PageReference('/'+taskrec.Id);
       pg.setRedirect(true);
       return pg;
    }
}