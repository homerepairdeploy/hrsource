/***************************************************************** 
Purpose: Bulk copy formatted contact phone fields to claim phone
History                                                             
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            XXXX          03/01/2020    Created      Home Repair Claim System  
*******************************************************************/
@istest 
public class OnceoffBatchInitialFormatCTCPhoneTest{ 
    @testSetup 
    static void setup(){
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact firstcon=HomeRepairTestDataFactory.createContact('Testcontactone');
        Case csone=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,firstcon.Id);
        claim_Job__c cjone=HomeRepairTestDataFactory.createClaimJob(csone.Id);
         
    }  
    static testmethod void testBatchformatphones() {  
            Test.startTest(); 
                database.executeBatch(new OnceoffBatchInitialFormatCTCPhone(),1);
               
            Test.stopTest();
        
    }
    
   
    
    
}