global without sharing class RecipientCreatedTaxInvoiceStatement {
    @InvocableMethod
    public static void RecipientCreatedTaxInvoiceStatement(List<String> invoices) {
        String netWorkId = Network.getNetworkId();
        RecipientCreatedTaxInvoice(Invoices,netWorkId);
    }

    @future(callout=true)
    public static void RecipientCreatedTaxInvoice(List<String> invoices,string netWorkId) {
        List<AP_Invoice__c> invoiceList = [Select id,Name,Generate_Invoice_Statement__c, OwnerId from AP_Invoice__c where id IN :invoices];
        String durableId = [SELECT Id, Name, Status FROM Site WHERE Name = 'RepairForce_Trade_Platform' AND Status = 'Active' LIMIT 1].Id;
        String baseURL = [SELECT id, SecureUrl from SiteDetail where DurableId = :durableId LIMIT 1].SecureUrl;
        List<Attachment> attToInsert = new List<Attachment>();

        for (AP_Invoice__c inv : invoiceList) {
            // PageReference pdfPage = new PageReference(baseURL + '/apex/RecipientCreatedTaxInvoiceStatement');
            PageReference pdfPage= Page.RecipientCreatedTaxInvoiceStatement;
            pdfPage.getParameters().put('id', inv.Id);

            Attachment att = new Attachment();
            att.ParentId = inv.Id;
            att.Name = 'RCTI-' + inv.Name + '-' + System.today().format() + '.pdf';
            if (!Test.isRunningTest()) {
                att.Body = pdfPage.getContentAsPDF();
            }
            else {
                att.Body = Blob.valueOf('Test Data');
            }
            attToInsert.add(att);
        }

        insert attToInsert;
    }
}