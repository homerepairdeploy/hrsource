@isTest
public class OnceOffUpdateWOValueTest {
    @TestSetup
    public static void setupTestData() {
        HomeRepairTestDataFactory.createHomeRepairsTriggerSwitch();
        Policy__c po=HomeRepairTestDataFactory.createPolicy();
        system.debug(po.id);
        Contact con=HomeRepairTestDataFactory.createContact('Test');
        Case cs=HomeRepairTestDataFactory.createCaseWithOriginAndPolicy(po.id,con.Id);
        WorkType wt=HomeRepairTestDataFactory.createWorkType('Repair Items','Painter');
		Case cj = HomeRepairTestDataFactory.createChildClaimAuthority(cs.Id);
        WorkOrder wo=createWorkOrderWithCase('Home Repair Trades',cs.id,wt.id,cj.id);        
        
    }
    public static testmethod void testBatchOnceoffUpdateWOValue() {  
            Test.startTest(); 
                database.executeBatch(new OnceOffUpdateWOValue(),1);
               
            Test.stopTest();
        
    }
	
	 public static WorkOrder createWorkOrderWithCase(String RecordTypeName,String CaseId,String workTypeId,string claimAuthorityId){ 
        Id pbId = Test.getStandardPricebookId();
        WorkOrder wo = new WorkOrder();
        wo.subject ='title';
        wo.WorkTypeId=workTypeId;
        wo.CaseId=CaseId;
        wo.Status='New';
        wo.Authority__c = claimAuthorityId;
        wo.Pricebook2Id = pbId;
        if(RecordTypeName != ''){
            wo.RecordTypeId= retriveRecordTypeId(RecordTypeName,'WorkOrder');
            system.debug(retriveRecordTypeId(RecordTypeName,'WorkOrder'));
        }
        system.debug(wo);
        insert wo;
        
               
        return wo;
    }
    
    public static string retriveRecordTypeId(String RecordTypeName,String ObjectName){
        string devRecordTypeId = Schema.getGlobalDescribe().get(ObjectName).getDescribe().getRecordTypeInfosByName().get(RecordTypeName).getRecordTypeId();
        return devRecordTypeId;
    }
    
}