/************************************************************************************************************
Name: ThumbnailImageComponentController
=============================================================================================================
Purpose: Controller of Lightning Component ThumbnailImageComponent 
===============================================================================================================
History
-----------
VERSION    AUTHOR           DATE             DETAIL       DESCRIPTION
1.0        Nikhil         8/03/2019         Created     ThumbnailImageComponentController
*************************************************************************************************************/
public class ThumbnailImageComponentController {

	@AuraEnabled
	public static ContentDocumentLinkWrapper getAllThumbnails(String WOId){
		Set<Id> setRoomIds = new Set<Id>();
		List<ContentDocumentLink> lstFileOnClaimRecord = new List<ContentDocumentLink>();
		List<ContentDocumentLink> lstFileOnWOLIRecords = new List<ContentDocumentLink>();

		WorkOrder wo = [SELECT Id, WorkOrderNumber, CaseId FROM WorkOrder WHERE Id = :WOId];

		List<WorkOrderLineItem> lstWOLineItem = [SELECT Id, LineItemNumber, Room__c FROM WorkOrderLineItem WHERE WorkOrderId = :wo.Id];
		for(WorkOrderLineItem woli : [SELECT Id, LineItemNumber, Room__c FROM WorkOrderLineItem WHERE WorkOrderId = :wo.Id]){
			setRoomIds.add(woli.Room__c);
		}

		for(ContentDocumentLink cdl : [SELECT ContentDocumentId, ContentDocument.FileType ,ContentDocument.title, LinkedEntityId, ShareType, Visibility 
									   FROM   ContentDocumentLink 
									   WHERE  LinkedEntityId = :wo.CaseId OR LinkedEntityId IN :setRoomIds
									   Order By ContentDocument.title]){
			if(cdl.ContentDocument.FileType.toUpperCase() == 'JPG' || cdl.ContentDocument.FileType.toUpperCase() == 'JPEG' || cdl.ContentDocument.FileType.toUpperCase() == 'PNG' || cdl.ContentDocument.FileType.toUpperCase() == 'MP4' ){
				if(cdl.LinkedEntityId == wo.CaseId){
					lstFileOnClaimRecord.add(cdl);
				}else{
					lstFileOnWOLIRecords.add(cdl);
				}
			}
		}

		ContentDocumentLinkWrapper cdlWrap = new ContentDocumentLinkWrapper(lstFileOnClaimRecord, lstFileOnWOLIRecords);
		system.debug('cdlWrap >>>'+cdlWrap);
		return cdlWrap;

	}

	public class ContentDocumentLinkWrapper{
		@AuraEnabled public List<ContentDocumentLink> lstFileOnClaimRecord = new List<ContentDocumentLink>();
		@AuraEnabled public  List<ContentDocumentLink> lstFileOnWOLIRecords = new List<ContentDocumentLink>();

		public ContentDocumentLinkWrapper(List<ContentDocumentLink> lstFileOnClaimRecord, List<ContentDocumentLink> lstFileOnWOLIRecords){
			this.lstFileOnClaimRecord = lstFileOnClaimRecord;
			this.lstFileOnWOLIRecords = lstFileOnWOLIRecords;
		}
	}
 
}