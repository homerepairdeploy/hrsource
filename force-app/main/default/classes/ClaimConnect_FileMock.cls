/********************************************************************************************** 
Name        : ClaimConnect_FileMock
Description : Test Class for mock callouts to upload photos to Amazon s3 server and
			  has been used in ClaimConnect_summaryControllerTest, ClaimConnect_UploadHandler_Test
                                                            
VERSION        AUTHOR           DATE           DETAIL      DESCRIPTION   
1.0            Prasanth         31-05-2020     Created     ClaimConnect APP
/***********************************************************************************************/
@isTest
global class ClaimConnect_FileMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.getBodyAsBlob();
        response.setStatusCode(200);
        response.setStatus('OK');
        response.setBody('Test content');
        return response; 
    }
}