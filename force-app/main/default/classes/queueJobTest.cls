@isTest
public class queueJobTest {
    
    
    @isTest
    static void testFirstQueueable(){
    
        Id strRecordTypeAccId = Schema.SobjectType.Account.getRecordTypeInfosByName().get('Insurance Provider').getRecordTypeId();
        /* Create common test Accounts */
        List<Account> testAccounts = new List<Account>();
        for(Integer i=0;i<2;i++) {
            testAccounts.add(new Account(Name = 'TestAcct'+i));
        }
        insert testAccounts;  
        
        /* Create common test Insurance Accounts */
        List<Account> testAccountInsurance = new List<Account>();
        testAccountInsurance.add(new Account(Name = 'TestAcct',RecordTypeId=strRecordTypeAccId,pds__c='www.google.com'));
        insert testAccountInsurance;     
        
        /* Create common test Contacts */
        List<Contact> testContacts = new List<Contact>();
        for(Integer i=0;i<2;i++) {
            testContacts.add(new Contact(FirstName = 'TestAcct'+i, LastName = 'TestAcct'+i));
        }
        insert testContacts;
        
        /* Create policy record */
        List<Policy__c > policyList = new List<Policy__c >();
        Policy__c policyobj = new Policy__c();
        policyobj.State__c='NSW';
        policyList.add(policyobj);
        insert policyList;
        
        case caseobj = new case();
        caseobj.ContactId = testContacts[0].id;
        caseobj.AccountId = testAccounts[0].id;
        caseobj.Status = 'Working';
        caseobj.Policy__c = policyList[0].id;
        caseobj.Excess__c = 300;
        
        insert caseObj;
        Test.startTest();
            System.enqueueJob(new AsyncgenCongoReport(caseObj.Id,'123'));
			
        Test.stopTest();
         
    }
        
    
}