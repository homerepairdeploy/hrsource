/***************************************************************** 
Purpose: Scheduledclass for uploading photos to S3                                                   
--------                                                            
VERSION        AUTHOR         DATE           DETAIL       Description 
1.0            HomeRepair    23/06/2020      Created      Home Repair Claim System  
*******************************************************************/
global class AWSUploadPhotosProcess implements Schedulable {
  global void execute(SchedulableContext sc) { 
    UploadAssessmentPhotos objClaim = new UploadAssessmentPhotos();
    Database.executeBatch(objClaim,1);       
  }
}