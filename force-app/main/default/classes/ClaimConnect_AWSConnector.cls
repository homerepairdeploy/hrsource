/********************************************************************************************** 
Name        : ClaimConnect_AWSConnector
Description : Class used to make http callout to Amazon s3 server through REST API and is referenced in ClaimConnect_AWSService
                                                            
VERSION        AUTHOR           		DATE           DETAIL      DESCRIPTION   
1.0            Vidhya Sivaperuman       25-05-2020     Created     ClaimConnect APP
/***********************************************************************************************/
public with sharing class ClaimConnect_AWSConnector {  
    
    
    public boolean uploadFileToAWS(Id parentId,String Objectname,String rName,String rootpath,ContentVersion content){
        List<AWS_Credential__mdt> lstAws=[Select Bucket_Name__c ,Host_Name__c from AWS_Credential__mdt];
        
        String filename=content.Title;
        filename=filename.replaceAll('[^a-zA-Z0-9]','%20');
        String fileextension=content.FileExtension;
        
        HttpRequest req = new HttpRequest();
        String attachmentBody =  EncodingUtil.base64Encode(content.VersionData); 
        Blob base64Content = EncodingUtil.base64Decode(attachmentBody);           
        String formattedDateString = Datetime.now().formatGMT('EEE, dd MMM yyyy HH:mm:ss z');
        String dateString = Datetime.now().format('ddMMYYYYHHmmss');        
        String fileID = content.ContentDocumentId;        
        String fullfilename=String.valueOf(UserInfo.getUserId()).substring(0,15)+'%20-%20'+UserInfo.getFirstName()+'%20'+UserInfo.getLastName()+'%20-%20'+filename.toLowerCase()+ '.'+fileextension.toLowerCase();
        string filepath=rootPath +'/'+Objectname+'/'+String.valueOf(parentId).substring(0, 15)+'%20-%20'+rName+'/'+fullfilename;
        String bucketname = lstAws.get(0).Bucket_Name__c; //AWS bucket name
        String host = lstAws.get(0).Host_Name__c; //'s3.amazonaws.com:443'
        String method = 'PUT';
        req.setMethod(method);        
        
        
         req.setEndpoint('callout:AWS_S3/'+filepath);
        //create header information
        req.setHeader('Host', bucketname + '.' + host); 
        req.setHeader('Content-Length', String.valueOf(attachmentBody.length()));
        req.setHeader('Content-Encoding', 'UTF-8');
        req.setHeader('Content-type', ContentType(fileextension));
        req.setHeader('Connection', 'keep-alive');
        req.setHeader('Date', formattedDateString); 
        req.setHeader('ACL', 'public-read');
        //store file as blob       
        req.setBodyAsBlob(base64Content);         
        Http http = new Http();
        HTTPResponse res = http.send(req);

        System.debug('*Resp:' + String.ValueOF(res.getBody()));
        System.debug('RESPONSE STRING: ' + res.toString());
        System.debug('RESPONSE STATUS: ' + res.getStatus());
        System.debug('STATUS_CODE: ' + res.getStatusCode());

        if(res.getStatusCode() == 200){
            System.debug('File Uploaded Successfully to AWS');
            return true;
        }
        else
        {
            System.debug('File Upload to AWS failed');
            return false;
        }
    }     

     public string ContentType(string fileType)
    {
        switch on fileType.toLowerCase()
        {
            when 'png'
            {
                return 'image/png';
                
            }
            when else {
                return 'image/jpg';
            }
        }
    }
    
    public FileData downloadFileFromAWS(String rootpath,String filepath,String filename,String fileextension){        
        List<AWS_Credential__mdt> lstAws=[Select Bucket_Name__c ,Host_Name__c from AWS_Credential__mdt];
        String formattedDateString = Datetime.now().formatGMT('EEE, dd MMM yyyy HH:mm:ss z');
        String bucketname = lstAws.get(0).Bucket_Name__c; //AWS bucket name
        String hostname = lstAws.get(0).Host_Name__c; //'s3.amazonaws.com:443'
        String contentType = ContentType(fileextension);       
        
        Integer startpos= filepath.indexOf(rootpath);       
        String fpath=filepath.substring(startpos);
        System.debug('fpath: ' + fpath);
            
        HttpRequest req = new HttpRequest();
        String method = 'GET';
        req.setMethod(method);         
        req.setEndpoint('callout:AWS_S3/'+fpath);        
        
        req.setHeader('Host', bucketname + '.' + hostname);        
        req.setHeader('Connection', 'keep-alive');
        req.setHeader('Content-type', contentType);
        req.setHeader('Date', formattedDateString);
        req.setHeader('ACL', 'public-read');
        
        Http http = new Http();
        HTTPResponse res = http.send(req);       
        
        System.debug('RESPONSE STRING: ' + res.toString());
        System.debug('RESPONSE STATUS: ' + res.getStatus());
        System.debug('STATUS_CODE: ' + res.getStatusCode());

        FileData data=new FileData();                       
        if(res.getStatusCode() == 200){
            data.Content= EncodingUtil.Base64Encode(res.getBodyAsBlob());              
            data.ContentType = contentType;
            data.ContentBlob= res.getBodyAsBlob();   
            data.status = true;
            return data;
        }
        else{
            data.status = false;    
            return data;
        }
    }
}