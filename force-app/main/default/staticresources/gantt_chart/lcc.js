'use strict';

console.log("***Loading lcc.js...");

// send a message to Lightning Component
LCC.sendMessage = function(userMessage) {
    if (typeof LCC !== "undefined" && typeof LCC.onlineSupport !== "undefined") {
        LCC.onlineSupport.sendMessage("containerUserMessage", {payload: userMessage});
    } else {
        // TODO: offline
    }
}

// Error Handling Support
LCC.addErrorHandler = function(handler) {
    if (typeof LCC !== "undefined" && typeof LCC.onlineSupport !== "undefined") {
        LCC.onlineSupport.addErrorHandler(handler);
    } else {
        // TODO: offline
    }
}

LCC.removeErrorHandler = function(handler) {
    if (typeof LCC !== "undefined" && typeof LCC.onlineSupport !== "undefined") {
        LCC.onlineSupport.removeErrorHandler(handler);
    } else {
        // TODO: offline
    }
}

// handle messages from Lightning Component
LCC.addMessageHandler = function(handler) {
    if (typeof LCC !== "undefined" && typeof LCC.onlineSupport !== "undefined") {
        LCC.onlineSupport.addMessageHandler(handler);
    } else {
        // TODO: offline
    }
}

LCC.removeMessageHandler = function(handler) {
    if (typeof LCC !== "undefined" && typeof LCC.onlineSupport !== "undefined") {
        LCC.onlineSupport.removeMessageHandler(handler);
    } else {
        //TODO: offline
    }
}
