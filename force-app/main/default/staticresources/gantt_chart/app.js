'use strict';

console.log("***Loading app.js...");

// sample task data
var data = {
    data: [
      {id: 1, workOrder: '00973', task: 'Electrician', text: 'Electrician', type:'project', open:true},
      {id: 2, task: 'Disconnect Downlights', start_date: '15-08-2018 08:00', text: '1797', workOrder: '', duration: 3, unit: 'hour', progress: 0, parent:"1" },
      {id: 3, task: 'Reconnect Downlights', start_date: '15-08-2018 15:00', text: '1798', workOrder: '', duration: 3, unit: 'hour', progress: 0, parent:"1"},
      {id: 4, workOrder: '00974', task: 'Plasterer', text: 'Plasterer', type:'project', open:true},
      {id: 5, task: 'Repair Ceiling 10m', start_date: '15-08-2018 08:00', text: '1799', workOrder: '', duration: 3, unit: 'hour', progress: 0, parent:"4"},
      {id: 6, task: 'Sanding', start_date: '16-08-2018 08:00', text: '1800', workOrder: '', duration: 2, unit: 'hour', progress: 0, parent:"4"},
      {id: 7, workOrder: '00974', task: 'Painter', text: 'Painter', type:'project', open:true},
      {id: 8, task: 'Paint Ceiling', start_date: '16-08-2018 12:00', text: '1801', workOrder: '', duration: 5, unit: 'hour', progress: 0, parent:"7"},
      
    ],
    links: [
      {id: 1, source: 5, target: 3, type: '0'},
      {id: 2, source: 3, target: 6, type: '0'},
      {id: 3, source: 6, target: 8, type: '0'},
      {id: 4, source: 2, target: 5, type: '1'},
    ]
  };

function initGanttConfig() {
    // set initial units to days
    gantt.config.min_column_width = 70;
    gantt.config.scale_unit = 'week';
    gantt.config.date_scale = 'Week %W';
    gantt.config.subscales = [
        {unit: 'day', step: 1, date: '%d %M'}
    ];
    gantt.config.scale_height = 60;
    gantt.config.ignore_time = function(date) {
        if (date.getHour() < 7 || date.getHour() > 18)
            return true;
    };

    // stop tasks being set to zero duration when they are dragged and dropped on Gantt
    // default value is "day" which cause tasks with hour(s) duration to be set to zero when
    // dragging and dropping
    gantt.config.duration_unit = "hour";

    // disable drag and drop of tasks
    gantt.config.drag_move = false;

    // disable creating dependency links by drag and drop
    gantt.config.drag_links = false;

    // disable changing of task progress by dragging knob
    gantt.config.drag_progress = false;

    // gantt columns
    //     {name: "add",        label:"",              width:44 }
    gantt.config.columns = [
        {name:"task",        label:"Task",          width:"*", align:"left", tree: true },
        {name:"workOrder",   label:"Work Order",    width:100, align:"center" }
    ];

}

function initGanttEvents() {
    if(gantt.ganttEventsInitialized){
        return;
    }
    gantt.ganttEventsInitialized = true;

    // attach to gantt chart events

    /*gantt.attachEvent("onGanttRender", function() {
        console.log("Gantt chart is completely rendered on the page...");
    });*/

    gantt.attachEvent("onTaskClick", function(id, e) {
        var task = this.getTask(id);
        if (task.workOrder !== '') {
            console.log("You've just clicked on Work Order with id: " + id);    
        } else {
            console.log("You've just clicked on Service Appointment with id: " + id);
        }
        
        console.log(this.getTask(id));
    });

    gantt.attachEvent("onBeforeLightbox", function(id) {
        // disable light box being displayed
        return false;
    });
}

function zoom_tasks(node) {
    switch(node.value) {
        case "week":
            console.log("***Zooming by Hours...");
            gantt.config.scale_unit = 'day';
            gantt.config.date_scale = '%d %M';
            gantt.config.scale_height = 60;
            gantt.config.min_column_width = 30;
            gantt.config.subscales = [
                {unit: 'hour', step:1, date: '%H'}
            ];
            break;

        case "trplweek":
            console.log("***Zooming by Days...");
            gantt.config.min_column_width = 70;
            gantt.config.scale_unit = 'week';
            gantt.config.date_scale = 'Week %W';
            gantt.config.subscales = [
                {unit: 'day', step: 1, date: '%d %M'}
            ];
            gantt.config.scale_height = 60;
            gantt.config.ignore_time = function(date) {
            if (date.getHour() < 7 || date.getHour() > 18)
                return true;
            };
            break;

        case "year":
            console.log("***Zooming by Months...");
            gantt.config.min_column_width = 70;
            gantt.config.scale_unit = 'month';
            gantt.config.date_scale = '%F';
            gantt.config.scale_height = 60;
            gantt.config.subscales = [
                {unit: 'week', step: 1, date: 'Week %W'}
            ];
            break;

        default:
            break;        
    }
    gantt.render();
}


  
initGanttConfig();
initGanttEvents();
gantt.init("gantt_here");


gantt.parse(data);

